webpackJsonp([0],{

/***/ 1080:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamRoom; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_exam_GrandExam_grantexam_component__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { examresultModal } from '../../../pages/exam/exam-room/resultModal/exam-result';

var ExamRoom = /** @class */ (function () {
    function ExamRoom(navCtrl, modalCtrl, examService, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.examService = examService;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.completed = false;
        this.completed = true;
        this.complete();
        //  document.getElementById("myCheck").click();
    }
    ExamRoom.prototype.complete = function () {
        var _this = this;
        this.completed = true;
        this.pendingsDiv = false;
        this.mode = 'completed';
        this.examService.fetchAllExam(this.mode).subscribe(function (res) {
            _this.examList = res.data.examList;
            _this.examService.fetchGrandExam().subscribe(function (res) {
                var completedGexam = [];
                try {
                    if (res.data.completedExam.length > 0) {
                        completedGexam = (res.data.completedExam).map(function (item) {
                            item.examType = "grand";
                            return item;
                        });
                    }
                }
                catch (e) {
                    return _this.examList;
                }
                ;
                _this.examList = (completedGexam.length > 0) ? _this.examList.concat(completedGexam) : _this.examList;
                if (!_this.examList.length) {
                    var toast = _this.toastCtrl.create({
                        message: 'You dont have any complete exam',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
            });
        });
    };
    ExamRoom.prototype.pendingExams = function () {
        var _this = this;
        this.completed = false;
        this.pendingsDiv = true;
        this.mode = 'pending';
        this.examService.fetchAllExam(this.mode).subscribe(function (res) {
            _this.examList = res.data.examList;
            console.log(_this.examList, "exam List data1");
            _this.examService.fetchGrandExam().subscribe(function (grandRes) {
                console.log("Pendin Grand", grandRes);
                var pendingGexam = [];
                try {
                    if (grandRes.data.pendingExams.length > 0) {
                        pendingGexam = (grandRes.data.pendingExams).map(function (item) {
                            item.examType = "grand";
                            return item;
                        });
                    }
                }
                catch (e) {
                    return _this.examList;
                }
                ;
                var examList = (pendingGexam.length > 0) ? _this.examList.concat(pendingGexam) : _this.examList;
                _this.examList = examList.reverse();
                console.log(_this.examList, "exam List data2");
            });
            if (!_this.examList.length) {
                var toast = _this.toastCtrl.create({
                    message: 'You dont have any pending exam',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        });
    };
    ExamRoom.prototype.DeletePop = function (i, item) {
        var _this = this;
        this.currentListId = i;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure want to delete this exam?',
            buttons: [
                {
                    text: 'Yes',
                    handler: function () {
                        // if (confirm('Are you sure you want to delete this exam?')){
                        if (_this.examList[_this.currentListId].examType == "grand") {
                            _this.examService.deleteGrandExam(_this.examList[_this.currentListId]._id).subscribe(function (res) {
                                var toast = _this.toastCtrl.create({
                                    message: 'Exam deleted.',
                                    duration: 3000,
                                    position: 'bottom'
                                });
                                toast.present();
                                _this.pendingExams();
                            }, function (error) {
                                var toast = _this.toastCtrl.create({
                                    message: 'OOPS! Something went wrong.',
                                    duration: 3000,
                                    position: 'bottom'
                                });
                                toast.present();
                            });
                        }
                        else {
                            var id = (_this.examList[_this.currentListId].examId) ? _this.examList[_this.currentListId].examId : _this.examList[_this.currentListId]._id;
                            _this.examService.deleteExam(id).subscribe(function (res) {
                                var toast = _this.toastCtrl.create({
                                    message: 'Exam deleted.',
                                    duration: 3000,
                                    position: 'bottom'
                                });
                                toast.present();
                                _this.pendingExams();
                            }, function (error) {
                                var toast = _this.toastCtrl.create({
                                    message: 'OOPS! Something went wrong.',
                                    duration: 3000,
                                    position: 'bottom'
                                });
                                toast.present();
                            });
                        }
                        // }
                        // else {
                        //   //Do Nothing
                        // }
                        console.log('yes');
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        console.log('noo');
                    }
                }
            ]
        });
        alert.present();
    };
    ExamRoom.prototype.resumeExam = function (exam) {
        var _this = this;
        if (exam.examType === "grand") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_exam_GrandExam_grantexam_component__["a" /* grantExam */], { subject: exam._id });
        }
        else {
            if (exam.examId) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { subject: exam._id });
            }
            else {
                this.examService.startExam(exam._id).subscribe(function (res) {
                    var userExamId = res.data.userExamId;
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { subject: userExamId });
                    console.log(userExamId, 'start');
                });
            }
        }
    };
    ExamRoom.prototype.viewResult = function (userExam) {
        if (userExam) {
            var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { data: userExam }, { cssClass: 'inset-modal' });
            profileModal.onDidDismiss(function (value) {
            });
            profileModal.present();
            //   console.log("userexamm",userExam);
            //   let exam_id;
            //   if(userExam.examType == "grand"){
            //     exam_id = userExam._id + '/g'
            //     this.navCtrl.push(examresultModal,{subject:exam_id});
            //   }
            //   else{
            //     exam_id = userExam.examId + '/'+ userExam._id;
            //     this.navCtrl.push(examresultModal,{subject:exam_id});
            // }
        }
    };
    ExamRoom = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-examroom',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam-room/examroom.html"*/'\n\n\n<ion-header>\n        <ion-navbar>\n          \n            <ion-title style="text-align: center;" >\n               Exam-rooms \n            </ion-title>\n            \n        </ion-navbar>\n    </ion-header>\n    \n    <!-- <ion-tabs tabsPlacement="top">\n        <ion-tab [root]="tab1Root" tabTitle="Complete" tabIcon="ios-calendar"></ion-tab>\n        <ion-tab [root]="tab2Root" tabTitle="Pending" tabIcon="ios-calendar-outline"></ion-tab>\n    </ion-tabs> -->\n    \n    <ion-content padding>\n         \n            <div class="row">\n                    <div class="col"> <button id="myCheck" [disabled]="completed" ion-button class="bgcolor"  color="light" (click)="complete()" block style="background-color:#108fd2; ">Complete </button>\n                    </div>\n                    <div class="col"><button  [disabled]="pendingsDiv"  ion-button class="bgcolor" color="light" (click)="pendingExams()"block style="background-color:#108fd2; ">Pending</button>\n                    </div>\n                   \n                  </div>\n    \n              \n     \n                  <!--complete-->\n                 \n                  <div *ngIf="completed">\n                        <div class="center" *ngIf="!examList">\n                        \n                               \n                                <ion-spinner  ></ion-spinner>\n                               \n                              </div>\n                           \n                        <ion-list *ngFor="let item of examList; let i = index">\n                                <ion-list-header style="background-color:aliceblue;">\n                                   <b> Exam Name:{{item.examName}} {{item.name}} </b>\n                                </ion-list-header>\n                                <ion-item-sliding >\n            \n                                       <ion-item style="background-color:beige!important;">\n                                         \n                                          Created Date:{{item.createdDate|date}}<br>\n                                          Submitted Date:{{item.updatedDate|date}}\n                                       </ion-item>\n                                       \n                                    \n                                       <ion-item-options>\n                                        <button ion-button color="primary" (click)="viewResult(item)" style="background-color:#108fd2;">View result</button>\n                                \n                                       </ion-item-options>\n                                     </ion-item-sliding><br>\n                                \n                              </ion-list>\n                  </div>\n                    <!--endcomplete-->\n                     <!--pending-->\n                     <div *ngIf="pendingsDiv">\n                            <div class="center" *ngIf="!examList">\n                                    \n                                   \n                                    <ion-spinner  ></ion-spinner>\n                                   \n                                  </div>\n                            <ion-list *ngFor="let item of examList; let i = index ">\n                                    <ion-list-header style="background-color:aliceblue;">\n                                      <b>  Exam Name:{{item.examName}}{{item.name}} </b>\n                                    </ion-list-header>\n                                    <ion-item-sliding>\n                                        \n                                           <ion-item style="background-color:beige!important;">\n                                              \n                                              Created Date:{{item.createdDate|date}}<br>\n                                              <!-- <p *ngIf="item.dueFlag.formatted" >\n                                      \n                                      \n                                                Due Date:{{item.dueObj.formatted|date}}\n                                              </p> -->\n                                           \n                                           </ion-item>\n                                       \n                                           \n                                        \n                                           <ion-item-options>\n                                            <button ion-button color="primary" (click)="resumeExam(item)"  style="background-color:#108fd2;">Write</button>\n                                        \n                                             <button ion-button color="danger" (click)="DeletePop(i,item)" style="background-color:#8b0404;">Delete</button>\n                                             \n                                            </ion-item-options>\n                                         </ion-item-sliding><br>\n                                    \n                                  </ion-list>\n                     </div>\n              \n                      <!--endpending-->\n                       <!--vew result-->\n                       \n                       <!--endvew result-->\n    </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam-room/examroom.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */]])
    ], ExamRoom);
    return ExamRoom;
}());

//# sourceMappingURL=examroom.js.map

/***/ }),

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Questions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_student_service__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_notes_service__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_question_service__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_learn_questions_note_note_modal__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_learn_questions_editModal_editModal__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_learn_questions_report_questions_report__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_learn_list__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var Questions = /** @class */ (function () {
    function Questions(navCtrl, events, platform, __auth, modalCtrl, toastCtrl, alertCtrl, studentService, navParams, questionService, noteService) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.platform = platform;
        this.__auth = __auth;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.studentService = studentService;
        this.navParams = navParams;
        this.questionService = questionService;
        this.noteService = noteService;
        this.url = __WEBPACK_IMPORTED_MODULE_10__environments_environment__["a" /* environment */].backendUrl + '/images/';
        this.star = false;
        this.quickview = false;
        this.listview = false;
        this.listStarView = false;
        this.starredView = false;
        this.questionDetails = "";
        this.questionName = "";
        this.clickedItem = null;
        this.resultData = [];
        this.countClick = 0;
        this.quickStarCount = 0;
        this.listStarCount = 0;
        this.lastLearnQuestionNumber = 0;
        this.listCount = 0;
        this.currentIndexToShow = 0;
        this.noQuestion = false;
        this.noPrevQuestion = false;
        this.totalId = navParams.get('subject');
        this.fields = this.totalId.split('/');
        this.subjectId = this.fields[0];
        this.sectionId = this.fields[1];
        this.chapterId = this.fields[2];
        this.questionId = this.fields[3];
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        if (!this.chapterId) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
        }
        this.events.publish('user:created', true, Date.now());
    }
    // createUser(user) {
    //   console.log('User created!')
    //   this.events.publish('user:created', user, Date.now());
    // }
    Questions.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    Questions.prototype.ionViewWillLeave = function () {
        this.tabBarElement.style.display = 'flex';
    };
    Questions.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
    };
    Questions.prototype.staredtoogle = function (event) {
        if (event._value) {
            this.studentService.getStredQuestion(this.chapterId, this.subjectArray).subscribe(function (res) {
            }, function (err) {
            });
        }
    };
    Questions.prototype.ngOnInit = function () {
        this.tabBarElement.style.display = 'none';
        var userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = userData._id;
        this.userDetailsId = userData.userDetailsId;
        this.userId = userData.userId;
        this.loadLearn();
        if (this.questionId)
            this.getFirstLearn();
    };
    Questions.prototype.loadLearn = function () {
        var _this = this;
        this.studentService.loadLearnIDs(this.subjectId, this.sectionId, this.chapterId).subscribe(function (res) {
            if (res.status) {
                _this.totalNumber = res.data.questionPalette.length;
                _this.learnQuestionPalette = res.data.questionPalette;
                if (_this.totalNumber == 0) {
                    _this.__auth.notificationInfo("No question found !");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                }
                else {
                    _this.questionService.getLastQuestion(_this.chapterId).subscribe(function (res) {
                        if ((res.status) && (res.data.question)) {
                            _this.questionData = res.data;
                            _this.lastLearnQuestionId = _this.questionData.question._id;
                            for (var k = 0; k < _this.learnQuestionPalette.length; k++) {
                                if (_this.learnQuestionPalette[k] === _this.lastLearnQuestionId) {
                                    _this.lastLearnQuestionNumber = k + 1;
                                }
                            }
                            console.log("lasteeeeeeeeeee", _this.lastLearnQuestionNumber);
                            console.log("lasteeeeedddddddddddeeeeee", _this.learnQuestionPalette.length);
                            if (_this.lastLearnQuestionNumber == _this.learnQuestionPalette.length) {
                                _this.getFirstLearn();
                            }
                            else {
                                _this.qsNumber = _this.lastLearnQuestionNumber;
                                _this.continueQus();
                                _this.loadnotes();
                            }
                        }
                        else {
                            _this.getFirstLearn();
                        }
                    });
                }
            }
            else {
                _this.__auth.notificationInfo("No question found !");
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
            }
        });
    };
    Questions.prototype.getFirstLearn = function () {
        var _this = this;
        this.studentService.loadLearnIDs(this.subjectId, this.sectionId, this.chapterId).subscribe(function (res) {
            if (res) {
                _this.learnQuestionPalette = res.data.questionPalette;
                _this.firstLearnId = _this.learnQuestionPalette[0];
                if (_this.questionId) {
                    for (var k = 0; k < _this.learnQuestionPalette.length; k++) {
                        if (_this.learnQuestionPalette[k] === _this.questionId) {
                            _this.learnQuestionNumber = k + 1;
                        }
                    }
                }
                if (_this.questionId) {
                    _this.firstLearnId = _this.questionId;
                }
                else {
                    _this.firstLearnId;
                }
                _this.studentService.getLearnQuestion(_this.firstLearnId).subscribe(function (res) {
                    if (res) {
                        _this.questionData = res.data;
                        if (_this.questionId) {
                            _this.qsNumber = _this.learnQuestionNumber;
                        }
                        else {
                            (_this.questionData.question !== null) ? _this.qsNumber = 1 : _this.qsNumber = 0;
                        }
                        _this.continueQus();
                        _this.loadnotes();
                    }
                    else {
                        _this.__auth.notificationInfo("No question found !");
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                    }
                });
            }
            else {
                _this.__auth.notificationInfo("No question found !");
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
            }
        });
    };
    Questions.prototype.continueQus = function () {
        this.questionDetails = this.questionData.question;
        this.questionName = this.questionData.question.question;
        if (this.quickview) {
            this.questionquickName = this.questionquickData.question.question;
        }
        this.questionData.question.options ? this.options = JSON.parse(this.questionData.question.options) : this.options = [];
        this.questionId = this.questionData.question._id;
        this.subjectArray = this.questionData.question.subjectArray;
        this.correctAnswer = this.questionData.question.answer;
        if (this.questionData.answer !== null) {
            /* when user already answered this question */
            this.answerForClass = this.questionData.question.answer;
            this.clickedItem = this.questionData.answer.answer;
        }
        else {
            this.clickedItem = null;
            this.answerForClass = null;
        }
        if (this.questionData.notes != null) {
        }
        if (this.questionData.staredData[0]) {
            this.star = this.questionData.staredData[0].stared;
        }
        else {
            this.star = false;
        }
    };
    Questions.prototype.previous = function (questionId) {
        var _this = this;
        var questionid = this.learnQuestionPalette[questionId - 2];
        if (questionid) {
            this.studentService.getLearnQuestion(questionid).subscribe(function (res) {
                if (res.data.question._id) {
                    _this.questionData = res.data;
                    _this.questionDetails = res.data.question;
                    _this.questionName = _this.questionData.question.question;
                    _this.options = JSON.parse(res.data.question.options);
                    (_this.questionData.question !== null) ? _this.qsNumber-- : _this.qsNumber = 0;
                    _this.questionId = _this.questionData.question._id;
                    _this.subjectArray = _this.questionData.question.subjectArray;
                    _this.correctAnswer = _this.questionData.question.answer;
                    if (_this.quickview) {
                        _this.questionquickData = res.data;
                        if (_this.questionquickData.staredData[0]) {
                            _this.star = _this.questionquickData.staredData[0].stared;
                        }
                        else {
                            _this.star = false;
                        }
                        _this.corectQuickAnswer = _this.questionquickData.question.answer;
                        _this.questionquickName = _this.questionquickData.question.question;
                        (_this.questionquickData.question !== null) ? _this.qstnNumber-- : _this.qstnNumber = 0;
                        if (_this.questionquickData.question.question_image) {
                            _this.imageFlag = true;
                            _this.imgUrl = _this.url + _this.questionquickData.question.question_image;
                        }
                        else {
                            _this.imageFlag = false;
                        }
                    }
                    if (_this.questionData.answer !== null) {
                        /* when user already answered this question */
                        _this.answerForClass = _this.questionData.question.answer;
                        _this.clickedItem = _this.questionData.answer.answer;
                    }
                    else {
                        _this.clickedItem = null;
                        _this.answerForClass = null;
                    }
                    if (_this.questionData.notes !== null) {
                        // this.notesData = this.questionData.notes;
                    }
                    if (_this.questionData.staredData[0]) {
                        _this.star = _this.questionData.staredData[0].stared;
                    }
                    else {
                        _this.star = false;
                    }
                }
                _this.loadnotes();
            });
        }
    };
    Questions.prototype.next = function (questionId) {
        var _this = this;
        var questionid = this.learnQuestionPalette[questionId];
        if (questionid) {
            if (!this.quickview) {
                this.questionService.createLastQuestion(this.chapterId, this.qsNumber, questionid).subscribe(function (res) {
                });
            }
            this.studentService.getLearnQuestion(questionid).subscribe(function (res) {
                if (res.data.question.question) {
                    _this.questionData = res.data;
                    _this.questionDetails = res.data.question;
                    _this.questionName = _this.questionData.question.question;
                    (_this.questionData.question !== null) ? _this.qsNumber++ : _this.qsNumber = 0;
                    if (res.data.question.options) {
                        _this.options = JSON.parse(res.data.question.options);
                    }
                    if (_this.quickview) {
                        _this.questionquickData = res.data;
                        if (_this.questionquickData.staredData[0]) {
                            _this.star = _this.questionquickData.staredData[0].stared;
                        }
                        else {
                            _this.star = false;
                        }
                        if (res.data.question.options) {
                            _this.quickOptions = JSON.parse(res.data.question.options);
                        }
                        _this.corectQuickAnswer = _this.questionquickData.question.answer;
                        _this.questionquickName = _this.questionquickData.question.question;
                        (_this.questionquickData.question !== null) ? _this.qstnNumber++ : _this.qstnNumber = 0;
                        if (_this.questionquickData.question.question_image) {
                            _this.imageFlag = true;
                            _this.imgUrl = _this.url + _this.questionquickData.question.question_image;
                        }
                        else {
                            _this.imageFlag = false;
                        }
                    }
                    _this.questionId = _this.questionData.question._id;
                    _this.subjectArray = _this.questionData.question.subjectArray;
                    _this.correctAnswer = _this.questionData.question.answer;
                    if (_this.questionData.answer !== null) {
                        /* when user already answered this question */
                        _this.answerForClass = _this.questionData.question.answer;
                        _this.clickedItem = _this.questionData.answer.answer;
                    }
                    else {
                        _this.clickedItem = null;
                        _this.answerForClass = null;
                    }
                    if (_this.questionData.notes !== null) {
                        // this.notesData = this.questionData.notes;
                    }
                    if (_this.questionData.staredData[0]) {
                        _this.star = _this.questionData.staredData[0].stared;
                    }
                    else {
                        _this.star = false;
                    }
                }
                else {
                    _this.__auth.notificationInfo("No question found !");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                }
                _this.loadnotes();
            });
        }
    };
    Questions.prototype.quickView = function () {
        var _this = this;
        this.countClick++;
        this.listview = false;
        if (this.countClick % 2 == 0) {
            this.quickclr = false;
            this.quickview = false;
            this.starredView = false;
            this.listClr = false;
            this.starclr = false;
            this.listview = false;
            this.loadLearn();
        }
        else {
            this.quickclr = true;
            this.starclr = false;
            this.listClr = false;
            this.starredView = false;
            this.quickview = true;
            this.listview = false;
            this.studentService.getQuestion(this.chapterId).subscribe(function (res) {
                if (res.data.question.question) {
                    _this.questionquickData = res.data;
                    _this.questionquickName = _this.questionquickData.question.question;
                    _this.corectQuickAnswer = _this.questionquickData.question.answer;
                    (_this.questionquickData.question !== null) ? _this.qstnNumber = 1 : _this.qstnNumber = 0;
                    if (_this.questionquickData.question.question_image) {
                        _this.imageFlag = true;
                        _this.imgUrl = _this.url + _this.questionquickData.question.question_image;
                    }
                    else {
                        _this.imageFlag = false;
                    }
                    if (_this.questionquickData.staredData[0]) {
                        _this.star = _this.questionquickData.staredData[0].stared;
                    }
                    else {
                        _this.star = false;
                    }
                    _this.loadnotes();
                }
                else {
                    _this.__auth.notificationInfo("No question found !");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                }
            });
        }
    };
    Questions.prototype.listView = function () {
        var _this = this;
        this.listCount++;
        this.quickview = false;
        this.starredView = false;
        this.listview = false;
        if (this.listCount % 2 == 0) {
            this.listClr = false;
            this.quickclr = false;
            this.starclr = false;
            this.listview = false;
            this.loadLearn();
        }
        else {
            this.listClr = true;
            this.quickclr = false;
            this.starclr = false;
            this.listview = true;
            this.quickview = false;
            this.starredView = false;
            this.studentService.getListQuestion(this.chapterId).subscribe(function (res) {
                if (res.data) {
                    _this.questionlistData = res.data;
                    var data = (res.data).map(function (item) {
                        item.options = JSON.parse(item.options);
                        return item;
                    });
                    if (data) {
                        _this.questionlistData = data;
                    }
                }
                else {
                    _this.__auth.notificationInfo("No question found !");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                }
            });
        }
    };
    Questions.prototype.listStarview = function () {
        var _this = this;
        this.listStarCount++;
        this.quickview = false;
        this.starredView = false;
        this.listStarView = false;
        this.listview = false;
        if (this.listStarCount % 2 == 0) {
            this.listClr = false;
            this.quickclr = false;
            this.starclr = false;
            this.listview = false;
            this.listStarView = false;
            this.quickStarredView = false;
            this.logEvent("liststar");
        }
        else {
            this.listClr = true;
            this.quickclr = false;
            this.starclr = false;
            this.listview = false;
            this.quickview = false;
            this.starredView = true;
            this.listStarView = true;
            this.quickStarredView = false;
            this.resultData.length = 0;
            this.questionService.getAllStaredQuestion(this.subjectArray).subscribe(function (res) {
                if (res.data.length > 0) {
                    _this.resultData.length = 0;
                    _this.questionlistStarData = res.data;
                    var data = (res.data).map(function (item) {
                        item.options = JSON.parse(item.question[0].options);
                        item.questions = item.question[0].question;
                        item.description = item.question[0].description;
                        item.answers = item.question[0].answer;
                        item.question_image = item.question[0].question_image;
                        return item;
                    });
                    if (data) {
                        _this.questionlistStarData = data;
                    }
                }
                else if (res.data.length == 0) {
                    _this.resultData.length = 0;
                    _this.questionlistStarData = res.data;
                    _this.__auth.notificationInfo("No question found !");
                }
                else {
                    _this.__auth.notificationInfo("No question found !");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                }
            });
        }
    };
    Questions.prototype.quickStarView = function (e) {
        var _this = this;
        if (e == "quickPage") {
            this.quickStarCount;
        }
        else {
            this.quickStarCount++;
        }
        this.quickview = false;
        this.starredView = false;
        this.listStarView = false;
        this.listview = false;
        if (this.quickStarCount % 2 == 0) {
            this.listClr = false;
            this.quickclr = false;
            this.starclr = false;
            this.listview = false;
            this.quickStarredView = false;
            this.listStarView = false;
            this.logEvent("liststar");
        }
        else {
            this.listClr = false;
            this.quickclr = true;
            this.starclr = false;
            this.listview = false;
            this.quickview = false;
            this.starredView = true;
            this.listStarView = false;
            this.quickStarredView = true;
            this.resultData.length = 0;
            this.questionService.getAllStaredQuestion(this.subjectArray).subscribe(function (res) {
                _this.currentIndexToShow = 0;
                if (res.data.length == 1) {
                    _this.noQuestion = true;
                    _this.noPrevQuestion = true;
                }
                if (res.data.length > 0) {
                    _this.noData = false;
                    _this.questionStarData = res.data;
                    _this.questions = _this.questionStarData[_this.currentIndexToShow].question[0].question;
                    (_this.questions !== null) ? _this.Qnumber = 1 : _this.Qnumber = 0;
                    _this.noPrevQuestion = true;
                    if (_this.Qnumber == res.data.length) {
                        _this.noQuestion = true;
                    }
                    else {
                        _this.noQuestion = false;
                    }
                    _this.description = _this.questionStarData[_this.currentIndexToShow].question[0].description;
                    _this.answers = _this.questionStarData[_this.currentIndexToShow].question[0].answer;
                    _this.question_image = _this.questionStarData[_this.currentIndexToShow].question[0].question_image;
                    _this.options = JSON.parse(_this.questionStarData[_this.currentIndexToShow].question[0].options);
                    if (_this.questionStarData[_this.currentIndexToShow].question[0].question_image) {
                        _this.imageFlag = true;
                        _this.imgUrl = _this.questionStarData[_this.currentIndexToShow].question[0].question_image;
                    }
                    else {
                        _this.imageFlag = false;
                    }
                    _this.star = true;
                    _this.loadnotes();
                }
                else if (res.data.length == 0) {
                    _this.noData = true;
                    _this.resultData.length = 0;
                    _this.star = false;
                    _this.__auth.notificationInfo("No question found !");
                }
                else {
                    _this.noData = true;
                    _this.star = false;
                    _this.resultData.length = 0;
                    _this.__auth.notificationInfo("No question found !");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                }
            });
        }
    };
    Questions.prototype.logEvent = function (e) {
        var _this = this;
        if (e._value == true || e == true || e == "liststar") {
            this.quickview = false;
            this.starredView = true;
            this.quickStarredView = false;
            this.listStarView = false;
            this.listview = false;
            this.listClr = false;
            this.quickclr = false;
            this.starclr = true;
            this.noData = false;
            this.questionService.getAllStaredQuestion(this.subjectArray).subscribe(function (res) {
                _this.currentIndexToShow = 0;
                if (res.data.length == 1) {
                    _this.noQuestion = true;
                    _this.noPrevQuestion = true;
                }
                if (res.data.length > 0) {
                    _this.noData = false;
                    _this.questionStarData = res.data;
                    _this.questions = _this.questionStarData[_this.currentIndexToShow].question[0].question;
                    if (_this.questionStarData[_this.currentIndexToShow].answer != null) {
                        _this.userAnswer = _this.questionStarData[_this.currentIndexToShow].answer.answer;
                    }
                    (_this.questions !== null) ? _this.Qnumber = 1 : _this.Qnumber = 0;
                    _this.noPrevQuestion = true;
                    if (_this.Qnumber == res.data.length) {
                        _this.noQuestion = true;
                    }
                    else {
                        _this.noQuestion = false;
                    }
                    _this.description = _this.questionStarData[_this.currentIndexToShow].question[0].description;
                    _this.answers = _this.questionStarData[_this.currentIndexToShow].question[0].answer;
                    _this.question_image = _this.questionStarData[_this.currentIndexToShow].question[0].question_image;
                    _this.options = JSON.parse(_this.questionStarData[_this.currentIndexToShow].question[0].options);
                    if (_this.questionStarData[_this.currentIndexToShow].question[0].question_image) {
                        _this.imageFlag = true;
                        _this.imgUrl = _this.questionStarData[_this.currentIndexToShow].question[0].question_image;
                    }
                    else {
                        _this.imageFlag = false;
                    }
                    _this.star = true;
                    _this.loadnotes();
                }
                else if (res.data.length == 0) {
                    _this.noData = true;
                    _this.resultData.length = 0;
                    _this.star = false;
                    _this.__auth.notificationInfo("No question found !");
                }
                else {
                    _this.noData = true;
                    _this.star = false;
                    _this.resultData.length = 0;
                    _this.__auth.notificationInfo("No question found !");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
                }
            });
        }
        else {
            this.quickview = false;
            this.starredView = false;
            this.listStarView = false;
            this.quickStarredView = false;
            this.listview = false;
            this.listClr = false;
            this.quickclr = false;
            this.starclr = false;
            this.loadLearn();
        }
    };
    Questions.prototype.nextButtonClick = function () {
        this.noPrevQuestion = false;
        this.currentIndexToShow++;
        if (this.currentIndexToShow == this.questionStarData.length - 1) {
            this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
            this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
            this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
            if (this.questionStarData[this.currentIndexToShow].answer != null) {
                this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
            }
            this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
            (this.questions !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.noQuestion = true;
            if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            }
            else {
                this.imageFlag = false;
            }
            this.loadnotes();
        }
        else {
            this.noQuestion = false;
            this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
            this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
            if (this.questionStarData[this.currentIndexToShow].answer != null) {
                this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
            }
            (this.questions !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
            this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
            if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            }
            else {
                this.imageFlag = false;
            }
            this.loadnotes();
        }
    };
    Questions.prototype.previousButtonClick = function () {
        this.noQuestion = false;
        this.currentIndexToShow--;
        if (this.currentIndexToShow > 0) {
            this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
            this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
            if (this.questionStarData[this.currentIndexToShow].answer != null) {
                this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
            }
            (this.questions !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
            this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
            if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            }
            else {
                this.imageFlag = false;
            }
            this.loadnotes();
        }
        else {
            this.noPrevQuestion = true;
            this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
            this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
            if (this.questionStarData[this.currentIndexToShow].answer != null) {
                this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
            }
            (this.questions !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
            this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
            if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            }
            else {
                this.imageFlag = false;
            }
            this.loadnotes();
        }
    };
    Questions.prototype.findImage = function (i) {
        if (this.listStarview) {
            if (this.questionlistStarData[i].question_image.length > 0) {
                this.imageFlag = true;
                this.imageUrl = this.questionlistStarData[i].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
        else {
            if (this.questionlistData[i].question_image.length > 0) {
                this.imageFlag = true;
                this.imageUrl = this.questionlistData[i].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
    };
    Questions.prototype.loadnotes = function () {
        var _this = this;
        if (this.starredView) {
            this.resultData.length = 0;
            if (this.questionStarData[this.currentIndexToShow].notes.length > 0) {
                this.resultData.length = 0;
                var loadMore = false;
                this.noteService.fetchAllNotes(loadMore).subscribe(function (res) {
                    if (res) {
                        if (res.data.notes.length > 0) {
                            _this.pageLoadNote = res.data.notes;
                            for (var i = 0; i < res.data.notes.length; i++) {
                                _this.newData = (res.data.notes).map(function (item) {
                                    return item;
                                });
                            }
                            _this.allNotesData = _this.newData;
                            _this.resultData.length = 0;
                            for (var k = 0; k < _this.allNotesData.length; k++) {
                                if (_this.starredView) {
                                    if (_this.noData) {
                                        _this.resultData.length = 0;
                                    }
                                    else if (_this.allNotesData[k]["questionId"] === _this.questionStarData[_this.currentIndexToShow].question[0]._id) {
                                        _this.resultData.push(_this.allNotesData[k]);
                                        _this.noteLength = _this.resultData.length;
                                    }
                                }
                            }
                        }
                        else {
                            _this.resultData.length = 0;
                        }
                    }
                });
            }
        }
        else if (this.questionData.notes.length > 0) {
            this.resultData.length = 0;
            var loadMore = false;
            this.noteService.fetchAllNotes(loadMore).subscribe(function (res) {
                if (res) {
                    if (res.data.notes.length > 0) {
                        _this.pageLoadNote = res.data.notes;
                        for (var i = 0; i < res.data.notes.length; i++) {
                            _this.newData = (res.data.notes).map(function (item) {
                                return item;
                            });
                        }
                        _this.allNotesData = _this.newData;
                        _this.resultData.length = 0;
                        for (var k = 0; k < _this.allNotesData.length; k++) {
                            if (_this.quickview) {
                                if (_this.allNotesData[k]["questionId"] === _this.questionquickData.question._id) {
                                    _this.resultData.push(_this.allNotesData[k]);
                                    _this.noteLength = _this.resultData.length;
                                }
                            }
                            else if (_this.starredView) {
                                if (_this.noData) {
                                    _this.resultData.length = 0;
                                }
                                else if (_this.allNotesData[k]["questionId"] === _this.questionStarData[_this.currentIndexToShow].question[0]._id) {
                                    _this.resultData.push(_this.allNotesData[k]);
                                    _this.noteLength = _this.resultData.length;
                                }
                            }
                            else {
                                if (_this.allNotesData[k]["questionId"] === _this.questionData.question._id) {
                                    _this.resultData.push(_this.allNotesData[k]);
                                    _this.noteLength = _this.resultData.length;
                                }
                            }
                        }
                    }
                    else {
                        _this.resultData.length = 0;
                    }
                }
            });
        }
        else {
            this.resultData.length = 0;
        }
    };
    Questions.prototype.addNotes = function () {
        var _this = this;
        var chooseModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_learn_questions_note_note_modal__["a" /* HintModalPage */], null, { cssClass: 'inset-modalNote' });
        chooseModal.onDidDismiss(function (content) {
            if (content) {
                if (_this.quickview) {
                    _this.noteService.createNote(content, _this.questionquickData.question._id, _this.subjectArray).subscribe(function (res) {
                        if (res.status === 1) {
                            _this.resultData.length = _this.resultData.length + 1;
                        }
                        else {
                            _this.__auth.notificationInfo('OOPS! Something went wrong');
                        }
                    });
                }
                else if (_this.starredView) {
                    _this.noteService.createNote(content, _this.questionStarData[_this.currentIndexToShow].question[0]._id, _this.subjectArray).subscribe(function (res) {
                        if (res.status === 1) {
                            _this.resultData.length = _this.resultData.length + 1;
                        }
                        else {
                            _this.__auth.notificationInfo('OOPS! Something went wrong');
                        }
                    });
                }
                else {
                    _this.noteService.createNote(content, _this.questionData.question._id, _this.subjectArray).subscribe(function (res) {
                        if (res.status === 1) {
                            _this.resultData.length = _this.resultData.length + 1;
                        }
                        else {
                            _this.__auth.notificationInfo('OOPS! Something went wrong');
                        }
                    });
                }
            }
        });
        chooseModal.present();
    };
    Questions.prototype.editNote = function () {
        var _this = this;
        var loadMore = false;
        this.noteService.fetchAllNotes(loadMore).subscribe(function (res) {
            _this.pageLoadNote = res.data.notes;
            if (res.data.notes.length > 0) {
                for (var i = 0; i < res.data.notes.length; i++) {
                    _this.newData = (res.data.notes).map(function (item) {
                        return item;
                    });
                }
            }
            _this.allNotesData = _this.newData;
            _this.resultData.length = 0;
            for (var k = 0; k < _this.allNotesData.length; k++) {
                if (_this.quickview) {
                    if (_this.allNotesData[k]["questionId"] === _this.questionquickData.question._id) {
                        _this.resultData.push(_this.allNotesData[k]);
                        _this.oldContent = _this.resultData[0].noteContent;
                        _this.noteId = _this.resultData[0]._id;
                    }
                }
                else if (_this.starredView) {
                    if (_this.allNotesData[k]["questionId"] === _this.questionStarData[_this.currentIndexToShow].question[0]._id) {
                        _this.resultData.push(_this.allNotesData[k]);
                        _this.oldContent = _this.resultData[0].noteContent;
                        _this.noteId = _this.resultData[0]._id;
                    }
                }
                else {
                    if (_this.allNotesData[k]["questionId"] === _this.questionData.question._id) {
                        _this.resultData.push(_this.allNotesData[k]);
                        _this.oldContent = _this.resultData[0].noteContent;
                        _this.noteId = _this.resultData[0]._id;
                    }
                }
            }
            var chooseEditModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__pages_learn_questions_editModal_editModal__["a" /* editModal */], { oldContent: _this.oldContent, noteId: _this.noteId }, { cssClass: 'inset-modal' });
            chooseEditModal.onDidDismiss(function (content) {
                if (content == "dataDeletedSuccessfully") {
                    _this.resultData.length = 0;
                }
                else {
                    var noteId = _this.resultData[0]._id;
                    if (content) {
                        _this.noteService.editNote(noteId, content, _this.subjectArray).subscribe(function (res) {
                            if (res.status === 1) {
                            }
                            else {
                                _this.__auth.notificationInfo('OOPS! Something went wrong');
                            }
                        });
                    }
                }
            });
            chooseEditModal.present();
        });
    };
    Questions.prototype.starQuestions = function () {
        var _this = this;
        if (this.starredView) {
            if (this.noData) {
                this.star = false;
            }
            else {
                this.star = !this.star;
            }
            this.questionService.starQuestion(this.questionStarData[this.currentIndexToShow].question[0]._id, this.subjectArray, this.star)
                .subscribe(function (res) {
                if (!res.status)
                    _this.__auth.notificationInfo('OOPS! Something went wrong');
            });
            if (this.quickStarredView) {
                this.quickStarView("quickPage");
            }
            else {
                this.logEvent(true);
            }
        }
        else if (this.quickview) {
            this.star = !this.star;
            this.questionService.starQuestion(this.questionquickData.question._id, this.subjectArray, this.star)
                .subscribe(function (res) {
                if (!res.status)
                    _this.__auth.notificationInfo('OOPS! Something went wrong');
            });
        }
        else {
            this.star = !this.star;
            this.questionService.starQuestion(this.questionId, this.subjectArray, this.star)
                .subscribe(function (res) {
                if (!res.status)
                    _this.__auth.notificationInfo('OOPS! Something went wrong');
            });
        }
    };
    Questions.prototype.ReportQue = function () {
        var _this = this;
        var chooseModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__pages_learn_questions_report_questions_report__["a" /* ReportProb */], null, { cssClass: 'inset-modal' });
        chooseModal.onDidDismiss(function (data) {
            if (data) {
                _this.questionService.createProblem(data, _this.questionDetails).subscribe(function (res) {
                    if (res) {
                        if (res.status) {
                            _this.__auth.notificationInfo(res.message);
                        }
                        else {
                            _this.__auth.notificationInfo(res.message);
                        }
                    }
                    else {
                        _this.__auth.notificationInfo('OOPS! Something went wrong');
                    }
                });
            }
        });
        chooseModal.present();
    };
    Questions.prototype.toggleDetails = function (item) {
        if (this.isGroupShown(item)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = item;
        }
    };
    Questions.prototype.isGroupShown = function (item) {
        return this.shownGroup === item;
    };
    Questions.prototype.ionViewDidEnter = function () {
        this.topOrBottom = this.contentHandle._tabsPlacement;
        this.contentBox = document.querySelector(".scroll-content");
        if (this.topOrBottom == "top") {
            this.tabBarHeight = this.contentBox.marginTop;
        }
        else if (this.topOrBottom == "bottom") {
            this.tabBarHeight = this.contentBox.marginBottom;
        }
    };
    Questions.prototype.scrollingFun = function (e) {
        if (e.scrollTop > this.contentHandle.getContentDimensions().contentHeight) {
            if (this.topOrBottom == "top") {
                this.contentBox.marginTop = 0;
            }
            else if (this.topOrBottom == "bottom") {
                this.contentBox.marginBottom = 0;
            }
        }
        else {
            if (this.topOrBottom == "top") {
                this.contentBox.marginTop = this.tabBarHeight;
            }
            else if (this.topOrBottom == "bottom") {
                this.contentBox.marginBottom = this.tabBarHeight;
            }
        }
    };
    Questions.prototype.goBackto = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
        this.chapterId = "";
    };
    Questions.prototype.finishLearn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_learn_list__["a" /* ListPage */]);
    };
    Questions.prototype.selectedOption = function (id) {
        if (id === this.correctAnswer) {
            this.answerStatus = "correct";
        }
        else {
            this.answerStatus = "wrong";
        }
        if (this.questionData.answer === null) {
            this.studentService.saveAnswer(this.questionId, id, this.answerStatus, this.subjectArray).subscribe(function (res) {
            });
        }
        this.answerStatus = "attended";
        this.answerForClass = this.correctAnswer;
        this.clickedItem = id;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("contentRef"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], Questions.prototype, "contentHandle", void 0);
    Questions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-questions',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/questions.html"*/'<head>\n\n  <style>\n    .scroll-content {\n      padding: 4px !important;\n      margin-top: 62px;\n    }\n  </style>\n</head>\n<html>\n\n<body>\n  <ion-header #head>\n     \n  \n    <ion-toolbar>\n        <ion-navbar>\n            <ion-buttons left>\n                <ion-row *ngIf="questionData">\n         \n  \n                    <ion-col style="padding:8px!important;" class="" *ngFor="let item of questionData.question.subjectArray; let i =  index;"\n                      (click)="goBackto()">\n                      <p class="headSec">{{item.name | slice:0:20}}...</p>\n                    </ion-col>\n                  </ion-row>\n    \n            </ion-buttons>\n          </ion-navbar>\n      \n\n\n    </ion-toolbar>\n  </ion-header>\n  <ion-content padding hideheader [header]="head" class="bxpdg" #contentRef (ionScroll)="scrollingFun($event)" style="margin-top:10px;">\n    <div class="center" *ngIf="!questionData">\n      <ion-spinner></ion-spinner>\n      `\n    </div>\n\n\n    <div *ngIf="questionData && !listview && !starredView && !listStarView && !quickStarredView">\n      <div>\n\n        <div *ngIf="!quickview">\n          <ion-item-divider text-wrap class="questionHead" style="padding:0px;">\n            <ion-grid class="question-wrap" style="background-color:#FFFEEE;">\n              <ion-row style="border-bottom-style: solid;\n              border-bottom-width: 1px; border-bottom-color: #f0f0f0;margin-top:8px; padding-bottom:13px">\n\n                <ion-col col-2 class="fntsze" style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:18px; padding-left: 0px;">\n                  <span [innerHtml]="qsNumber"></span>.\n                </ion-col>\n\n                <ion-col col-10 class="fntsze" style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                  <p style="font-size: 18px;" class="question" [innerHtml]="questionName"> </p>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n          </ion-item-divider>\n\n          <div class="answer-wrap pdd" style="padding:0px!important;border-bottom:none!important;">\n            <div *ngFor="let item of options ;let i= index;">\n\n              <ion-item class="pdd" style="padding-left:0px!important; min-height: 0.0rem !important;" text-wrap (click)="selectedOption(item.id)"\n                disabled="answered">\n\n                <ion-grid class="pdd" style="background-color:#FFFEEE; padding: 0px!important;">\n                  <ion-row class="pdd" class="fntsze" style=" margin:0!important;" [ngClass]="{\'correct\': item.id === answerForClass, \'wrong\': item.id !== answerForClass && clickedItem === item.id }">\n                    <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;; padding-bottom:13px; padding-top: 13px;">{{item.id}}.</ion-col>\n                    <ion-col col-10 class="fntsze" style="padding-left:5px!important;margin-left: 0px;line-height:22px;padding-bottom:13px; padding-top: 13px; line-height:26px;">\n                      <span>{{item.text}}</span>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-item>\n            </div>\n          </div>\n\n          <div>\n            <ion-grid style="padding-left: 0px;">\n              <ion-row style="padding-left: 0px;">\n                <ion-col style="font-size: 14px;">\n                  <a (click)="ReportQue()">Report this question</a>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <button float-left ion-button color="primary" icon-left (click)="previous(qsNumber)" [disabled]="qsNumber<=1"\n              style="background-color: #108fd2; margin-top: 7px; margin-bottom: 7px;">\n              <ion-icon name=\'arrow-back\'></ion-icon>\n              Previous\n            </button>\n\n            <button float-right ion-button color="primary" icon-right (click)="!(qsNumber==totalNumber) && next(qsNumber);(qsNumber==totalNumber) && finishLearn()"\n              style="background-color: #108fd2;  margin-top: 7px; margin-bottom: 7px; ">\n              {{(qsNumber==totalNumber)?\'Finish\':\'Next\'}}\n              <ion-icon name=\'arrow-forward\'></ion-icon>\n            </button>\n          </div>\n\n          <ion-grid *ngIf="clickedItem !== null">\n\n            <!-- <ion-scroll scrollY="true" class="Vscroll"> -->\n            <ion-row style="background-color:#FFFEEE;">\n              <ion-col col-12>\n                <div  class="scrollbar" [innerHtml]="questionData.question.description" style="line-height:27px!important; font-size:18px!important; font-weight:unset;"></div>\n              </ion-col>\n            </ion-row>\n            <!-- </ion-scroll> -->\n\n          </ion-grid>\n        </div>\n        <div *ngIf="quickview">\n          <div class="center" *ngIf="!questionquickData">\n            <ion-spinner></ion-spinner>\n            \n          </div>\n          <ion-item-divider text-wrap class="questionHead" style="padding:0px;">\n            <ion-grid class="question-wrap" style="background-color:#FFFEEE;">\n              <ion-row>\n\n                <ion-col col-2 class="fntsze" style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:18px; padding-left: 0px;">\n                  <span [innerHtml]="qstnNumber"></span>.\n                </ion-col>\n\n                <ion-col col-10 class="fntsze" style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                  <p style="font-size: 18px;" class="question" [innerHtml]="questionquickName"> </p>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n            <div class="col-sm-12 bdr" *ngIf="imageFlag">\n\n              <br>\n              <img class="img-responsive" style="width: 400px;height: 400" [src]="imgUrl">\n              <br>\n            </div>\n          </ion-item-divider>\n\n          <div class="answer-wrap pdd" style="padding:0px!important;border-bottom:none!important;">\n            <div *ngFor="let item of options ;let i= index;">\n\n              <ion-item class="pdd" style="padding-left:0px!important; min-height: 0.0rem !important;  " text-wrap disabled="answered">\n\n                <ion-grid class="pdd" style="background-color:#FFFEEE; padding: 0px!important;">\n                  <ion-row class="pdd" class="fntsze" style=" margin:0!important;" [ngClass]="{\'correct\': item.id === corectQuickAnswer}">\n                    <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;; padding-bottom:13px; padding-top: 13px;">{{item.id}}.</ion-col>\n                    <ion-col col-10 class="fntsze" style="padding-left:5px!important;margin-left: 0px;line-height:22px;padding-bottom:13px; padding-top: 13px;line-height:26px;">\n                      <span>{{item.text}}</span>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-item>\n\n            </div>\n          </div>\n\n          <div>\n            <ion-grid style="padding-left: 0px;">\n              <ion-row style="padding-left: 0px;">\n                <ion-col style="font-size: 14px;">\n                  <a (click)="ReportQue()">Report this question</a>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <button float-left ion-button color="primary" icon-left (click)="previous(qstnNumber)" [disabled]="qstnNumber<=1"\n              style="background-color: #108fd2; margin-top: 7px; margin-bottom: 7px;">\n              <ion-icon name=\'arrow-back\'></ion-icon>\n              Previous\n            </button>\n\n            <button float-right ion-button color="primary" icon-right (click)="!(qstnNumber==totalNumber) && next(qstnNumber);(qstnNumber==totalNumber) && finishLearn()"\n              style="background-color: #108fd2;  margin-top: 7px; margin-bottom: 7px; ">\n              {{(qstnNumber==totalNumber)?\'Finish\':\'Next\'}}\n              <ion-icon name=\'arrow-forward\'></ion-icon>\n            </button>\n          </div>\n          <ion-grid>\n\n            <!-- <ion-scroll scrollY="true" class="Vscroll"> -->\n            <ion-row style="background-color:#FFFEEE;">\n              <ion-col col-12>\n                <div class="scrollbar" [innerHtml]="questionquickData.question.description" style="line-height:27px!important; font-size:18px!important; font-weight:unset;"></div>\n              </ion-col>\n            </ion-row>\n            <!-- </ion-scroll> -->\n\n          </ion-grid>\n        </div>\n\n      </div>\n    </div>\n    <div *ngIf="starredView && !listStarView && !quickStarredView ">\n      <div *ngIf="!noData">\n        <div class="center" *ngIf="!questionStarData && !noData">\n          <ion-spinner></ion-spinner>\n        </div>\n        <ion-item-divider text-wrap class="questionHead" style="padding:0px;">\n          <ion-grid class="question-wrap" style="background-color:#FFFEEE;">\n            <ion-row>\n\n              <ion-col col-2 class="fntsze" style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:18px; padding-left: 0px;">\n                <span> {{Qnumber}}.</span>\n              </ion-col>\n\n              <ion-col col-10 class="fntsze" style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                <p style="font-size: 18px;" class="question" [innerHtml]="questions "> </p>\n              </ion-col>\n\n            </ion-row>\n\n          </ion-grid>\n          <div class="col-sm-12 bdr" *ngIf="imageFlag">\n\n            <br>\n            <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + imgUrl\'>\n            <br>\n          </div>\n        </ion-item-divider>\n\n        <div class="answer-wrap pdd" style="padding:0px!important;border-bottom:none!important;">\n          <div *ngFor="let option of options ;let i= index;">\n\n            <ion-item class="pdd" style="padding-left:0px!important; min-height: 0.0rem !important;  " text-wrap disabled="answered">\n\n              <ion-grid class="pdd" style="background-color:#FFFEEE; padding: 0px!important;">\n                <ion-row class="pdd" class="fntsze" style=" margin:0!important;" [ngClass]="{\'correct\':answers===option.id,\'wrong\':userAnswer===option.id}">\n                  <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;; padding-bottom:13px; padding-top: 13px;">{{option.id}}.</ion-col>\n                  <ion-col col-10 class="fntsze" style="padding-left:5px!important;margin-left: 0px;line-height:22px;padding-bottom:13px; padding-top: 13px;line-height:26px;">\n                    <span>{{option.text}}</span>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-item>\n\n          </div>\n        </div>\n        <ion-grid style="padding-left: 0px;">\n          <ion-row style="padding-left: 0px;">\n            <ion-col style="font-size: 14px;">\n              <a (click)="ReportQue()">Report this question</a>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <ion-row>\n          <ion-col col-4>\n            <button float-center ion-button color="#108fd2" style="background-color:#108fd2;" icon-left (click)="previousButtonClick()"\n              [disabled]=noPrevQuestion>Previous</button>\n          </ion-col>\n          <ion-col col-4>\n          </ion-col>\n          <ion-col col-4>\n            <button float-center ion-button style="background-color:#108fd2;color:white; padding: 0px 30px 0px 30px;" (click)="nextButtonClick()"\n              [disabled]=noQuestion>Next</button>\n\n          </ion-col>\n        </ion-row>\n        <div *ngIf="description">\n          <p style="font-weight:bold;font-size:18px!important;">Description</p>\n          <p [innerHtml]="description" class="scrollbar" style="line-height:27px!important; font-size:18px!important; font-weight:unset;"></p>\n        </div>\n      </div>\n    </div>\n    <div *ngIf="starredView && !listStarView && quickStarredView ">\n      <div *ngIf="!noData">\n        <div class="center" *ngIf="!questionStarData && !noData">\n          <ion-spinner></ion-spinner>\n        </div>\n        <ion-item-divider text-wrap class="questionHead" style="padding:0px;">\n          <ion-grid class="question-wrap" style="background-color:#FFFEEE;">\n            <ion-row>\n\n              <ion-col col-2 class="fntsze" style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:18px; padding-left: 0px;">\n                <span> {{Qnumber}}.</span>\n              </ion-col>\n\n              <ion-col col-10 class="fntsze" style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                <p style="font-size: 18px;" class="question" [innerHtml]="questions "> </p>\n              </ion-col>\n\n            </ion-row>\n\n          </ion-grid>\n          <div class="col-sm-12 bdr" *ngIf="imageFlag">\n\n            <br>\n            <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + imgUrl\'>\n            <br>\n          </div>\n        </ion-item-divider>\n\n        <div class="answer-wrap pdd" style="padding:0px!important;border-bottom:none!important;">\n          <div *ngFor="let option of options ;let i= index;">\n\n            <ion-item class="pdd" style="padding-left:0px!important; min-height: 0.0rem !important;  " text-wrap disabled="answered">\n\n              <ion-grid class="pdd" style="background-color:#FFFEEE; padding: 0px!important;">\n                <ion-row class="pdd" class="fntsze" style=" margin:0!important;" [ngClass]="{\'correct\':answers===option.id,\'wrong\':userAnswers===option.id}">\n                  <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;; padding-bottom:13px; padding-top: 13px;">{{option.id}}.</ion-col>\n                  <ion-col col-10 class="fntsze" style="padding-left:5px!important;margin-left: 0px;line-height:22px;padding-bottom:13px; padding-top: 13px;line-height:26px;">\n                    <span>{{option.text}}</span>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-item>\n\n          </div>\n        </div>\n        <ion-grid style="padding-left: 0px;">\n          <ion-row style="padding-left: 0px;">\n            <ion-col style="font-size: 14px;">\n              <a (click)="ReportQue()">Report this question</a>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <ion-row>\n          <ion-col col-4>\n            <button float-center ion-button color="#108fd2" style="background-color:#108fd2;" icon-left (click)="previousButtonClick()"\n              [disabled]=noPrevQuestion>Previous</button>\n          </ion-col>\n          <ion-col col-4>\n          </ion-col>\n          <ion-col col-4>\n            <button float-center ion-button style="background-color:#108fd2;color:white; padding: 0px 30px 0px 30px;" (click)="nextButtonClick()"\n              [disabled]=noQuestion>Next</button>\n\n          </ion-col>\n        </ion-row>\n        <div *ngIf="description">\n          <p style="font-weight:bold;font-size:18px!important;">Description</p>\n          <p [innerHtml]="description" class="scrollbar" style="line-height:27px!important; font-size:18px!important; font-weight:unset;"></p>\n        </div>\n      </div>\n    </div>\n    <div *ngIf="starredView && listStarView && !quickStarredView">\n      <div *ngFor="let item of questionlistStarData;let i=index;">\n\n        <ion-row class="qsnHead">\n\n          <ion-grid (click)="toggleDetails(item);findImage(i)" [ngClass]="{active: isGroupShown(item)}">\n            <ion-row>\n\n              <ion-col col-2 style="padding-top: 15px;max-width: 13.76667%;padding-left:0px;padding-right:5px;">\n                <span style="font-size: 18px;">{{i+1}}.</span>\n              </ion-col>\n              <ion-col col-10 style="padding-top: 0px;font-size: 15px;padding-left:0px;">\n                <span style="font-size: 18px;line-height: 20px" [innerHtml]="item.questions"></span>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n\n        </ion-row>\n        <div *ngIf="isGroupShown(item)" style="background-color: #fffeee">\n\n          <div class="col-sm-12 bdr" *ngIf="imageFlag">\n            <br>\n            <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + item.question_image\'>\n            <br>\n          </div>\n          <div *ngFor="let option of item.options">\n            <ion-grid style="padding-left:0px;padding:0px;">\n              <ion-row style=" margin:0!important;border-bottom:none;" [ngClass]="{\'correct\':item.answers===option.id,\'wrong\':item.userAnswer===option.id}">\n                <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;max-width: 13.76667%;padding-left:5px;padding-right:0px;line-height:34px;">{{option.id}}.</ion-col>\n                <ion-col col-10 style="padding-left: 0px!important; margin-left: 0px;line-height:20px;font-size: 18px;padding-left:0px;line-height:34px;"\n                  [innerHtml]="option.text"></ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n\n          <div *ngIf="item.description">\n            <p style="font-weight:bold;font-size: 18px; padding-left:5px;">Description:</p>\n            <p [innerHtml]="item.description" class="scrollbar" style="line-height: 20px;font-size: 18px; padding-left:5px;"></p>\n\n          </div>\n\n\n        </div>\n      </div>\n    </div>\n\n    <div *ngIf="listview">\n      <div class="center" *ngIf="!questionlistData">\n        <ion-spinner></ion-spinner>\n        `\n      </div>\n      <div *ngFor="let item of questionlistData;let i=index;">\n\n        <ion-row class="qsnHead">\n\n          <ion-grid (click)="toggleDetails(item);findImage(i)" [ngClass]="{active: isGroupShown(item)}">\n            <ion-row>\n\n              <ion-col col-2 style="padding-top: 15px;max-width: 13.76667%;padding-left:0px;padding-right:5px;">\n                <span style="font-size: 18px;">{{i+1}}.</span>\n              </ion-col>\n              <ion-col col-10 style="padding-top: 0px;font-size: 15px;padding-left:0px;">\n                <span style="font-size: 18px;line-height: 20px" [innerHtml]="item.question"></span>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n\n        </ion-row>\n        <div *ngIf="isGroupShown(item)" style="background-color: #fffeee">\n\n          <div class="col-sm-12 bdr" *ngIf="imageFlag">\n            <br>\n            <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + item.question_image\'>\n            <br>\n          </div>\n\n          <div *ngFor="let option of item.options">\n            <ion-grid style="padding-left:0px;padding:0px;">\n              <ion-row style=" margin:0!important;border-bottom:none;" [ngClass]="{\'correct\':item.answer===option.id,\'wrong\':item.userAnswer===option.id}">\n                <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;max-width: 13.76667%;padding-left:5px;padding-right:0px;line-height:34px;">{{option.id}}.</ion-col>\n                <ion-col col-10 style="padding-left: 0px!important; margin-left: 0px;line-height:20px;font-size: 18px;padding-left:0px;line-height:34px;"\n                  [innerHtml]="option.text"></ion-col>\n              </ion-row>\n              <!-- <ion-item  style="font-size: 15px;" [ngClass]="{\'correct\':item.answer===option.id,\'wrong\':item.userAnswer===option.id}" [innerHtml]="option.text" >\n                       \n                        </ion-item> -->\n            </ion-grid>\n          </div>\n          <div *ngIf="item.description" >\n            <p style="font-weight:bold;font-size: 18px; padding-left:5px;">Description:</p>\n            <p [innerHtml]="item.description" class="scrollbar" style="line-height: 20px;font-size: 18px; padding-left:5px;"></p>\n\n          </div>\n        </div>\n      </div>\n    </div>\n  </ion-content>\n  <ion-footer>\n    <div class="bar bar-footer bar-assertive">\n      <ion-grid style="padding-left: 0px;padding-right: 0px; padding-top: 0px;padding-bottom:0px;">\n        <ion-col col-12 style="background-color:#cecece; ">\n          <ion-row>\n            <ion-col col-2 style="font-size:20px;text-align:center; padding: 0px; margin: 0px;">\n\n              <ion-icon ios="ios-bookmark" md="md-bookmark" *ngIf="listview"></ion-icon>\n              <ion-icon ios="ios-bookmark" md="md-bookmark" *ngIf="listStarView" style="color: #108fd2!important; "></ion-icon>\n              <ion-icon ios="ios-bookmark" md="md-bookmark" *ngIf="!listview && !listStarView " [ngClass]="{\'starred\': star}" (click)="starQuestions()"></ion-icon>\n\n            </ion-col>\n            <ion-col col-3 style="font-size:20px;text-align:center;padding-top:5px;padding: 0px; margin: 0px; padding-left: 3px;">\n              <ion-toggle float-center checked="true" style="  display: block;\n                margin: auto;padding-top:4px; " [(ngModel)]="starredView" (ionChange)="logEvent($event)">\n\n              </ion-toggle>\n\n            </ion-col>\n            <ion-col col-3 *ngIf="!starredView && !listStarView && !quickStarredView" style="font-size:20px;text-align:center!important;padding: 0px; margin: 0px;">\n              <ion-icon [ngClass]="(quickclr)?\'blueIcon\':\'icn\'" ios="ios-eye" md="md-eye" (click)="quickView()"></ion-icon>\n\n            </ion-col>\n            <ion-col col-3 *ngIf="starredView || listStarView || quickStarredView" style="font-size:20px;text-align:center!important;padding: 0px; margin: 0px;">\n              <ion-icon [ngClass]="(quickclr)?\'blueIcon\':\'icn\'" ios="ios-eye" md="md-eye" (click)="quickStarView(newvalue)"></ion-icon>\n\n            </ion-col>\n            <ion-col col-2 style="font-size:20px;text-align:center;padding: 0px; margin: 0px;">\n              <ion-icon class="fabbb" *ngIf="listview || listStarView" ios="ios-clipboard" md="md-clipboard">\n              </ion-icon>\n              <ion-icon class="fabbb" *ngIf="!listview && !listStarView && resultData.length == 0" ios="ios-clipboard" (click)="addNotes()"\n                md="md-clipboard">\n              </ion-icon>\n              <ion-icon class="fabbb" *ngIf="!listview && !listStarView &&  resultData.length != 0" ios="ios-clipboard" (click)="editNote()"\n                md="md-clipboard">\n                <ion-icon style="padding-top: 8px;" id="notifications-badge" ios="ios-checkmark" md="md-checkmark"></ion-icon>\n              </ion-icon>\n            </ion-col>\n            <ion-col col-2  *ngIf="!starredView && !listStarView && !quickStarredView" style="font-size:20px;text-align:center;padding: 0px; margin: 0px;">\n              <ion-icon [ngClass]="(listClr)?\'blueIcon\':\'icn\'" ios="ios-list" md="md-list" (click)="listView()"></ion-icon>\n            </ion-col>\n            <ion-col col-2 *ngIf="starredView || listStarView || quickStarredView" style="font-size:20px;text-align:center;padding: 0px; margin: 0px;">\n              <ion-icon [ngClass]="(listClr)?\'blueIcon\':\'icn\'" ios="ios-list" md="md-list" (click)="listStarview()"></ion-icon>\n            </ion-col>\n            <ion-col col-2 style="text-align: center; padding: 0px; margin: 0px;">\n              Bookmark\n            </ion-col>\n            <ion-col col-3 style="text-align: center; padding: 0px; margin: 0px;padding-left: 3px;">\n              Bookmarked only\n            </ion-col>\n            <ion-col col-3 style="text-align: center; padding: 0px; margin: 0px;">\n              Quick View\n            </ion-col>\n            <ion-col col-2 style="text-align: center; padding: 0px; margin: 0px;">\n              Notes\n            </ion-col>\n            <ion-col col-2 style="text-align: center; padding: 0px; margin: 0px;">\n              list View\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-grid>\n    </div>\n  </ion-footer>\n</body>\n\n</html>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/questions.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_student_service__["a" /* StudentService */], __WEBPACK_IMPORTED_MODULE_3__services_notes_service__["a" /* NotesService */], __WEBPACK_IMPORTED_MODULE_4__services_question_service__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_9__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_9__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_student_service__["a" /* StudentService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__services_question_service__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_3__services_notes_service__["a" /* NotesService */]])
    ], Questions);
    return Questions;
}());

//# sourceMappingURL=questions.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotesService = /** @class */ (function () {
    function NotesService(http) {
        this.http = http;
        this.url = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].backendUrl;
        var userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = userData._id;
        this.userDetailsId = userData.userDetailsId;
        this.userId = userData.userId;
    }
    NotesService.prototype.createNote = function (noteContent, questionId, subjectArray) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            "questionId": questionId,
            "noteTitle": "No Title",
            "color": "#ffc",
            "noteContent": noteContent,
            "subjectArray": subjectArray
        };
        console.log("Note Data sending: ", data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/createStudyNote", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.editNote = function (noteId, noteContent, subjectArray) {
        var data = {
            'activeUserId': this.activeUserId,
            "noteId": noteId,
            "noteTitle": "No Title",
            "color": '#ffc',
            "noteContent": noteContent,
            "subjectArray": subjectArray
        };
        console.log(data);
        return this.http.post(this.url + "/editStudyNote", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.deleteNote = function (noteId) {
        var data = {
            'activeUserId': this.activeUserId,
            "userId": this.userId,
            "noteId": noteId
        };
        return this.http.post(this.url + "/deleteStudyNote", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.fetchAllNotes = function (loadMore) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'loadMore': loadMore
        };
        console.log("Data sending to server", data);
        return this.http.post(this.url + "/fetchAllNotes", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.fetchAllMiniNotes = function () {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId
        };
        console.log("Data sending to server", data);
        return this.http.post(this.url + "/get-all-mini-note", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.saveRecentNote = function (notId, mode) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            "noteId": notId,
            "mode": mode
        };
        console.log("Data sending to server//", data);
        return this.http.post(this.url + "/recent-daily-notes", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.loadRecentNote = function () {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            "mode": "get"
        };
        console.log("Data sending to server//", data);
        return this.http.post(this.url + "/recent-daily-notes", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.loadMininote = function (noteid, regId, demoFlag) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            "noteId": noteid,
            "registrationId": regId,
            "demoFlag": demoFlag
        };
        console.log("Data sending to server//", data);
        return this.http.post(this.url + "/load-mini-note", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.bookmarkedOnly = function () {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId
        };
        return this.http.post(this.url + "/bookmarked-notes-all", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService.prototype.bookmarkMiniNote = function (noteId, bookmark) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "noteId": noteId,
            "bookmarked": bookmark
        };
        console.log("Data sending to server", data);
        return this.http.post(this.url + "/finish-bookmark-mini-note", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    NotesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], NotesService);
    return NotesService;
}());

//# sourceMappingURL=notes.service.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateExam; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_exam_examModal_examModal__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_exam_createexam_write_exam_write_exam__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_exam_GrandExam_grantModal_grantModal__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_exam_GrandExam_grantexam_component__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_student_service__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_exam_examNew_examComponent__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var CreateExam = /** @class */ (function () {
    function CreateExam(navCtrl, studentService, params, __auth, toastCtrl, examService, modalCtrl) {
        this.navCtrl = navCtrl;
        this.studentService = studentService;
        this.__auth = __auth;
        this.toastCtrl = toastCtrl;
        this.examService = examService;
        this.modalCtrl = modalCtrl;
        this.cart = [];
        this.dueFlag = false;
        this.timeFlag = false;
        this.examconfig = [];
        this.sectionArray = [];
        this.subjectArray = [];
        this.walkdis = false;
        // this.showSection = false;
        // this.showChapter = false;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
        this.userId = this.userData.userId;
        this.getSubject(this.userId, this.activeUserId);
        this.walkthroug();
    }
    CreateExam.prototype.getSubject = function (userId, activeUserId) {
        this.subjectList = JSON.parse(localStorage.getItem('subjectData'));
    };
    CreateExam.prototype.walkthroug = function () {
        var _this = this;
        this.studentService.dashboardInfo().subscribe(function (res) {
            if (res.data.exam)
                _this.walkdis = false;
            else
                _this.walkdis = true;
        });
    };
    CreateExam.prototype.ViewSections = function (index) {
        this.showSection = true;
        this.chapter = null;
        this.index = index;
        this.viewData = true;
        this.section = this.subjectList[index].children;
    };
    CreateExam.prototype.Viewchapters = function (index) {
        this.showChapter = true;
        if (this.section == []) {
            this.chapter = [];
        }
        else {
            this.chapter = this.section[index].children;
        }
    };
    CreateExam.prototype.selectSubject = function (index) {
        this.selectedSubject = this.subjectList[index];
        /* Check for double entry in array */
        var status;
        var kstatus;
        if (this.cart.length != 0) {
            for (var i = 0; i < this.cart.length; i++) {
                /* double entry check */
                if (this.selectedSubject.id === this.cart[i].id) {
                    status = 1;
                    break;
                }
                else {
                    status = 0;
                }
                if (this.cart[i].parentArray) {
                    for (var j = 0; j < this.cart.length; j++) {
                        if (this.selectedSubject.id === this.cart[i].parentArray[0]) {
                            kstatus = 0;
                            this.cart.splice(i, 1);
                            j--;
                        }
                        else {
                            kstatus = 0;
                        }
                    }
                }
            }
            if (status === 0 && kstatus == 0) {
                this.cart.push(this.selectedSubject);
            }
        }
        else {
            this.cart.push(this.selectedSubject);
        }
    };
    CreateExam.prototype.selectSection = function (index) {
        //this.showChapter = true;
        this.selectedSection = this.section[index];
        /* Checking section for double entry */
        var pstatus = 1;
        var kstatus = 1;
        var stat = 1;
        if (this.cart.length != 0) {
            for (var i = 0; i < this.cart.length; i++) {
                if (this.section[index].id === this.cart[i].id) {
                    stat = 1;
                    break;
                }
                else {
                    stat = 0;
                }
                if (this.selectedSection.imParentObj.id === this.cart[i].id) {
                    pstatus = 1;
                    break;
                }
                else {
                    pstatus = 0;
                }
                if (this.selectedSection.id === this.cart[i].parentArray[1]) {
                    this.cart.splice(i, 1);
                    kstatus = 0;
                }
                else {
                    kstatus = 0;
                }
            }
            if (stat === 0 && pstatus === 0 && kstatus === 0) {
                this.cart.push(this.section[index]);
            }
            else {
                console.log("onnu podo");
            }
        }
        else {
            this.cart.push(this.selectedSection);
        }
    };
    CreateExam.prototype.selectChapter = function (index) {
        var cstatus;
        var lstatus;
        var gstatus;
        if (this.cart.length != 0) {
            /* check chapter double entry */
            for (var k = 0; k < this.cart.length; k++) {
                /* Check for grand parent */
                for (var g = 0; g < this.chapter[index].parentArray.length; g++) {
                    if (this.chapter[index].parentArray[g] === this.cart[k].id) {
                        console.log("Kids, Grandpa here ! ESCAPE!!!");
                        gstatus = 1;
                        break;
                    }
                    else {
                        gstatus = 0;
                    }
                }
                if (this.chapter[index].id === this.cart[k].id) {
                    cstatus = 1;
                    break;
                }
                else {
                    cstatus = 0;
                }
                if (this.chapter[index].imParentObj.id === this.cart[k].id) {
                    console.log("Found you father! Kid, Dn't go there!");
                    lstatus = 1;
                    break;
                }
                else {
                    lstatus = 0;
                }
            }
            /* If kids,parents,grandpa are not there in cart */
            if (cstatus === 0 && lstatus == 0 && gstatus === 0) {
                this.cart.push(this.chapter[index]);
            }
        }
        else {
            this.cart.push(this.chapter[index]);
        }
    };
    CreateExam.prototype.dltSubjct = function (index) {
        this.removingIndex = index;
        if (!this.viewData) {
            this.subjectList.splice(index, 1);
        }
        else {
            this.subjectList.splice(index, 1);
            this.showSection = false;
            this.showChapter = false;
            this.section = [];
            this.chapter = [];
        }
    };
    CreateExam.prototype.dltSection = function (index) {
        this.removingIndex = index;
        this.section.splice(index, 1);
        //this.section[index].children=[];
        this.chapter = this.section[index].children;
        // this.chapter.splice(index, 1);
        this.chapter = [];
        this.showChapter = false;
        if (this.chapter.length == 0) {
            this.showChapter = false;
        }
    };
    CreateExam.prototype.dltChapter = function (index) {
        this.removingIndex = index;
        //this.section.splice(index, 1);
        this.chapter.splice(index, 1);
        if (this.chapter.length == 0) {
            this.showChapter = false;
        }
    };
    CreateExam.prototype.removeFromCart = function (index) {
        // this.cart.splice(index, 1);
        if (this.cart[index].title == "subject") {
            // this.subjectList.push(this.cart[index])
            this.subjectList.splice(this.removingIndex, 0, this.cart[index]);
            this.cart.splice(index, 1);
        }
        else if (this.cart[index].title == "section") {
            //this.section.push(this.cart[index]);
            this.section.splice(this.removingIndex, 0, this.cart[index]);
            console.log("section remove222222222", this.section);
            this.cart.splice(index, 1);
        }
        else {
            console.log("chapter remove", this.removingIndex);
            //  this.chapter.push(this.cart[index]);
            this.chapter.splice(this.removingIndex, 0, this.cart[index]);
            this.cart.splice(index, 1);
        }
    };
    CreateExam.prototype.backToeaxam = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    CreateExam.prototype.createExam = function () {
        var _this = this;
        this.section = "";
        this.chapter = "";
        this.shortname = '';
        this.totalQuestionsCount = 0;
        for (var i = 0; i < this.cart.length; i++) {
            this.totalQuestionsCount = this.totalQuestionsCount + this.cart[i].count;
            var shortedname = void 0;
            var nameShort = this.cart[i].name.split(" ")[0];
            if (this.shortname == '') {
                this.shortname = this.shortname.concat(nameShort);
            }
            else {
                this.shortname = this.shortname.concat("-", nameShort);
            }
        }
        var data = {
            'shortname': this.shortname,
            "totalQuestionsCount": this.totalQuestionsCount
        };
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__pages_exam_examModal_examModal__["a" /* ExamModal */], { data: data }, { cssClass: 'inset-modal' });
        profileModal.onDidDismiss(function (value) {
            if (value) {
                if (value.names && value.questionNumber) {
                    _this.numOfQuestion = value.questionNumber;
                    _this.examName = value.names;
                    _this.defMint = value.minute;
                    _this.hourss = value.hour;
                    _this.cartLength = _this.cart.length;
                    var averageNumber = _this.numOfQuestion / _this.cartLength;
                    var averageNumberRound = Math.floor(averageNumber);
                    var decimal = averageNumber % 1;
                    var needMore = _this.numOfQuestion - (averageNumberRound * _this.cartLength);
                    var questionsWanted = void 0;
                    for (var i = 0; i < _this.cartLength; i++) {
                        var currentId = _this.cart[i].id;
                        var currentName = _this.cart[i].name;
                        if (i == 0) {
                            questionsWanted = averageNumberRound + needMore;
                        }
                        else {
                            questionsWanted = averageNumberRound;
                        }
                        var data_1 = {
                            "id": currentId,
                            "name": currentName,
                            "noOfQuestions": questionsWanted
                        };
                        _this.examconfig.push(data_1);
                    }
                    if (_this.hourss) {
                        _this.timeFlag = true;
                    }
                    else {
                        _this.timeFlag = false;
                    }
                    if (value.box == true) {
                        _this.dueFlag = true;
                        _this.dueObj = value.date;
                    }
                    else {
                        _this.dueFlag = false;
                    }
                    var data_2 = {
                        'userId': _this.userId,
                        'examName': _this.examName,
                        'examConfig': _this.examconfig,
                        'noOfQuestions': _this.numOfQuestion,
                        'dueFlag': _this.dueFlag,
                        'timeFlag': _this.timeFlag,
                        'dueObj': _this.dueObj
                    };
                    _this.examService.createExam(_this.examName, _this.examconfig, _this.numOfQuestion, _this.dueFlag, _this.dueObj, _this.timeFlag, _this.defMint, _this.hourss).subscribe(function (res) {
                        var examId = res.data.examId;
                        if (res.status === 1) {
                            if (_this.dueFlag == true) {
                                _this.examService.startExam(examId).subscribe(function (res) {
                                    _this.__auth.notificationInfo('Exam saved successfully');
                                });
                            }
                            else {
                                _this.examService.startExam(examId).subscribe(function (res) {
                                    if (res.data.userExamId) {
                                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */], { subject: res.data.userExamId });
                                        _this.cart = [];
                                    }
                                    else {
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        profileModal.present();
    };
    //grand exam
    CreateExam.prototype.grandExam = function () {
        var _this = this;
        var chooseModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__pages_exam_GrandExam_grantModal_grantModal__["a" /* grantModal */], null, { cssClass: 'inset-modal' });
        chooseModal.onDidDismiss(function (value) {
            console.log(value, "fvalue");
            if (value.names && value.nofoq) {
                console.log(value, "2value");
                _this.numOfQuestion = value.nofoq;
                _this.examName = value.names;
                _this.defMint = value.minutes;
                _this.hourss = value.hour;
                _this.time = value.time;
                _this.examType = "grand";
                if (_this.hourss) {
                    _this.timeFlag = true;
                }
                else {
                    _this.timeFlag = false;
                }
                if (value.box == true) {
                    _this.dueFlag = true;
                    _this.dueObj = value.date;
                }
                else {
                    _this.dueFlag = false;
                }
                _this.examService.createGrandExam(_this.examName, _this.examconfig, _this.numOfQuestion, _this.dueFlag, _this.timeFlag, _this.defMint, _this.hourss, _this.dueObj, _this.examType).subscribe(function (res) {
                    console.log(res.data, "first");
                    var examId = res.data.exam_id;
                    if (res.status === true) {
                        if (_this.dueFlag == true) {
                            _this.examService.loadGrandExam(examId).subscribe(function (res) {
                                _this.__auth.notificationInfo('Grant Test saved successfully');
                                // this.router.navigate(['/exam/room'])
                            });
                        }
                        else {
                            _this.examService.loadGrandExam(examId).subscribe(function (res) {
                                console.log(res.data, "secnd");
                                if (res.data.userExamObj) {
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_GrandExam_grantexam_component__["a" /* grantExam */], { subject: examId });
                                    // this.router.navigate(['/grand', examId]);
                                }
                                else {
                                    console.log("Can't find user Exam id");
                                }
                            });
                        }
                    }
                });
            }
        });
        chooseModal.present();
    };
    CreateExam = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-createExam',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/createexam/createExam.html"*/'\n\n<!-- <ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n          </button>\n        <ion-title>Create exam</ion-title>\n    </ion-navbar>\n</ion-header> -->\n\n<style>\n    .card-md {\n        margin: -15px !important;\n        border-radius: 2px !important;\n        width: calc(115% - 20px) !important;\n    }\n\n    a {\n        color: #108fd2 !important;\n    }\n\n</style>\n\n<ion-header>\n    <ion-toolbar>\n\n        <ion-icon name="md-arrow-back" style="font-size: 21px;\n                margin-left: 8px;\n                color: #fff;" (click)="backToeaxam()"></ion-icon>\n        &nbsp;&nbsp;\n        <span style="font-size: 20px;\n             margin-left: 4px;\n             color: #fff;">Create exam </span>\n\n    </ion-toolbar>\n</ion-header>\n<ion-content padding class="bgpic">\n\n    <p class="head">Subjects</p>\n\n\n\n    <!-- <ion-card class="cardStyle" style="height: 86px;" >\n                  \n                        <div class="scroll " style="\n                        background-color: #fff;">\n                        <ion-grid>\n                        <ion-row>\n                                <ion-col col-4  class="bxcolumn"   *ngFor="let subject of subjectList; let i=index;" [ngClass]="(selectSub==i)?\'selected\':\'unselected\'" (click)="ViewSections(i)"   data-id="{{i}}" >\n                              \n                                    <ion-col>   \n                    {{subject.name}} \n              \n   </ion-col>\n   <ion-col>\n                                    <span   (click)="selectSubject(i);dltSubjct(i);"><ion-icon  name="md-add"  class="iconSize" ></ion-icon></span>\n                            \n    \n                            </ion-col>\n                           \n                            </ion-col>\n                            </ion-row>\n                            </ion-grid>\n                        </div>\n                   \n                    </ion-card> -->\n    <ion-card class="cardStyle" style="height: 86px;">\n        <ion-scroll scrollX="true"  style="text-align: center;margin-top:0px;">\n\n            <ion-col class="scroll-item " *ngFor="let subject of subjectList; let i=index;" [ngClass]="(selectSub==i)?\'selected\':\'unselected\'"\n                (click)="ViewSections(i)" data-id="{{i}}">\n                <ion-row>\n                    <ion-col col-12>\n                        <span>{{subject.name}} </span>\n\n                    </ion-col>\n                    <ion-col col-12>\n                        <span (click)="selectSubject(i);dltSubjct(i);">\n                            <ion-icon name="md-add" class="iconSize"></ion-icon>\n                        </span>\n\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-scroll>\n\n    </ion-card>\n\n\n\n\n\n\n\n\n    <div *ngIf=" section && showSection">\n\n        <p style=" font-size: 18px;\n       \n                font-weight: bold;padding:0px!important; margin-bottom:20px!important;  margin-top:27px!important">Section</p>\n        <ion-card class="cardStyle" style="height: 86px;">\n            <ion-scroll scrollX="true"  style="text-align: center;margin-top:0px;">\n                <ion-col class="scroll-item " *ngFor="let sec of section; let j = index;" [ngClass]="(selectSec==j || greyClr)?\'selected\':\'unselected\'"\n                    (click)="Viewchapters(j)" data-id="{{j}}">\n\n                    <ion-row>\n                        <ion-col col-12>\n                            <span>{{sec.name}}</span>\n\n                        </ion-col>\n                        <ion-col col-12>\n\n                            <span style="padding-top: 5px;" (click)="selectSection(j);dltSection(j)">\n                                <ion-icon name="md-add" class="iconSize"></ion-icon>\n                            </span>\n\n                        </ion-col>\n                    </ion-row>\n\n\n                </ion-col>\n            </ion-scroll>\n\n        </ion-card>\n\n\n\n\n        <!-- <ion-card class="cardStyle" >\n                  \n                    <ion-row  >\n                        <div class="scroll " style="padding: 36px 3px;\n                        background-color: #fff;">\n                            <ion-col col-12 col-sm class="bxcolumn" *ngFor="let sec of section; let j = index;" [ngClass]="(selectSec==j || greyClr)?\'selected\':\'unselected\'" >\n                                <ion-col  (click)="Viewchapters(j)" >\n                                    <span  data-id="{{j}}"  >\n                    {{sec.name}}\n              \n                                    </span>\n    \n                                    <span  (click)="selectSection(j);dltSection(j)"><ion-icon name="md-add"  class="iconSize" ></ion-icon></span>\n                                </ion-col>\n    \n                            </ion-col>\n                        </div>\n                    </ion-row>\n                </ion-card>\n         -->\n    </div>\n\n    <div *ngIf="chapter && showChapter">\n\n        <p style="font-size: 18px;\n       \n                font-weight: bold;padding:0px!important; margin-bottom: 20px!important;  margin-top:27px!important">Chapter</p>\n        <ion-card class="cardStyle" style="height: 86px;">\n            <ion-scroll scrollX="true" style="text-align: center;margin-top:0px;">\n\n                <ion-col class="scroll-item " [ngClass]="(selectChap==j || greyClr || greyClr1)?\'selected\':\'unselected\'" *ngFor="let item of chapter; let j = index;"\n                    (click)="Viewchapters(j)" data-id="{{j}}">\n                    <ion-row>\n                        <ion-col col-12>\n                            <span>{{item.name}}</span>\n\n                        </ion-col>\n                        <ion-col col-12>\n                            <span style="padding-top: 5px;" (click)="selectChapter(j);dltChapter(j)">\n                                <ion-icon name="md-add" class="iconSize"></ion-icon>\n                            </span>\n                        </ion-col>\n\n\n                    </ion-row>\n                </ion-col>\n            </ion-scroll>\n\n        </ion-card>\n\n\n\n\n\n\n        <!-- <ion-card class="cardStyle" >\n                    \n                    <ion-row>\n                        <div class="scroll " style="padding: 36px 3px;\n                            background-color: #fff;">\n                            <ion-col col-12 col-sm class="bxcolumn"   [ngClass]="(selectChap==j || greyClr || greyClr1)?\'selected\':\'unselected\'" *ngFor="let item of chapter; let j = index;">\n                                <ion-col>\n                                    <span >\n                        {{item.name}}\n                  \n                                    </span>\n                                    <span  (click)="selectChapter(j);dltChapter(j)"><ion-icon name="md-add"   class="iconSize"></ion-icon></span>\n                                </ion-col>\n    \n                            </ion-col>\n                        </div>\n                    </ion-row>\n                </ion-card> -->\n    </div>\n    <ion-grid>\n        <ion-row style="padding-top:20px;">\n            <div class="myBox">\n\n\n                <span class="tags" *ngFor="let item of cart ;let j = index;"> {{item.name}}\n                    <ion-icon name="close" (click)="removeFromCart(j)" style="font-weight: bold;"></ion-icon>\n                </span>\n            </div>\n            <!-- <ion-card *ngFor="let item of cart" style="height: auto;margin-left:-10px;margin-right: -10px; width: 800px;">\n                    <ion-row>\n                        <ion-col col-8>\n                            <p style="font-size: 15px;\n                                        padding: 8px;" >{{item.name}}</p>\n                        </ion-col>\n                        <ion-col col-4>\n                            <button ion-button (click)="removeFromCart(i)" class="deleteButton"><ion-icon name="ios-trash" ></ion-icon></button>\n    \n                        </ion-col>\n                    </ion-row>\n    \n    \n    \n    \n    \n    \n                </ion-card> -->\n        </ion-row>\n    </ion-grid>\n    <ion-grid id="focusItemtwo" >\n        <ion-row>\n\n            <ion-col >\n                <button  ion-button block (click)="grandExam()" style="color: #fff;background-color:#108fd2;">One click grandtest</button>\n\n\n            </ion-col>\n            <ion-col>\n                <button ion-button block (click)="createExam()" [disabled]=\'!cart.length\' style="color: #fff;background-color:#108fd2;">Create Exam</button>\n\n            </ion-col>\n\n        </ion-row>\n    </ion-grid>\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/createexam/createExam.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_8__services_student_service__["a" /* StudentService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_8__services_student_service__["a" /* StudentService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], CreateExam);
    return CreateExam;
}());

//# sourceMappingURL=createExam.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Exam; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_exam_createexam_createExam__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_exam_createexam_write_exam_write_exam__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_exam_GrandExam_grantexam_component__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_exam_weekend_TermsAndRules_terms_component__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_exam_exam_room_details_details__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_exam_exam_room_aiimsResult_aiimsResult__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_exam_popoverPage_popoverPage__ = __webpack_require__(444);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_exam_weekend_weekend_component__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_exam_createexam_aiimsExam_aiimsExam__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var Exam = /** @class */ (function () {
    function Exam(platform, popoverCtrl, viewCtrl, navCtrl, navParams, examService, __auth) {
        this.platform = platform;
        this.popoverCtrl = popoverCtrl;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.examService = examService;
        this.__auth = __auth;
        this.demoExams = [];
        this.demoExam = false;
        this.isRegistered = false;
        this.custmizedExam = [];
        this.completedWeekExam = [];
        this.customized = false;
        this.week = false;
        this.pendingWeekexam = [];
        this.totalAiims = [];
        this.shownGroup = null;
        this.dailyExams = [];
        this.totalWeek = [];
        this.daily = false;
        this.aiims = false;
        this.aiimsdemoExam = false;
        this.examType = navParams.get('examType');
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.type = "All";
        if (this.examType == "cutomized") {
            this.examTypes = "customized";
        }
        else if (this.examType == "week") {
            this.examTypes = "week";
        }
        else if (this.examType == "daily") {
            this.examTypes = "daily";
        }
        else {
            this.examTypes = "aiims";
        }
    }
    Exam.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    Exam.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__pages_dashboard_home__["a" /* HomePage */]);
        //this.viewCtrl.dismiss();
    };
    Exam.prototype.ngOnInit = function () {
        if (this.examType == "cutomized") {
            this.customized = true;
            this.week = false;
            this.daily = false;
            this.aiims = false;
            this.getAllCustomized();
        }
        else if (this.examType == "week") {
            this.week = true;
            this.customized = false;
            this.daily = false;
            this.aiims = false;
            this.getAllWeek();
        }
        else if (this.examType == "daily") {
            this.week = false;
            this.customized = false;
            this.daily = true;
            this.aiims = false;
            this.getAllDaily();
        }
        else {
            this.aiims = true;
            this.week = false;
            this.customized = false;
            this.daily = false;
            this.getAllAiims();
        }
        this.checkAplay();
        this.username = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
    };
    Exam.prototype.getAllAiims = function () {
        var _this = this;
        this.examService.getSubscribeExam().subscribe(function (res) {
            console.log("res subcribed exams", res);
            if (res.data) {
                if (res.data.activeExams.length > 0) {
                    var exam = (res.data.activeExams).map(function (item) {
                        item.examPending = true;
                        item.examId = item._id;
                        return item;
                    });
                    _this.totalAiims = exam;
                }
                if (res.data.completedExams.length > 0) {
                    var exam = (res.data.completedExams).map(function (item) {
                        item.examCompleted = true;
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.numberOfQuestions = item.examResult.total;
                        item.examinformation = item.examDetails.description;
                        return item;
                    });
                    _this.totalAiims = (exam.length > 0) ? _this.totalAiims.concat(exam) : _this.totalAiims;
                }
                if (res.data.inactiveExams.length > 0) {
                    var exam = (res.data.inactiveExams).map(function (item) {
                        item.inactiveExam = true;
                        return item;
                    });
                    _this.totalAiims = (exam.length > 0) ? _this.totalAiims.concat(exam) : _this.totalAiims;
                }
                if (res.data.pendingExams.length > 0) {
                    var exam = (res.data.pendingExams).map(function (item) {
                        item.examPending = true;
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examinformation = item.examDetails.description;
                        item.packageId = item.examDetails.packageId;
                        item.examDate = item.examDetails.examDate;
                        return item;
                    });
                    _this.totalAiims = (exam.length > 0) ? _this.totalAiims.concat(exam) : _this.totalAiims;
                    _this.totalAiims = _this.totalAiims.sort(function (a, b) { return new Date(a.examDate).getTime() - new Date(b.examDate).getTime(); });
                }
                console.log("total aiims exam", _this.totalAiims);
            }
            else {
                _this.__auth.notificationInfo("There is no data found!");
            }
        });
    };
    Exam.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    Exam.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    Exam.prototype.presentPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_10__pages_exam_popoverPage_popoverPage__["a" /* PopoverPage */], { myEvent: myEvent }, { enableBackdropDismiss: false });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (data, event) {
            if (event == "pending") {
                event = "Pending";
            }
            else if (event == "completed") {
                event = "Completed";
            }
            else if (event == "demo") {
                event = "Demo";
            }
            if (myEvent == "customized") {
                _this.custmizedExam = data;
                _this.type = event;
            }
            else if (myEvent == "week") {
                _this.totalWeek = data;
                _this.type = event;
                if (event == "demo") {
                    _this.demoExam = true;
                }
                else {
                    _this.demoExam = false;
                }
            }
            else if (myEvent == "daily") {
                _this.dailyExams = data;
                _this.type = event;
                if (event == "demo") {
                    _this.aiimsdemoExam = true;
                }
                else {
                    _this.aiimsdemoExam = false;
                }
            }
            else {
                _this.type = event;
                if (event == "demo") {
                    _this.demoExam = true;
                    _this.demoExams = data;
                }
                else {
                    _this.demoExam = false;
                    _this.totalAiims = data;
                }
            }
            // Navigate to new page.  Popover should be gone at this point completely
        });
    };
    Exam.prototype.register = function (category) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_exam_weekend_weekend_component__["a" /* weekendReg */], { exam: category });
    };
    Exam.prototype.checkAplay = function () {
        var _this = this;
        this.examService.checkAppliedForExam().subscribe(function (data) {
            if (data.status) {
                _this.registerLabel = "Register";
                _this.isRegistered = false;
                _this.paymentDetails = data;
            }
            else {
                _this.registerLabel = "Already registered";
                _this.RegisterMsg = "You are registered";
            }
        }, function (err) {
            _this.__auth.notificationInfo("OOPS Something went wrong.");
        });
    };
    Exam.prototype.newExamfunction = function (plan, desc, item) {
        this.__auth.notificationInfo("only for institutional users");
        // if (item) {
        //   this.singleExamId = item._id;
        // }
        // this.plan = plan;
        // if (!this.isRegistered) {
        //   let options = {
        //     // key: 'rzp_test_V1RjYTq2Xll4NR',
        //     key: this.paymentDetails.data.keyId,
        //     amount: (this.paymentDetails.data.weeklyDetails[plan].amount) * 100,
        //     name: desc,
        //     prefill: {
        //       name: this.userData.profile.firstName + this.userData.profile.lastName,
        //       email: this.userData.username
        //     },
        //     notes: {
        //       address: ""
        //     },
        //     theme: {
        //       color: "#108fd2"
        //     }
        //   }
        //   var successCallback = (payment_id) => {
        //     this.callApi(payment_id);
        //   };
        //   var cancelCallback = (error) => {
        //   };
        //   this.platform.ready().then(() => {
        //     RazorpayCheckout.open(options, successCallback, cancelCallback);
        //   })
        // }
        // else this.__auth.notificationInfo('Somthing went wrong.')
    };
    Exam.prototype.callApi = function (payment_id) {
        var _this = this;
        this.paymentId = payment_id;
        if (this.singleExamId) {
            this.postData = {
                examId: this.singleExamId,
                paymentId: this.paymentId,
                paymentStatus: "done",
                paymentMethod: "online",
                PlaneName: this.plan
            };
        }
        else {
            this.postData = {
                paymentId: this.paymentId,
                paymentStatus: "done",
                paymentMethod: "online",
                PlaneName: this.plan
            };
        }
        this.examService.SinglePaymentExam(this.postData)
            .subscribe(function (res) {
            if (res.status) {
                _this.checkAplay();
                _this.getAllWeek();
            }
        }, function (err) {
            _this.__auth.notificationInfo('Somthing went wrong.');
        });
    };
    Exam.prototype.viewResult = function (item) {
        var exam_id;
        if (item.examType == "grand") {
            exam_id = item._id + '/g' + '/fromCompleted';
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: exam_id });
        }
        else if (item.examType === "week" || item.examType === "special" || item.examType === "daily") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: item._id + '/week' });
        }
        else if (item.examType == "AIIMS" || item.examType == "JIPMER") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_exam_exam_room_aiimsResult_aiimsResult__["a" /* aiimsResult */], { ExamIds: item.examId + '/' + item._id + '/' + item.examType });
        }
        else {
            exam_id = item.examId + '/' + item._id + '/fromCompleted';
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: exam_id });
        }
    };
    Exam.prototype.segmentChanged = function (event) {
        this.type = "All";
        if (event._value == "week") {
            this.week = true;
            this.customized = false;
            this.daily = false;
            this.aiims = false;
            this.getAllWeek();
        }
        else if (event._value == "customized") {
            this.customized = true;
            this.week = false;
            this.daily = false;
            this.aiims = false;
            this.getAllCustomized();
        }
        else if (event._value == "daily") {
            this.week = false;
            this.customized = false;
            this.daily = true;
            this.aiims = false;
            this.getAllDaily();
        }
        else {
            this.aiims = true;
            this.week = false;
            this.customized = false;
            this.daily = false;
            this.getAllAiims();
        }
    };
    Exam.prototype.getAllDaily = function () {
        var _this = this;
        this.examService.getWeekExams().subscribe(function (weekRes) {
            if (weekRes.data) {
                if (weekRes.data.completedExams.length > 0) {
                    _this.completedWeekExam = weekRes.data.completedExams;
                    var exam_1 = (_this.completedWeekExam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.examCompleted = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        else if (item.examResult.totalQuestions) {
                            item.numberOfQuestions = item.examResult.totalQuestions;
                        }
                        return item;
                    });
                    _this.completedWeekExam = exam_1;
                    //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.activeExams.length > 0) {
                    var activeWeekExam = [];
                    activeWeekExam = weekRes.data.activeExams;
                    var exam_2 = (activeWeekExam).map(function (item) {
                        item.examPending = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    activeWeekExam = exam_2;
                    _this.completedWeekExam = (activeWeekExam.length > 0) ? _this.completedWeekExam.concat(activeWeekExam) : _this.completedWeekExam;
                    //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.inactiveExams.length > 0) {
                    var pendingWeekExaminactive = [];
                    pendingWeekExaminactive = weekRes.data.inactiveExams;
                    var exam_3 = (pendingWeekExaminactive).map(function (item) {
                        item.inactiveExam = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    pendingWeekExaminactive = exam_3;
                    _this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? _this.completedWeekExam.concat(pendingWeekExaminactive) : _this.completedWeekExam;
                    // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.pendingExams.length > 0) {
                    var pendingWeekexam = [];
                    pendingWeekexam = weekRes.data.pendingExams;
                    var exam_4 = (pendingWeekexam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.examPending = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    pendingWeekexam = exam_4;
                    _this.completedWeekExam = (pendingWeekexam.length > 0) ? _this.completedWeekExam.concat(pendingWeekexam) : _this.completedWeekExam;
                }
                var exam = (_this.completedWeekExam).map(function (item, index) {
                    if (item.examType == "daily") {
                        item.dailyExam = true;
                        _this.dailyExams.push(item);
                    }
                    return item;
                });
                //this.completedWeekExam = exam;
                console.log("//////////////", _this.dailyExams);
                _this.dailyExams = _this.dailyExams.sort(function (a, b) { return new Date(b.examDate).getTime() - new Date(a.examDate).getTime(); });
                if (_this.dailyExams.length == 0) {
                    _this.daily = false;
                    _this.__auth.notificationInfo("There is no daily exams found!");
                }
            }
            else {
                _this.__auth.notificationInfo("There is no data found!");
            }
        });
    };
    Exam.prototype.getAllWeek = function () {
        var _this = this;
        this.examService.getWeekExams().subscribe(function (weekRes) {
            if (weekRes.data) {
                if (weekRes.data.completedExams.length > 0) {
                    _this.completedWeekExam = weekRes.data.completedExams;
                    var exam_5 = (_this.completedWeekExam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.examCompleted = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        else if (item.examResult.totalQuestions) {
                            item.numberOfQuestions = item.examResult.totalQuestions;
                        }
                        return item;
                    });
                    _this.completedWeekExam = exam_5;
                    //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.activeExams.length > 0) {
                    var activeWeekExam = [];
                    activeWeekExam = weekRes.data.activeExams;
                    var exam_6 = (activeWeekExam).map(function (item) {
                        item.examPending = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    activeWeekExam = exam_6;
                    _this.completedWeekExam = (activeWeekExam.length > 0) ? _this.completedWeekExam.concat(activeWeekExam) : _this.completedWeekExam;
                    //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.inactiveExams.length > 0) {
                    var pendingWeekExaminactive = [];
                    pendingWeekExaminactive = weekRes.data.inactiveExams;
                    var exam_7 = (pendingWeekExaminactive).map(function (item) {
                        item.inactiveExam = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    pendingWeekExaminactive = exam_7;
                    _this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? _this.completedWeekExam.concat(pendingWeekExaminactive) : _this.completedWeekExam;
                    // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.pendingExams.length > 0) {
                    var pendingWeekexam = [];
                    pendingWeekexam = weekRes.data.pendingExams;
                    var exam_8 = (pendingWeekexam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.examPending = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    pendingWeekexam = exam_8;
                    //pendingWeekexam = pendingWeekexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    _this.completedWeekExam = (pendingWeekexam.length > 0) ? _this.completedWeekExam.concat(pendingWeekexam) : _this.completedWeekExam;
                    _this.completedWeekExam = _this.completedWeekExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                }
                console.log(" this.completedWeekExam", _this.completedWeekExam);
                var exam = (_this.completedWeekExam).map(function (item, index) {
                    if (item.examType == "daily") {
                        //item.dailyExam=true;
                        // this.dailyExams.push(item);
                    }
                    else {
                        item.dailyExam = false;
                        _this.totalWeek.push(item);
                    }
                    return item;
                });
                console.log("this.totalWeek", _this.totalWeek);
                //this.completedWeekExam = exam;
                if (_this.totalWeek.length == 0) {
                    _this.week = false;
                    _this.__auth.notificationInfo("There is no week exams found!");
                }
            }
            else {
                _this.__auth.notificationInfo("There is no data found!");
            }
        });
    };
    Exam.prototype.resumeExam = function (exam) {
        var _this = this;
        console.log("exaaaaaaaaaam weeeeeeeeeeee", exam);
        if (!exam.examType) {
            if (exam.examId) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */], { subject: exam._id });
            }
            else {
                this.examService.startExam(exam._id).subscribe(function (res) {
                    var userExamId = res.data.userExamId;
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */], { subject: userExamId });
                });
            }
        }
        else if (exam.examType == "grand") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_exam_GrandExam_grantexam_component__["a" /* grantExam */], { subject: exam._id });
        }
        else if (exam.examType == "week" || exam.examType == "special" || exam.examType == "daily") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_weekend_TermsAndRules_terms_component__["a" /* termsrules */], { subject: (exam.examId) ? exam.examId : exam._id });
        }
        else if (exam.examType == "AIIMS") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_exam_createexam_aiimsExam_aiimsExam__["a" /* aiimsExam */], { subject: exam.examId, package: exam.packageId });
        }
        else if (exam.examType == "JIPMER") {
            console.log("exam.examId", exam.examId);
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_exam_createexam_aiimsExam_aiimsExam__["a" /* aiimsExam */], { subject: exam.examId, package: exam.packageId });
        }
    };
    Exam.prototype.getAllCustomized = function () {
        var _this = this;
        this.mode = 'completed';
        this.examService.fetchAllExam(this.mode).subscribe(function (res) {
            if (res.data.examList.length > 0) {
                _this.custmizedExam = res.data.examList;
                var exam = (_this.custmizedExam).map(function (item) {
                    item.customizedExam = true;
                    item.completed = true;
                    return item;
                });
                _this.custmizedExam = exam;
            }
            // this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            _this.mode = 'pending';
            _this.examService.fetchAllExam(_this.mode).subscribe(function (res) {
                var customizedPending = [];
                if (res.data.examList.length > 0) {
                    customizedPending = res.data.examList;
                    var exam = (customizedPending).map(function (item) {
                        item.customizedExam = true;
                        item.pending = true;
                        return item;
                    });
                    customizedPending = exam;
                    // customizedPending = customizedPending.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    _this.custmizedExam = (customizedPending.length > 0) ? _this.custmizedExam.concat(customizedPending) : _this.custmizedExam;
                }
                _this.examService.fetchGrandExam().subscribe(function (res) {
                    var completedGexam = [];
                    if (res.data.completedExam.length > 0) {
                        completedGexam = (res.data.completedExam).map(function (item) {
                            item.examType = "grand";
                            item.examName = item.name;
                            item.completed = true;
                            return item;
                        });
                        // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                        _this.custmizedExam = (completedGexam.length > 0) ? _this.custmizedExam.concat(completedGexam) : _this.custmizedExam;
                    }
                    if (res.data.pendingExams.length > 0) {
                        var pendingGexam = [];
                        pendingGexam = (res.data.pendingExams).map(function (item) {
                            item.examType = "grand";
                            item.examName = item.name;
                            item.pending = true;
                            return item;
                        });
                        //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                        _this.custmizedExam = (pendingGexam.length > 0) ? _this.custmizedExam.concat(pendingGexam) : _this.custmizedExam;
                    }
                    _this.custmizedExam = _this.custmizedExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    if (_this.custmizedExam.length == 0) {
                        _this.customized = false;
                        _this.__auth.notificationInfo("There is no data found!");
                    }
                });
            });
        });
    };
    Exam.prototype.AIIMSPAYROUTE = function () {
        this.__auth.notificationInfo("only for institutional users");
        // this.navCtrl.push(weekendReg)
    };
    Exam.prototype.writeExam = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_createexam_createExam__["a" /* CreateExam */]);
    };
    Exam = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-exam',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam.html"*/'\n\n<ion-header>\n  <ion-navbar>\n    <!-- <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button> -->\n    <ion-title>Exam Room</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding class="bgpic" >\n  <ion-segment [(ngModel)]="examTypes" color="primary" (ionChange)="segmentChanged($event)" >\n      \n      <ion-segment-button  value="customized" >\n     {{username}}\'s Exam\n      </ion-segment-button>\n      <ion-segment-button  value="week" >\n        Weekend Exam \n      </ion-segment-button>\n      <ion-segment-button value="daily" >\n         Daily Eaxm\n        </ion-segment-button>\n        <ion-segment-button value="aiims" >\n         AIIMS/JIPMER\n         </ion-segment-button>\n    </ion-segment>\n   \n    <ion-row>\n      <ion-col col-12  *ngIf="customized && !week && !daily && !aiims" >\n     <p  style=" font-size: 20px;\n         text-align: center;color: black;"> Be independant , <br>\n      Now create exam you want </p>\n    </ion-col>\n    <ion-col col-12   *ngIf="!customized && week && !daily && !aiims" >\n      <p style=" font-size: 20px;\n      text-align: center;color: black;"> Subjectwise exam , <br>\n     By \'nextfellow\' faculty </p>\n     </ion-col>\n     <ion-col col-12   *ngIf="!customized && !week && daily && !aiims" >\n      <p style=" font-size: 20px;\n      text-align: center;color: black;"> Daily exam , <br>\n     On high yielding topics </p>\n     </ion-col>\n     <ion-col col-12   *ngIf="!customized && !week && !daily && aiims" >\n      <p style=" font-size: 20px;\n      text-align: center;color: black;"> Handpicked by authors of Target AIIMS and Target JIPMER\n      \n   </p>\n     </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col col-12 class="strt-btn ">\n        <button class="strt-btn" ion-button outline (click)="writeExam()" *ngIf="customized && !week  && !daily && !aiims" >Start</button>\n      </ion-col>\n      <!-- <ion-col col-12 class="strt-btn " *ngIf="!customized && week && !daily && !aiims">\n        <button class="reg-btn" ion-button outline (click)="register(\'week\')">Pro users</button>\n      </ion-col>\n      <ion-col col-12 class="strt-btn " *ngIf="!customized && !week && daily && !aiims">\n          <button class="reg-btn" ion-button outline (click)="register(\'daily\')">Pro users</button>\n        </ion-col>\n        <ion-col col-12 class="strt-btn " *ngIf="!customized && !week && !daily && aiims">\n            <button class="reg-btn" ion-button outline (click)="register(\'aiims\')">Pro users</button>\n          </ion-col> -->\n    </ion-row>\n    <div *ngIf="customized && !week && !daily">\n        <div class="center" *ngIf="!custmizedExam">\n            <ion-spinner></ion-spinner>\n            \n          </div>\n    <ion-row>\n      <ion-col col-7>\n          <p style="font-size: 16px; text-align: left;"> {{username}}\'s Exam</p>\n      </ion-col>\n      <ion-col col-5>\n          <!-- <ion-select [(ngModel)]="selectedExam" (ionChange)="optionsFn($event);">\n              <ion-option value="all" style="font-size: 16px;" selected>All</ion-option>\n              <ion-option value="pending"  style="font-size: 16px;" >Pending Exams</ion-option>\n              <ion-option value="completed" style="font-size: 16px;">Completed Exams</ion-option>\n          </ion-select> -->\n         \n         <ion-row>\n           <ion-col col-10 style="padding: 10px;">\n          <span style=" font-size: 17px;">{{type}}</span>\n          </ion-col>\n          <ion-col col-2>\n          <ion-icon name="more" (click)="presentPopover(\'customized\')" style="color:#108fd2;\n          font-size: 26px;\n          font-weight: bold;"></ion-icon>\n</ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n   \n    <ion-card class="cdclr"  *ngFor="let item of custmizedExam    let i = index" (click)="toggleGroup(item)">\n        <ion-card-header>\n        <ion-col col-12>\n              {{item.examName | slice:0:30}}\n              \n              <ion-icon class="clr"   name="create"  *ngIf="item.pending " (click)="resumeExam(item)"></ion-icon>\n              <ion-icon class="clr" *ngIf="item.completed "   name="eye" (click)="viewResult(item)"></ion-icon>    \n        </ion-col>\n        </ion-card-header>\n        <div *ngIf="isGroupShown(item)">\n            \n          <p    style="padding-left: 20px;\n      font-size: 14px;" >   {{item.examName}}</p>\n          <p    style="padding-left: 20px;\n          font-size: 14px;" >{{item.createdDate | date: "yyyy/MM/dd"}}</p>\n          <p    style="padding-left: 20px;\n          font-size: 14px;" >{{item.numberOfQuestions||item.noOfQuestions}} No of questions</p>\n      </div>\n      </ion-card>\n     </div> \n     \n     <div *ngIf="week && !customized ">\n        <div class="center" *ngIf="!completedWeekExam">\n            <ion-spinner></ion-spinner>\n            \n          </div>\n        <ion-row>\n          <ion-col col-7>\n              <p style="font-size: 16px;float:left">Weekend Exam</p>\n  \n          </ion-col>\n          <ion-col col-5>\n             \n            \n          <ion-row>\n              <ion-col col-10 style="padding: 10px;">\n             <span style=" font-size: 17px;">{{type}}</span>\n             </ion-col>\n             <ion-col col-2>\n             <ion-icon name="more" (click)="presentPopover(\'week\')" style="color:#108fd2;\n             font-size: 26px;\n             font-weight: bold;"></ion-icon>\n </ion-col>\n           </ion-row>\n\n             \n          </ion-col>\n        </ion-row>\n      \n        <div *ngIf="!demoExam" >\n        <ion-card class="cdclr"  *ngFor="let items of totalWeek    let i = index" (click)="toggleGroup(items)">\n            <ion-card-header>\n            <ion-col col-12>\n                  <span *ngIf="items.examType!=\'daily\'"> {{items.examName | slice:0:30}}</span> \n\n                  <span *ngIf="items.examType==\'daily\'" style="color:#108fd2"> {{items.examName | slice:0:30}}</span> \n           \n                  <ion-icon class="clr"   name="create"  *ngIf="items.examPending " (click)="resumeExam(items)"></ion-icon>\n                  <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)"></ion-icon>  \n                  <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="newExamfunction(\'single\', items.examName, items)"></ion-icon> \n\n            </ion-col>\n            </ion-card-header>\n            <div *ngIf="isGroupShown(items) && items.examType!=\'daily\'">\n    \n              <p    style="padding-left: 20px;\n              font-size: 14px;" >   {{items.examName}}</p>\n              <p  style="padding-left: 20px;\n              font-size: 14px;" [innerHtml]="items.examinformation" *ngIf="items.examinformation"></p>\n              <p style="padding-left: 20px;\n         font-size: 14px;" >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n         <p    style="padding-left: 20px;\n      font-size: 14px;" *ngIf="items.numberOfQuestions " >{{items.numberOfQuestions}} No of questions</p>\n            </div>\n          </ion-card>\n          </div>\n\n         \n          <div class="center" *ngIf="!demoExams">\n            <ion-spinner></ion-spinner>\n            \n          </div>\n          <div *ngIf="demoExam" >\n            <ion-card class="cdclr"  *ngFor="let items of demoExams    let i = index" (click)="toggleGroup(items)">\n                <ion-card-header>\n                <ion-col col-12>\n                      {{items.examName | slice:0:30}}\n  \n                      <ion-icon class="clr"   name="create"  *ngIf="items.examPending " (click)="resumeExam(items)"></ion-icon>\n                      <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)"></ion-icon>  \n                      <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="newExamfunction(\'single\', items.examName, items)"></ion-icon> \n    \n                </ion-col>\n                </ion-card-header>\n                <div *ngIf="isGroupShown(items)">\n                  <p    style="padding-left: 20px;\n                  font-size: 14px;" >   {{items.examName}}</p>\n         \n                  <p [innerHtml]="items.examinformation" *ngIf="items.examinformation"></p>\n                  <p style="padding-left: 20px;\n                  font-size: 14px;" >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n                  <p    style="padding-left: 20px;\n               font-size: 14px;"  *ngIf="  items.numberOfQuestions  ">{{items.numberOfQuestions }} No of questions</p>\n               \n             \n              \n              </div>\n              </ion-card>\n              </div>\n         </div> \n\n\n\n\n\n         <div *ngIf="!week && !customized && daily ">\n          <div class="center" *ngIf="!completedWeekExam">\n              <ion-spinner></ion-spinner>\n              \n            </div>\n          <ion-row>\n            <ion-col col-7>\n                <p style="font-size: 16px;float:left">Daily Exam</p>\n    \n            </ion-col>\n            <ion-col col-5>\n               \n              \n            <ion-row>\n                <ion-col col-10 style="padding: 10px;">\n               <span style=" font-size: 17px;">{{type}}</span>\n               </ion-col>\n               <ion-col col-2>\n               <ion-icon name="more" (click)="presentPopover(\'daily\')" style="color:#108fd2;\n               font-size: 26px;\n               font-weight: bold;"></ion-icon>\n   </ion-col>\n             </ion-row>\n\n               \n            </ion-col>\n          </ion-row>\n        \n          <div *ngIf="!demoExam" >\n          <ion-card class="cdclr"  *ngFor="let items of dailyExams    let i = index" (click)="toggleGroup(items)">\n              <ion-card-header>\n              <ion-col col-12>\n                    <span > {{items.examName | slice:0:30}}</span> \n\n             \n                    <ion-icon class="clr"   name="create"  *ngIf="items.examPending " (click)="resumeExam(items)"></ion-icon>\n                    <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)"></ion-icon>  \n                    <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="newExamfunction(\'single\', items.examName, items)"></ion-icon> \n  \n              </ion-col>\n              </ion-card-header>\n              <div *ngIf="isGroupShown(items)" >\n      \n                <p    style="padding-left: 20px;\n                font-size: 14px;" >   {{items.examName}}</p>\n                <p  style="padding-left: 20px;\n                font-size: 14px;" [innerHtml]="items.examinformation" *ngIf="items.examinformation"></p>\n                <p style="padding-left: 20px;\n           font-size: 14px;" >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n           <p    style="padding-left: 20px;\n        font-size: 14px;" *ngIf="items.numberOfQuestions " >{{items.numberOfQuestions}} No of questions</p>\n              </div>\n            </ion-card>\n            </div>\n\n           \n            <div class="center" *ngIf="!demoExams">\n              <ion-spinner></ion-spinner>\n              \n            </div>\n            <div *ngIf="demoExam" >\n              <ion-card class="cdclr"  *ngFor="let items of demoExams    let i = index" (click)="toggleGroup(items)">\n                  <ion-card-header>\n                  <ion-col col-12>\n                        {{items.examName | slice:0:30}}\n    \n                        <ion-icon class="clr"   name="create"  *ngIf="items.examPending " (click)="resumeExam(items)"></ion-icon>\n                        <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)"></ion-icon>  \n                        <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="newExamfunction(\'single\', items.examName, items)"></ion-icon> \n      \n                  </ion-col>\n                  </ion-card-header>\n                  <div *ngIf="isGroupShown(items)">\n                    <p    style="padding-left: 20px;\n                    font-size: 14px;" >   {{items.examName}}</p>\n           \n                    <p [innerHtml]="items.examinformation" *ngIf="items.examinformation"></p>\n                    <p style="padding-left: 20px;\n                    font-size: 14px;" >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n                    <p    style="padding-left: 20px;\n                 font-size: 14px;" *ngIf="items.numberOfQuestions" >{{items.numberOfQuestions}} No of questions</p>\n                  </div>\n                </ion-card>\n                </div>\n           </div> \n    <div *ngIf="!customized && !week && !daily && aiims">\n            \n      <ion-row>\n        <ion-col col-7>\n            <p style="font-size: 16px;float:left">AIIMS/JIPMER</p>\n\n        </ion-col>\n        <ion-col col-5>\n           \n          \n        <ion-row>\n            <ion-col col-10 style="padding: 10px;">\n           <span style=" font-size: 17px;">{{type}}</span>\n           </ion-col>\n           <ion-col col-2>\n           <ion-icon name="more" (click)="presentPopover(\'aiims\')" style="color:#108fd2;\n           font-size: 26px;\n           font-weight: bold;"></ion-icon>\n</ion-col>\n         </ion-row>\n\n           \n        </ion-col>\n      </ion-row>\n    \n      <div *ngIf="!aiimsdemoExam " >\n      <ion-card class="cdclr"  *ngFor="let items of totalAiims    let i = index" (click)="toggleGroup(items)">\n          <ion-card-header>\n          <ion-col col-12>\n                <span > {{items.examName | slice:0:30}}</span> \n\n         \n                <ion-icon class="clr"   name="create"  *ngIf="items.examPending " (click)="resumeExam(items)"></ion-icon>\n                <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)"></ion-icon>  \n                <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="AIIMSPAYROUTE(\'single\', items.examName, items)"></ion-icon> \n\n          </ion-col>\n          </ion-card-header>\n          <div *ngIf="isGroupShown(items)">\n  \n            <p    style="padding-left: 20px;\n            font-size: 14px;" >   {{items.examName}}</p>\n            <p  style="padding-left: 20px;\n            font-size: 14px;"  *ngIf="items.examinformation">{{items.examinformation}}</p>\n            <p style="padding-left: 20px;\n       font-size: 14px;"*ngIf="items.examDate || items.examDetails.examDate"  >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n       <p    style="padding-left: 20px;\n    font-size: 14px;" *ngIf="items.numberOfQuestions " >{{items.numberOfQuestions}} No of questions</p>\n          </div>\n        </ion-card>\n        </div>\n\n       \n       \n        <div *ngIf="aiimsdemoExam" >\n          <ion-card class="cdclr"  *ngFor="let items of demoExams    let i = index" (click)="toggleGroup(items)">\n              <ion-card-header>\n              <ion-col col-12>\n                    {{items.examName | slice:0:30}}\n\n                    <ion-icon class="clr"   name="create"  *ngIf="items.examPending " (click)="resumeExam(items)"></ion-icon>\n                    <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)"></ion-icon>  \n                    <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="AIIMSPAYROUTE()"></ion-icon> \n  \n              </ion-col>\n              </ion-card-header>\n              <div *ngIf="isGroupShown(items)">\n                <p    style="padding-left: 20px;\n                font-size: 14px;" >   {{items.examName}}</p>\n       \n                <p  *ngIf="items.examinformation">{{items.examinformation}}</p>\n                <p style="padding-left: 20px;\n                font-size: 14px;" >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n                <p    style="padding-left: 20px;\n             font-size: 14px;"  *ngIf="  items.numberOfQuestions  ">{{items.numberOfQuestions }} No of questions</p>\n             \n           \n            \n            </div>\n            </ion-card>\n         \n       \n         </div> \n         </div>\n         \n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]])
    ], Exam);
    return Exam;
}());

//# sourceMappingURL=exam.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return aiimsResult; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_question_service__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_exam_examNew_examComponent__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var aiimsResult = /** @class */ (function () {
    function aiimsResult(questionService, events, sanitizer, navCtrl, __auth, toastCtrl, platform, navParams, examService) {
        this.questionService = questionService;
        this.events = events;
        this.sanitizer = sanitizer;
        this.navCtrl = navCtrl;
        this.__auth = __auth;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.examService = examService;
        this.QstnDiv = false;
        this.currentIndexToShow = 0;
        this.questionArray = [];
        this.url = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].backendUrl + '/images/';
        this.scoreDiv = false;
        this.imageFlag = false;
        this.noQuestion = false;
        this.noPrevQuestion = false;
        this.showLeaderBoard = false;
        this.indexArray = [];
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;
        this.countClick = 0;
        this.listview = false;
        this.listClr = false;
        this.urlRead = navParams.get('ExamIds');
        console.log("urlread", this.urlRead);
        this.events.publish('user:created', true, Date.now());
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    }
    aiimsResult.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    aiimsResult.prototype.ionViewWillLeave = function () {
        this.tabBarElement.style.display = 'flex';
    };
    aiimsResult.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    aiimsResult.prototype.ngOnInit = function () {
        this.tabBarElement.style.display = 'none';
        this.fields = this.urlRead.split('/');
        this.examId = this.fields[0];
        this.userExamId = this.fields[1];
        this.examType = this.fields[2];
        this.getResultScore();
        this.correctClr = true;
        this.wrongClr = false;
        this.unutendedClr = false;
        this.listview = false;
        this.getQuiestions("CORRECT");
    };
    aiimsResult.prototype.getQuiestions = function (category) {
        var _this = this;
        this.listview = false;
        this.listClr = false;
        this.resultType = category;
        if (category == "CORRECT") {
            this.correctClr = true;
            this.wrongClr = false;
            this.unutendedClr = false;
        }
        else if (category == "WRONG") {
            this.correctClr = false;
            this.wrongClr = true;
            this.unutendedClr = false;
        }
        else {
            this.correctClr = false;
            this.wrongClr = false;
            this.unutendedClr = true;
        }
        this.scoreDiv = false;
        this.showLeaderBoard = false;
        this.currentIndexToShow = 0;
        this.questionArray = [];
        //this.answerArray = [];
        this.examService.getResultAiims(this.userExamId, this.examId, this.examType, category).subscribe(function (res) {
            console.log("res subcribed exams", res);
            if (res.data.length == 0) {
                console.log("noo", res.data.length);
                _this.QstnDiv = false;
                _this.__auth.notificationInfo(res.message);
            }
            else {
                console.log("here", res.data.length);
                _this.noPrevQuestion = true;
                _this.noQuestion = false;
                _this.QstnDiv = true;
                _this.questionArray = res.data.question;
                _this.answerData = res.data.answer;
                if (category == "WRONG") {
                    // this.answerData = res.data.answer[this.currentIndexToShow];
                    _this.userAnswer = _this.answerData[_this.currentIndexToShow].answer;
                }
                else {
                    _this.userAnswer = null;
                }
                _this.qstnName = _this.questionArray[_this.currentIndexToShow].question;
                (_this.qstnName !== null) ? _this.Qnumber = 1 : _this.Qnumber = 0;
                _this.options = JSON.parse(_this.questionArray[_this.currentIndexToShow].options);
                _this.description = _this.questionArray[_this.currentIndexToShow].description;
                _this.correctAnswer = _this.questionArray[_this.currentIndexToShow].answer;
                if (_this.questionArray[_this.currentIndexToShow].question_image) {
                    _this.imageFlag = true;
                    _this.imgUrl = _this.questionArray[_this.currentIndexToShow].question_image;
                }
                else {
                    _this.imageFlag = false;
                }
                if (_this.questionArray.length == 1) {
                    _this.noQuestion = true;
                    _this.noPrevQuestion = true;
                }
            }
        });
    };
    aiimsResult.prototype.previousButtonClick = function () {
        this.noQuestion = false;
        this.currentIndexToShow--;
        if (this.currentIndexToShow > 0) {
            this.noPrevQuestion = false;
            if (this.resultType == "WRONG") {
                this.userAnswer = this.answerData[this.currentIndexToShow].answer;
            }
            else {
                this.userAnswer = null;
            }
            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
        else {
            this.noPrevQuestion = true;
            if (this.answerData) {
                this.userAnswer = this.answerData.answer;
            }
            else {
                this.userAnswer = null;
            }
            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
    };
    aiimsResult.prototype.nextButtonClick = function () {
        this.noPrevQuestion = false;
        this.currentIndexToShow++;
        if (this.currentIndexToShow == this.questionArray.length - 1) {
            this.noQuestion = true;
            if (this.resultType == "WRONG") {
                this.userAnswer = this.answerData[this.currentIndexToShow].answer;
            }
            else {
                this.userAnswer = null;
            }
            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;
            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
        else {
            this.noQuestion = false;
            if (this.answerData) {
                this.userAnswer = this.answerData.answer;
            }
            else {
                this.userAnswer = null;
            }
            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;
            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
    };
    aiimsResult.prototype.getScore = function () {
        this.listview = false;
        this.scoreDiv = true;
        this.showLeaderBoard = false;
        this.QstnDiv = false;
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;
    };
    aiimsResult.prototype.toggleDetails = function (item) {
        if (this.isGroupShown(item)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = item;
        }
    };
    aiimsResult.prototype.isGroupShown = function (item) {
        return this.shownGroup === item;
    };
    aiimsResult.prototype.getResultScore = function () {
        var _this = this;
        this.examService.getResultScoreAiims(this.examId).subscribe(function (res) {
            if (res.data) {
                _this.rankData = res.data.totalResult;
                _this.totalUsers = res.data.totalResult.length;
                _this.userResult = res.data.userResult[0];
                _this.examName = _this.userResult.examDetails.name;
                _this.totalQuestion = _this.userResult.examResult.total;
                _this.correct = _this.userResult.examResult.correct;
                _this.wrong = _this.userResult.examResult.wrong;
                _this.unanswered = _this.userResult.examResult.unanswered;
                var Nscore = _this.userResult.examResult.score;
                _this.score = String(Nscore).substr(0, 4);
                if (_this.examType == "JIPMER")
                    _this.totalMark = _this.userResult.examResult.total * 4;
                else
                    _this.totalMark = _this.userResult.examResult.total * 1;
                for (var k = 0; k < _this.rankData.length; k++) {
                    _this.indexArray.push({ "score": String(_this.rankData[k].examResult.score).substr(0, 4), "name": _this.rankData[k].userDetails.name });
                }
                _this.indexArray.sort(function (a, b) {
                    return b.score - a.score;
                });
                var rank = 1;
                for (var i = 0; i < _this.indexArray.length; i++) {
                    if (i > 0 && _this.indexArray[i].score < _this.indexArray[i - 1].score) {
                        rank++;
                    }
                    _this.indexArray[i].rank = rank;
                }
                _this.userRank = _this.rankData.findIndex(function (item) {
                    return item.userId == _this.userId;
                });
                _this.topScorers = _this.rankData.slice(0, 9);
            }
        });
    };
    aiimsResult.prototype.showLeaderborad = function () {
        this.listClr = false;
        this.listview = false;
        this.showLeaderBoard = true;
        this.scoreDiv = false;
        this.QstnDiv = false;
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;
    };
    aiimsResult.prototype.backToCompletedPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    aiimsResult.prototype.listView = function () {
        var _this = this;
        this.countClick++;
        if (this.countClick % 2 == 0) {
            this.listview = false;
            this.listClr = false;
            this.QstnDiv = true;
        }
        else {
            this.listview = true;
            this.listClr = true;
            this.QstnDiv = false;
            this.listviewData = [];
            this.examService.getResultAiims(this.userExamId, this.examId, this.examType, this.resultType).subscribe(function (res) {
                console.log("res subcribed exams", res);
                if (!res.status) {
                }
                else {
                    console.log("this.resultType", _this.resultType);
                    _this.listviewData = res.data.question;
                    var data = (_this.listviewData).map(function (item) {
                        item.options = JSON.parse(item.options);
                        return item;
                    });
                    if (data) {
                        _this.listviewData = data;
                        console.log("listdata", _this.listviewData);
                    }
                }
            });
        }
    };
    aiimsResult = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-aiimsResult',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam-room/aiimsResult/aiimsResult.html"*/'<style>\n    .crdmd {\n        margin-left: -16px !important;\n        border-radius: 2px!important;\n        width: calc(100% - -30px)!important;\n    }\n\n    .optionStyle .label-md {\n        margin: 0px 0px 13px 0;\n    }\n\n\n    .item-md.item-block .item-inner {\n        padding-right: 2px;\n        border-bottom: 1px solid #dedede;\n    }\n</style>\n\n<ion-header>\n    <ion-toolbar>\n\n        <ion-icon name="md-arrow-back" style="font-size: 21px;\n                  margin-left: 8px;\n                  color: #fff;" (click)="backToCompletedPage()"></ion-icon>\n        &nbsp;&nbsp;\n        <span style="font-size: 16px;color: #fff;"  *ngIf="userResult">{{examName}}</span>\n\n    </ion-toolbar>\n</ion-header>\n<ion-content padding #contentRef  class="bgpic">\n    <ion-card style="height:33px;\n    margin-left: 0px;" class="crdmd ">\n    <ion-slides [slidesPerView]="2" >\n\n        <ion-slide class="blocks" [ngClass]="(scoreDiv)?\'selected\':\'unselected\'" (click)="getScore()">\n            <span style="font-size:18px; text-align: center;">Score &rarr;</span>\n            &nbsp;\n            <span style="font-size:18px;text-align: center; ">{{score}}</span>\n\n        </ion-slide>\n        <ion-slide [ngClass]="(correctClr)?\'selected\':\'unselected\'" id="correctButton" (click)="getQuiestions(\'CORRECT\')">\n            <span style="font-size:18px; text-align: center;">Correct &rarr;</span>\n           \n            <span style="font-size:18px; text-align: center;">{{correct}}</span>\n        </ion-slide>\n\n        <ion-slide [ngClass]="(wrongClr)?\'selected\':\'unselected\'" (click)="getQuiestions(\'WRONG\')">\n            <span style="font-size:18px; text-align: center;">Wrong &rarr;</span>\n           \n            <span style="font-size:18px; text-align: center;">{{wrong}}</span>\n        </ion-slide>\n        <ion-slide [ngClass]="(unutendedClr)?\'selected\':\'unselected\'" (click)="getQuiestions(\'UNANSWERED\')">\n            <span style="font-size:18px; text-align: center;">Unattended &rarr;</span>\n          \n            <span style="font-size:18px; text-align: center;">{{unanswered}}</span>\n        </ion-slide>\n        <ion-slide class="blocks"  [ngClass]="(showLeaderBoard)?\'selected\':\'unselected\'" (click)=showLeaderborad()>\n            <span style="font-size:18px; text-align: center;">Leaderboard &rarr;</span>\n           \n            <span style="font-size:18px; text-align: center;">{{totalUsers}}</span>\n\n        </ion-slide>\n\n\n    </ion-slides>\n</ion-card>\n\n\n<div *ngIf="QstnDiv">\n        <ion-card style="margin-top:7px;width: 100%;margin-left: 0px; padding-left:-1px;">\n            <ion-card-content style=" padding: 0px 0px 0px 0px; padding: 13px 3px;" style="background-color:#FFFEEE;margin-left:-13px!important; margin-right:-13px!important;">\n                <ion-item-group>\n\n                    <ion-item-divider text-wrap class="questionHead" style="padding:0px;background-color:#FFFEEE">\n                        <ion-grid style="padding-bottom:0px !important;">\n                            <ion-row>\n                                <ion-col col-2 style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:18px; padding-left: 0px;">\n                                    {{Qnumber}}\n                                </ion-col>\n                                <ion-col col-10 style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                                    <p style="font-size: 18px;" [innerHtml]="qstnName"> </p>\n\n\n                                </ion-col>\n                            </ion-row>\n                        </ion-grid>\n                        <div class="col-sm-12 bdr" *ngIf="imageFlag">\n\n                            <br>\n                            <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + imgUrl\'>\n                            <br>\n                        </div>\n                    </ion-item-divider>\n\n\n\n\n                    <div style="padding:0px!important;border-bottom:none!important;">\n                        <ion-list *ngFor="let option of options ;let i= index;">\n\n                            <ion-item text-wrap class="optionStyle" style="font-weight: unset!important;" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important; "\n                                class="optionStyle" style="font-weight: unset!important;" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important; ">\n                                <div [ngClass]="{\'correct\':correctAnswer===option.id,\'wrong\':userAnswer===option.id}">\n\n\n                                    <ion-row>\n\n                                        <!-- <ion-row  > -->\n                                        <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;; margin-right: 0px;line-height:23px;">{{option.id}}.</ion-col>\n                                        <ion-col col-10 style="padding-left:5px!important;margin-left: 0px;line-height:22px; line-height:23px;font-size:18px;">\n                                            <span> {{option.text}}</span>\n                                        </ion-col>\n                                    </ion-row>\n\n                                </div>\n\n\n                            </ion-item>\n                        </ion-list>\n                    </div>\n\n                </ion-item-group>\n\n\n\n\n\n                <ion-row style="margin-top:10px;">\n                    <ion-col col-4>\n                        <button float-center ion-button color="#108fd2" style="background-color:#108fd2;" icon-left (click)="previousButtonClick()"\n                            [disabled]=noPrevQuestion>Previous</button>\n\n                    </ion-col>\n                    <ion-col col-4>\n\n                    </ion-col>\n                    <ion-col col-4>\n                        <!-- <button class="btn btn-success col-md-3 btnSize exampages " (click)="mark((questionPalette[currentPaletteId].status == \'marked\') ? \'unmarked\': \'marked\')">{{(questionPalette[currentPaletteId].status == \'marked\') ? \'Unmark\': \'Mark\'}}</button> -->\n\n\n                        <button float-center ion-button style="background-color:#108fd2;color:white; padding: 0px 30px 0px 30px;" (click)="nextButtonClick()"\n                            [disabled]=noQuestion>Next</button>\n\n                    </ion-col>\n                </ion-row>\n            </ion-card-content>\n        </ion-card>\n        <div *ngIf="description" style="background-color:#FFFEEE;">\n\n            <div style="font-weight:bold; font-size:18px;padding-left:10px;padding-top:8px;">Description</div>\n           \n            <!-- <ion-scroll scrollX="true" class="dec"> -->\n                <p class="scrollbar" [innerHTML]="sanitizer.bypassSecurityTrustHtml(description)" style="line-height: 20px;font-size:18px;padding-left:10px;padding-left:10px; padding-bottom:5px; "></p>\n            <!-- </ion-scroll> -->\n\n\n        </div>\n    </div>\n    <ion-grid *ngIf="scoreDiv" style="padding-top: 121px;">\n            <ion-row>\n                <ion-col>\n                    <h2 style="text-align: center;\n                       font-size: 30px;">Total</h2>\n                    <!-- <h2 style="text-align: center;font-size: 29px;">\n                        <b style="font-size: 42px;\n                          color: green; ">{{score}}</b> /{{totalMark}}</h2> -->\n                          <!-- <ion-badge item-end  class="badg">{{score}}/{{totalMark}}</ion-badge> -->\n                          <ion-row>\n                            <ion-col col-12>\n                                <ion-badge item-end  class="badg">{{score}}/{{totalMark}}</ion-badge>\n\n                            </ion-col>\n                           \n                        </ion-row>\n                        </ion-col>\n            </ion-row>\n    \n        </ion-grid>\n        <ion-grid *ngIf="showLeaderBoard">\n               \n                    <ion-row class="tableRow">\n                        <ion-col col-6>\n                            Name\n                        </ion-col>\n                        <ion-col col-3>\n                            Rank\n                        </ion-col>\n                        <ion-col col-3>\n                            Mark\n                        </ion-col>\n                    </ion-row>\n                    <ion-row *ngFor="let item of indexArray; let i = index;" class="tableColmn">\n                        <ion-col col-6>\n                            {{item.name}}\n                        </ion-col>\n                        <ion-col col-3 ng>\n                            {{item.rank}}\n                        </ion-col>\n                        <ion-col col-3>\n                            {{item.score}}\n                        </ion-col>\n        \n        \n                    </ion-row>\n                \n            </ion-grid>\n            <div *ngIf="listview">\n                                   <div *ngFor="let item of listviewData;let i=index;">\n                                       <ion-row class="qsnHead">\n                                           <ion-grid (click)="toggleDetails(item);" [ngClass]="{active: isGroupShown(item)}">\n                                          <ion-row>\n                                                  <ion-col col-2 style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:26px; padding-left: 0px;">\n                                                       <span>{{i+1}}.</span>\n                \n                \n                \n                \n                \n                \n                \n                \n                \n                                                    </ion-col>\n                                                   <ion-col col-10 style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                                                        <span [innerHtml]="item.question"></span>\n                                                    </ion-col>\n                                                </ion-row>\n                                           </ion-grid>\n                      \n                                       </ion-row>\n                                    <div *ngIf="isGroupShown(item)" style="background-color: #fffeee">\n                           \n                                           <div class="col-sm-12 bdr" *ngIf="imageFlag">\n                                               <br>\n                                       <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + item.question_image\'>\n                                               <br>\n                                           </div>\n                           \n                                        <div *ngFor="let option of item.options">\n                                               <ion-grid style="padding-bottom: 0px;\n                                               padding-top: 0px;">\n                                                   <ion-row style=" margin:0!important;border-bottom:none;" [ngClass]="{\'correct\':item.answer===option.id,\'wrong\':item.userAnswer===option.id}">\n                                                       <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3px !important;max-width: 13.76667%;">{{option.id}}.</ion-col>\n                                                       <ion-col col-10 style="padding-left:5px!important;margin-left: 0px;line-height:23px; line-height:23px;font-size:18px;" [innerHtml]="option.text"></ion-col>\n                                                   </ion-row>\n                                               </ion-grid>\n                                           </div>\n                        \n                                           <ion-row>\n                                                <ion-col col-12>\n                                                   <p style="font-weight:bold; font-size:18px;padding-left:10px;padding-top:8px;">Description:</p>\n                                                   <!-- <ion-scroll scrollX="true" scrollY="true" class="dec" style="min-height:200px;height:auto;"> -->\n                                                       <p class="scrollbar" [innerHTML]="sanitizer.bypassSecurityTrustHtml(item.description)" style="line-height: 20px;font-size:18px;"></p>\n                                                   <!-- </ion-scroll> -->\n                                               </ion-col>\n                                           </ion-row>\n                        \n                                        </div>\n                                   </div>\n                                </div>\n</ion-content>\n<ion-footer style="padding: 0px;" *ngIf="!scoreDiv">\n                  <div class="bar bar-footer bar-assertive" >\n                          <ion-grid style="padding-left: 0px;padding-right: 0px; padding-top: 0px;padding-bottom:0px;">\n                               <ion-col col-12 style="background-color:#cecece; ">\n                                   <ion-row>\n                                       <!-- <ion-col col-4 style="font-size:20px;text-align:center;">\n                   \n                                           <ion-icon ios="ios-bookmark" md="md-bookmark" [ngClass]="{\'starred\': star}"></ion-icon>\n                                       </ion-col>\n                                    <ion-col col-4 style="font-size:20px;text-align:center;">\n                                           <ion-icon class="fabbb" ios="ios-clipboard" (click)="addNotes()" md="md-clipboard">\n                                           </ion-icon>\n                                       </ion-col> -->\n                                       <!-- <ion-icon class="fabbb" *ngIf="!listview && !listStarView" ios="ios-clipboard" (click)="editNote()"\n                                    md="md-clipboard">\n                                       <ion-icon style="padding-top: 8px;" id="notifications-badge" ios="ios-checkmark" md="md-checkmark"></ion-icon>\n                                     </ion-icon> -->\n                                       <ion-col col-4 style="font-size:20px;text-align:center;float:right;">\n                                           <ion-icon [ngClass]="(listClr)?\'blueIcon\':\'icn\'" ios="ios-list" md="md-list" (click)="listView()"></ion-icon>\n                                    </ion-col>\n                                </ion-row>\n                               </ion-col>\n                        </ion-grid>\n                       </div>\n                   </ion-footer>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam-room/aiimsResult/aiimsResult.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5__services_question_service__["a" /* QuestionService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__services_question_service__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */]])
    ], aiimsResult);
    return aiimsResult;
}());

//# sourceMappingURL=aiimsResult.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Evaluate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_evaluate_evaluateOver_evaluateOver__ = __webpack_require__(446);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_evaluate_examOver_examOver__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_evaluate_learnOver_learnOver__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_dashboard_home__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Evaluate = /** @class */ (function () {
    function Evaluate(navCtrl, platform) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_4__pages_evaluate_learnOver_learnOver__["a" /* LearnOver */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__pages_evaluate_evaluateOver_evaluateOver__["a" /* EvaluateOver */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__pages_evaluate_examOver_examOver__["a" /* ExamOver */];
    }
    Evaluate.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    Evaluate.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_dashboard_home__["a" /* HomePage */]);
    };
    Evaluate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eval',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/evaluate.html"*/'<ion-header>\n        <ion-navbar>\n          <!-- <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n          </button> -->\n          <ion-title>Evaluate</ion-title>\n        </ion-navbar>\n      </ion-header>\n\n      <ion-content padding >\n\n        <img src="assets/img/evaluateImg.png" alt="evaluate" style="border: none; width: 100%;height: 104%;" >\n        <!-- <ion-tabs tabsPlacement="top">\n          <ion-tab [root]="tab1Root" tabTitle="LearnOverView" tabIcon="ios-book"></ion-tab>\n          <ion-tab [root]="tab2Root" tabTitle="Evaluate" tabIcon="ios-calendar-outline"></ion-tab>\n          <ion-tab [root]="tab3Root" tabTitle="ExamOverView" tabIcon="md-git-network"></ion-tab>\n          \n      </ion-tabs> -->\n      </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/evaluate.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], Evaluate);
    return Evaluate;
}());

//# sourceMappingURL=evaluate.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return homeNew; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_note_note__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_learn_list__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_exam_examNew_examComponent__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__commen_setting_setting__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_navBarService__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var homeNew = /** @class */ (function () {
    function homeNew(navCtrl, events, platform, navParams, _navBar) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.events = events;
        this.platform = platform;
        this.navParams = navParams;
        this._navBar = _navBar;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_4__pages_dashboard_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__pages_learn_list__["a" /* ListPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__pages_exam_examNew_examComponent__["a" /* examComponent */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_2__pages_note_note__["a" /* Note */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_6__commen_setting_setting__["a" /* Setting */];
        this.user = false;
        this.hideTabs = false;
        this.selectedTabIndex = 0;
        events.subscribe('user:created', function (user, time) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            console.log('Welcome', user, 'at', time);
            _this.hideTabs = user;
            // alert(user);
        });
        this.navBarHide = this._navBar.localNavbar;
        //this.selectedTabIndex = JSON.parse(localStorage.getItem('indexdata'));
    }
    homeNew.prototype.tabChanged = function (ev) {
        console.log("evvv", ev);
    };
    homeNew.prototype.indexPass = function (index) {
        this.indexValue = index;
        console.log(this.indexValue, "vlaue here");
        //localStorage.setItem('indexdata', JSON.stringify(this.indexValue));  
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('myTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Tabs */])
    ], homeNew.prototype, "tabRef", void 0);
    homeNew = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-homeNew',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/homeNew/homeNew.html"*/'<!-- <ion-header>\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Dashbord</ion-title>\n    </ion-navbar>\n  </ion-header> -->\n\n  <ion-content padding >\n\n <ion-tabs tabsPlacement="bottom" [ngClass]="{\'hidden\': hideTabs}" [selectedIndex]="selectedTabIndex" (ionChange)="tabChanged($event)" >\n      <ion-tab  [tab-hidden]="" [root]="tab1Root"  tabTitle="Home" tabIcon="ios-home"></ion-tab>\n      <ion-tab  [tab-hidden]="navBarHide" [root]="tab2Root" tabTitle="Learn" tabIcon="ios-book"></ion-tab>\n      <ion-tab [tab-hidden]="navBarHide" [root]="tab3Root" tabTitle="Exam" tabIcon="ios-create"></ion-tab>\n      <ion-tab  [tab-hidden]="navBarHide" [root]="tab4Root" tabTitle="Note" tabIcon="bookmarks"></ion-tab>\n      <ion-tab [tab-hidden]="navBarHide" [root]="tab5Root" tabTitle="More" tabIcon="menu"></ion-tab>\n\n  </ion-tabs>\n  </ion-content>ß'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/homeNew/homeNew.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_7__services_navBarService__["a" /* naveBarShowSrvice */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_7__services_navBarService__["a" /* naveBarShowSrvice */]])
    ], homeNew);
    return homeNew;
}());

//# sourceMappingURL=homeNew.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return aiimsExam; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_exam_exam_room_aiimsResult_aiimsResult__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//declare var jQuery: any;
var aiimsExam = /** @class */ (function () {
    function aiimsExam(navCtrl, events, alertCtrl, platform, __auth, navParams, examService) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.__auth = __auth;
        this.navParams = navParams;
        this.examService = examService;
        this.zeroTime = false;
        this.buttonName = false;
        this.paused = false;
        this.resumed = false;
        this.marked = false;
        this.buttonToggle = false;
        this.questionPalette = [];
        this.currentPaletteId = 0;
        this.options = [];
        this.subjectArray = [];
        this.url = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].backendUrl + '/images/';
        this.paletteStatus = [];
        this.timer = false;
        this.testObject = [];
        this.countClick = 0;
        this.events.publish('user:created', true, Date.now());
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        if (this.tabBarElement) {
            this.tabBarElement.style.display = 'none';
        }
        this.userExamId = navParams.get('subject');
        this.packageId = navParams.get('package');
        console.log(" this.userExamId", this.userExamId);
    }
    aiimsExam.prototype.ngOnInit = function () {
        var _this = this;
        this.examService.loadPackageExams(this.userExamId, this.packageId).subscribe(function (res) {
            console.log("res....exam", res);
            var examDetails = res.data;
            var count = 1;
            _this.examId = examDetails.examId;
            console.log("exam details", examDetails);
            _this.examType = examDetails.examDetails.examType;
            _this.userExamId = examDetails.userExamId;
            console.log(" this.userExamId ", _this.userExamId);
            _this.questionPalette = (examDetails.questionPalette).filter(function (item) {
                item.number = count;
                count++;
                return item;
            });
            console.log("this.questionPalette", _this.questionPalette);
            _this.qPallete = _this.questionPalette;
            _this.timeFlag = examDetails.examDetails.timeFlag;
            if (examDetails.examDetails.timeFlag == true) {
                _this.timerObj = examDetails.examDetails.timeObject;
                _this.testObject = JSON.parse(localStorage.getItem('testObject'));
                if (_this.testObject) {
                    _this.testObject.map(function (item, i) {
                        if (item.userExamId == _this.userExamId) {
                            _this.timerIndex = i;
                            _this.previousTimer = _this.testObject[_this.timerIndex];
                        }
                        ;
                    });
                }
                else {
                    _this.testObject = [];
                    _this.previousTimer = null;
                }
                if (_this.previousTimer == null || !_this.previousTimer) {
                    _this.time = new Date(2017, 1, 1, _this.timerObj.hour, _this.timerObj.min, 0);
                    _this.timer = true;
                }
                else {
                    _this.time = new Date(2017, 1, 1, _this.previousTimer.hours, _this.previousTimer.minutes, _this.previousTimer.seconds);
                    _this.timer = true;
                }
                _this._timerTick();
            }
            _this.getQuestion(_this.currentPaletteId);
        });
    };
    aiimsExam.prototype._timerTick = function () {
        var _this = this;
        this.time.setSeconds(this.time.getSeconds(), -1);
        var timerObject = {
            hours: this.time.getHours(),
            minutes: this.time.getMinutes(),
            seconds: this.time.getSeconds(),
            userExamId: this.userExamId
        };
        if (this.timerIndex)
            this.testObject[this.timerIndex] = timerObject;
        else {
            this.testObject.push(timerObject);
            this.testObject.map(function (item, i) {
                if (item.userExamId == _this.userExamId)
                    _this.timerIndex = i;
            });
        }
        localStorage.setItem('testObject', JSON.stringify(this.testObject));
        if (timerObject.hours === 0 && timerObject.minutes === 5 && timerObject.seconds === 0) {
            this.__auth.notificationInfo('5 Minutes Remaining');
        }
        if (timerObject.hours === 0 && timerObject.minutes === 0 && timerObject.seconds === 0) {
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
            this.statusCount();
            this.finishExam("");
        }
        this.x = setTimeout(function () { return _this._timerTick(); }, 1000);
    };
    aiimsExam.prototype.playPause = function () {
        var _this = this;
        if (this.buttonToggle) {
            this.buttonName = false;
            this.pauseTitle = "Pause Timer";
            if (this.pauseTime) {
                this.playTime = this.pauseTime;
            }
            else {
                this.playTime = this.time;
            }
            this.paused = false;
            this.resumed = true;
            this.playTime.setSeconds(this.playTime.getSeconds(), -1);
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 5 && this.playTime.getSeconds() === 0) {
                this.__auth.notificationInfo('5 Minutes Remaining');
            }
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 0 && this.playTime.getSeconds() === 0) {
                window.clearInterval(this.x);
                localStorage.removeItem('timerObject');
                this.finishExam("");
                this.statusCount();
            }
            var x = setTimeout(function () { return _this._timerTick(); }, 1000);
        }
        else {
            this.buttonName = true;
            this.playTitle = "Resume Timer";
            this.pauseTime = this.time;
            this.resumed = false;
            this.paused = true;
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
        }
        this.buttonToggle = !this.buttonToggle;
    };
    aiimsExam.prototype.getQuestion = function (index) {
        var _this = this;
        this.currentPaletteId = index;
        console.log("this.questionPalette[this.currentPaletteId]", this.questionPalette[this.currentPaletteId]);
        this.questionId = this.questionPalette[this.currentPaletteId]._id;
        console.log("this.questionId///////////", this.questionId);
        console.log("this.userExamId//////", this.userExamId);
        this.examService.getAiimsQuestions(this.userExamId, this.questionId)
            .subscribe(function (res) {
            console.log("aiims", res);
            if (!res.status)
                _this.__auth.notificationInfo("OOPS! Something went wrong.");
            else {
                _this.questionData = (res.data.question[0]) ? res.data.question[0] : {};
                _this.answerData = (res.data.userAnswer[0]) ? res.data.userAnswer[0] : {};
                console.log("this.questionData", _this.questionData);
                console.log("this.questionPalette", _this.questionPalette);
                _this.question = _this.questionData.question;
                _this.options = JSON.parse(_this.questionData.options);
                _this.slNumber = _this.questionPalette[_this.currentPaletteId].number;
                _this.subjectArray = _this.questionData.subjectArray;
                if (_this.questionData.question_image) {
                    _this.imageFlag = true;
                    _this.imageUrl = _this.url + _this.questionData.question_image;
                }
                else {
                    _this.imageFlag = false;
                }
                if (_this.answerData.answer) {
                    _this.answered = true;
                    _this.clickedItem = _this.answerData.answer;
                }
                else {
                    _this.answered = false;
                    _this.clickedItem = '';
                }
            }
        });
    };
    aiimsExam.prototype.nextQuestion = function () {
        if (this.questionPalette.length === (this.currentPaletteId + 1)) {
            this.statusCount();
            this.finishExam("");
        }
        else {
            this.currentPaletteId++;
            this.getQuestion(this.currentPaletteId);
        }
    };
    aiimsExam.prototype.statusCount = function () {
        var marked = 0, unanswered = 0, answered = 0;
        this.qPallete.filter(function (item) {
            if (item.status == 'answered')
                answered++;
            else if (item.status == 'marked')
                marked++;
            else
                unanswered++;
        });
        this.paletteStatus = [answered, marked, unanswered];
    };
    aiimsExam.prototype.previousQuestion = function () {
        this.currentPaletteId--;
        this.getQuestion(this.currentPaletteId);
    };
    aiimsExam.prototype.goToQuestion = function (index) {
        this.currentPaletteId = index;
        this.getQuestion(index);
    };
    aiimsExam.prototype.filter = function (type) {
        var _this = this;
        var mapArray = function (type) {
            var tempArray = [];
            tempArray = (_this.qPallete).filter(function (item) {
                if (item.status == type)
                    return item;
            });
            if (tempArray.length)
                _this.questionPalette = tempArray, _this.getQuestion(0), _this.currentPaletteId = 0;
            else
                _this.__auth.notificationInfo("No questions!");
        };
        if (type == 'm') {
            mapArray('marked');
        }
        else if (type == 'a') {
            mapArray('answered');
        }
        else if (type == 'na') {
            mapArray('');
        }
        else {
            this.questionPalette = this.qPallete;
            this.currentPaletteId = 0;
            this.getQuestion(0);
        }
    };
    aiimsExam.prototype.finishExam = function (userId) {
        var _this = this;
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
        var alert = this.alertCtrl.create({
            title: 'Confirm submit',
            message: 'Do you want to submit exam?',
            buttons: [
                {
                    text: 'Yes',
                    role: 'cancel',
                    handler: function () {
                        _this.examService.finishAiimsExam(_this.userExamId, _this.examId, _this.examType).subscribe(function (res) {
                            console.log("resss", res);
                            if (res.status) {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_exam_exam_room_aiimsResult_aiimsResult__["a" /* aiimsResult */], { ExamIds: _this.examId + '/' + _this.userExamId + '/' + _this.examType });
                            }
                            else {
                                _this.__auth.notificationInfo('OOPS! Something went wrong.');
                            }
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    aiimsExam.prototype.saveAnswer = function (answer, index) {
        //this.countClick++;
        var _this = this;
        var result = null;
        var answerStatus;
        if (this.examType == "AIIMS") {
            if (this.questionPalette[this.currentPaletteId].status == "marked") {
                this.marked = true;
            }
            else {
                this.marked = false;
            }
        }
        else {
            this.marked = false;
        }
        console.log("this.clickedItem", this.clickedItem);
        if (this.clickedItem && this.clickedItem == answer) {
            console.log("haaaai");
            this.clickedItem = 'ABCD';
            answerStatus = "unanswered";
            result = "unanswered";
            this.answered = false;
        }
        else {
            console.log("..................................");
            this.answered = true;
            this.clickedItem = answer;
            var correctAnswer = this.questionData.answer;
            if (answer == correctAnswer) {
                result = 'correct';
            }
            else {
                result = 'wrong';
            }
            answerStatus = "answered";
        }
        console.log("answerStatus", answerStatus);
        this.examService.saveAiimsExamAnswer(this.questionId, this.examId, this.userExamId, result, this.clickedItem, this.examType, this.marked)
            .subscribe(function (res) {
            console.log("ressss", res);
            console.log("ressss", res);
            if (!res.status)
                _this.__auth.notificationInfo("OOPS! Something went wrong.");
            else {
                if (_this.examType == "AIIMS") {
                    if (_this.questionPalette[_this.currentPaletteId].status == "marked" && _this.answered) {
                        _this.questionPalette[_this.currentPaletteId].status = "marked";
                        _this.qPallete[_this.currentPaletteId].status = "marked";
                    }
                    else if (answerStatus == 'unanswered') {
                        _this.questionPalette[_this.currentPaletteId].status = "";
                        _this.qPallete[_this.currentPaletteId].status = "";
                    }
                    else {
                        _this.questionPalette[_this.currentPaletteId].status = "answered";
                        _this.qPallete[_this.currentPaletteId].status = "answered";
                    }
                }
                else {
                    if (answerStatus == 'unanswered') {
                        _this.questionPalette[_this.currentPaletteId].status = "";
                        _this.qPallete[_this.currentPaletteId].status = "";
                    }
                    else {
                        _this.questionPalette[_this.currentPaletteId].status = "answered";
                        _this.qPallete[_this.currentPaletteId].status = "answered";
                    }
                }
            }
        });
    };
    aiimsExam.prototype.markQuestion = function (type) {
        var _this = this;
        console.log(type, "type da");
        this.examService.markAiimsQuestions(this.questionId, this.userExamId, type)
            .subscribe(function (res) {
            console.log("mark", res);
            if (!res.status)
                _this.__auth.notificationInfo("OOPS! Something went wrong.");
            else {
                if (type == "mark") {
                    type = "marked";
                }
                else {
                    type = "unmarked";
                }
                if (_this.examType == "JIPMER") {
                    if ((type == 'marked') && _this.answered) {
                        _this.questionPalette[_this.currentPaletteId].status = 'marked';
                        _this.qPallete[_this.currentPaletteId].status = "marked";
                    }
                    else if ((type == 'unmarked') && _this.answered) {
                        _this.questionPalette[_this.currentPaletteId].status = 'answered';
                        _this.qPallete[_this.currentPaletteId].status = 'answered';
                    }
                    else if ((type == 'unmarked') && !_this.answered) {
                        _this.questionPalette[_this.currentPaletteId].status = '';
                        _this.qPallete[_this.currentPaletteId].status = '';
                    }
                    else {
                        _this.questionPalette[_this.currentPaletteId].status = type;
                        _this.qPallete[_this.currentPaletteId].status = type;
                    }
                }
                else {
                    if ((type == 'marked') && _this.answered) {
                        _this.questionPalette[_this.currentPaletteId].status = 'marked';
                        _this.qPallete[_this.currentPaletteId].status = "marked";
                    }
                    else if ((type == 'unmarked') && _this.answered) {
                        _this.questionPalette[_this.currentPaletteId].status = 'answered';
                        _this.qPallete[_this.currentPaletteId].status = 'answered';
                    }
                    else if ((type == 'unmarked') && !_this.answered) {
                        _this.questionPalette[_this.currentPaletteId].status = '';
                        _this.qPallete[_this.currentPaletteId].status = '';
                    }
                    else {
                        _this.questionPalette[_this.currentPaletteId].status = type;
                        _this.qPallete[_this.currentPaletteId].status = type;
                    }
                }
            }
        });
    };
    aiimsExam = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-aiimsExam',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/createexam/aiimsExam/aiimsExam.html"*/'<style>\n    .card-content-md {\n        padding-left: -1px;\n      }\n      .card-md {\n        margin-left:-4px !important;\n        border-radius: 2px!important;\n        width: calc(100% - -8px)!important;  \n      }\n      \n      page-write .optionStyle {\n          border-bottom:unset!important;  \n      }\n    </style>\n \n \n <ion-content padding style="background-color: #efefef;"  class="bgpic">\n\n    <!-- <ion-grid style=" background-color:#b4aeae;">\n                    <ion-row>\n                        <ion-col col-6>\n                                <ion-icon name="arrow-back" style="font-size: 24px;" (click)="back()"></ion-icon>\n                          &nbsp; &nbsp;  <ion-icon name="star" style="color:#ff6b01;font-size: 24px;"></ion-icon>\n                        </ion-col>\n                        <ion-col col-2 class="align-icon">\n                            <ion-icon name="share-alt" style="font-size: 24px;"></ion-icon>\n                        </ion-col>\n                        <ion-col col-2 class="align-icon">\n                            <ion-icon name="switch" style="font-size: 24px;"></ion-icon>\n                        </ion-col>\n                        <ion-col col-2 class="align-icon">\n                            <ion-icon name="apps" style="font-size: 24px;"></ion-icon>\n                        </ion-col>\n                    </ion-row>\n                </ion-grid> -->\n \n                <ion-card style="margin-top:0px;" *ngIf="questionData">\n \n                    <div *ngIf="questionData">\n             \n                        <ion-row>\n             \n                            <ion-col col-12 class="count" style=" background-color:#cecece; padding: 8px!important" *ngIf="timeFlag">\n                                <div class="count" style="float:left; padding-top:5px;">\n             \n                                    <div style="font-size: 15px;color: #e13838;">\n                                        <span>{{ time.getHours() }}</span> :\n                                        <span>{{ time.getMinutes() }}</span> :\n                                        <span>{{ time.getSeconds() }}</span>\n                                    </div>\n                                </div>\n             \n                                <div style=" float: right!important;">\n                                    <!-- <button ion-button class="playBtn" title={{pauseTitle}}  aria-hidden="true" (click)="playPause()" *ngIf="!buttonName"><ion-icon name="pause"></ion-icon>\n                                                    </button> -->\n                                    <ion-icon name="pause" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="!buttonName"></ion-icon>\n             \n                                    <!-- <button ion-button  class="playBtn" title={{playTitle}}  aria-hidden="true"  (click)="playPause()" *ngIf="buttonName"><ion-icon name="play"></ion-icon></button>   -->\n                                    <ion-icon name="play" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="buttonName"></ion-icon>\n             \n                                </div>\n                            </ion-col>\n            \n                        </ion-row>\n            \n                    </div>\n            \n                    <ion-card-content style="background-color:#FFFEEE;margin-left:-13px!important; margin-right:-13px!important;">\n                       \n            \n                            <ion-item-group>\n                                <ion-item-divider style="padding:0px!important;background-color:#FFFEEE;" text-wrap class="questionHead">\n                                    <ion-grid style="padding-bottom:0px !important;" >\n            \n                                        <ion-row>\n                                            <ion-col col-2 class="fntsze" style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:11px; padding-left:0px;">\n            \n                                                <span >{{slNumber}}</span>.\n            \n                                            </ion-col>\n                                            <ion-col col-10 class="fntsze"style=" padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n            \n                                                <span [innerHtml]="question"></span>\n                                            </ion-col>\n                                            <ion-row class="col-sm-12 bdr" *ngIf="imageFlag">\n                                                <br>\n                                                <img class="img-responsive" style="width: 400px;height: 300" [src]="imageUrl">\n                                                <br>\n                                            </ion-row>\n                                        </ion-row>\n            \n                                    </ion-grid>\n                                </ion-item-divider>\n            \n            \n                                <ion-list *ngFor="let item of options ;let i= index;">\n                                    <ion-item text-wrap class="optionStyle" disabled="answered" (click)="saveAnswer(item.id,i)" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important;">\n            \n                                        <div [ngClass]="{\'selected\': answered === true && clickedItem === item.id}">\n            \n            \n                                            <ion-grid style="padding:0px!important;">\n                                                <ion-row>\n                                                    <ion-col col-2 style=" padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;">{{item.id}}.</ion-col>\n                                                    <ion-col col-10 style=" font-size:18px!important;padding-left:2!important;margin-left: 0px;line-height:22px;line-height:26px;">\n                                                        <span style="font-size: 18px!important;"> {{item.text}}</span>\n                                                    </ion-col>\n                                                </ion-row>\n                                            </ion-grid>\n                                        </div>\n            \n                                    </ion-item>\n                                </ion-list>\n            \n            \n            \n                            </ion-item-group>\n                       \n                    </ion-card-content>\n                    <ion-row style="margin-top:10px;">\n                        <ion-col col-4 style="text-align:center;">\n                            <button float-center ion-button color="primary" style="background-color:#108fd2;color:white;" icon-left (click)="previousQuestion()"\n                            [disabled]="currentPaletteId<=0 || paused">Previous</button>\n            \n                        </ion-col>\n                        <ion-col col-4 style="text-align:center;">\n                                 \n                                \n                            <button float-center ion-button style="margin-left: 29px;" style=" text-align: center; background-color:#108fd2;margin-left:22px;" (click)="markQuestion((questionPalette[currentPaletteId].status == \'marked\') ? \'unmark\': \'mark\')" [disabled]=paused>{{(questionPalette[currentPaletteId].status == \'marked\') ? \'Unmark\': \'Mark\'}}</button>\n                        </ion-col>\n                        <ion-col col-4 style="text-align:center;">\n                            <button float-right ion-button style="background-color:#108fd2;color:white;margin-right:4px; " (click)="nextQuestion()"\n                                [disabled]=paused>{{(currentPaletteId+1>=questionPalette.length)?\'Submit\':\'Next\'}}</button>\n            \n            \n                        </ion-col>\n                    </ion-row>\n                </ion-card>\n            \n            \n            \n                <ion-grid style="height:180px;background-color: #fff;">\n                    <p style="text-align:center;margin-top:0px!important; margin-bottom:4px!important;font-size: 18px;">Question Palette</p>\n            \n                    <ion-scroll scrollY="true" style="height:160px;">\n            \n                        <ion-row>\n            \n                            <ion-col col-2 col-md-4 col-xl-3 style="margin-top:0px; max-width: 12.0000%!important;" *ngFor="let item of questionPalette ;let i= index;">\n                                <ion-col class="circleStyle" [ngClass]="{\'selected\': item.status === \'answered\', \'marked\': item.status === \'marked\'}" (click)="goToQuestion(i)">\n                                    {{item.count+1||i+1}}\n            \n                                </ion-col>\n            \n                            </ion-col>\n            \n                        </ion-row>\n                    </ion-scroll>\n                </ion-grid>\n                <!-- <ion-card style=" min-height:100px; margin-top:0px;background-color: #f8f8f8;">-->\n                <hr>\n                <ion-grid style="margin-top:0px;background-color:#cecece;">\n                    <ion-row>\n            \n                        <ion-col col-6 (click)="filter(\'na\')">\n                            <ion-col col-1 class="circle">\n                                &nbsp;&nbsp;\n                            </ion-col>\n                            &nbsp;&nbsp;&nbsp; Not Answered\n                        </ion-col>\n                        <ion-col col-6 (click)="filter(\'a\')">\n                            <ion-col col-1 class="circle1">\n                                &nbsp;&nbsp;\n                            </ion-col>\n                            &nbsp;&nbsp;&nbsp;Answered\n                        </ion-col>\n                    </ion-row>\n            \n                    <ion-row style="padding-top: 10px;">\n                        <ion-col col-6 (click)="filter(\'m\')">\n                            <ion-col col-1 class="circle2">\n                                &nbsp;&nbsp;\n                            </ion-col>\n                            &nbsp;&nbsp;&nbsp; Marked\n                        </ion-col>\n                        <ion-col col-6 (click)="filter(\'\')">\n                            <ion-col col-1 class="circle">\n                                &nbsp;&nbsp;\n                            </ion-col>\n                            &nbsp;&nbsp;&nbsp; AllQuestion\n                        </ion-col>\n            \n                    </ion-row>\n                    <!-- <button ion-button style="background-color:#e48684;color:white; float:right;padding-top:2px;" (click)="finishExam(\'\')">Submit Exam</button> -->\n                </ion-grid>\n            \n            \n            \n            </ion-content>\n            <ion-footer>\n                <button ion-button block  style="background-color:#e48684;color:white; " (click)="finishExam(\'\')">Submit Exam </button>\n            </ion-footer>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/createexam/aiimsExam/aiimsExam.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]])
    ], aiimsExam);
    return aiimsExam;
}());

//# sourceMappingURL=aiimsExam.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return termsrules; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_weekend_service__ = __webpack_require__(441);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_exam_exam_room_details_details__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_exam_examNew_examComponent__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_navBarService__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var termsrules = /** @class */ (function () {
    function termsrules(_nav, navCtrl, events, platform, alertCtrl, __auth, weeklyService, toastCtrl, navParams) {
        this._nav = _nav;
        this.navCtrl = navCtrl;
        this.events = events;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.__auth = __auth;
        this.weeklyService = weeklyService;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.questionPalette = [];
        this.currentPaletteId = 0;
        this.options = [];
        this.subjectArray = [];
        this.zeroTime = false;
        this.buttonName = false;
        this.paused = false;
        this.resumed = false;
        this.buttonToggle = false;
        this.url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].backendUrl + '/images/';
        this.paletteStatus = [];
        this.timer = false;
        this.testObject = [];
        this.examId = navParams.get('subject');
        this.events.publish('user:created', true, Date.now());
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        console.log(this.tabBarElement, "nav show or hiden");
        this.tabBarElement.style.display = 'none';
        this._nav.navHide(true);
    }
    termsrules.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    termsrules.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    termsrules.prototype.ionViewWillLeave = function () {
        this.tabBarElement.style.display = 'flex';
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
    };
    termsrules.prototype.ngOnInit = function () {
        var _this = this;
        this.weeklyService.loadWeekExam(this.examId)
            .subscribe(function (res) {
            if (!res.status)
                _this.__auth.notificationInfo('OOPS! Something went wrong.');
            else {
                var examDetails = res.data;
                console.log(examDetails, "exam data here say");
                var count_1 = 1;
                _this.examId = examDetails.examId;
                _this.userExamId = examDetails.userExamId;
                _this.questionPalette = (examDetails.questionPalette).filter(function (item) {
                    item.number = count_1;
                    count_1++;
                    return item;
                });
                _this.qPallete = _this.questionPalette;
                _this.timeFlag = examDetails.examDetails.timeFlag;
                if (_this.timeFlag) {
                    _this.timerObj = examDetails.examDetails.timeObject;
                    _this.testObject = JSON.parse(localStorage.getItem('testObject'));
                    if (_this.testObject) {
                        _this.testObject.map(function (item, i) {
                            if (item.userExamId == _this.userExamId) {
                                _this.timerIndex = i;
                                _this.previousTimer = _this.testObject[_this.timerIndex];
                            }
                            ;
                        });
                    }
                    else {
                        _this.testObject = [];
                        _this.previousTimer = null;
                    }
                    if (_this.previousTimer == null || !_this.previousTimer || _this.zeroTime) {
                        _this.time = new Date(2017, 1, 1, _this.timerObj.hour, _this.timerObj.min, 0);
                        _this.timer = true;
                    }
                    else {
                        _this.time = new Date(2017, 1, 1, _this.previousTimer.hours, _this.previousTimer.minutes, _this.previousTimer.seconds);
                        _this.timer = true;
                    }
                    _this._timerTick();
                }
                _this.getQuestion(_this.currentPaletteId);
            }
        });
    };
    termsrules.prototype._timerTick = function () {
        var _this = this;
        this.time.setSeconds(this.time.getSeconds(), -1);
        var timerObject = {
            hours: this.time.getHours(),
            minutes: this.time.getMinutes(),
            seconds: this.time.getSeconds(),
            userExamId: this.userExamId
        };
        if (this.timerIndex)
            this.testObject[this.timerIndex] = timerObject;
        else {
            this.testObject.push(timerObject);
            this.testObject.map(function (item, i) {
                if (item.userExamId == _this.userExamId)
                    _this.timerIndex = i;
            });
        }
        localStorage.setItem('testObject', JSON.stringify(this.testObject));
        if (timerObject.hours === 0 && timerObject.minutes === 5 && timerObject.seconds === 0) {
            this.__auth.notificationInfo('5 Minutes Remaining');
        }
        if (timerObject.hours === 0 && timerObject.minutes === 0 && timerObject.seconds === 0) {
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
            this.statusCount();
            this.finishWeekExam();
        }
        this.x = setTimeout(function () { return _this._timerTick(); }, 1000);
    };
    termsrules.prototype.playPause = function () {
        var _this = this;
        if (this.buttonToggle) {
            this.buttonName = false;
            this.pauseTitle = "Pause Timer";
            if (this.pauseTime) {
                this.playTime = this.pauseTime;
            }
            else {
                this.playTime = this.time;
            }
            this.paused = false;
            this.resumed = true;
            var examurl = "/exam/write/" + this.userExamId;
            this.playTime.setSeconds(this.playTime.getSeconds(), -1);
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 5 && this.playTime.getSeconds() === 0) {
                this.__auth.notificationInfo('5 Minutes Remaining');
            }
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 0 && this.playTime.getSeconds() === 0) {
                window.clearInterval(this.x);
                localStorage.removeItem('timerObject');
                this.finishWeekExam();
                this.statusCount();
            }
            var x = setTimeout(function () { return _this._timerTick(); }, 1000);
        }
        else {
            this.buttonName = true;
            this.playTitle = "Resume Timer";
            this.pauseTime = this.time;
            this.resumed = false;
            this.paused = true;
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
        }
        this.buttonToggle = !this.buttonToggle;
    };
    termsrules.prototype.getQuestion = function (index) {
        var _this = this;
        this.currentPaletteId = index;
        console.log(this.currentPaletteId, "id here1");
        console.log(this.questionPalette[this.currentPaletteId], "palet22");
        this.questionId = this.questionPalette[this.currentPaletteId]._id;
        console.log(this.questionId, "question id3");
        this.weeklyService.getExamQuestion(this.questionId, this.userExamId)
            .subscribe(function (res) {
            if (!res.status)
                _this.__auth.notificationInfo("OOPS! Something went wrong.");
            else {
                _this.questionData = (res.data.question[0]) ? res.data.question[0] : {};
                _this.answerData = (res.data.answer[0]) ? res.data.answer[0] : {};
                _this.question = _this.questionData.question;
                _this.options = JSON.parse(_this.questionData.options);
                _this.slNumber = _this.questionPalette[_this.currentPaletteId].number;
                _this.subjectArray = _this.questionData.subjectArray;
                if (_this.questionData.question_image) {
                    _this.imageFlag = true;
                    _this.imageUrl = _this.url + _this.questionData.question_image;
                }
                else {
                    _this.imageFlag = false;
                }
                if (_this.answerData.answer) {
                    _this.answered = true;
                    _this.clickedItem = _this.answerData.answer;
                }
                else {
                    _this.answered = false;
                    _this.clickedItem = '';
                }
            }
        });
    };
    termsrules.prototype.nextQuestion = function () {
        if (this.questionPalette.length === (this.currentPaletteId + 1)) {
            this.finishExam("");
            this.statusCount();
        }
        else {
            this.currentPaletteId++;
            this.getQuestion(this.currentPaletteId);
        }
    };
    termsrules.prototype.previousQuestion = function () {
        this.currentPaletteId--;
        this.getQuestion(this.currentPaletteId);
    };
    termsrules.prototype.goToQuestion = function (index) {
        if (!this.paused) {
            this.currentPaletteId = index;
            this.getQuestion(index);
        }
        else {
            console.log("user paused");
        }
    };
    termsrules.prototype.saveAnswer = function (answer, index) {
        var _this = this;
        if (!this.paused) {
            var result = null;
            var answerStatus_1;
            if (this.clickedItem && this.clickedItem == answer) {
                this.clickedItem = 'O';
                answerStatus_1 = "unanswered";
                result = "wrong";
                this.answered = false;
            }
            else {
                this.answered = true;
                this.clickedItem = answer;
                var correctAnswer = this.questionData.answer;
                if (answer == correctAnswer) {
                    result = 'correct';
                }
                else {
                    result = 'wrong';
                }
                answerStatus_1 = "answered";
            }
            this.weeklyService.saveExamAnswer(this.questionId, this.examId, this.userExamId, answerStatus_1, this.clickedItem, result, this.subjectArray)
                .subscribe(function (res) {
                if (!res.status)
                    _this.__auth.notificationInfo("OOPS! Something went wrong.");
                else {
                    if (answerStatus_1 == 'unanswered') {
                        _this.questionPalette[_this.currentPaletteId].status = "";
                        _this.qPallete[_this.currentPaletteId].status = "";
                    }
                    else {
                        _this.questionPalette[_this.currentPaletteId].status = "answered";
                        _this.qPallete[_this.currentPaletteId].status = "answered";
                    }
                }
            });
        }
        else {
            console.log("user paused");
        }
    };
    termsrules.prototype.markQuestion = function (type) {
        var _this = this;
        console.log(type, "type da");
        this.weeklyService.markExamQuestion(this.questionId, this.userExamId, type)
            .subscribe(function (res) {
            if (!res.status)
                _this.__auth.notificationInfo("OOPS! Something went wrong.");
            else {
                // let index = (this.qPallete).findIndex(item => item.question == this.questionId)
                if (type == 'marked') {
                    // this.questionPalette[this.currentPaletteId].status = 'answered'
                    _this.qPallete[_this.currentPaletteId].marked = "marked";
                }
                else if (type == 'unmarked') {
                    _this.questionPalette[_this.currentPaletteId].marked = '';
                }
            }
        });
    };
    termsrules.prototype.filter = function (type) {
        var _this = this;
        var mapArray = function (type) {
            var tempArray = [];
            tempArray = (_this.qPallete).filter(function (item) {
                if (item.status == type)
                    return item;
            });
            if (tempArray.length)
                _this.questionPalette = tempArray, _this.getQuestion(0), _this.currentPaletteId = 0;
            else
                _this.__auth.notificationInfo("No questions!");
        };
        var markArray = function (type) {
            var tempArray = [];
            tempArray = (_this.qPallete).filter(function (item) {
                if (item.status == type)
                    return item;
            });
            if (tempArray.length)
                _this.questionPalette = tempArray, _this.getQuestion(0), _this.currentPaletteId = 0;
            else
                _this.__auth.notificationInfo("No questions!");
        };
        if (type == 'm') {
            markArray('marked');
        }
        else if (type == 'a') {
            mapArray('answered');
        }
        else if (type == 'na') {
            mapArray('');
        }
        else {
            this.questionPalette = this.qPallete;
            this.currentPaletteId = 0;
            this.getQuestion(0);
        }
    };
    termsrules.prototype.finishExam = function (userId) {
        var _this = this;
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
        var alert = this.alertCtrl.create({
            title: 'Confirm submit',
            message: 'Do you want to submit exam?',
            buttons: [
                {
                    text: 'Yes',
                    role: 'cancel',
                    handler: function () {
                        _this.weeklyService.submitExam(_this.userExamId).subscribe(function (res) {
                            if (res.status) {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: _this.userExamId + '/week' });
                            }
                            else {
                                _this.__auth.notificationInfo('OOPS! Something went wrong.');
                            }
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    termsrules.prototype.finishWeekExam = function () {
        var _this = this;
        this.weeklyService.submitExam(this.userExamId).subscribe(function (res) {
            if (res.status) {
                window.clearInterval(_this.x);
                localStorage.removeItem('timerObject');
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: _this.userExamId + '/week' });
            }
            else {
                _this.__auth.notificationInfo('OOPS! Something went wrong.');
            }
        });
    };
    // finishExam() {
    //   window.clearInterval(this.x);
    //   localStorage.removeItem('timerObject');
    //   this.weeklyService.submitExam(this.userExamId)
    //     .subscribe(
    //       res => {
    //         if (!res.status) this.notify("OOPS! Something went wrong.", "error");
    //         else {
    //           this.notify('Successfully submitted the mock exam', 'success')
    //           this.navCtrl.push(examresultModal, { ExamIds:  this.userExamId + '/week' });
    //         }
    //       }
    //     )
    // }
    termsrules.prototype.statusCount = function () {
        var marked = 0, unanswered = 0, answered = 0;
        this.qPallete.filter(function (item) {
            if (item.status == 'answered')
                answered++;
            else if (item.status == 'marked')
                marked++;
            else
                unanswered++;
        });
        this.paletteStatus = [answered, marked, unanswered];
    };
    termsrules = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-termsrules',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/weekend/TermsAndRules/terms.component.html"*/'<style>\n    .card-content-md {\n        padding-left: -1px;\n      }\n      .card-md {\n        margin-left:-4px !important;\n        border-radius: 2px!important;\n        width: calc(100% - -8px)!important;  \n      }\n      \n      page-write .optionStyle {\n          border-bottom:unset!important;  \n      }\n    </style>\n   <ion-header #head>\n     \n  \n        <ion-toolbar>\n            <ion-navbar>\n                <ion-buttons left>\n           \n        \n                </ion-buttons>\n              </ion-navbar>\n          \n    \n    \n        </ion-toolbar>\n      </ion-header>\n \n <ion-content padding style="background-color: #efefef;"  class="bgpic">\n\n    <!-- <ion-grid style=" background-color:#b4aeae;">\n                    <ion-row>\n                        <ion-col col-6>\n                                <ion-icon name="arrow-back" style="font-size: 24px;" (click)="back()"></ion-icon>\n                          &nbsp; &nbsp;  <ion-icon name="star" style="color:#ff6b01;font-size: 24px;"></ion-icon>\n                        </ion-col>\n                        <ion-col col-2 class="align-icon">\n                            <ion-icon name="share-alt" style="font-size: 24px;"></ion-icon>\n                        </ion-col>\n                        <ion-col col-2 class="align-icon">\n                            <ion-icon name="switch" style="font-size: 24px;"></ion-icon>\n                        </ion-col>\n                        <ion-col col-2 class="align-icon">\n                            <ion-icon name="apps" style="font-size: 24px;"></ion-icon>\n                        </ion-col>\n                    </ion-row>\n                </ion-grid> -->\n \n    <ion-card style="margin-top:0px;" *ngIf="questionData">\n \n        <div *ngIf="questionData">\n \n            <ion-row>\n \n                <ion-col col-12 class="count" style=" background-color:#cecece; padding: 8px!important" *ngIf="timeFlag">\n                    <div class="count" style="float:left; padding-top:5px;">\n \n                        <div style="font-size: 15px;color: #e13838;">\n                            <span>{{ time.getHours() }}</span> :\n                            <span>{{ time.getMinutes() }}</span> :\n                            <span>{{ time.getSeconds() }}</span>\n                        </div>\n                    </div>\n \n                    <div style=" float: right!important;">\n                        <!-- <button ion-button class="playBtn" title={{pauseTitle}}  aria-hidden="true" (click)="playPause()" *ngIf="!buttonName"><ion-icon name="pause"></ion-icon>\n                                        </button> -->\n                        <ion-icon name="pause" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="!buttonName"></ion-icon>\n \n                        <!-- <button ion-button  class="playBtn" title={{playTitle}}  aria-hidden="true"  (click)="playPause()" *ngIf="buttonName"><ion-icon name="play"></ion-icon></button>   -->\n                        <ion-icon name="play" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="buttonName"></ion-icon>\n \n                    </div>\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n        <ion-card-content style="background-color:#FFFEEE;margin-left:-13px!important; margin-right:-13px!important;">\n           \n\n                <ion-item-group>\n                    <ion-item-divider style="padding:0px!important;background-color:#FFFEEE;" text-wrap class="questionHead">\n                        <ion-grid style="padding-bottom:0px !important;" >\n\n                            <ion-row style="border-bottom: 2px solid #f0f0f0;">\n                                <ion-col col-2 class="fntsze" style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:11px; padding-left:0px;">\n\n                                    <span [innerHtml]="currentPaletteId+1">/</span>.\n\n                                </ion-col>\n                                <ion-col col-10 class="fntsze"style=" padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n\n                                    <span [innerHtml]="questionData.question"></span>\n                                </ion-col>\n                                <ion-row class="col-sm-12 bdr" *ngIf="imageFlag">\n                                    <br>\n                                    <img class="img-responsive" style="width: 400px;height: 300" [src]="imageUrl">\n                                    <br>\n                                </ion-row>\n                            </ion-row>\n\n                        </ion-grid>\n                    </ion-item-divider>\n\n\n                    <ion-list *ngFor="let item of options ;let i= index;">\n                        <ion-item text-wrap class="optionStyle" disabled="answered" (click)="saveAnswer(item.id,i)" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important;">\n\n                            <div [ngClass]="{\'selected\': answered === true && clickedItem === item.id}">\n\n\n                                <ion-grid style="padding:0px!important;">\n                                    <ion-row>\n                                        <ion-col col-2 style=" padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;">{{item.id}}.</ion-col>\n                                        <ion-col col-10 style=" font-size:18px!important;padding-left:2!important;margin-left: 0px;line-height:22px;line-height:26px;">\n                                            <span style="font-size: 18px!important;"> {{item.text}}</span>\n                                        </ion-col>\n                                    </ion-row>\n                                </ion-grid>\n                            </div>\n\n                        </ion-item>\n                    </ion-list>\n\n\n\n                </ion-item-group>\n           \n        </ion-card-content>\n        <ion-row style="margin-top:10px;">\n            <ion-col col-4 >\n                <button  style="text-align: left;padding: 0px 30px 0px 30px; font-size:13px;" ion-button color="primary" style="background-color:#108fd2;color:white;" icon-left (click)="\n                Question()"\n                [disabled]="currentPaletteId<=0 || paused">Previous</button>\n\n            </ion-col>\n            <ion-col col-4 style="text-align:center;">\n                     \n                    \n                <button float-center ion-button style="margin-left: 29px;" style=" text-align: center; background-color:#108fd2;margin-left:22px;padding: 0px 30px 0px 30px; font-size:13px;" (click)="markQuestion((questionPalette[currentPaletteId].marked == \'marked\') ? \'unmarked\': \'marked\')" [disabled]=paused>{{(questionPalette[currentPaletteId].marked == \'marked\') ? \'Unmark\': \'Mark\'}}</button>\n            </ion-col>\n            <ion-col col-4 style="text-align:center;">\n                <button float-right ion-button style="background-color:#108fd2;color:white;margin-right:4px;padding: 0px 30px 0px 30px; font-size:13px; " (click)="nextQuestion()"\n                    [disabled]=paused>{{(currentPaletteId+1>=questionPalette.length)?\'Submit\':\'Next\'}}</button>\n\n\n            </ion-col>\n        </ion-row>\n    </ion-card>\n\n\n\n    <ion-grid style="height:180px;background-color: #fff;margin-top:10px!important;">\n        <p style="text-align:center;margin-top:0px!important; margin-bottom:4px!important;font-size: 18px;">Question Palette</p>\n\n        <ion-scroll scrollY="true" style="height:160px;">\n\n            <ion-row>\n\n                <ion-col col-2 col-md-4 col-xl-3 style="margin-top:0px; max-width: 12.0000%!important;" *ngFor="let item of questionPalette ;let i= index;">\n                    <ion-col class="circleStyle" [ngClass]="{\'selected\': item.status === \'answered\', \'marked\': item.marked === \'marked\'}" (click)="goToQuestion(i)">\n                        {{item.count+1||i+1}}\n\n                    </ion-col>\n\n                </ion-col>\n\n            </ion-row>\n        </ion-scroll>\n    </ion-grid>\n    <!-- <ion-card style=" min-height:100px; margin-top:0px;background-color: #f8f8f8;">-->\n    <hr>\n    <ion-grid style="margin-top:0px;background-color:#cecece;">\n        <ion-row>\n\n            <ion-col col-6 (click)="filter(\'na\')">\n                <ion-col col-1 class="circle">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp; Not Answered\n            </ion-col>\n            <ion-col col-6 (click)="filter(\'a\')">\n                <ion-col col-1 class="circle1">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp;Answered\n            </ion-col>\n        </ion-row>\n\n        <ion-row style="padding-top: 10px;">\n            <ion-col col-6 (click)="filter(\'m\')">\n                <ion-col col-1 class="circle2">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp;&nbsp;&nbsp; Marked\n            </ion-col>\n            <ion-col col-6 (click)="filter(\'\')">\n                <ion-col col-1 class="circle">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp; All Question\n            </ion-col>\n\n        </ion-row>\n        <!-- <button ion-button style="background-color:#e48684;color:white; float:right;padding-top:2px;" (click)="finishExam(\'\')">Submit Exam</button> -->\n    </ion-grid>\n\n\n\n</ion-content>\n<ion-footer>\n    <button ion-button block  style="background-color:#e48684;color:white; " (click)="finishExam(\'\')">Submit Exam </button>\n</ion-footer>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/weekend/TermsAndRules/terms.component.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_weekend_service__["a" /* WeeklyService */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_7__services_navBarService__["a" /* naveBarShowSrvice */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__services_navBarService__["a" /* naveBarShowSrvice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_3__services_weekend_service__["a" /* WeeklyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], termsrules);
    return termsrules;
}());

//# sourceMappingURL=terms.component.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Setting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__commen_login_login__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_profile_profile__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__commen_setting_about_about__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__commen_setting_FAQ_faq__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__commen_setting_teachersPage_teachers__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__commen_setting_timetable_timetable__ = __webpack_require__(581);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_exam_weekend_weekend_component__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_navBarService__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__commen_setting_moreSettings_moreSettings__ = __webpack_require__(582);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_evaluate_evaluate__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var Setting = /** @class */ (function () {
    function Setting(_navBar, events, navCtrl, platform, authservice, fireBase) {
        this._navBar = _navBar;
        this.events = events;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.authservice = authservice;
        this.fireBase = fireBase;
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.events.publish('user:created', false, Date.now());
    }
    Setting.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    Setting.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_dashboard_home__["a" /* HomePage */]);
    };
    Setting.prototype.ngOnInit = function () {
        this.userData = JSON.parse(localStorage.getItem('userData'));
        console.log("userdata", this.userData);
        this.UserName = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
    };
    Setting.prototype.evaluate = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__pages_evaluate_evaluate__["a" /* Evaluate */]);
    };
    Setting.prototype.subscribe = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_exam_weekend_weekend_component__["a" /* weekendReg */]);
    };
    Setting.prototype.timetable = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__commen_setting_timetable_timetable__["a" /* timetable */]);
    };
    Setting.prototype.aboutPage = function () {
        console.log("kkkkkkkkkkkkkkkkkk");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__commen_setting_about_about__["a" /* About */]);
    };
    Setting.prototype.faq = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__commen_setting_FAQ_faq__["a" /* faq */]);
    };
    Setting.prototype.teachers = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__commen_setting_teachersPage_teachers__["a" /* teachers */]);
    };
    Setting.prototype.EditProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_profile_profile__["a" /* Profile */]);
    };
    Setting.prototype.logOut = function () {
        var _this = this;
        localStorage.removeItem('userData');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__commen_login_login__["a" /* Login */]);
        this._navBar.navHide(true);
        this.tabBarElement.style.display = 'none';
        this.logoutFromSocial();
        this.authservice.logout(this.userData._id).subscribe(function (result) {
            if (_this.tabBarElement) {
                _this.tabBarElement.style.display = 'none';
            }
            _this._navBar.navHide(true);
        }, function (error) {
            console.log(error);
        });
    };
    Setting.prototype.settings = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__commen_setting_moreSettings_moreSettings__["a" /* moreSetting */]);
    };
    Setting.prototype.logoutFromSocial = function () {
        this.fireBase.auth.signOut();
    };
    Setting = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/setting.html"*/'\n\n<style>\n.button-inner{\n  background-color: #108fd2!important;\n}\n\n</style>\n\n<ion-header>\n  <ion-navbar>\n    \n    <ion-title>Menu</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="bgpic">\n  <!-- <ion-list no-border>\n      <ion-list-header style="padding-left:0px;">\n      <ion-grid>\n          <ion-row>\n<ion-col col-3><img style="width:60px;height:60px;" src="assets/img/image.png"></ion-col>\n<ion-col col-5 style="padding-top:19px;"><p class="username"><b>{{UserName}}</b></p></ion-col>\n<ion-col col-4 style="text-align: right;">\n    <ion-icon item-end  name="ios-arrow-forward" style="font-size: 33px;\n    color: #108fd2;\n    font-weight: bold;" (click)="EditProfile()"></ion-icon>\n  </ion-col>\n\n  </ion-row>\n        </ion-grid>\n\n    \n     \n      \n      \n  \n             \n              \n           \n    </ion-list-header>\n  </ion-list> -->\n  <ion-list  style="border: 1px solid #108fd2; background-color:#108fd2;" >\n\n      <!-- <ion-list-header>\n      <b>General</b> \n      </ion-list-header> -->\n      <ion-item (click)="evaluate()" >\n        <ion-icon name=\'paper\' style="color:#108fd2" item-start></ion-icon>\n     Evaluate\n        <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n      </ion-item>\n      <!-- <ion-item (click)="subscribe()" >\n        <ion-icon name=\'card\' style="color:#108fd2" item-start></ion-icon>\n        \n        Pro users\n      \n        <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n      </ion-item> -->\n      <!-- <ion-item>\n          <ion-toggle [(ngModel)]="enableNotifications" (click)="toggleNotifications()"  ></ion-toggle>\n        <ion-label>\n         Enable notification\n        </ion-label>\n        <ion-icon name=\'notifications\' style="color:#108fd2" item-start></ion-icon>\n      </ion-item> -->\n      <ion-item (click)="teachers()" >\n          <ion-icon name=\'md-contacts\' style="color:#108fd2" item-start></ion-icon>\n        Our Team\n          <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n        </ion-item>\n        <!-- <ion-item  (click)="faq()">\n          <ion-icon name=\'md-help\'  style="color:#108fd2" item-start></ion-icon>\n         FAQ\n          <ion-icon name="ios-arrow-forward-outline" item-end   style="color:#108fd2"></ion-icon>\n        </ion-item> -->\n        <ion-item (click)="settings()" >\n          <ion-icon name="cog" style="color:#108fd2" item-start></ion-icon>\n     Settings\n          <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n        </ion-item>\n       \n        <ion-item (click)="timetable()" >\n          <ion-icon name="md-calendar" style="color:#108fd2" item-start></ion-icon>\n     Timetable\n          <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n        </ion-item>\n       \n     \n        <ion-item (click)="logOut()" >\n          <ion-icon name="ios-log-out" style="color:#108fd2; font-weight: bold;" item-start></ion-icon>\n     Logout\n          <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n        </ion-item>\n  \n  \n    </ion-list>\n    <!-- <ion-list >\n\n      <ion-list-header>\n      <b>Settings</b> \n      </ion-list-header>\n       <ion-item  (click)="faq()">\n            <ion-icon name=\'md-help\'  style="color:#108fd2" item-start></ion-icon>\n           FAQ\n            <ion-icon name="ios-arrow-forward-outline" item-end   style="color:#108fd2"></ion-icon>\n          </ion-item>\n      \n      <ion-item (click)="aboutPage()">\n        <ion-icon name=\'md-information-circle\'  style="color:#108fd2" item-start></ion-icon>\n        About us\n        <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n      </ion-item  >\n\n  </ion-list> -->\n \n    <!-- <ion-list >\n\n        \n        \n      <ion-item >\n        <ion-row>\n         <!-- <ion-col col-12 style="text-align: right; color: #108fd2;font-size:22px;">\n            Log Out\n            <ion-icon (click)="logOut()" ios="ios-log-out" md="md-log-out" style="font-size:22px;" >   </ion-icon>\n\n         </ion-col> -->\n         <!-- <button ion-button block style="background-color:#108fd2;color:white;" (click)="logOut()">Logout </button>\n\n        </ion-row>\n          \n              \n         \n        </ion-item>\n\n      </ion-list> --> \n</ion-content>\n'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/setting.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_12__services_navBarService__["a" /* naveBarShowSrvice */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_12__services_navBarService__["a" /* naveBarShowSrvice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], Setting);
    return Setting;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Profile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_navBarService__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Profile = /** @class */ (function () {
    function Profile(navCtrl, _navBar, toastCtrl) {
        this.navCtrl = navCtrl;
        this._navBar = _navBar;
        this.toastCtrl = toastCtrl;
        this.testArry = [];
        this.profileDiv = true;
        this.Editdiv = false;
        this.spinnerLoad = false;
        this.profile = { fname: "", lname: "", bday: "", phone: "", city: "", zip: "", country: "" };
    }
    Profile.prototype.ngOnInit = function () {
        this.profileDiv = true;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.testArry.push(this.userData);
        this.UserName = this.testArry[0].profile.firstName;
    };
    Profile.prototype.edit = function () {
        this.Editdiv = true;
        this.profileDiv = false;
    };
    Profile.prototype.profileSubmit = function (form) {
        var _this = this;
        if (form.valid) {
            this.spinnerLoad = true;
            this.activeUID = this.userData._id;
            this.firstName = this.testArry[0].profile.firstName;
            this.Lastname = this.testArry[0].profile.lastName;
            this.birthDay = this.profile.bday;
            this.phoneNo = this.testArry[0].profile.phone;
            this.city = this.testArry[0].profile.city;
            this.zip = this.profile.zip;
            this.country = this.testArry[0].profile.country;
            console.log(this.activeUID, this.firstName, this.Lastname, this.phoneNo, this.city, this.country, "credetioal details");
            this._navBar.profileEdit(this.activeUID, this.firstName, this.Lastname, this.phoneNo, this.city, this.country).subscribe(function (res) {
                if (res.status) {
                    localStorage.setItem('userData', JSON.stringify(_this.testArry[0]));
                    _this.UserName = _this.testArry[0].profile.firstName;
                    _this.profile.phone = _this.userData.profile.phone;
                    _this.profile.city = _this.userData.profile.city;
                    _this.profile.country = _this.userData.profile.country;
                    _this.Editdiv = false;
                    _this.profileDiv = true;
                    _this.spinnerLoad = false;
                    var toast = _this.toastCtrl.create({
                        message: 'You profile has been updated',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'OOPS! Something went wrong',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
                // this.tostr.notify("You profile has been updated", "success");
            }, function (error) {
                _this.spinnerLoad = false;
                var toast = _this.toastCtrl.create({
                    message: 'OOPS! Something went wrong',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            });
        }
    };
    Profile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/profile/profile.html"*/'<ion-header>\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Profile</ion-title>\n    </ion-navbar>\n  </ion-header>\n\n<ion-content parallax-header no-bounce class="bg-modal" *ngFor="let item of testArry">\n    <div class="header-image"><img src="assets/img/background/background-2.jpg"></div>\n    <div class="main-content" text-wrap text-center *ngIf="profileDiv">\n        <div class="circular"> <img src="assets/img/logo.png"></div>\n        <h2 >{{UserName}}</h2>\n        <div align="center">\n           \n            <ion-spinner  *ngIf="spinnerLoad"></ion-spinner>\n           \n          </div>\n        <!-- <p>{{user.occupation}} &bull; {{user.location}}</p>\n        <p class="profile-description">{{user.description}}</p> -->\n        <hr>\n        <ion-list no-lines >\n            <ion-item *ngIf="item.username">\n                <ion-icon name="mail" color="primary" item-start style="color:#108fd2;"></ion-icon>\n                <p>Email</p>\n                {{ item.username }}\n                <!-- <a button ion-button href="mailto:{{user.email}}" color="primary" item-end medium>Open App</a> -->\n            </ion-item>\n            <ion-item *ngIf="item.profile.phone">\n                <ion-icon item-start name="call" color="danger"></ion-icon>\n                <p>Mobile</p>\n                {{item.profile.phone}}\n                \n            </ion-item>\n            <!-- <ion-item>\n                <ion-icon item-start name="logo-whatsapp" color="secondary"></ion-icon>\n                <p>Whatsapp</p>\n                {{user.whatsapp}}\n                <a button ion-button color="secondary" item-end medium href="whatsapp://send?text=Hi from ionic&phone=+{{user.phone}}&abid=+{{user.phone}}">Open App</a>\n            </ion-item> -->\n            <ion-item *ngIf="item.profile.city">\n                <ion-icon item-start name="pin" color="twitter"></ion-icon>\n                <p>{{item.profile.city}}</p>\n            </ion-item>\n        </ion-list>\n     \n        <hr>\n        <button ion-button block color="linkedin" icon-start (click)="edit()" style="background-color:#108fd2;">\n            <ion-icon name=\'md-create\'   ></ion-icon>\n            Edit\n        </button>\n    \n      \n    </div>\n    <div class="main-content" text-wrap text-center *ngIf="Editdiv">\n        <div class="circular"> <img src="assets/img/logo.png"></div>\n        <form class="form-horizontal" role="form" #profileForm="ngForm" (ngSubmit)="profileSubmit(profileForm)" novalidate>\n        <ion-list >\n                <ion-item>\n                        <ion-label>Name</ion-label>\n                        <ion-input type="text"  class="form-control" [(ngModel)]="item.profile.firstName" name="fname" type="text" #fname="ngModel"  required></ion-input>\n                    \n          \n          \n                      </ion-item>\n                    <ion-item >\n                            <ion-label>e-mail</ion-label>\n                            <ion-input type="text"  class="form-control" [(ngModel)]="item.username" name="email" type="text" #emails="ngModel" disabled>\n                          </ion-input>\n              \n              \n                          </ion-item>\n                          <ion-item>\n                                <ion-label>Phone-no</ion-label>\n                                <ion-input type="text" class="form-control" id="phone"\n                                \n                                [(ngModel)]="item.profile.phone" name="phone"\n                                pattern="[0-9]*"\n                                minlength="10"\n                                maxlength="10" \n                                #mobiles="ngModel">\n                               </ion-input>\n                  \n                  \n                              </ion-item>\n                              <ion-item>\n                                    <ion-label>City</ion-label>\n                                    <ion-input type="text"  class="form-control" [(ngModel)]="item.profile.city" name="city"  #city="ngModel">\n                                  </ion-input>\n                      \n                      \n                                  </ion-item>\n                                  <ion-item>\n                                        <ion-label>Country</ion-label>\n                                        <ion-input type="text"  class="form-control" [(ngModel)]="item.profile.country" name="country"  #country="ngModel">\n                                       </ion-input>\n                          \n                          \n                                      </ion-item>\n        </ion-list>\n\n       \n      \n     \n  \n        <button ion-button block color="linkedin" icon-start  [disabled]="!profileForm.form.valid" style="background-color:#108fd2;">\n            <ion-icon name=\'md-create\'></ion-icon>\n            Update\n        </button>\n        </form>\n    \n      \n    </div>\n\n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/profile/profile.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_navBarService__["a" /* naveBarShowSrvice */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_navBarService__["a" /* naveBarShowSrvice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */]])
    ], Profile);
    return Profile;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return About; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_app_version__ = __webpack_require__(1008);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__commen_setting_privacyPolicy_privacyPolicy__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__commen_setting_returnPolicy_returnPolicy__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__commen_setting_contactUs_contactUs__ = __webpack_require__(580);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var About = /** @class */ (function () {
    function About(navCtrl, appVersion) {
        this.navCtrl = navCtrl;
        this.appVersion = appVersion;
    }
    About.prototype.ngOnInit = function () {
        this.getAppversion();
        // this.appversions=this.appVersion.getVersionNumber();
    };
    About.prototype.getAppversion = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.appVersion.getVersionNumber()];
                    case 1:
                        _a.appversions = _b.sent();
                        console.log("appversion", this.appversions);
                        return [2 /*return*/];
                }
            });
        });
    };
    About.prototype.privacy = function () {
        console.log("privacy");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__commen_setting_privacyPolicy_privacyPolicy__["a" /* privacyPolicy */]);
    };
    About.prototype.return = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__commen_setting_returnPolicy_returnPolicy__["a" /* returnPolicy */]);
    };
    About.prototype.contact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__commen_setting_contactUs_contactUs__["a" /* contactUs */]);
    };
    About = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/about/about.html"*/'<ion-header>\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>AboutUs</ion-title>\n    </ion-navbar>\n  </ion-header>\n    <ion-content class="bgpic" >\n      <ion-grid style="background-color: white; " style=" background-color:#108fd2; color:white; border: 2px solid #108fd2; border-radius: 15px 15px 0px 0px;">\n          <ion-row>\n              <ion-col col-12>\n                <img src="assets/img/logo.png" style="width:95px;height:100px; display: block; margin:auto;">\n                \n              </ion-col>\n              <ion-col col-12>\n                    <p class="version"> V{{appversions}}</p>\n              </ion-col>\n\n          </ion-row>\n          <ion-row >\n              <p class="paragraph">\n\n                    Nextfellow Pvt Ltd is an ed-tech company working with a vision to build a platform where everyone can learn, share and contribute to global education. Currently, our focus is on medical sector. We are building a website and mobile app for medical students to realize their dream of postgraduation.\n\n              </p>\n          </ion-row>\n        \n               \n          \n      </ion-grid>\n      <ion-list style="border: 1px solid #108fd2; background-color:#108fd2;">\n            <ion-item (click)="privacy()">\n              \n               <b>Privacy Policy</b> \n              <ion-icon name="ios-arrow-forward-outline"  item-end></ion-icon>\n            </ion-item>\n            <ion-item (click)="return()">\n              \n                    <b>Return Policy</b> \n                   <ion-icon name="ios-arrow-forward-outline"   item-end></ion-icon>\n                 </ion-item>\n                 <ion-item (click)="contact()">\n              \n                        <b>Contact us</b> \n                       <ion-icon name="ios-arrow-forward-outline"   item-end></ion-icon>\n                     </ion-item>\n            \n          </ion-list>\n      <!-- <ion-list>\n                    \n            <button ion-item  (click)="itemSelected(item)">\n             privacy\n            </button>       <ion-icon name="ios-arrow-forward-outline" item-end></ion-icon>\n\n            <button ion-item  (click)="itemSelected(item)">\n                    privacy\n                   </button> \n                   <button ion-item  (click)="itemSelected(item)">\n                        privacy\n                       </button>  \n          </ion-list> -->\n         \n      </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/about/about.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__ionic_native_app_version__["a" /* AppVersion */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_app_version__["a" /* AppVersion */]])
    ], About);
    return About;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return faq; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var faq = /** @class */ (function () {
    function faq(navCtrl, examservice) {
        this.navCtrl = navCtrl;
        this.examservice = examservice;
        this.shownGroup = null;
        this.noteQstn = [];
        this.dashQstn = [];
        this.learnQstn = [];
        this.examQstn = [];
        this.regQstn = [];
    }
    faq.prototype.ngOnInit = function () {
        var _this = this;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        console.log("userdata", this.userData.userId);
        this.userId = this.userData.userId;
        console.log("userid,", this.userId);
        this.examservice.faq(this.userId).subscribe(function (res) {
            console.log("res", res);
            if (res.status) {
                console.log("resdata", res.data);
                _this.questionData = res.data;
                for (var i = 0; i < _this.questionData.length; i++) {
                    if (_this.questionData[i].category == "Note") {
                        _this.noteHead = _this.questionData[i].category;
                        _this.noteQstn.push({ "quiestion": _this.questionData[i].Questions, "answer": _this.questionData[i].answer });
                        console.log(_this.noteQstn, "this.noteQstnthis.noteQstn");
                    }
                    else if (_this.questionData[i].category == "Dashboard") {
                        _this.dashbrdHead = _this.questionData[i].category;
                        _this.dashQstn.push({ "quiestion": _this.questionData[i].Questions, "answer": _this.questionData[i].answer });
                    }
                    else if (_this.questionData[i].category == "Learn") {
                        _this.learnHead = _this.questionData[i].category;
                        _this.learnQstn.push({ "quiestion": _this.questionData[i].Questions, "answer": _this.questionData[i].answer });
                    }
                    else if (_this.questionData[i].category == "Exam Room") {
                        _this.examHead = _this.questionData[i].category;
                        _this.examQstn.push({ "quiestion": _this.questionData[i].Questions, "answer": _this.questionData[i].answer });
                    }
                    else {
                        _this.regHead = _this.questionData[i].category;
                        _this.regQstn.push({ "quiestion": _this.questionData[i].Questions, "answer": _this.questionData[i].answer });
                        console.log("this.regQstn", _this.regQstn);
                    }
                }
            }
        });
    };
    faq.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    faq.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    faq = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-faq',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/FAQ/faq.html"*/'<ion-header>\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>FAQ</ion-title>\n    </ion-navbar>\n  </ion-header>\n  <ion-content class="bgpic">\n    <ion-row>\n    <ion-col col-12 class="accordion" >\n      <ul>\n        <li style="margin-top:10px;">\n          <a href="#" class="acc-handle" tabindex="0" style="font-size:20px;background-color:#f0f0f0;">Dashboard</a>\n          <div class="acc-panel">\n<ion-list>\n<ion-item  text-wrap *ngFor="let item of dashQstn;let i=index;" style="background-color:#fffeee;">\n  <ion-row  style=" border-bottom: 1px solid  gray;">\n      <ion-col col-2 style="font-size: 18px;" >\n        <span>{{i+1}}.</span>\n      </ion-col>\n      <ion-col col-10 style="font-size: 18px;" >\n        <span >{{item.quiestion}}</span>\n      </ion-col>\n    </ion-row>\n\n    \n        <ion-row>\n            <ion-col col-2 style="font-size: 18px;" >\n                Ans.\n              </ion-col>\n          <ion-col col-10 style="font-size: 18px;" >{{item.answer}}</ion-col>\n        </ion-row>\n     \n\n\n</ion-item>\n</ion-list>  \n  </div>\n        </li>\n        <li >\n          <a href="#" class="acc-handle" tabindex="0" style="font-size:20px;background-color:#f0f0f0;">Exam</a>\n          <div class="acc-panel">\n              <ion-list>\n              <ion-item   text-wrap *ngFor="let item of examQstn;let i=index;"  style="background-color:#fffeee;">\n                <ion-row style=" border-bottom: 1px solid  gray;" >\n                    <ion-col col-2 style="font-size: 18px;" >\n                      <span>{{i+1}}.</span>\n                    </ion-col>\n                    <ion-col col-10 style="font-size: 18px;" >\n                      <span >{{item.quiestion}}</span>\n                    </ion-col>\n                  </ion-row>\n              \n                \n               \n                      <ion-row >\n                          <ion-col col-2 style="font-size: 18px;" >\n                              Ans.\n                            </ion-col>\n                        <ion-col col-10 style="font-size: 18px;" >{{item.answer}}</ion-col>\n                      </ion-row>\n                  \n              \n              \n              </ion-item>\n              </ion-list>  \n                </div>\n        </li>\n        <li>\n            <a href="#" class="acc-handle" tabindex="0"  style="font-size:20px;background-color:#f0f0f0;">Learn</a>\n            <div class="acc-panel">\n                <ion-list>\n                <ion-item   text-wrap *ngFor="let item of learnQstn;let i=index;" style="background-color:#fffeee;">\n                  <ion-row style=" border-bottom: 1px solid  gray;"   >\n                      <ion-col col-2    >\n                        <span style="font-size: 18px;">{{i+1}}.</span>\n                      </ion-col>\n                      <ion-col col-10 >\n                        <span style="font-size: 18px;" >{{item.quiestion}}</span>\n                      </ion-col>\n                    </ion-row>\n                \n                  \n                   \n                        <ion-row  >\n                            <ion-col col-2 style="font-size: 18px;"  >\n                                Ans.\n                              </ion-col>\n                          <ion-col col-10 style="font-size: 18px;"  >{{item.answer}}</ion-col>\n                        </ion-row>\n                      \n                \n                \n                </ion-item>\n                </ion-list>  \n                  </div>\n          </li>\n          <li>\n              <a href="#" class="acc-handle" tabindex="0" style="font-size:20px;background-color:#f0f0f0;">Subscribe</a>\n              <div class="acc-panel">\n                  <ion-list>\n                  <ion-item  text-wrap *ngFor="let item of regQstn;let i=index;"  style="background-color:#fffeee;" >\n                    <ion-row  style=" border-bottom: 1px solid  gray;" >\n                        <ion-col col-2 style="font-size: 18px;" >\n                          <span>{{i+1}}.</span>\n                        </ion-col>\n                        <ion-col col-10 style="font-size: 18px;" >\n                          <span >{{item.quiestion}}</span>\n                        </ion-col>\n                      </ion-row>\n                  \n                    \n                     \n                          <ion-row  >\n                              <ion-col col-2 style="font-size: 18px;" >\n                                  Ans.\n                                </ion-col>\n                            <ion-col col-10 style="font-size: 18px;"  >{{item.answer}}</ion-col>\n                          </ion-row>\n                      \n                  \n                  \n                  </ion-item>\n                  </ion-list>  \n                    </div>\n            </li>\n        <li style="margin-bottom:10px;">\n          <a href="#" class="acc-handle" tabindex="0"  style="font-size:20px;background-color:#f0f0f0; ">Note</a>\n          <div class="acc-panel">\n              <ion-list >\n              <ion-item  text-wrap *ngFor="let item of noteQstn;let i=index;"  style="background-color:#fffeee;" >\n                <ion-row  style=" border-bottom: 1px solid  gray;" >\n                    <ion-col col-2 style="font-size: 18px;" >\n                      <span>{{i+1}}.</span>\n                    </ion-col>\n                    <ion-col col-10 style="font-size: 18px;" >\n                      <span >{{item.quiestion}}</span>\n                    </ion-col>\n                  </ion-row>\n              \n                \n                  <ion-grid >\n                      <ion-row  >\n                          <ion-col col-2 style="font-size: 18px;" >\n                              Ans.\n                            </ion-col>\n                        <ion-col col-10 style="font-size: 18px;" >{{item.answer}}</ion-col>\n                      </ion-row>\n                    </ion-grid>\n              \n              \n              </ion-item>\n              </ion-list>  \n                </div>\n        </li>\n      \n      </ul>\n    </ion-col>\n  </ion-row>\n      </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/FAQ/faq.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]])
    ], faq);
    return faq;
}());

//# sourceMappingURL=faq.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return teachers; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var teachers = /** @class */ (function () {
    function teachers(navCtrl, examservice) {
        this.navCtrl = navCtrl;
        this.examservice = examservice;
        this.url = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].backendUrl + '/images/';
    }
    teachers.prototype.ngOnInit = function () {
        var _this = this;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        console.log("userdata", this.userData.userId);
        this.userId = this.userData.userId;
        console.log("userid,", this.userId);
        this.examservice.viewTeachers(this.userId).subscribe(function (res) {
            console.log("res", res);
            if (res.status) {
                _this.teachersData = res.data;
                console.log("texhers data", _this.teachersData);
            }
            else {
                console.log("no data");
            }
        });
    };
    teachers = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-teachers',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/teachersPage/teachers.html"*/'<ion-header>\n        <ion-navbar>\n          <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n          </button>\n          <ion-title>Teachers</ion-title>\n        </ion-navbar>\n      </ion-header>\n      <ion-content padding class="bgpic">\n           \n<!-- <ion-list>\n        <ion-item *ngFor="let item of teachersData;let i=index;">\n          <ion-avatar item-start>\n            <img class="img-responsive"  [src]=\'url + item.imageId\'>\n          </ion-avatar>\n          <h2>{{item.name}}</h2>\n          <h3>{{item.role}}</h3>\n          <h3>{{item.qualification}}</h3>\n          <p>{{item.message}}</p>\n        </ion-item>\n        \n      </ion-list>  \n      \n      -->\n      <ion-list>\n        <ion-card class="cdcolr"  *ngFor="let item of teachersData;let i=index; style">\n          <ion-row>\n            <ion-col col-12 class="six" >\n          <ion-col col-12 class="imgb">\n            <img  class="img-responsive" style=" width:95px;  border-radius: 50%;\n            height:100px; display: block; margin: auto; margin-top:4px; \n        " alt="Avatar"  [src]=\'url + item.imageId\'>\n          </ion-col>\n          <ion-col col-12 style="margin-top: 4px;">\n            <h2 class="algn"><b>{{item.name}}</b></h2>\n            <h3 class="algnc"><b>{{item.role}}</b></h3>\n            <h3 class="algn">{{item.qualification}}</h3>\n          </ion-col>\n          <p class="algn-q" ><i class="fas fa-quote-left"></i>"{{item.message}}"</p>\n        </ion-col>\n        </ion-row>\n        </ion-card>\n\n      </ion-list>\n          </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/teachersPage/teachers.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]])
    ], teachers);
    return teachers;
}());

//# sourceMappingURL=teachers.js.map

/***/ }),

/***/ 245:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 245;

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    //  backendUrl: 'http://localhost:8081',
    backendUrl: 'https://d4v.nextfellow.com',
    //backendUrl: 'https://d4v.nextfellow.in',
    firebaseConfig: {
        apiKey: "AIzaSyAF9h3usyyt8LvOU7IAxO_ZEPCPfI_-kq0",
        authDomain: "nextfellow-1521033569049.firebaseapp.com",
        databaseURL: "https://nextfellow-1521033569049.firebaseio.com",
        projectId: "nextfellow-1521033569049",
        storageBucket: "nextfellow-1521033569049.appspot.com",
        messagingSenderId: "885727504851"
    }
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthService = /** @class */ (function () {
    function AuthService(http, Toaser) {
        this.http = http;
        this.Toaser = Toaser;
        this.loggedIn = false;
        this.url = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].backendUrl;
    }
    AuthService.prototype.login = function (email, password) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(this.url + "/login", JSON.stringify({ email: email, password: password }), { headers: headers })
            .map(function (res) { return res.json(); })
            .map(function (res) {
            console.log("Result: ", res);
            if (res.status)
                _this.loggedIn = true;
            else
                _this.loggedIn = false;
            return res;
        });
    };
    AuthService.prototype.logout = function (activeUserId) {
        var _this = this;
        var data = {
            activeUserId: activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(this.url + "/logout", JSON.stringify(data), { headers: headers })
            .map(function (res) { return res.json(); })
            .map(function (res) {
            localStorage.removeItem('userData');
            localStorage.removeItem('subjectData');
            _this.loggedIn = false;
            return res;
        });
    };
    AuthService.prototype.isLoggedIn = function () {
        return this.loggedIn;
    };
    AuthService.prototype.signup = function (firstName, email, password) {
        var _this = this;
        var data = {
            email: email,
            password: password,
            firstName: firstName,
            lastName: ""
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(this.url + "/signup", JSON.stringify(data), { headers: headers })
            .map(function (res) { return res.json(); })
            .map(function (res) {
            if (res.status === true) {
                console.log("Response from AuthService: ", res);
                _this.loggedIn = true;
            }
            else {
                console.log(res.status);
            }
            return res;
        });
    };
    AuthService.prototype.forgotPassword = function (email) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/login/reset", email).map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    AuthService.prototype.resetPassword = function (resetCode, pass) {
        var data = {
            "newPassword": pass,
            "resetCode": resetCode
        };
        return this.http.post(this.url + "/login/reset/newpassword", data).map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    AuthService.prototype.verifyEmail = function (code) {
        return this.http.get(this.url + "/login/verify/" + code)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    AuthService.prototype.facebookLogin = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', 'http://devservice.eslurn.com');
        headers.append('Access-Control-Allow-Credentials', 'true');
        return this.http.get(this.url + "/login/facebook/", { withCredentials: true })
            .map(function (res) { return console.log("response from fb", res); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    AuthService.prototype.notificationInfo = function (message) {
        var toast = this.Toaser.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    AuthService.prototype.loginWithFirebase = function (userinfo) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/firebase-login", userinfo).map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ToastController */]])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExamService = /** @class */ (function () {
    function ExamService(http) {
        this.http = http;
        this.url = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].backendUrl;
        var userData = JSON.parse(localStorage.getItem('userData'));
        console.log("userData", userData);
        this.activeUserId = userData._id;
        this.userDetailsId = userData.userDetailsId;
        this.userId = userData.userId;
    }
    ExamService.prototype.registerWeeklyExam = function (paramss) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        paramss.activeUserId = this.activeUserId,
            paramss.userId = this.userId;
        console.log(paramss, "date to send");
        return this.http.post(this.url + "/week_exam_reg", paramss, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.SinglePaymentExam = function (paramss) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        paramss.activeUserId = this.activeUserId,
            paramss.userId = this.userId;
        console.log(paramss, "date to send");
        return this.http.post(this.url + "/week_exam_single_reg", paramss, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.loadPackageExams = function (userExamId, packageId) {
        var data = {
            "activeUserId": this.activeUserId,
            "examId": userExamId,
            "packageId": packageId,
            "userId": this.userId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/load-package-exam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.createExam = function (examName, examconfig, numOfQuestion, dueflag, dueObj, timeflag, minutes, hours) {
        var data = {
            'userId': this.userId,
            'activeUserId': this.activeUserId,
            'examName': examName,
            'examConfig': examconfig,
            'noOfQuestions': numOfQuestion,
            'dueFlag': dueflag,
            'dueObj': dueObj,
            'timeFlag': timeflag,
            'timeObj': {
                'hour': hours,
                'minute': minutes
            }
        };
        console.log("Data to send nidhfdsfjksafasfnkjasfnkj", data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/createExam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.startExam = function (examId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'examId': examId
        };
        console.log("Data sending from startExam");
        return this.http.post(this.url + "/startExam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getSubscribeExam = function () {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId
        };
        console.log("Data sending from startExam");
        return this.http.post(this.url + "/get-subscribed-exams", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.viewTeam = function () {
        return this.http.get('http://localhost:3000/api/view/team')
            .map(function (res) { return res.json(); });
    };
    // getDailyExams(){
    //   let data = {
    //     'activeUserId': this.activeUserId
    //   };
    //   return this.http.post(`${this.url}/fetch-daily-exams`, data)
    //   .map(res => res.json())
    //   .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    // }
    ExamService.prototype.loadExam = function (userExamId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userExamId': userExamId,
            'userId': this.userId
        };
        return this.http.post(this.url + "/loadExam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.faq = function (userId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': userId
        };
        console.log("examservicedata", data);
        return this.http.post(this.url + "/dynamicFaqGet", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.viewTeachers = function (userId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': userId
        };
        console.log("examservicedata", data);
        return this.http.post(this.url + "/viewallteachers", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getExamQuestions = function (userExamId, questionId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'userExamId': userExamId,
            'questionId': questionId
        };
        return this.http.post(this.url + "/getExamQuestion", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getAiimsQuestions = function (userExamId, questionId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'questionId': questionId,
            'userExamId': userExamId
        };
        return this.http.post(this.url + "/fetch-package-question", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.finishAiimsExam = function (userExamId, examId, examType) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'userExamId': userExamId,
            'examId': examId,
            'examType': examType
        };
        return this.http.post(this.url + "/finish-package-exam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.markAiimsQuestions = function (questionId, userExamId, markType) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'questionId': questionId,
            'userExamId': userExamId,
            'markType': markType
        };
        return this.http.post(this.url + "/mark-package-exam-questions", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getResultScoreAiims = function (examId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'examId': examId,
        };
        return this.http.post(this.url + "/get-top-rank-package-exam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getResultAiims = function (userExamId, examId, examType, resultType) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'userExamId': userExamId,
            'examId': examId,
            'examType': examType,
            'resultType': resultType
        };
        return this.http.post(this.url + "/get-package-result-details", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.saveExamAnswer = function (index, userExamId, examId, questionId, answer, result, updateFlag, subjectArray) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var FirstCallData = {
            'activeUserId': this.activeUserId,
            'answer': answer,
            'examId': examId,
            'questionId': questionId,
            'result': result,
            'subjectArray': subjectArray,
            'updateFlag': updateFlag,
            'userExamId': userExamId,
            'userId': this.userId,
            'questionStatus': 'answered',
            'qPaletteIndex': index
        };
        console.log("Save answer sending to db :", FirstCallData);
        return this.http.post(this.url + "/saveExamAnswer", FirstCallData, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.saveAiimsExamAnswer = function (questionId, examId, userExamId, result, answer, examType, marked) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'questionId': questionId,
            'userExamId': userExamId,
            'examId': examId,
            'examType': examType,
            'answer': answer,
            'marked': marked,
            'result': result
        };
        return this.http.post(this.url + "/save-package-exam-answers", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.fetchAllExam = function (mode) {
        console.log("mode", mode);
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'mode': mode
        };
        return this.http.post(this.url + "/fetchExams", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.checkAppliedForExam = function () {
        var data = {
            'activeUserId': this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/get-payment-weeklyexam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.fetchGrandExam = function () {
        var data = {
            'activeUserId': this.activeUserId
        };
        return this.http.post(this.url + "/pending-grand-exam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.finishExam = function (userExamId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'userExamId': userExamId
        };
        return this.http.post(this.url + "/finishExam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.deleteExam = function (examId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'examId': examId
        };
        console.log("Data sending for delete", data);
        return this.http.post(this.url + "/deleteExam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.createGrandExam = function (examName, examconfig, numOfQuestion, dueflag, timeflag, minutes, hours, dueObj, examType) {
        var data = {
            'userId': this.userId,
            'activeUserId': this.activeUserId,
            'name': examName,
            'examConfig': examconfig,
            'numberOfQuestion': numOfQuestion,
            'dueFlag': dueflag,
            'timeFlag': timeflag,
            'dueObj': dueObj,
            'examType': examType,
            'timeDetails': {
                'hour': hours,
                'minut': minutes
            }
        };
        console.log("Data to send to sever", data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/create-grand-exam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.loadGrandExam = function (grandExamId) {
        var data = {
            'activeUserId': this.activeUserId,
            'grandExamId': grandExamId,
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/load-grant-exam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getGrandExamQuestions = function (userExamId, questionId) {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId,
            'grandExamId': userExamId,
            'questionId': questionId
        };
        return this.http.post(this.url + "/get-grand-exam-question", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.saveGrandExamAnswer = function (userExamId, currentPaletteId, questionStatus, questionId, answer, result, updateFlag, subjectArray) {
        var data = {
            "activeUserId": this.activeUserId,
            "grandExamId": userExamId,
            "questionStatus": questionStatus,
            "qPaletteIndex": currentPaletteId,
            "questionId": questionId,
            "answer": answer,
            "subjectArray": subjectArray,
            "result": result,
            "updateFlag": updateFlag,
        };
        return this.http.post(this.url + "/save-grand-exam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.finishGrandExam = function (userExamId) {
        var data = {
            "activeUserId": this.activeUserId,
            "grandExamId": userExamId,
        };
        return this.http.post(this.url + "/finish-grant-exam", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.deleteGrandExam = function (userExamId) {
        var data = {
            "activeUserId": this.activeUserId,
            "grandExamId": userExamId
        };
        console.log("Data to delete grand", data);
        return this.http.post(this.url + "/delete-grand-exam-question", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getQuestions = function (userExamId, examType, resultType) {
        var data = {
            "activeUserId": this.activeUserId,
            "examId": userExamId,
            "examType": examType,
            "resultType": resultType
        };
        return this.http.post(this.url + "/result-details", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getLeaderboard = function (userexamid) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "examId": userexamid
        };
        console.log(data, "sending");
        return this.http.post(this.url + "/get-top-rank", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getUserResult = function (userexamid) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "userExamId": userexamid
        };
        return this.http.post(this.url + "/get-week-exam-details", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getResult = function (userexamid, category) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "userExamId": userexamid,
            "type": category
        };
        return this.http.post(this.url + "/get-answered-questions", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.updateExamAnswer = function (index, answer, result, userExamAnswerId, userExamId, examId, questionId) {
        var secondCall = {
            'userExamAnswerId': userExamAnswerId,
            'activeUserId': this.activeUserId,
            'answer': answer,
            'result': result,
            'updateFlag': true,
            'userExamId': userExamId,
            'questionStatus': 'answered',
            'userId': this.userId,
            'qPaletteIndex': index,
            'examId': examId,
            'questionId': questionId,
        };
        return this.http.post(this.url + "/saveExamAnswer", secondCall)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.markExam = function (userExamId, questionId, status) {
        var data = {
            "userExamId": userExamId,
            "questionId": questionId,
            "type": status
        };
        return this.http.post(this.url + "/mark-answer", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.markGrandExamQuestion = function (userExamId, questionId, status) {
        var data = {
            "activeUserId": this.activeUserId,
            "grandExamId": userExamId,
            "questionId": questionId,
            "type": status
        };
        return this.http.post(this.url + "/mark-grand-exam-question", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService.prototype.getWeekExams = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            activeUserId: this.activeUserId
        };
        return this.http.post(this.url + "/get-weekend-exam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ExamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ExamService);
    return ExamService;
}());

//# sourceMappingURL=exam.service.js.map

/***/ }),

/***/ 290:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 290;

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_learn_list__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_note_note__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_exam_createexam_createExam__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_student_service__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_learn_questions_questions__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_exam_weekend_weekend_component__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_exam_exam_room_details_details__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_exam_createexam_write_exam_write_exam__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_evaluate_evaluate__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__commen_login_login__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_exam_examNew_examComponent__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_navBarService__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_homeNew_homeNew__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













// import { NestedTab1Page } from '../../pages/exam/exam-room/completeExam/nested-tab1';







var HomePage = /** @class */ (function () {
    function HomePage(params, events, myTabs, _navBar, navCtrl, modalCtrl, examService, viewCtrl, http, studentService) {
        this.params = params;
        this.events = events;
        this.myTabs = myTabs;
        this._navBar = _navBar;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.examService = examService;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.studentService = studentService;
        this.welcomeDiv = false;
        this.studyonly = false;
        this.examonly = false;
        this.learnDiv = true;
        this.learnBig = false;
        this.GreenDivF = true;
        this.liveDiv = true;
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.googleParam = JSON.parse(localStorage.getItem('googledata'));
        console.log(this.googleParam, "my data here just test");
        this.events.publish('user:created', false, Date.now());
        //  if(this.googleParam){
        //   this.navCtrl.setRoot(homeNew);
        //   localStorage.removeItem('googledata');    
        //   console.log(this.googleParam,"my data here just test");
        // }
    }
    // createUser(user) {
    //   console.log('User created!')
    //   this.events.publish('user:created', user, Date.now());
    // }
    HomePage.prototype.ngAfterViewInit = function () {
        if (this.googleParam) {
            console.log('itsworking');
            localStorage.removeItem('googledata');
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_18__pages_homeNew_homeNew__["a" /* homeNew */]);
            console.log(this.googleParam, "my data here just test");
        }
    };
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.date = new Date();
        this.userData = JSON.parse(localStorage.getItem('userData'));
        console.log(this.userData, "user data");
        if (!this.userData) {
            localStorage.removeItem('userData');
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__commen_login_login__["a" /* Login */]);
        }
        this.greetingFn();
        this.username = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        //dashbord info
        this.studentService.dashboardInfo().subscribe(function (res) {
            _this.dashInfo = res.data;
            if (_this.dashInfo.message.message) {
                _this.live = _this.dashInfo.message.message;
                _this.liveDiv = true;
            }
            else {
                _this.liveDiv = false;
            }
            if (res.data.exam && res.data.study) {
                for (var _i = 0, _a = res.data.study.subjectArray; _i < _a.length; _i++) {
                    var item = _a[_i];
                    if (item.title == "chapter")
                        _this.chapterId = item.id;
                    if (item.title == "subject")
                        _this.subjectId = item.id;
                }
                _this.examonly = true;
                _this.studyonly = true;
                _this.examName = res.data.exam.examName;
                _this.examId = res.data.exam._id;
                _this.chapterName = res.data.study.subjectArray[2].name;
                _this.sectionName = res.data.study.subjectArray[0].name;
            }
            else if (!res.data.exam && res.data.study) {
                _this.studyonly = true;
                _this.examonly = false;
                _this.chapterName = res.data.study.subjectArray[2].name;
                _this.sectionName = res.data.study.subjectArray[0].name;
            }
            else if (res.data.exam && !res.data.study) {
                _this.examonly = true;
                _this.studyonly = false;
                _this.examName = res.data.exam.examName;
            }
            else {
                _this.examonly = false;
                _this.studyonly = false;
            }
            if ((_this.chapterName) && (_this.sectionName)) {
                _this.learnDiv = true;
                _this.learnBig = false;
                _this.GreenDivF = true;
                _this.welcomeDiv = false;
            }
            else {
                _this.learnBig = true;
                _this.liveDiv = false;
                _this.learnDiv = false;
                _this.welcomeDiv = true;
                _this.GreenDivF = false;
            }
        }, function (error) {
            console.log(error);
            localStorage.removeItem('userData');
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__commen_login_login__["a" /* Login */]);
        });
        var data = {
            'activeUserId': this.userData._id,
            'userId': this.userData.userId
        };
        this.http.post(__WEBPACK_IMPORTED_MODULE_8__environments_environment__["a" /* environment */].backendUrl + "/getSubjects", data)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            localStorage.setItem('subjectData', JSON.stringify(res.data));
        }, function (error) { });
    };
    HomePage.prototype.myShowFunction = function () {
    };
    HomePage.prototype.myHideFunction = function () {
    };
    HomePage.prototype.greetingFn = function () {
        var time = this.date.getHours();
        if (this.date.getHours() < 12 && this.date.getHours() > 0) {
            this.greeting = "Good Morning";
        }
        else if (this.date.getHours() >= 12 && this.date.getHours() <= 17) {
            this.greeting = "Good Afternoon";
        }
        else {
            this.greeting = "Good evening";
            console.log(this.date.getHours());
        }
    };
    HomePage.prototype.firtChap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_learn_list__["a" /* ListPage */]);
    };
    HomePage.prototype.grantTest = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_createexam_createExam__["a" /* CreateExam */]);
    };
    HomePage.prototype.gotoLearn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_learn_questions_questions__["a" /* Questions */], { subject: this.chapterId });
    };
    HomePage.prototype.golive = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    HomePage.prototype.goLastExam = function () {
        var _this = this;
        this.examService.loadExam(this.dashInfo.exam._id).subscribe(function (res) {
            if (res.data.userExamObj) {
                (res.data.userExamObj.status == "completed") ? _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: res.data.userExamObj.examId }) : _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */], { subject: _this.dashInfo.exam._id });
            }
            else
                console.log("OOPS ! Something went wrong.", "error");
        });
    };
    HomePage.prototype.gotoLeranPage = function () {
        this.myTabs.indexPass(1);
        // this._navBar.indexPass(1)
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_learn_list__["a" /* ListPage */]);
    };
    HomePage.prototype.gotoExamPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    HomePage.prototype.gotoEvaluatePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__pages_evaluate_evaluate__["a" /* Evaluate */]);
    };
    HomePage.prototype.writeExam = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_note_note__["a" /* Note */]);
    };
    HomePage.prototype.mockreg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_exam_weekend_weekend_component__["a" /* weekendReg */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/dashboard/home.html"*/'<style>\n    .card-md {\n        margin-left: -9px !important;\n        border-radius: 2px!important;\n        width: calc(100% - -17px)!important;\n    }\n\n\n    .swiper-pagination-bullet .swiper-pagination-bullet-active {\n\n        margin-top: 10px;\n\n    }\n</style>\n\n\n<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">\n\n<!-- <ion-header style="background-color: #108fd2; padding:7px;">\n\n\n  \n        <span style="color: #fff;font-size:22px;font-family: \'Roboto\', sans-serif !important;font-weight:800; padding-left: 33px;    \n            ">nextfellow</span>\n</ion-header> -->\n<ion-header >\n        <ion-navbar  style="color:#108fd2;">\n          <!-- <button ion-button menuToggle >\n            <ion-icon name="menu"></ion-icon>\n          </button> -->\n          <ion-title >nextfellow</ion-title>\n        </ion-navbar>\n      </ion-header>\n\n<ion-content padding style="background-color: #efefef;">\n\n    \n    <!-- <button ion-button secondary menuToggle>  <ion-icon name="ios-apps" style="background-color:#108fd2"></ion-icon>\n  </button> -->\n    <ion-grid>\n        <ion-card style="padding:15px; padding-bottom: 2px;" >\n\n            <ion-slides pager>\n\n                <ion-slide style="background-color:#ffff">\n                    <ion-col col-12 *ngIf="GreenDivF">\n                        <ion-col col-12 style="margin-bottom:10px;">\n                            <b>{{greeting}},</b>&nbsp;{{username}}</ion-col>\n                        <br>\n                        \n\n                        <ion-col col-12>\n                                <ion-col col-12 *ngIf="liveDiv" >\n                                        <a class="link"   (click)="golive()" style="color: #ef1111; font-size:15px;margin:9px!important;text-align: center;">\n                                            <span>{{live}}</span>\n                                        </a>\n                                    </ion-col>\n                            <p *ngIf="(examonly && studyonly) === true" style="font-size: 15px; margin-bottom: 10px; margin-top:0px;">Last time you studied\n                                <span>\n                                    <a (click)="gotoLearn()" style="color: #108fd2;">{{chapterName}}</a>\n                                </span> in {{sectionName}} and took an exam on\n                                <span>\n                                    <a (click)="goLastExam(i)" style="color: #108fd2;">{{(dashInfo.exam.examName)?dashInfo.exam.examName:""}} </a>\n                                </span>\n                            </p>\n                        </ion-col>\n                    </ion-col>\n                    <ion-col col-12 *ngIf="welcomeDiv" >\n                        <p> Welcome to our nextfellow family</p>\n                        <p style="font-size: 15px;" >\n                            <span>Start with\n                                <a (click)="firtChap()" style="color: #108fd2;">First Chapter</a>\n                            </span>\n                            <span>or Try\n                                <a (click)="grantTest()" style="color: #108fd2;">Grand Exam</a>\n                            </span>\n                        </p>\n                 \n                    </ion-col>\n                    <div id="focusItem"></div>\n                    \n                </ion-slide>\n\n                <!-- <ion-slide style="background-color:#ffff;">\n                    <ion-col col-12>\n                        <a (click)="mockreg()" class="crd">\n                            <b> Weekend Exam Registration </b>\n                        </a>\n                    </ion-col>\n                    <ion-col col-12>\n                        <p style="font-size: 15px; padding: 10px; margin-top: 3px;">Every friday a subject wise test (100 questions) and sunday a grand or special test (300 questions).</p>\n                    </ion-col>\n                </ion-slide> -->\n\n            </ion-slides>\n        </ion-card>\n    </ion-grid>\n\n    <ion-grid style="padding-left:0px;padding-right:0px;">\n        <ion-row>\n\n            <ion-col col-6 class="bxclr" (click)="gotoLeranPage()">\n                    <!-- <ion-col col-6 class="bxclr" (click)="createUser(true)"> -->\n                   \n                <img src="assets/img/learn.png" style="padding-top: 33px;">\n\n                <p>LEARN</p>\n            </ion-col>\n            <ion-col col-6 class=" bxclr-2" (click)="gotoExamPage()">\n\n                <img src="assets/img/exam.png" style="padding-top: 33px;">\n                <p>EXAM</p>\n            </ion-col>\n\n            <ion-col col-6 class=" bxclr-3" style="padding-top: 15px;" (click)="writeExam()">\n\n                <img src="assets/img/notes.png" style="padding-top: 33px;">\n                <p>NOTE</p>\n            </ion-col>\n\n            <ion-col   col-6 class=" bxclr-4" (click)="gotoEvaluatePage()">\n\n                <img src="assets/img/evaluate.png" style="padding-top: 41px;">\n                <p>EVALUATE</p>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/dashboard/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_5__services_student_service__["a" /* StudentService */], __WEBPACK_IMPORTED_MODULE_12__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_17__services_navBarService__["a" /* naveBarShowSrvice */], __WEBPACK_IMPORTED_MODULE_18__pages_homeNew_homeNew__["a" /* homeNew */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_18__pages_homeNew_homeNew__["a" /* homeNew */], __WEBPACK_IMPORTED_MODULE_17__services_navBarService__["a" /* naveBarShowSrvice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_12__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_5__services_student_service__["a" /* StudentService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HintModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HintModalPage = /** @class */ (function () {
    function HintModalPage(viewCtrl, params, platform) {
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.myParam = params.get('myParam');
        console.log("myparams", this.myParam);
    }
    HintModalPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    HintModalPage.prototype.backButtonFunc = function () {
        this.viewCtrl.dismiss();
    };
    HintModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    HintModalPage.prototype.saveNote = function (data) {
        this.viewCtrl.dismiss(data);
    };
    HintModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modal',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/note/note.modal.html"*/'<style>\n  .modal-wrapper {\n      z-index: 10;\n      min-height:281px;\n      contain: strict;\n  }\n  \n  </style>\n  <!--<ion-header>\n      <ion-navbar>\n        <ion-title>Make a Note</ion-title>\n        <ion-buttons end>\n          <button ion-button icon-only>\n          <ion-icon name=\'bulb\'></ion-icon>\n        </button>\n        </ion-buttons>\n      </ion-navbar>\n    </ion-header>-->\n  \n    <ion-content padding >\n  \n        <ion-grid>\n            <ion-row>\n     \n      <ion-col col-6 style="text-align: left;"  class="pop" (click)="saveNote(note)">\n          Save\n        </ion-col>\n        <ion-col col-6 style="text-align:right;" class="pop" (click)="dismiss()" id="loginShow" type="submit" block >\n         <ion-icon ios="ios-close-circle" md="md-close-circle"></ion-icon>\n        </ion-col>\n            </ion-row>\n          </ion-grid>\n       \n          <ion-grid>\n              <ion-row>\n                <ion-list>\n                  <ion-item>\n                    \n        <textarea style="min-height: 200px; margin-left: 5px;" placeholder="Note here"  [(ngModel)]="note"  id="noteme" name="text" rows="4" ngModel required></textarea> \n        </ion-item>\n        </ion-list>\n        </ion-row>\n        </ion-grid>\n    </ion-content>\n    \n    \n    '/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/note/note.modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], HintModalPage);
    return HintModalPage;
}());

//# sourceMappingURL=note.modal.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_notes_service__ = __webpack_require__(125);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var editModal = /** @class */ (function () {
    function editModal(toastCtrl, noteService, alertCtrl, viewCtrl, params, platform) {
        this.toastCtrl = toastCtrl;
        this.noteService = noteService;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.myParam = params.get('oldContent');
        this.myParamId = params.get('noteId');
        console.log("myparams", this.myParamId);
        if (this.myParam) {
            this.note = this.myParam;
        }
    }
    editModal.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    editModal.prototype.backButtonFunc = function () {
        this.viewCtrl.dismiss();
    };
    editModal.prototype.deleteNote = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure want to delete this note?',
            buttons: [
                {
                    text: 'Yes',
                    handler: function () {
                        // let noteId = this.allNotesData[index]._id;
                        _this.noteService.deleteNote(_this.myParamId).subscribe(function (res) {
                            var toast = _this.toastCtrl.create({
                                message: 'Note deleted.',
                                duration: 3000,
                                position: 'bottom'
                            });
                            toast.present();
                        }, function (error) {
                            var toast = _this.toastCtrl.create({
                                message: 'OOPS! Something went wrong',
                                duration: 3000,
                                position: 'bottom'
                            });
                            toast.present();
                        });
                        _this.viewCtrl.dismiss("dataDeletedSuccessfully");
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        console.log('No');
                    }
                }
            ]
        });
        alert.present();
    };
    editModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    editModal.prototype.saveNote = function (data) {
        this.viewCtrl.dismiss(data);
    };
    editModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'editModal',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/editModal/editModal.html"*/'<!--<ion-header>\n    <ion-navbar>\n      <ion-title>Make a Note</ion-title>\n      <ion-buttons end>\n        <button ion-button icon-only>\n        <ion-icon name=\'bulb\'></ion-icon>\n      </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>-->\n<!-- \n  <ion-content padding  style="height: 60%;">\n        <ion-navbar>\n            <ion-title>Make a Note</ion-title>\n            <ion-buttons end>\n                <button ion-fab class="pop-in"   (click)="deleteNote()" style="float: right;\n                width: 32px;\n                height: 32px; background-color:#e24a4a;">  <ion-icon ios="ios-trash" md="md-trash"></ion-icon></button>\n            </ion-buttons>\n          </ion-navbar>\n          <ion-grid>\n              <ion-row>\n                <ion-list>\n                  <ion-item>\n                    \n        <textarea   [(ngModel)]="note"  id="noteme" name="text" rows="4" ngModel required></textarea> \n        </ion-item>\n        </ion-list>\n        </ion-row>\n        <ion-row responsive-sm class="mdl-btn" style="margin-bottom:5px;">\n            <ion-col>\n              <button ion-button (click)="dismiss()" id="loginShow" type="submit" block style="background-color:#108fd2; color: white;">Cancel</button>\n            </ion-col>\n            <ion-col>\n      \n              <button type="submit" ion-button color="light"  block (click)="saveNote(note)" [disabled]="!note" style="background-color:#108fd2; color: white;">Save</button>\n            </ion-col>\n          </ion-row>\n    \n        </ion-grid>\n    </ion-content> -->\n\n\n<!--<ion-header>\n    <ion-navbar>\n      <ion-title>Make a Note</ion-title>\n      <ion-buttons end>\n        <button ion-button icon-only>\n        <ion-icon name=\'bulb\'></ion-icon>\n      </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>-->\n\n<ion-content padding>\n\n  <ion-grid>\n    <ion-row>\n\n      <ion-col col-4 class="pop" (click)="saveNote(note)">\n        Save\n      </ion-col>\n      <ion-col col-4 style="text-align:center;" class="pop" (click)="deleteNote()">\n        <ion-icon ios="ios-trash" md="md-trash"></ion-icon>\n      </ion-col>\n      <ion-col col-4 style="text-align:right;" class="pop" (click)="dismiss()" id="loginShow" type="submit" block>\n        <ion-icon ios="ios-close" md="md-close"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n  <ion-grid>\n    <ion-row>\n      <ion-list>\n        <ion-item>\n\n          <textarea style="min-height: 200px; margin-left: 5px;" [(ngModel)]="note" id="noteme" name="text" rows="4" ngModel required></textarea>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n\n\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/editModal/editModal.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_notes_service__["a" /* NotesService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__services_notes_service__["a" /* NotesService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], editModal);
    return editModal;
}());

//# sourceMappingURL=editModal.js.map

/***/ }),

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportProb; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReportProb = /** @class */ (function () {
    function ReportProb(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        this.myParam = params.get('myParam');
    }
    ReportProb.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ReportProb.prototype.submitRepo = function (data) {
        this.viewCtrl.dismiss(data);
    };
    ReportProb = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-report',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/report-questions/report.html"*/'<!--<ion-header >\n    \n        <ion-navbar color="color3">\n          <ion-title>Report this question</ion-title>\n          <ion-buttons end>\n            <button ion-button icon-only>\n            <ion-icon name=\'bulb\'></ion-icon>\n          </button>\n          </ion-buttons>\n        </ion-navbar>\n      </ion-header>-->\n      <ion-content padding >\n          <ion-navbar >\n              <ion-title style="text-align: center;">Report this question</ion-title>\n             \n            </ion-navbar>\n      <ion-grid>\n<ion-row>\n  <ion-list>\n    <ion-item>\n     <textarea placeholder="Report Question" [(ngModel)]="report"  id="text" name="text" rows="4" ngModel required ></textarea> \n\n    </ion-item>\n    \n    </ion-list>\n\n    \n     \n    </ion-row>\n    <ion-row responsive-sm style="    margin-top: 39%;">\n        <ion-col>\n          <button ion-button (click)="dismiss()" id="loginShow" type="submit" block  style="background-color:#108fd2;">Cancel</button>\n        </ion-col>\n        <ion-col>\n  \n          <button type="submit" ion-button color="light"  block (click)="submitRepo(report)" [disabled]="!report" style="background-color:#108fd2; color: white;">Submit</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n   \n        \n      \n      </ion-content>\n   <!--   <ion-footer>\n            <ion-row responsive-sm>\n                    <ion-col>\n                      <button ion-button (click)="dismiss()" id="loginShow" type="submit" block  style="background-color:#108fd2;">Cancel</button>\n                    </ion-col>\n                    <ion-col>\n              \n                      <button type="submit" ion-button color="light"  block (click)="submitRepo(report)" [disabled]="!report" style="background-color:#108fd2; color: white;">Submit</button>\n                    </ion-col>\n                  </ion-row>\n\n      </ion-footer>-->\n      \n      '/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/questions/report-questions/report.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], ReportProb);
    return ReportProb;
}());

//# sourceMappingURL=report.js.map

/***/ }),

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyNote; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_notes_service__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_note_note__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(961);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DailyNote = /** @class */ (function () {
    function DailyNote(socialSharing, navCtrl, events, navParams, __auth, platform, noteService, alertCtrl) {
        var _this = this;
        this.socialSharing = socialSharing;
        this.navCtrl = navCtrl;
        this.events = events;
        this.navParams = navParams;
        this.__auth = __auth;
        this.platform = platform;
        this.noteService = noteService;
        this.alertCtrl = alertCtrl;
        this.indexShow = 0;
        this.miniNoteData = [];
        this.noNext = false;
        this.noPrev = false;
        this.demoFlag = false;
        this.imgFlag = false;
        this.bookmark = false;
        this.bookmarkNote = false;
        this.StarCount = 0;
        this.bookmarkedNote = false;
        this.bookmarkicon = false;
        this.url = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].backendUrl + '/images/';
        this.imgUrl = [];
        this.StarimgUrl = [];
        this.newNote = [];
        this.starimgFlag = false;
        this.noQuestion = false;
        this.noPrevQuestion = false;
        this.currentIndexToShow = 0;
        this.noPrevBtn = false;
        this.noNextBtn = false;
        this.showBar = false;
        this.noteDiv = false;
        this.demoFlagstar = false;
        this.notIndex = 0;
        this.recentBookmarkedNote = false;
        this.recentBookmarkicon = false;
        this.recentNoteDiv = false;
        this.recentNotes = [];
        this.noteId = navParams.get('noteId');
        this.regId = navParams.get('RegId');
        this.demoFlag = navParams.get('demoflag');
        this.ModeOfNote = navParams.get('mode');
        console.log("this.ModeOfNote", this.ModeOfNote);
        this.events.publish('user:created', true, Date.now());
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    }
    DailyNote.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_note_note__["a" /* Note */], { parms: "daily" });
    };
    DailyNote.prototype.ngOnInit = function () {
        this.tabBarElement.style.display = 'none';
        if (this.ModeOfNote == "daily") {
            this.loadMiniNotes();
        }
        else if (this.ModeOfNote == "recent") {
            console.log("haaaaaaaaaaaaaai");
            this.loadRecent();
        }
    };
    DailyNote.prototype.ionViewWillEnter = function () {
        this.tabBarElement.style.display = 'none';
    };
    DailyNote.prototype.ionViewWillLeave = function () {
        this.tabBarElement.style.display = 'flex';
    };
    DailyNote.prototype.onCardInteract = function (event) {
    };
    DailyNote.prototype.bookmarked = function (id) {
        var _this = this;
        if (this.bookmarkedNote) {
            this.bookmark = false;
        }
        else {
            this.bookmark = true;
        }
        this.noteService.bookmarkMiniNote(id, this.bookmark).subscribe(function (res) {
            if (res.status) {
                _this.noteService.loadMininote(id, _this.regId, _this.demoFlag).subscribe(function (res) {
                    if (res.data) {
                        _this.bookmarkedNote = res.data[0].bookmarked;
                        if (_this.bookmarkedNote) {
                            _this.bookmarkicon = true;
                        }
                        else {
                            _this.bookmarkicon = false;
                        }
                    }
                });
            }
        });
    };
    DailyNote.prototype.bookmarkedStar = function (nostarId) {
        var _this = this;
        this.noteService.bookmarkMiniNote(nostarId, false).subscribe(function (res) {
            if (res.status) {
                _this.logEvent(true);
            }
        });
    };
    DailyNote.prototype.logEvent = function (event) {
        var _this = this;
        this.questionStarData = [];
        if (event._value == true || event == true) {
            this.showBar = true;
            this.noteDiv = false;
            //this.noPrevBtn = true;
            this.bookmarkNote = true;
            this.bookmarkicon = true;
            this.currentIndexToShow = 0;
            this.noteService.bookmarkedOnly().subscribe(function (res) {
                console.log("bookmrked res", res);
                if (res.data) {
                    _this.questionStarData = res.data;
                    if (res.data.length == 1) {
                        _this.noPrevBtn = true;
                        _this.noNextBtn = true;
                        _this.nostarId = _this.questionStarData[_this.currentIndexToShow].noteId;
                        _this.notestarContent = _this.questionStarData[_this.currentIndexToShow].noteContent;
                        _this.ministarNoteTitle = _this.questionStarData[_this.currentIndexToShow].noteTitle;
                        _this.notestarTags = _this.questionStarData[_this.currentIndexToShow].noteTags[0];
                        _this.bookmarkedStarNote = _this.questionStarData[_this.currentIndexToShow].bookmarked;
                        //this.demoFlagstar = this.miniNotes[this.currentIndex].demoFlag;
                    }
                    if (res.data.length > 1) {
                        _this.noPrevBtn = true;
                        _this.noNextBtn = false;
                        _this.nostarId = _this.questionStarData[_this.currentIndexToShow].noteId;
                        _this.notestarContent = _this.questionStarData[_this.currentIndexToShow].noteContent;
                        _this.ministarNoteTitle = _this.questionStarData[_this.currentIndexToShow].noteTitle;
                        _this.notestarTags = _this.questionStarData[_this.currentIndexToShow].noteTags[0];
                        _this.bookmarkedStarNote = _this.questionStarData[_this.currentIndexToShow].bookmarked;
                        ////this.demoFlagstar = this.miniNotes[this.currentIndex].demoFlag;
                    }
                }
                else {
                    //this.questionStarData=[];
                    _this.showBar = true;
                    _this.noteDiv = false;
                    _this.bookmarkNote = false;
                    _this.noPrevBtn = true;
                    _this.noNextBtn = true;
                    _this.__auth.notificationInfo("No Notes found !");
                }
            });
        }
        else {
            console.log("(this.ModeOfNote", this.ModeOfNote);
            this.loadMiniNotes();
            // if(this.ModeOfNote == "daily"){
            //  
            // }else if(this.ModeOfNote == "recent"){
            //   console.log("hooooooooooooooooooo");
            //   this.loadRecent();
            // }
            //       this.showBar=false;
            //       this.noteDiv = true;
            // console.log("mmmmmmmmmmmmmmmmmmm",this.bookmarkedStarNote);
            //       if (this.bookmarkedStarNote == true) {
            //         this.bookmarkicon = true;
            //       } else {
            //         this.bookmarkicon = false;
            //       }
            //       this.bookmarkNote = false
        }
    };
    DailyNote.prototype.next = function () {
        var _this = this;
        if (!this.bookmarkNote) {
            this.noPrev = false;
            this.currentIndex++;
            if (this.currentIndex == this.miniNotes.length - 1) {
                this.noNext = true;
                this.noId = this.miniNotes[this.currentIndex]._id;
                this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
                this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(function (res) {
                    if (res.data) {
                        _this.bookmarkedNote = res.data[_this.indexShow].bookmarked;
                        if (_this.bookmarkedNote) {
                            _this.bookmarkicon = true;
                        }
                        else {
                            _this.bookmarkicon = false;
                        }
                    }
                });
                this.noteContent = this.miniNotes[this.currentIndex].noteContent;
                this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
                this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];
            }
            else {
                this.noNext = false;
                this.noId = this.miniNotes[this.currentIndex]._id;
                this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
                this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(function (res) {
                    if (res.data) {
                        _this.bookmarkedNote = res.data[_this.indexShow].bookmarked;
                        if (_this.bookmarkedNote) {
                            _this.bookmarkicon = true;
                        }
                        else {
                            _this.bookmarkicon = false;
                        }
                    }
                });
                this.noteContent = this.miniNotes[this.currentIndex].noteContent;
                this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
                this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];
            }
        }
        else {
            this.noPrevBtn = false;
            this.currentIndexToShow++;
            if (this.currentIndexToShow == this.questionStarData.length - 1) {
                this.noNextBtn = true;
                this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
                this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
                this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
                this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
                this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;
            }
            else {
                this.noNextBtn = false;
                this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
                this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
                this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
                this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
                this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;
            }
        }
    };
    DailyNote.prototype.prev = function () {
        var _this = this;
        if (!this.bookmarkNote) {
            this.noNext = false;
            this.currentIndex--;
            if (this.currentIndex > 0) {
                this.noPrev = false;
                this.noId = this.miniNotes[this.currentIndex]._id;
                this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
                this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(function (res) {
                    if (res.data) {
                        _this.bookmarkedNote = res.data[_this.indexShow].bookmarked;
                        if (_this.bookmarkedNote) {
                            _this.bookmarkicon = true;
                        }
                        else {
                            _this.bookmarkicon = false;
                        }
                    }
                });
                this.noteContent = this.miniNotes[this.currentIndex].noteContent;
                this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
                this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];
            }
            else {
                this.noPrev = true;
                this.noId = this.miniNotes[this.currentIndex]._id;
                this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
                this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(function (res) {
                    if (res.data) {
                        _this.bookmarkedNote = res.data[_this.indexShow].bookmarked;
                        if (_this.bookmarkedNote) {
                            _this.bookmarkicon = true;
                        }
                        else {
                            _this.bookmarkicon = false;
                        }
                    }
                });
                this.noteContent = this.miniNotes[this.currentIndex].noteContent;
                this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
                this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];
            }
        }
        else {
            this.noNextBtn = false;
            this.currentIndexToShow--;
            if (this.currentIndexToShow > 0) {
                this.noPrevBtn = false;
                this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
                this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
                this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
                this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
                this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;
            }
            else {
                this.noPrevBtn = true;
                this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
                this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
                this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
                this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
                this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;
            }
        }
    };
    DailyNote.prototype.saveRecentNote = function (id, mode) {
        console.log("idkkkkkkkkkk", id, mode);
        this.noteService.saveRecentNote(id, mode).subscribe(function (res) {
            if (res) {
                console.log("recent note save ", res);
            }
        });
    };
    DailyNote.prototype.shareData = function () {
        this.socialSharing.share(document.getElementById("demo").innerHTML, this.miniNoteTitle, "URL to file or image", "www.nextfellow.com").then(function () {
            console.log("shareSheetShare: Success");
        }).catch(function () {
            console.error("shareSheetShare: failed");
        });
    };
    DailyNote.prototype.loadRecent = function () {
        var _this = this;
        console.log("recent inssssssssssssssssssssssssssssssssssssssss");
        this.recentNoteDiv = true;
        this.noteDiv = false;
        this.bookmarkNote = false;
        this.noteService.loadRecentNote().subscribe(function (res) {
            if (res.data) {
                console.log("recent note////// ", res.data);
                _this.recentNotes = res.data;
                if (res.data.length == 1) {
                    _this.noPrev = true;
                    _this.noNext = true;
                }
                (res.data).map(function (item, index) {
                    if (item.noteId == _this.noteId) {
                        console.log("indexx", index);
                        _this.recentNoteId = item.noteId;
                        _this.notIndex = index;
                        _this.recentNoteContent = item.noteContent;
                        _this.recentMiniNoteTitle = item.noteTitle;
                        _this.recentNoteTags = item.noteTags[0];
                        _this.recentBookmarkedNote = item.bookmarked;
                        if (_this.recentBookmarkedNote) {
                            _this.recentBookmarkicon = true;
                        }
                        else {
                            _this.recentBookmarkicon = false;
                        }
                    }
                    return item;
                });
                if (_this.notIndex == 0) {
                    _this.noPrev = true;
                }
            }
        });
    };
    DailyNote.prototype.nextRecent = function () {
        this.noPrev = false;
        this.notIndex++;
        console.log("this.notIndex", this.notIndex);
        if (this.notIndex == this.recentNotes.length - 1) {
            this.noNext = true;
            this.recentNoteContent = this.recentNotes[this.notIndex].noteContent;
            this.recentMiniNoteTitle = this.recentNotes[this.notIndex].noteTitle;
            this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
            this.recentNoteId = this.recentNotes[this.notIndex].noteId;
            this.recentBookmarkedNote = this.recentNotes[this.notIndex].bookmarked;
            if (this.recentBookmarkedNote) {
                this.recentBookmarkicon = true;
            }
            else {
                this.recentBookmarkicon = false;
            }
        }
        else {
            this.noNext = false;
            this.recentNoteContent = this.recentNotes[this.notIndex].noteContent;
            this.recentMiniNoteTitle = this.recentNotes[this.notIndex].noteTitle;
            this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
            this.recentNoteId = this.recentNotes[this.notIndex].noteId;
            this.recentBookmarkedNote = this.recentNotes[this.notIndex].bookmarked;
            if (this.recentBookmarkedNote) {
                this.recentBookmarkicon = true;
            }
            else {
                this.recentBookmarkicon = false;
            }
        }
    };
    DailyNote.prototype.prevRecent = function () {
        this.noNext = false;
        this.notIndex--;
        if (this.notIndex > 0) {
            this.noPrev = false;
            this.recentNoteContent = this.recentNotes[this.notIndex].noteContent;
            this.recentMiniNoteTitle = this.recentNotes[this.notIndex].noteTitle;
            this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
            this.recentNoteId = this.recentNotes[this.notIndex].noteId;
            this.recentBookmarkedNote = this.recentNotes[this.notIndex].bookmarked;
            if (this.recentBookmarkedNote) {
                this.recentBookmarkicon = true;
            }
            else {
                this.recentBookmarkicon = false;
            }
        }
        else {
            this.noPrev = true;
            this.recentNoteContent = this.recentNotes[this.notIndex].noteContent;
            this.recentMiniNoteTitle = this.recentNotes[this.notIndex].noteTitle;
            this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
            this.recentNoteId = this.recentNotes[this.notIndex].noteId;
            this.recentBookmarkedNote = this.recentNotes[this.notIndex].bookmarked;
            if (this.recentBookmarkedNote) {
                this.recentBookmarkicon = true;
            }
            else {
                this.recentBookmarkicon = false;
            }
        }
    };
    DailyNote.prototype.loadMiniNotes = function () {
        var _this = this;
        this.noteDiv = true;
        this.recentNoteDiv = false;
        this.bookmarkNote = false;
        this.noteService.loadMininote(this.noteId, this.regId, this.demoFlag).subscribe(function (res) {
            if (res.data) {
                _this.noteService.fetchAllMiniNotes().subscribe(function (res) {
                    if (res) {
                        _this.miniNotes = res.data.notes;
                        if (_this.miniNotes.length == 1) {
                            _this.noPrev = true;
                            _this.noNext = true;
                        }
                        var newData = (_this.miniNotes).map(function (item, index) {
                            if (item._id == _this.noId) {
                                _this.currentIndex = index;
                            }
                            return item;
                        });
                    }
                    if (_this.currentIndex == 0) {
                        _this.noPrev = true;
                    }
                });
                _this.miniNoteData = res.data;
                _this.bookmarkedNote = res.data[_this.indexShow].bookmarked;
                if (_this.bookmarkedNote) {
                    _this.bookmarkicon = true;
                }
                else {
                    _this.bookmarkicon = false;
                }
                _this.noId = res.data[_this.indexShow].noteId;
                _this.noteContent = res.data[_this.indexShow].noteContent;
                _this.miniNoteTitle = res.data[_this.indexShow].noteTitle;
                _this.noteTags = res.data[_this.indexShow].noteTags[0];
            }
            else {
                _this.noteDiv = false;
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('sliderOne'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
    ], DailyNote.prototype, "sliderOne", void 0);
    DailyNote = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dailynotes',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/note/dailyNotes/dailynotes.html"*/'<!-- <ion-header>\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Dashbord</ion-title>\n    </ion-navbar>\n  </ion-header> -->\n  <ion-header #head>\n     \n  \n      <ion-toolbar>\n          <ion-navbar>\n              <ion-buttons left>\n         \n      \n              </ion-buttons>\n            </ion-navbar>\n        \n  \n  \n      </ion-toolbar>\n    </ion-header>\n\n  <ion-content padding class="bgpic">\n  \n    <!-- <div class="center" *ngIf=" !bookmarkNote && !miniNoteTitle">\n      <ion-spinner></ion-spinner>\n    </div> -->\n    <div *ngIf="!bookmarkNote && noteDiv">\n    <ion-card style="margin-top: 0px;">\n      <!-- <ion-row>\n      <ion-col col-6 col-md-4 col-xl-3 *ngFor="let img of imgUrl; let i= index;">\n        <img [src]=\'url + img\' *ngIf="imgFlag"  style="border: none; width: 105%; padding-top: 40px;\n        height: 123%;">\n      </ion-col>\n      </ion-row> -->\n      <ion-card-content>\n        <ion-card-title style="color:#108fd2">\n          {{miniNoteTitle}}\n        </ion-card-title>\n        <p id ="demo"style="text-align: justify;\n  line-height: 20px;" [innerHtml]="noteContent" class="scrollbar">\n        </p>\n      </ion-card-content>\n  \n    </ion-card>\n  </div>\n    <ion-grid  *ngIf="!bookmarkNote && noteDiv">\n      <ion-row class="bottom_bar" class="footer">\n          <ion-col col-2 style="padding-top: 0px;">\n              <!-- <a (click)="prev()" *ngIf="!noPrev">Previous</a> -->\n              <ion-icon name="ios-arrow-back" (click)="prev()" style="font-size: 34px;float: left;font-weight: bold;" *ngIf="!noPrev"></ion-icon>\n            </ion-col>\n        <!-- <ion-col col-2 style="padding-top: 0px;">\n          <ion-icon name="md-share" (click)="shareData()" style="font-size: 34px;float: left;font-weight: bold;"></ion-icon>\n        </ion-col> -->\n        <ion-col col-4 style="padding: 7px;">\n          <span> {{noteTags}}</span>\n        </ion-col>\n        <ion-col col-2>\n          <ion-icon name="bookmark" class="iconFooter" [ngClass]="(bookmarkicon)?\'selected\':\'unselected\'" (click)="bookmarked(noId)"></ion-icon>\n        </ion-col>\n        <ion-col col-2>\n          <ion-toggle float-center checked="true" style="  display: block;\n                  margin: auto;padding-top:4px; " [(ngModel)]="starredView" (ionChange)="logEvent($event)">\n          </ion-toggle>\n        </ion-col>\n        <ion-col col-2 style="padding-top: 0px;">\n          <!-- <a *ngIf="!noNext" (click)="next()">Next </a>\n          <a *ngIf="noNext">Finish </a> -->\n          <ion-icon name="ios-arrow-forward"style="font-size: 34px;float: right;font-weight: bold;" *ngIf="!noNext" (click)="next();saveRecentNote(noId,\'save\')"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n   \n  \n\n<!---recent note-->\n\n\n<!-- <div class="center" *ngIf=" !bookmarkNote && !recentMiniNoteTitle">\n  <ion-spinner></ion-spinner>\n</div> -->\n<div *ngIf="!bookmarkNote && recentNoteDiv">\n<ion-card style="margin-top: 0px;">\n \n  <ion-card-content>\n    <ion-card-title style="color:#108fd2">\n      {{recentMiniNoteTitle}}\n    </ion-card-title>\n    <p id ="demo"style="text-align: justify;\nline-height: 20px;" [innerHtml]="recentNoteContent" class="scrollbar">\n    </p>\n  </ion-card-content>\n\n</ion-card>\n</div>\n<ion-grid  *ngIf="!bookmarkNote && recentNoteDiv">\n  <ion-row class="bottom_bar" class="footer">\n      <ion-col col-2 style="padding-top: 0px;">\n          <ion-icon name="ios-arrow-back" (click)="prevRecent()" style="font-size: 34px;float: left;font-weight: bold;" *ngIf="!noPrev"></ion-icon>\n        </ion-col>\n   \n    <ion-col col-8 style="padding: 7px;">\n      <span> {{recentNoteTags}}</span>\n    </ion-col>\n    <!-- <ion-col col-2>\n      <ion-icon name="bookmark" class="iconFooter" [ngClass]="(recentBookmarkicon)?\'selected\':\'unselected\'" (click)="bookmarked(recentNoteId)"></ion-icon>\n    </ion-col> -->\n    <!-- <ion-col col-2>\n      <ion-toggle float-center checked="true" style="  display: block;\n              margin: auto;padding-top:4px; " [(ngModel)]="starredView" (ionChange)="logEvent($event)">\n      </ion-toggle>\n    </ion-col> -->\n    <ion-col col-2 style="padding-top: 0px;">\n    \n      <ion-icon name="ios-arrow-forward"style="font-size: 34px;float: right;font-weight: bold;" *ngIf="!noNext" (click)="nextRecent()"></ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n\n\n\n\n\n\n\n\n\n  \n  \n    <!-- <div class="center" *ngIf=" bookmarkNote && !ministarNoteTitle ">\n        <ion-spinner></ion-spinner>\n      </div> -->\n      <div *ngIf="bookmarkNote && !noteDiv && showBar ">\n      <ion-card style="margin-top: 0px;">\n          <!-- <ion-row>\n              <ion-col col-6 col-md-4 col-xl-3 *ngFor="let img of StarimgUrl; let i= index;">\n                <img [src]=\'url + img\' *ngIf="starimgFlag"  style="border: none; width: 105%; padding-top: 40px;\n                height: 123%;">\n              </ion-col>\n              </ion-row> -->\n        <ion-card-content>\n          <ion-card-title style="color:#108fd2">\n            {{ministarNoteTitle}}\n          </ion-card-title>\n          <p style="text-align: justify;\n    line-height: 20px;" [innerHtml]="notestarContent" class="scrollbar">\n          </p>\n        </ion-card-content>\n    \n      </ion-card>\n    </div>\n      <ion-grid *ngIf="!noteDiv && showBar">\n        <ion-row class="bottom_bar" class="footer">\n          <ion-col col-2 style="padding-top: 0px;">\n            <!-- <a (click)="prev()" *ngIf="!noPrevBtn">Previous</a> -->\n            <ion-icon name="ios-arrow-back" (click)="prev()" style="font-size: 34px;float: left;font-weight: bold;"  *ngIf="!noPrevBtn"></ion-icon>\n\n          </ion-col>\n          <ion-col col-4 style="padding: 7px;">\n            <span> {{notestarTags}}</span>\n          </ion-col>\n          <ion-col col-2>\n            <ion-icon name="bookmark" class="iconFooter" [ngClass]="(bookmarkicon)?\'selected\':\'unselected\'" (click)="bookmarkedStar(nostarId)"></ion-icon>\n          </ion-col>\n          <ion-col col-2>\n            <ion-toggle float-center checked="true" style="  display: block;\n                    margin: auto;padding-top:4px; " [(ngModel)]="starredView" (ionChange)="logEvent($event)">\n            </ion-toggle>\n          </ion-col>\n          <ion-col col-2 style="padding-top: 0px;">\n            <!-- <a *ngIf="!noNextBtn" (click)="next()">Next </a>\n            <a *ngIf="noNextBtn">Finish </a> -->\n            <ion-icon name="ios-arrow-forward"style="font-size: 34px;float: right;font-weight: bold;" *ngIf="!noNextBtn"  (click)="next()"></ion-icon>\n\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    \n  </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/note/dailyNotes/dailynotes.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_notes_service__["a" /* NotesService */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__services_notes_service__["a" /* NotesService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], DailyNote);
    return DailyNote;
}());

//# sourceMappingURL=dailynotes.js.map

/***/ }),

/***/ 440:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ExamModal = /** @class */ (function () {
    function ExamModal(viewCtrl, params, formBuilder) {
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.myParam = params.get('data');
        this.shortName = this.myParam.shortname;
        this.totalQue = this.myParam.totalQuestionsCount;
        console.log(this.shortName, "short name");
    }
    ExamModal.prototype.ionViewWillLoad = function () {
        this.quenos = ["300", "250", "200", "150", "100", "50"];
        this.timehour = ["1", "2", "3", "4", "5", "6"];
        this.timeMinut = ['15', '30', '45'];
    };
    ExamModal.prototype.createNewExam = function (value) {
        console.log(value, "valutttttttttttty");
        if (value.minute || value.hour) {
            this.min = value.minute.toString();
            this.hou = value.hour.toString();
        }
        else {
            this.min = "";
            this.hou = "";
        }
        value.minute = this.min;
        value.hour = this.hou;
        console.log(value, "value da");
        this.viewCtrl.dismiss(value);
    };
    ExamModal.prototype.CancellBut = function () {
        this.viewCtrl.dismiss();
    };
    ExamModal.prototype.startCheck = function (value, qValu) {
        var res = qValu * .7;
        this.hourss = Math.trunc(res / 60);
        this.defMint = res % 60;
        // this.defMint = Math.ceil(minutes);
        // this.hourss = hours;
    };
    ExamModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-examModal',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/examModal/examModal.html"*/'<ion-header>\n  <ion-navbar>\n  <ion-title>Create Exam</ion-title>\n\n <!-- <button ion-fab class="pop-in" color="danger"  type="submit" style="float: right;\n  width: 32px;\n  height: 32px;"  id="loginShow"> <ion-icon  name="close"></ion-icon></button>-->\n  </ion-navbar>\n  </ion-header>\n  <ion-content padding>\n   \n  <p> Total Number Of Questions Available:{{totalQue}} \n  <form #grantFrom="ngForm" (ngSubmit)="createNewExam(grantFrom.value)">\n   \n  <ion-list>\n   \n  <ion-item>\n  <ion-label>Exam name</ion-label>\n<ion-input type="text"  [(ngModel)]="shortName" name="names" type="text" #names="ngModel"  required></ion-input>\n\n  <!-- <ion-input type="text" #names="ngModel" id="name" name="names" required ngModel>\n\n  </ion-input> -->\n   \n  </ion-item>\n  <div class="error" *ngIf="names?.errors && (names?.touched || names?.dirty)">\n  <p class="error" [hidden]="!names.errors.required"> Please enter exam name </p>\n   \n  </div>\n\n  \n  <ion-item  style="border-bottom: 1px solid #dddada;">\n  <ion-label >No of questions</ion-label>\n  <ion-input type="number" min=0 step=\'1\' max={{totalQue}} #questNum="ngModel" name="questionNumber" [(ngModel)]="qutno" required ngModel (ngModelChange)="startCheck($event,qutno)">\n   \n  </ion-input>\n  </ion-item>\n  <div class="error" *ngIf="questNum.errors && (questNum.touched || questNum.dirty)">\n  <p class="error" [hidden]="!questNum.errors.required"> This is a required field. </p>\n   \n  </div>\n  <div class="error" *ngIf="qutno<5">\n  <p class="error" [hidden]="qutno>5 || !qutno "> Please enter a number at least five </p>\n   \n  </div>\n  <div class="error" *ngIf="qutno>totalQue">\n  <p class="error" [hidden]="qutno<totalQue ||!qutno"> Please enter a number less than {{totalQue}} </p>\n   \n  </div>\n  <div class="error" *ngIf="qutno>300">\n  <p class="error" [hidden]="(qutno<300 && qutno > 300 ) || !qutno "> Please enter a number less than 300 </p>\n   \n  </div>\n  <ion-row *ngIf="starttimer">\n   \n  <ion-col width-50>\n  <ion-item>\n  <ion-label>Hours</ion-label>\n  <ion-input  type="number" min="0" step="1" max="3"  name="hour" #hours="ngModel" [(ngModel)]="hourss">\n  </ion-input>\n\n   \n \n  </ion-item>\n  </ion-col>\n  <ion-col width-50>\n  <ion-item>\n  <ion-label>minute</ion-label>\n  <ion-input  type="number" min="0" step="1" max="59"  name="minute" #minute="ngModel" [(ngModel)]="defMint" ngModel value="00">\n  </ion-input>\n \n  </ion-item>\n  </ion-col>\n  </ion-row>\n  <ion-item>\n    <ion-label>Start Timer</ion-label>\n     \n    <ion-checkbox [disabled]="!qutno || qutno<5 || qutno>300 || qutno>totalQue" [(ngModel)]="starttimer" name="strtbox"></ion-checkbox>\n    </ion-item>\n  <ion-item>\n  <ion-label>Write later</ion-label>\n   \n  <ion-checkbox [disabled]="!qutno || qutno<5 || qutno>300 || qutno>totalQue" [(ngModel)]="writelater" name="box"></ion-checkbox>\n  </ion-item>\n  <ion-item *ngIf="writelater">\n  <ion-label >Date</ion-label>\n  <ion-datetime displayFormat="YYYY-MM-DD" #date="ngModel" name="date" ngModel ></ion-datetime>\n  </ion-item>\n   \n  </ion-list>\n   \n  <ion-row responsive-sm>\n\n  <ion-col>\n  <button type="submit" ion-button color="light" block *ngIf="!writelater" [disabled]="!grantFrom.form.valid" style="background-color:#108fd2; color: white;">Write now</button>\n  <button type="submit" ion-button color="light" block *ngIf="writelater" [disabled]="!grantFrom.form.valid" style="background-color:#108fd2;color: white; ">Finish</button>\n  </ion-col>\n  </ion-row>\n   \n   \n\n  </form>\n  <ion-row responsive-sm>\n  <ion-col>\n    <button ion-button id="loginShow" type="submit" block style="background-color:#108fd2;" (click)="CancellBut()">Cancel</button>\n    </ion-col>\n  </ion-row>\n  </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/examModal/examModal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], ExamModal);
    return ExamModal;
}());

//# sourceMappingURL=examModal.js.map

/***/ }),

/***/ 441:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeeklyService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WeeklyService = /** @class */ (function () {
    function WeeklyService(http) {
        this.http = http;
        this.url = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].backendUrl;
        var userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = userData._id;
        this.userId = userData.userId;
    }
    //get coupencode
    WeeklyService.prototype.getallCode = function () {
        var data = {
            'userId': this.userId,
            "activeUserId": this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/get-all-coupencode", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.checkAppliedForExam = function () {
        var data = {
            'activeUserId': this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/get-payment-weeklyexam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.basicUserTest = function () {
        var data = {
            'userId': this.userId,
            'activeUserId': this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/check-user-weekend-subscription", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.registerWeeklyExamBasic = function (paymentid) {
        var data = {
            'paymentId': paymentid,
            'userId': this.userId,
            'activeUserId': this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/upgrade-weekend-subscription", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.registerWeeklyExam = function (paramss) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        paramss.activeUserId = this.activeUserId,
            paramss.userId = this.userId;
        console.log(paramss, "date to send");
        return this.http.post(this.url + "/week_exam_reg", paramss, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.SinglePaymentExam = function (paramss) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        paramss.activeUserId = this.activeUserId,
            paramss.userId = this.userId;
        console.log(paramss, "date to send");
        return this.http.post(this.url + "/week_exam_single_reg", paramss, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.getWeekExams = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            activeUserId: this.activeUserId
        };
        return this.http.post(this.url + "/get-weekend-exam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.loadWeekExam = function (weekexamid) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            activeUserId: this.activeUserId,
            examId: weekexamid
        };
        return this.http.post(this.url + "/load-exam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.getExamQuestion = function (questionid, userexamid) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "questionId": questionid,
            "userExamId": userexamid
        };
        return this.http.post(this.url + "/get-weekend-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.saveExamAnswer = function (questionid, examid, userexamid, answerStatus, answer, result, subjectarray) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "questionId": questionid,
            "examId": examid,
            "userExamId": userexamid,
            "answerType": answerStatus,
            "answer": answer,
            "result": result,
            "subjectArray": subjectarray
        };
        return this.http.post(this.url + "/save-weekend-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.markExamQuestion = function (questionid, userexamid, qstatus) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "questionId": questionid,
            "userExamId": userexamid,
            "type": qstatus
        };
        return this.http.post(this.url + "/mark-week-exam-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.submitExam = function (userexamid) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "userExamId": userexamid
        };
        return this.http.post(this.url + "/finish-week-exam", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    // aiims pay
    WeeklyService.prototype.__AiimsPay = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "userId": this.userId,
            "activeUserId": this.activeUserId,
        };
        return this.http.post(this.url + "/get-packages", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.__couponcode = function (packageId, code) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "userId": this.userId,
            "activeUserId": this.activeUserId,
            "packageId": packageId,
            "couponCode": code
        };
        return this.http.post(this.url + "/check-package-coupon-code", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.__SUBSCRIBEPAY = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId
        };
        return this.http.post(this.url + "/get-subscribed-packages", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService.prototype.___AIIMSPAY = function (packageId, couponCode, paymentId, amount) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "userId": this.userId,
            "activeUserId": this.activeUserId,
            "paymentId": paymentId,
            "packageId": packageId,
            "couponCode": couponCode,
            'amount': amount
        };
        return this.http.post(this.url + "/package-registration", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    //send enquery
    WeeklyService.prototype.MSGIOS = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var data = {
            "userId": this.userId,
            "activeUserId": this.activeUserId,
        };
        return this.http.post(this.url + "/mail-registration", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    WeeklyService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], WeeklyService);
    return WeeklyService;
}());

//# sourceMappingURL=weekend.service.js.map

/***/ }),

/***/ 444:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_dashboard_home__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PopoverPage = /** @class */ (function () {
    function PopoverPage(viewCtrl, navCtrl, platform, examService, __auth, params) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.examService = examService;
        this.__auth = __auth;
        this.params = params;
        this.custmizedExam = [];
        this.customized = false;
        this.week = false;
        this.daily = false;
        this.demoExam = false;
        this.completedWeekExam = [];
        this.pendingWeekexam = [];
        this.demoExams = [];
        this.dailyExams = [];
        this.totalWeek = [];
        // this.myParam = params.get('ev');
        this.myParam = params.get('myEvent');
        if (this.myParam == "week") {
            this.week = true;
        }
        else if (this.myParam == "customized") {
            this.customized = true;
        }
        else {
            this.daily = true;
        }
    }
    PopoverPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    PopoverPage.prototype.backButtonFunc = function () {
        this.viewCtrl.dismiss();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_dashboard_home__["a" /* HomePage */]);
    };
    PopoverPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    PopoverPage.prototype.allExam = function (event) {
        var _this = this;
        if (event == "pending") {
            // this.custmizedExam = [];
            this.examService.fetchAllExam(event).subscribe(function (res) {
                if (res.data.examList.length > 0) {
                    var customizedPending = [];
                    customizedPending = res.data.examList;
                    var exam = (customizedPending).map(function (item) {
                        item.customizedExam = true;
                        item.pending = true;
                        return item;
                    });
                    customizedPending = exam;
                    _this.custmizedExam = (customizedPending.length > 0) ? _this.custmizedExam.concat(customizedPending) : _this.custmizedExam;
                }
            });
            this.examService.fetchGrandExam().subscribe(function (res) {
                if (res.data.pendingExams.length > 0) {
                    var pendingGexam = [];
                    pendingGexam = (res.data.pendingExams).map(function (item) {
                        item.examType = "grand";
                        item.examName = item.name;
                        item.pending = true;
                        return item;
                    });
                    //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    _this.custmizedExam = (pendingGexam.length > 0) ? _this.custmizedExam.concat(pendingGexam) : _this.custmizedExam;
                }
                _this.custmizedExam = _this.custmizedExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                if (_this.custmizedExam.length == 0) {
                    _this.__auth.notificationInfo("There is no pending exams found!");
                }
                _this.viewCtrl.dismiss(_this.custmizedExam, event);
            });
        }
        else if (event == "completed") {
            // this.custmizedExam = [];
            this.examService.fetchAllExam(event).subscribe(function (res) {
                if (res.data.examList.length > 0) {
                    _this.custmizedExam = res.data.examList;
                    var exam = (_this.custmizedExam).map(function (item) {
                        item.customizedExam = true;
                        item.completed = true;
                        return item;
                    });
                    _this.custmizedExam = exam;
                    //this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
            });
            this.examService.fetchGrandExam().subscribe(function (res) {
                var completedGexam = [];
                if (res.data.completedExam.length > 0) {
                    completedGexam = (res.data.completedExam).map(function (item) {
                        item.examType = "grand";
                        item.examName = item.name;
                        item.completed = true;
                        return item;
                    });
                    // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    _this.custmizedExam = (completedGexam.length > 0) ? _this.custmizedExam.concat(completedGexam) : _this.custmizedExam;
                }
                _this.custmizedExam = _this.custmizedExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                if (_this.custmizedExam.length == 0) {
                    _this.__auth.notificationInfo("There is no completed exams found!");
                }
                _this.viewCtrl.dismiss(_this.custmizedExam, event);
            });
        }
        else {
            this.getAllCustomized();
        }
    };
    PopoverPage.prototype.allWeekExam = function (category) {
        var _this = this;
        if (category == "pending") {
            this.demoExam = false;
            this.completedWeekExam = [];
            this.examService.getWeekExams().subscribe(function (weekRes) {
                if (weekRes.data) {
                    if (weekRes.data.pendingExams.length > 0) {
                        //let  pendingWeek=[];
                        _this.pendingWeekexam = weekRes.data.pendingExams;
                        var exam_1 = (_this.pendingWeekexam).map(function (item) {
                            item.examName = item.examDetails.name;
                            item.examPending = true;
                            if (item.numberOfQuestions) {
                                item.numberOfQuestions = item.numberOfQuestions;
                            }
                            else if (item.noOfQuestions) {
                                item.numberOfQuestions = item.noOfQuestions;
                            }
                            //item.examType = "week";
                            // this.pendingWeekExaminactive.push(item);
                            return item;
                        });
                        _this.pendingWeekexam = exam_1;
                        // this.pendingWeekexam = this.pendingWeekexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                        _this.completedWeekExam = _this.pendingWeekexam;
                    }
                    if (weekRes.data.activeExams.length > 0) {
                        var activeWeekExam = [];
                        // let actWeek=[];
                        activeWeekExam = weekRes.data.activeExams;
                        var exam_2 = (activeWeekExam).map(function (item) {
                            //  item.examName =item.examDetails.name;
                            item.examPending = true;
                            // if(item.examType=="week"){
                            //   item.examType = "week";
                            // }else if( item.examType =="daily"){
                            //   item.examType = "daily";
                            // }else{
                            //   item.examType = "special";
                            // }
                            if (item.numberOfQuestions) {
                                item.numberOfQuestions = item.numberOfQuestions;
                            }
                            else if (item.noOfQuestions) {
                                item.numberOfQuestions = item.noOfQuestions;
                            }
                            // item.examType = "week";
                            return item;
                        });
                        activeWeekExam = exam_2;
                        _this.completedWeekExam = (activeWeekExam.length > 0) ? _this.completedWeekExam.concat(activeWeekExam) : _this.completedWeekExam;
                        // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    }
                    if (weekRes.data.inactiveExams.length > 0) {
                        // let inactiveExam=[];
                        var pendingWeekExaminactive = [];
                        pendingWeekExaminactive = weekRes.data.inactiveExams;
                        var exam_3 = (pendingWeekExaminactive).map(function (item) {
                            item.inactiveExam = true;
                            // if(item.examType=="week"){
                            //   item.examType = "week";
                            // }else if( item.examType =="daily"){
                            //   item.examType = "daily";
                            // }else{
                            //   item.examType = "special";
                            // }
                            // item.examType = "week";
                            if (item.numberOfQuestions) {
                                item.numberOfQuestions = item.numberOfQuestions;
                            }
                            else if (item.noOfQuestions) {
                                item.numberOfQuestions = item.noOfQuestions;
                            }
                            return item;
                        });
                        pendingWeekExaminactive = exam_3;
                        _this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? _this.completedWeekExam.concat(pendingWeekExaminactive) : _this.completedWeekExam;
                        _this.completedWeekExam = _this.completedWeekExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    }
                    var exam = (_this.completedWeekExam).map(function (item, index) {
                        if (item.examType == "daily") {
                            item.dailyExam = true;
                            _this.dailyExams.push(item);
                        }
                        else {
                            item.dailyExam = false;
                            _this.totalWeek.push(item);
                        }
                        return item;
                    });
                    _this.dailyExams = _this.dailyExams.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    _this.totalWeek = _this.totalWeek.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    if (_this.myParam == "week") {
                        if (_this.totalWeek.length == 0) {
                            _this.__auth.notificationInfo("There is no pending exams found!");
                        }
                        _this.viewCtrl.dismiss(_this.totalWeek, category);
                    }
                    else if (_this.myParam == "daily") {
                        if (_this.dailyExams.length == 0) {
                            _this.__auth.notificationInfo("There is no pending exams found!");
                        }
                        _this.viewCtrl.dismiss(_this.dailyExams, category);
                    }
                }
            });
        }
        else if (category == "completed") {
            this.demoExam = false;
            this.completedWeekExam = [];
            this.examService.getWeekExams().subscribe(function (weekRes) {
                if (weekRes.data) {
                    if (weekRes.data.completedExams.length > 0) {
                        _this.completedWeekExam = weekRes.data.completedExams;
                        var exam = (_this.completedWeekExam).map(function (item) {
                            item.examName = item.examDetails.name;
                            item.examCompleted = true;
                            //item.examType = "week";
                            // if(item.examType=="week"){
                            //   item.examType = "week";
                            // }else if( item.examType =="daily"){
                            //   item.examType = "daily";
                            // }else{
                            //   item.examType = "special";
                            // }
                            if (item.numberOfQuestions) {
                                item.numberOfQuestions = item.numberOfQuestions;
                            }
                            else if (item.noOfQuestions) {
                                item.numberOfQuestions = item.noOfQuestions;
                            }
                            else if (item.examResult.totalQuestions) {
                                item.numberOfQuestions = item.examResult.totalQuestions;
                            }
                            if (item.examType == "daily") {
                                item.dailyExam = true;
                                _this.dailyExams.push(item);
                            }
                            else {
                                item.dailyExam = false;
                                _this.totalWeek.push(item);
                            }
                            return item;
                        });
                        _this.completedWeekExam = exam;
                        _this.totalWeek = _this.totalWeek.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                        _this.dailyExams = _this.dailyExams.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                        if (_this.myParam == "week") {
                            if (_this.totalWeek.length == 0) {
                                _this.__auth.notificationInfo("There is no completed exams found!");
                            }
                            _this.viewCtrl.dismiss(_this.totalWeek, category);
                        }
                        else if (_this.myParam == "daily") {
                            if (_this.dailyExams.length == 0) {
                                _this.__auth.notificationInfo("There is no completed exams found!");
                            }
                            _this.viewCtrl.dismiss(_this.dailyExams, category);
                        }
                    }
                    else {
                        _this.__auth.notificationInfo("There is no completed exams found!");
                    }
                }
            });
        }
        else if (category == "demo") {
            this.demoExam = true;
            this.examService.getWeekExams().subscribe(function (weekRes) {
                if (weekRes.data) {
                    console.log("weekRes.data0 demo", weekRes.data);
                    if (weekRes.data.activeExams.length > 0) {
                        var exam_4 = (weekRes.data.activeExams).map(function (item) {
                            if (item.examCategory == "demo") {
                                item.examPending = true;
                                // if(item.examType=="week"){
                                //   item.examType = "week";
                                // }else if( item.examType =="daily"){
                                //   item.examType = "daily";
                                // }else{
                                //   item.examType = "special";
                                // }
                                _this.demoExams.push(item);
                            }
                            return item;
                        });
                        //             for(let i=0;i<weekRes.data.activeExams.length;i++){
                        //               if(weekRes.data.activeExams[i].examCategory=="demo"){
                        //                if(weekRes.data.activeExams[i].numberOfQuestions){
                        //                 this.noOfQstn=weekRes.data.activeExams[i].numberOfQuestions;
                        //                    }else if(weekRes.data.activeExams[i].noOfQuestions){
                        //                     this.noOfQstn =weekRes.data.activeExams[i].noOfQuestions;
                        //                    }else if(weekRes.data.activeExams[i].examResult.totalQuestions ){
                        //                     this.noOfQstn=weekRes.data.activeExams[i].examResult.totalQuestions;
                        //                    }
                        // this.demoExams.push({"examName":weekRes.data.activeExams[i].examName,"examDate":weekRes.data.activeExams[i].examDate,
                        // "examinformation":weekRes.data.activeExams[i].examinformation,"examPending":true,
                        // "examId":weekRes.data.activeExams[i].examConfig[0].id,
                        // "examType":weekRes.data.activeExams[i].examType,"numberOfQuestions":this.noOfQstn});
                        //               }
                        //             }
                    }
                    var demoCompleted = [];
                    if (weekRes.data.completedExams.length > 0) {
                        var exam_5 = (weekRes.data.completedExams).map(function (item) {
                            if (item.examCategory == "demo") {
                                item.examCompletd = true;
                                item.examName = item.examDetails.name;
                                item.examType = item.examDetails.examType;
                                // if(item.examType=="week"){
                                //   item.examType = "week";
                                // }else if( item.examType =="daily"){
                                //   item.examType = "daily";
                                // }else{
                                //   item.examType = "special";
                                // }
                                _this.demoExams.push(item);
                            }
                            return item;
                        });
                        //             for(let i=0;i<weekRes.data.completedExams.length;i++){
                        //               if(weekRes.data.completedExams[i].examCategory=="demo"){
                        //                 if(weekRes.data.completedExams[i].numberOfQuestions){
                        //                   this.noOfQstn=weekRes.data.completedExams[i].numberOfQuestions;
                        //                      }else if(weekRes.data.completedExams[i].noOfQuestions){
                        //                       this.noOfQstn =weekRes.data.completedExams[i].noOfQuestions;
                        //                      }else if(weekRes.data.completedExams[i].examResult.totalQuestions ){
                        //                       this.noOfQstn=weekRes.data.completedExams[i].examResult.totalQuestions;
                        //                      }
                        // demoCompleted.push({"examName":weekRes.data.completedExams[i].examName,"examDate":weekRes.data.completedExams[i].examDate,
                        // "examinformation":weekRes.data.completedExams[i].examinformation,"examCompleted":true,
                        // "examId":weekRes.data.completedExams[i].examConfig[0].id,
                        // "examType":weekRes.data.completedExams[i].examType,"numberOfQuestions":this.noOfQstn});
                        //               }
                        //             }
                        //             this.demoExams = (demoCompleted.length > 0) ? this.demoExams.concat(demoCompleted) : this.demoExams;
                    }
                    var demoInactive = [];
                    if (weekRes.data.inactiveExams.length > 0) {
                        var exam_6 = (weekRes.data.inactiveExams).map(function (item) {
                            if (item.examCategory == "demo") {
                                item.inactiveExam = true;
                                // if(item.examType=="week"){
                                //   item.examType = "week";
                                // }else if( item.examType =="daily"){
                                //   item.examType = "daily";
                                // }else{
                                //   item.examType = "special";
                                // }
                                _this.demoExams.push(item);
                            }
                            return item;
                        });
                        // for(let i=0;i<weekRes.data.inactiveExams.length;i++){
                        //   if(weekRes.data.inactiveExams[i].examCategory=="demo"){
                        //     if(weekRes.data.inactiveExams[i].numberOfQuestions){
                        //       this.noOfQstn=weekRes.data.inactiveExams[i].numberOfQuestions;
                        //          }else if(weekRes.data.inactiveExams[i].noOfQuestions){
                        //           this.noOfQstn =weekRes.data.inactiveExams[i].noOfQuestions;
                        //          }else if(weekRes.data.inactiveExams[i].examResult.totalQuestions ){
                        //           this.noOfQstn=weekRes.data.inactiveExams[i].examResult.totalQuestions;
                        //          }
                        //     demoInactive.push({"examName":weekRes.data.inactiveExams[i].examName,
                        //     "examId":weekRes.data.inactiveExams[i].examConfig[0].id,
                        //     "numberOfQuestions":this.noOfQstn,"examDate":weekRes.data.inactiveExams[i].examDate,"examinformation":weekRes.data.inactiveExams[i].examinformation,"inactiveExam":true,"examType":weekRes.data.inactiveExams[i].examType});
                        //   }
                        // }
                        // this.demoExams = (demoInactive.length > 0) ? this.demoExams.concat(demoInactive) : this.demoExams;
                    }
                    var demoPending = [];
                    if (weekRes.data.pendingExams.length > 0) {
                        // for(let i=0;i<weekRes.data.pendingExams.length;i++){
                        //   if(weekRes.data.pendingExams[i].examCategory=="demo"){
                        //     if(weekRes.data.pendingExams[i].numberOfQuestions){
                        //       this.noOfQstn=weekRes.data.pendingExams[i].numberOfQuestions;
                        //          }else if(weekRes.data.pendingExams[i].noOfQuestions){
                        //           this.noOfQstn =weekRes.data.pendingExams[i].noOfQuestions;
                        //          }else if(weekRes.data.pendingExams[i].examResult.totalQuestions ){
                        //           this.noOfQstn=weekRes.data.pendingExams[i].examResult.totalQuestions;
                        //          }
                        //     demoPending.push({"examName":weekRes.data.pendingExams[i].examName,"examId":weekRes.data.pendingExams[i].examConfig[0].id,"numberOfQuestions":this.noOfQstn,"examDate":weekRes.data.pendingExams[i].examDate,"examinformation":weekRes.data.pendingExams[i].examinformation,"examPending":true,"examType":weekRes.data.pendingExams[i].examType});
                        //   }
                        // }
                        var exam_7 = (weekRes.data.pendingExams).map(function (item) {
                            if (item.examCategory == "demo") {
                                item.examPending = true;
                                item.examName = item.examDetails.name;
                                item.examType = item.examDetails.examType;
                                // if(item.examType=="week"){
                                //   item.examType = "week";
                                // }else if( item.examType =="daily"){
                                //   item.examType = "daily";
                                // }else{
                                //   item.examType = "special";
                                // }
                                _this.demoExams.push(item);
                            }
                            return item;
                        });
                        // this.demoExams = (demoPending.length > 0) ? this.demoExams.concat(demoPending) : this.demoExams;
                        console.log("demo exams", _this.demoExams);
                    }
                    var exam = (_this.demoExams).map(function (item, index) {
                        if (item.examType == "daily") {
                            item.dailyExam = true;
                            _this.dailyExams.push(item);
                        }
                        else {
                            item.dailyExam = false;
                            _this.totalWeek.push(item);
                        }
                        return item;
                    });
                    _this.completedWeekExam = exam;
                    _this.demoExams = _this.demoExams.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    _this.dailyExams = _this.dailyExams.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    _this.totalWeek = _this.totalWeek.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    if (_this.myParam == "week") {
                        _this.viewCtrl.dismiss(_this.totalWeek, category);
                        if (_this.totalWeek.length == 0) {
                            _this.__auth.notificationInfo("There is no demo exams found!");
                        }
                    }
                    else if (_this.myParam == "daily") {
                        _this.viewCtrl.dismiss(_this.dailyExams, category);
                        if (_this.dailyExams.length == 0) {
                            _this.__auth.notificationInfo("There is no demo exams found!");
                        }
                    }
                }
            });
        }
        else {
            this.demoExam = false;
            this.getAllWeek();
        }
    };
    PopoverPage.prototype.getAllWeek = function () {
        var _this = this;
        this.examService.getWeekExams().subscribe(function (weekRes) {
            if (weekRes.data) {
                if (weekRes.data.completedExams.length > 0) {
                    _this.completedWeekExam = weekRes.data.completedExams;
                    var exam_8 = (_this.completedWeekExam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examCompleted = true;
                        item.examType = item.examDetails.examType;
                        // if(item.examType=="week"){
                        //   item.examType = "week";
                        // }else if( item.examType =="daily"){
                        //   item.examType = "daily";
                        // }else{
                        //   item.examType = "special";
                        // }
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        else if (item.examResult.totalQuestions) {
                            item.numberOfQuestions = item.examResult.totalQuestions;
                        }
                        // item.examType = "week";
                        return item;
                    });
                    _this.completedWeekExam = exam_8;
                    //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.activeExams.length > 0) {
                    var activeWeekExam = [];
                    activeWeekExam = weekRes.data.activeExams;
                    var exam_9 = (activeWeekExam).map(function (item) {
                        item.examPending = true;
                        //item.examType = "week";
                        // if(item.examType=="week"){
                        //   item.examType = "week";
                        // }else if( item.examType =="daily"){
                        //   item.examType = "daily";
                        // }else{
                        //   item.examType = "special";
                        // }
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    activeWeekExam = exam_9;
                    _this.completedWeekExam = (activeWeekExam.length > 0) ? _this.completedWeekExam.concat(activeWeekExam) : _this.completedWeekExam;
                    //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.inactiveExams.length > 0) {
                    var pendingWeekExaminactive = [];
                    pendingWeekExaminactive = weekRes.data.inactiveExams;
                    var exam_10 = (pendingWeekExaminactive).map(function (item) {
                        item.inactiveExam = true;
                        // item.examType = "week";
                        //  if(item.examType=="week"){
                        //   item.examType = "week";
                        // }else if( item.examType =="daily"){
                        //   item.examType = "daily";
                        // }else{
                        //   item.examType = "special";
                        // }
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    pendingWeekExaminactive = exam_10;
                    _this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? _this.completedWeekExam.concat(pendingWeekExaminactive) : _this.completedWeekExam;
                    // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                }
                if (weekRes.data.pendingExams.length > 0) {
                    var pendingWeekexam = [];
                    pendingWeekexam = weekRes.data.pendingExams;
                    var exam_11 = (pendingWeekexam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examPending = true;
                        item.examType = item.examDetails.examType;
                        // if(item.examType=="week"){
                        //   item.examType = "week";
                        // }else if( item.examType =="daily"){
                        //   item.examType = "daily";
                        // }else{
                        //   item.examType = "special";
                        // }
                        //item.examType = "week";
                        // this.pendingWeekExaminactive.push(item);
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    pendingWeekexam = exam_11;
                    pendingWeekexam = pendingWeekexam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    _this.completedWeekExam = (pendingWeekexam.length > 0) ? _this.completedWeekExam.concat(pendingWeekexam) : _this.completedWeekExam;
                    _this.completedWeekExam = _this.completedWeekExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                }
                var exam = (_this.completedWeekExam).map(function (item, index) {
                    if (item.examType == "daily") {
                        item.dailyExam = true;
                        _this.dailyExams.push(item);
                    }
                    else {
                        item.dailyExam = false;
                        _this.totalWeek.push(item);
                    }
                    return item;
                });
                _this.completedWeekExam = exam;
                _this.totalWeek = _this.totalWeek.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                _this.dailyExams = _this.dailyExams.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                if (_this.myParam == "week") {
                    if (_this.totalWeek.length == 0) {
                        _this.__auth.notificationInfo("There is no Exams found!");
                    }
                    _this.viewCtrl.dismiss(_this.totalWeek, "All");
                }
                else if (_this.myParam == "daily") {
                    if (_this.dailyExams.length == 0) {
                        _this.__auth.notificationInfo("There is no Exams found!");
                    }
                    _this.viewCtrl.dismiss(_this.dailyExams, "All");
                }
            }
            else {
                _this.__auth.notificationInfo("There is no data found!");
            }
        });
    };
    PopoverPage.prototype.getAllCustomized = function () {
        var _this = this;
        this.mode = 'completed';
        this.examService.fetchAllExam(this.mode).subscribe(function (res) {
            if (res.data.examList.length > 0) {
                _this.custmizedExam = res.data.examList;
                var exam = (_this.custmizedExam).map(function (item) {
                    item.customizedExam = true;
                    item.completed = true;
                    return item;
                });
                _this.custmizedExam = exam;
            }
            // this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            _this.mode = 'pending';
            _this.examService.fetchAllExam(_this.mode).subscribe(function (res) {
                var customizedPending = [];
                if (res.data.examList.length > 0) {
                    customizedPending = res.data.examList;
                    var exam = (customizedPending).map(function (item) {
                        item.customizedExam = true;
                        item.pending = true;
                        return item;
                    });
                    customizedPending = exam;
                    // customizedPending = customizedPending.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    _this.custmizedExam = (customizedPending.length > 0) ? _this.custmizedExam.concat(customizedPending) : _this.custmizedExam;
                }
                _this.examService.fetchGrandExam().subscribe(function (res) {
                    var completedGexam = [];
                    if (res.data.completedExam.length > 0) {
                        completedGexam = (res.data.completedExam).map(function (item) {
                            item.examType = "grand";
                            item.examName = item.name;
                            item.completed = true;
                            return item;
                        });
                        // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                        _this.custmizedExam = (completedGexam.length > 0) ? _this.custmizedExam.concat(completedGexam) : _this.custmizedExam;
                    }
                    if (res.data.pendingExams.length > 0) {
                        var pendingGexam = [];
                        pendingGexam = (res.data.pendingExams).map(function (item) {
                            item.examType = "grand";
                            item.examName = item.name;
                            item.pending = true;
                            return item;
                        });
                        //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                        _this.custmizedExam = (pendingGexam.length > 0) ? _this.custmizedExam.concat(pendingGexam) : _this.custmizedExam;
                    }
                    _this.custmizedExam = _this.custmizedExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                    if (_this.custmizedExam.length == 0) {
                        _this.__auth.notificationInfo("There is no exams found!");
                    }
                    _this.viewCtrl.dismiss(_this.custmizedExam, "All");
                });
            });
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-popoverPage',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/popoverPage/popoverPage.html"*/'<ion-list *ngIf="customized && !week && !daily ">\n    <ion-list-header>Exams</ion-list-header>\n    <button ion-item (click)="allExam(\'all\')">All</button>\n    <button ion-item (click)="allExam(\'pending\')">Pending</button>\n    <button ion-item (click)="allExam(\'completed\')">Completed</button>\n  </ion-list>\n  <ion-list *ngIf="week && !customized  || daily">\n    <ion-list-header>Exams</ion-list-header>\n    <button ion-item (click)="allWeekExam(\'all\')">All</button>\n    <button ion-item (click)="allWeekExam(\'pending\')">Pending</button>\n    <button ion-item (click)="allWeekExam(\'completed\')">Completed</button>\n    <button ion-item (click)="allWeekExam(\'demo\')">Demo</button>\n\n  </ion-list>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/popoverPage/popoverPage.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=popoverPage.js.map

/***/ }),

/***/ 445:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return grantModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var grantModal = /** @class */ (function () {
    function grantModal(viewCtrl, params, formBuilder) {
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.shortName = "grand exam";
    }
    grantModal.prototype.ionViewWillLoad = function () {
        this.quenos = ["300", "250", "200", "150", "100", "50"];
        this.timehour = ["1", "2", "3", "4", "5", "6"];
        this.timeMinut = ['15', '30', '45'];
    };
    grantModal.prototype.writeGrandExamNow = function (value) {
        if (value.minute || value.hour) {
            this.min = value.minute.toString();
            this.hou = value.hour.toString();
        }
        else {
            this.min = "";
            this.hou = "";
        }
        value.minute = this.min;
        value.hour = this.hou;
        this.viewCtrl.dismiss(value);
    };
    grantModal.prototype.startCheck = function (value, qValu) {
        console.log(qValu.viewModel, "queeeeeeeeeeeeeee");
        var res = qValu.viewModel * .7;
        this.hourss = Math.trunc(res / 60);
        this.defMint = res % 60;
    };
    grantModal.prototype.CancelClick = function () {
        this.viewCtrl.dismiss();
    };
    grantModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-grant',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/GrandExam/grantModal/grantModal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Create Exam</ion-title>\n\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <form #loginForm="ngForm" (ngSubmit)="writeGrandExamNow(loginForm.value)" novalidate>\n        <ion-list>\n\n            <ion-item>\n                <ion-label>Exam name</ion-label>\n                <ion-input type="text" [(ngModel)]="shortName" name="names" type="text" #names="ngModel" required></ion-input>\n\n                <!-- <ion-input type="text" #names="ngModel" id="name" name="names" required ngModel> -->\n                <!-- <ion-input [(ngModel)]="username" name="username" type="text" #usernames="ngModel" spellcheck="false" autocapitalize="off"\n        required> -->\n                <!-- </ion-input> -->\n\n            </ion-item>\n            <div class="error" *ngIf="names?.errors && (names?.touched || names?.dirty)">\n                <p class="error" [hidden]="!names.errors.required"> Please enter exam name </p>\n\n            </div>\n            <ion-item>\n                <ion-label>No of questions</ion-label>\n                <ion-select #questions="ngModel" name="nofoq" required ngModel (ngModelChange)="startCheck($event,questions)">\n                    <ion-option *ngFor="let key of quenos" value="{{key}}">{{key}}</ion-option>\n                </ion-select>\n            </ion-item>\n            <div class="error" *ngIf="questions?.errors && (questions?.touched || questions?.dirty)">\n                <p class="error" [hidden]="!questions.errors.required"> Please enter the no of questions </p>\n\n            </div>\n            <ion-row *ngIf="starttimer">\n\n                <ion-col width-50>\n                    <ion-item>\n                        <ion-label>Hours</ion-label>\n                        <ion-input type="number" min="0" step="1" max="3" name="hour" #hours="ngModel" [(ngModel)]="hourss">\n                        </ion-input>\n\n\n\n                    </ion-item>\n\n                </ion-col>\n                <ion-col width-50>\n                    <ion-item>\n                        <ion-label>minute</ion-label>\n                        <ion-input type="number" min="0" step="1" max="59" name="minute" #minute="ngModel" [(ngModel)]="defMint" ngModel value="00">\n                        </ion-input>\n\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n            <ion-item>\n                <ion-label>Start Timer</ion-label>\n                 \n                <ion-checkbox [(ngModel)]="starttimer" name="strtbox"></ion-checkbox>\n                </ion-item>\n            <ion-item>\n                <ion-label>Write later</ion-label>\n\n                <ion-checkbox [(ngModel)]="writelater" name="box"></ion-checkbox>\n            </ion-item>\n            <ion-item *ngIf="writelater">\n                <ion-label>Date</ion-label>\n                <ion-datetime displayFormat="YYYY-MM-DD" #date="ngModel" name="date" ngModel></ion-datetime>\n            </ion-item>\n\n        </ion-list>\n\n        <ion-row responsive-sm>\n\n            <ion-col>\n                <button type="submit" ion-button color="light" block *ngIf="!writelater" [disabled]="!loginForm.form.valid" style="background-color:#108fd2;color:white;">Write now</button>\n                <button type="submit" ion-button color="light" block *ngIf="writelater" [disabled]="!loginForm.form.valid" style="background-color:#108fd2; color:white;">Finish</button>\n            </ion-col>\n        </ion-row>\n\n\n\n    </form>\n    <ion-row responsive-sm>\n        <ion-col>\n            <button ion-button id="loginShow" type="submit" block style="background-color:#108fd2;" (click)="CancelClick()">Cancel</button>\n        </ion-col>\n    </ion-row>\n    <div>\n\n\n\n\n\n\n\n\n\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/GrandExam/grantModal/grantModal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], grantModal);
    return grantModal;
}());

//# sourceMappingURL=grantModal.js.map

/***/ }),

/***/ 446:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EvaluateOver; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_question_service__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Rx__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EvaluateOver = /** @class */ (function () {
    function EvaluateOver(navCtrl, questionService, navParams, examService) {
        this.navCtrl = navCtrl;
        this.questionService = questionService;
        this.navParams = navParams;
        this.examService = examService;
        this.examTotQuestions = 0;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
        this.subjectList = JSON.parse(localStorage.getItem('subjectData'));
    }
    EvaluateOver.prototype.ionViewDidLoad = function () {
        this.learnSum();
        this.examTotCount(this.userId, this.activeUserId);
    };
    EvaluateOver.prototype.learnSum = function () {
        var _this = this;
        console.log("subject list", this.subjectList);
        var subjectId;
        var count = 0;
        var promise = [];
        for (var i = 0; i < this.subjectList.length; i++) {
            subjectId = this.subjectList[i].id;
            promise.push(this.questionService.learnPerformance(this.activeUserId, subjectId));
        }
        __WEBPACK_IMPORTED_MODULE_5_rxjs_Rx__["Observable"].forkJoin(promise)
            .subscribe(function (response) {
            var total = 0;
            for (var i = 0; i < _this.subjectList.length; i++) {
                total += response[i]['data'].correct + response[i]['data'].wrong;
                if (i == 18) {
                    _this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](_this.doughnutCanvas.nativeElement, {
                        type: 'doughnut',
                        data: {
                            labels: ["Total", "Attend"],
                            datasets: [{
                                    label: '# of Votes',
                                    data: [28000, total],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)'
                                    ],
                                    hoverBackgroundColor: [
                                        "#FF6384",
                                        "#36A2EB"
                                    ]
                                }]
                        }
                    });
                }
            }
        });
    };
    EvaluateOver.prototype.examTotCount = function (userId, activeUserId) {
        // Exam question Count
        var _this = this;
        this.mode = 'completed';
        var count;
        this.examService.fetchAllExam(this.mode).subscribe(function (res) {
            _this.examList = res.data.examList;
            for (var i = 0; i < _this.examList.length; i++) {
                _this.examTotQuestions = _this.examTotQuestions + _this.examList[i]['examResult'].noOfQueAttended;
            }
            var count = res.data.examList.length;
            if (count > 0) {
                _this.examCount = res.data.examList.length;
            }
            else {
                _this.examCount = 0;
            }
        });
        // Grand test question count
        var complete, pending, compCount, pendCount;
        this.examService.fetchGrandExam().subscribe(function (res) {
            var pendingCount = 0;
            var completeCount = 0;
            var compCount = 0;
            compCount = res.data.completedExam.length;
            if (compCount > 0) {
                _this.grandCount = res.data.completedExam.length;
            }
            else {
                _this.grandCount = 0;
            }
            complete = res.data.completedExam;
            pending = res.data.pendingExams;
            for (var i = 0; i < pending.length; i++) {
                for (var j = 0; j < pending[i].questionPalette.length; j++) {
                    if (pending[i].questionPalette[j].status == 'answered') {
                        pendingCount = pendingCount + 1;
                    }
                }
            }
            //  For Completed Grand Exams
            for (var i = 0; i < complete.length; i++) {
                for (var j = 0; j < complete[i].questionPalette.length; j++) {
                    if (complete[i].questionPalette[j].status == 'answered') {
                        completeCount = completeCount + 1;
                    }
                }
            }
            _this.grandTotQuestions = completeCount + pendingCount;
            console.log(_this.grandTotQuestions, "grantg");
            _this.examTotQuestions = _this.grandTotQuestions + _this.examTotQuestions;
            console.log(_this.examTotQuestions, "value of last");
            //gdsgfdsg
            _this.suprExamGraph = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](_this.supExam.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ["Total", "Attend"],
                    datasets: [{
                            label: '# of Votes',
                            data: [28000, _this.examTotQuestions],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)'
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#36A2EB"
                            ]
                        }]
                }
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], EvaluateOver.prototype, "barCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], EvaluateOver.prototype, "doughnutCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('supExam'),
        __metadata("design:type", Object)
    ], EvaluateOver.prototype, "supExam", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], EvaluateOver.prototype, "lineCanvas", void 0);
    EvaluateOver = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-evaluateOver',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/evaluateOver/evaluateOver.html"*/'<!--\n  Generated template for the NestedTab1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Exam-overview</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="center" *ngIf="!subjectList">\n        \n       \n        <ion-spinner  ></ion-spinner>\n       \n      </div>\n\n    <ion-card  >\n        <ion-card-header >\n        Learn\n        </ion-card-header>\n        <ion-card-content>\n          <canvas #doughnutCanvas></canvas>\n        </ion-card-content>\n      \n      \n      </ion-card>\n      <ion-card  >\n          \n          <ion-card-content>\n              <ion-item *ngIf="examCount">\n                  <ion-icon name="ios-close" item-start></ion-icon>\n                  Exams Written\n                  \n                  \n                  <ion-badge item-end color="danger">{{examCount}}</ion-badge>\n                </ion-item>\n                <ion-item *ngIf="grandCount">\n                  <ion-icon name="ios-checkmark" item-start></ion-icon>\n                  Total Grant Tests\n                                    \n                  <ion-badge item-end color="secondary">{{grandCount}}</ion-badge>\n                </ion-item>\n              \n          </ion-card-content>\n        \n        \n        </ion-card>\n      <ion-card  >\n          <ion-card-header >\n          Exams\n          </ion-card-header>\n          <ion-card-content>\n            <canvas #supExam></canvas>\n          </ion-card-content>\n        \n        \n        </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/evaluateOver/evaluateOver.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_4__services_question_service__["a" /* QuestionService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_question_service__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */]])
    ], EvaluateOver);
    return EvaluateOver;
}());

//# sourceMappingURL=evaluateOver.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return examComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_exam_exam_room_aiimsResult_aiimsResult__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__exam_exam__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_exam_createexam_write_exam_write_exam__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_exam_createexam_aiimsExam_aiimsExam__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_exam_createexam_createExam__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_exam_GrandExam_grantexam_component__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_exam_weekend_TermsAndRules_terms_component__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_exam_weekend_weekend_component__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var examComponent = /** @class */ (function () {
    function examComponent(platform, navCtrl, examService, __auth, navParams, toastCtrl) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.examService = examService;
        this.__auth = __auth;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.isRegistered = false;
        this.completedWeekExam = [];
        this.pendingWeekExam = [];
        this.weekExam = [];
        this.pendingWeekExaminactive = [];
        this.pendingWeekexam = [];
        this.activeWeekExam = [];
        this.custmizedExam = [];
        this.shownGroup = null;
        this.dailyExams = [];
        this.totalWeek = [];
        this.dailyDiv = false;
        this.weekDiv = false;
        this.totalAiims = [];
        this.userData = JSON.parse(localStorage.getItem('userData'));
    }
    examComponent.prototype.ngOnInit = function () {
        this.getWeek();
        this.getCustomizedExam();
        this.checkAplay();
        this.getAiims();
        //this.getDailyExams();
        this.username = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
    };
    examComponent.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    examComponent.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__pages_dashboard_home__["a" /* HomePage */]);
    };
    examComponent.prototype.subscribe = function (category) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_exam_weekend_weekend_component__["a" /* weekendReg */], { exam: category });
    };
    examComponent.prototype.createExam = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_exam_createexam_createExam__["a" /* CreateExam */]);
    };
    examComponent.prototype.writeAiims = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_exam_createexam_aiimsExam_aiimsExam__["a" /* aiimsExam */]);
    };
    examComponent.prototype.getAiims = function () {
        var _this = this;
        this.examService.getSubscribeExam().subscribe(function (res) {
            console.log("res subcribed exams", res);
            if (res.data) {
                if (res.data.activeExams.length > 0) {
                    var exam = (res.data.activeExams).map(function (item) {
                        item.examPending = true;
                        item.examId = item._id;
                        return item;
                    });
                    _this.totalAiims = exam;
                }
                if (res.data.completedExams.length > 0) {
                    var completedAiims = [];
                    var exam = (res.data.completedExams).map(function (item) {
                        item.examCompleted = true;
                        item.examName = item.examDetails.name;
                        item.examDate = item.examDetails.examDate;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.numberOfQuestions = item.examResult.total;
                        item.examinformation = item.examDetails.description;
                        return item;
                    });
                    completedAiims = exam;
                    console.log("res.data.completedExams111111111111", _this.totalAiims);
                    _this.totalAiims = (completedAiims.length > 0) ? _this.totalAiims.concat(completedAiims) : _this.totalAiims;
                    console.log("res.data.completedExams", _this.totalAiims);
                }
                if (res.data.inactiveExams.length > 0) {
                    var exam = (res.data.inactiveExams).map(function (item) {
                        item.inactiveExam = true;
                        return item;
                    });
                    _this.totalAiims = (exam.length > 0) ? _this.totalAiims.concat(exam) : _this.totalAiims;
                }
                if (res.data.pendingExams.length > 0) {
                    var exam = (res.data.pendingExams).map(function (item) {
                        item.examPending = true;
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.examinformation = item.examDetails.description;
                        item.packageId = item.examDetails.packageId;
                        item.examDate = item.examDetails.examDate;
                        return item;
                    });
                    _this.totalAiims = (exam.length > 0) ? _this.totalAiims.concat(exam) : _this.totalAiims;
                    console.log("before sorting", _this.totalAiims);
                    _this.totalAiims = _this.totalAiims.sort(function (a, b) { return new Date(a.examDate).getTime() - new Date(b.examDate).getTime(); });
                    console.log("after sorting", _this.totalAiims);
                }
                console.log("total aiims exam", _this.totalAiims);
            }
            else {
                _this.__auth.notificationInfo("There is no data found!");
            }
        });
    };
    examComponent.prototype.getWeek = function () {
        var _this = this;
        this.examService.getWeekExams().subscribe(function (weekRes) {
            console.log("weeeekres", weekRes);
            if (weekRes.data) {
                if (weekRes.data.completedExams.length > 0) {
                    _this.completedWeekExam = weekRes.data.completedExams;
                    var exam_1 = (_this.completedWeekExam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.examCompleted = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        else if (item.examResult.totalQuestions) {
                            item.numberOfQuestions = item.examResult.totalQuestions;
                        }
                        return item;
                    });
                    _this.completedWeekExam = exam_1;
                    console.log(" this.completedWeekExam", _this.completedWeekExam);
                }
                if (weekRes.data.activeExams.length > 0) {
                    _this.activeWeekExam = weekRes.data.activeExams;
                    console.log("this.activeWeekExam", _this.activeWeekExam);
                    var exam_2 = (_this.activeWeekExam).map(function (item) {
                        //  item.examName =item.examDetails.name;
                        item.examPending = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    _this.activeWeekExam = exam_2;
                    console.log(" this.activeWeekExam", _this.activeWeekExam);
                    _this.completedWeekExam = (_this.activeWeekExam.length > 0) ? _this.completedWeekExam.concat(_this.activeWeekExam) : _this.completedWeekExam;
                }
                if (weekRes.data.inactiveExams.length > 0) {
                    _this.pendingWeekExaminactive = weekRes.data.inactiveExams;
                    var exam_3 = (_this.pendingWeekExaminactive).map(function (item) {
                        item.inactiveExam = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    _this.pendingWeekExaminactive = exam_3;
                    console.log("this.pendingWeekExaminactive", _this.pendingWeekExaminactive);
                    _this.completedWeekExam = (_this.pendingWeekExaminactive.length > 0) ? _this.completedWeekExam.concat(_this.pendingWeekExaminactive) : _this.completedWeekExam;
                }
                if (weekRes.data.pendingExams.length > 0) {
                    _this.pendingWeekexam = weekRes.data.pendingExams;
                    var exam_4 = (_this.pendingWeekexam).map(function (item) {
                        item.examName = item.examDetails.name;
                        item.examType = item.examDetails.examType;
                        item.examDate = item.examDetails.examDate;
                        item.examPending = true;
                        if (item.numberOfQuestions) {
                            item.numberOfQuestions = item.numberOfQuestions;
                        }
                        else if (item.noOfQuestions) {
                            item.numberOfQuestions = item.noOfQuestions;
                        }
                        return item;
                    });
                    _this.pendingWeekexam = exam_4;
                    console.log(" this.pendingWeekexam", _this.pendingWeekexam);
                    _this.completedWeekExam = (_this.pendingWeekexam.length > 0) ? _this.completedWeekExam.concat(_this.pendingWeekexam) : _this.completedWeekExam;
                }
                //  this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                var exam = (_this.completedWeekExam).map(function (item, index) {
                    if (item.examType == "daily") {
                        item.dailyExam = true;
                        _this.dailyExams.push(item);
                    }
                    else {
                        item.dailyExam = false;
                        _this.totalWeek.push(item);
                    }
                    return item;
                });
                _this.completedWeekExam = exam;
                console.log("qqqqqqqqqqqqqqqqqq", _this.totalWeek);
                _this.dailyExams = _this.dailyExams.sort(function (a, b) { return new Date(b.examDate).getTime() - new Date(a.examDate).getTime(); });
                _this.totalWeek = _this.totalWeek.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                console.log("wwwwwww", _this.dailyExams);
                if (_this.dailyExams.length == 0) {
                    _this.dailyDiv = true;
                    _this.__auth.notificationInfo("There is no daily exams found!");
                }
                else {
                    _this.dailyDiv = false;
                }
                if (_this.totalWeek.length == 0) {
                    _this.weekDiv = true;
                    _this.__auth.notificationInfo("There is no daily exams found!");
                }
                else {
                    _this.weekDiv = false;
                }
            }
            else {
                _this.__auth.notificationInfo("There is no data found!");
            }
        });
    };
    examComponent.prototype.getCustomizedExam = function () {
        var _this = this;
        this.mode = 'completed';
        this.examService.fetchAllExam(this.mode).subscribe(function (res) {
            console.log("res info////", res);
            if (res.data) {
                _this.custmizedExam = res.data.examList;
                var exam = (_this.custmizedExam).map(function (item) {
                    item.customizedExam = true;
                    item.completed = true;
                    return item;
                });
                _this.custmizedExam = exam;
                // this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                _this.mode = 'pending';
                _this.examService.fetchAllExam(_this.mode).subscribe(function (res) {
                    var customizedPending = [];
                    customizedPending = res.data.examList;
                    var exam = (customizedPending).map(function (item) {
                        item.customizedExam = true;
                        item.pending = true;
                        return item;
                    });
                    customizedPending = exam;
                    // customizedPending = customizedPending.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    _this.custmizedExam = (customizedPending.length > 0) ? _this.custmizedExam.concat(customizedPending) : _this.custmizedExam;
                    _this.examService.fetchGrandExam().subscribe(function (res) {
                        var completedGexam = [];
                        if (res.data.completedExam.length > 0) {
                            completedGexam = (res.data.completedExam).map(function (item) {
                                item.examType = "grand";
                                item.examName = item.name;
                                item.completed = true;
                                return item;
                            });
                            // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                            _this.custmizedExam = (completedGexam.length > 0) ? _this.custmizedExam.concat(completedGexam) : _this.custmizedExam;
                        }
                        if (res.data.pendingExams.length > 0) {
                            var pendingGexam = [];
                            pendingGexam = (res.data.pendingExams).map(function (item) {
                                item.examType = "grand";
                                item.examName = item.name;
                                item.pending = true;
                                return item;
                            });
                            //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                            _this.custmizedExam = (pendingGexam.length > 0) ? _this.custmizedExam.concat(pendingGexam) : _this.custmizedExam;
                        }
                        _this.custmizedExam = _this.custmizedExam.sort(function (a, b) { return new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime(); });
                        console.log("get all custm", _this.custmizedExam);
                        if (_this.custmizedExam.length == 0) {
                            _this.customDiv = true;
                        }
                        else {
                            _this.customDiv = false;
                        }
                    });
                });
            }
            else {
                _this.__auth.notificationInfo("There is no data found!");
            }
        });
    };
    examComponent.prototype.resumeExam = function (exam) {
        var _this = this;
        console.log("exammmm", exam);
        if (!exam.examType) {
            if (exam.examId) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */], { subject: exam._id });
            }
            else {
                this.examService.startExam(exam._id).subscribe(function (res) {
                    var userExamId = res.data.userExamId;
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */], { subject: userExamId });
                });
            }
        }
        else if (exam.examType == "grand") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_exam_GrandExam_grantexam_component__["a" /* grantExam */], { subject: exam._id });
        }
        else if (exam.examType == "week" || exam.examType == "special" || exam.examType == "daily") {
            console.log("exam._id ", exam._id, exam.examId);
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_exam_weekend_TermsAndRules_terms_component__["a" /* termsrules */], { subject: (exam.examId) ? exam.examId : exam._id });
        }
        else if (exam.examType == "AIIMS") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_exam_createexam_aiimsExam_aiimsExam__["a" /* aiimsExam */], { subject: exam.examId, package: exam.packageId });
        }
        else if (exam.examType == "JIPMER") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_exam_createexam_aiimsExam_aiimsExam__["a" /* aiimsExam */], { subject: exam.examId, package: exam.packageId });
        }
    };
    examComponent.prototype.getmoreWeek = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__exam_exam__["a" /* Exam */], { examType: "week" });
    };
    examComponent.prototype.getDailyExams = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__exam_exam__["a" /* Exam */], { examType: "daily" });
    };
    examComponent.prototype.getMoreAiims = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__exam_exam__["a" /* Exam */], { examType: "aiims" });
    };
    examComponent.prototype.getmoreCustomized = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__exam_exam__["a" /* Exam */], { examType: "cutomized" });
    };
    // backToCompletedPage() {
    //   this.navCtrl.push(HomePage);
    // }
    examComponent.prototype.checkAplay = function () {
        var _this = this;
        this.examService.checkAppliedForExam().subscribe(function (data) {
            if (data.status) {
                _this.registerLabel = "Register";
                _this.isRegistered = false;
                _this.paymentDetails = data;
            }
            else {
                _this.registerLabel = "Already registered";
                _this.RegisterMsg = "You are registered";
            }
        }, function (err) {
            _this.__auth.notificationInfo("OOPS Something went wrong.");
        });
    };
    examComponent.prototype.newExamfunction = function (plan, desc, item) {
        this.__auth.notificationInfo("only for institutional users");
        // if (item) {
        //   this.singleExamId = item._id;
        // }
        // this.plan = plan;
        // if (!this.isRegistered) {
        //   let options = {
        //     // key: 'rzp_test_V1RjYTq2Xll4NR',
        //     key: this.paymentDetails.data.keyId,
        //     amount: (this.paymentDetails.data.weeklyDetails[plan].amount) * 100,
        //     name: desc,
        //     prefill: {
        //       name: this.userData.profile.firstName + this.userData.profile.lastName,
        //       email: this.userData.username
        //     },
        //     notes: {
        //       address: ""
        //     },
        //     theme: {
        //       color: "#108fd2"
        //     }
        //   }
        //   var successCallback = (payment_id) => {
        //     this.callApi(payment_id);
        //   };
        //   var cancelCallback = (error) => {
        //   };
        //   this.platform.ready().then(() => {
        //     RazorpayCheckout.open(options, successCallback, cancelCallback);
        //   })
        // }
        // else this.__auth.notificationInfo('Somthing went wrong.')
    };
    examComponent.prototype.callApi = function (payment_id) {
        var _this = this;
        this.paymentId = payment_id;
        if (this.singleExamId) {
            this.postData = {
                examId: this.singleExamId,
                paymentId: this.paymentId,
                paymentStatus: "done",
                paymentMethod: "online",
                PlaneName: this.plan
            };
        }
        else {
            this.postData = {
                paymentId: this.paymentId,
                paymentStatus: "done",
                paymentMethod: "online",
                PlaneName: this.plan
            };
        }
        this.examService.SinglePaymentExam(this.postData)
            .subscribe(function (res) {
            if (res.status) {
                // localStorage.removeItem('paymentId');
                // this.registerLabel = "Already registered"
                // this.isRegistered = true;
                _this.checkAplay();
                _this.getWeek();
            }
        }, function (err) {
            _this.__auth.notificationInfo('Somthing went wrong.');
        });
    };
    examComponent.prototype.viewResult = function (item) {
        console.log("item", item);
        var exam_id;
        if (item.examType == "grand") {
            exam_id = item._id + '/g' + '/fromCompleted';
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: exam_id });
        }
        else if (item.examType === "week" || item.examType === "special" || item.examType === "daily") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: item._id + '/week' });
        }
        else if (item.examType == "AIIMS" || item.examType == "JIPMER") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_exam_exam_room_aiimsResult_aiimsResult__["a" /* aiimsResult */], { ExamIds: item.examId + '/' + item._id + '/' + item.examType });
        }
        else {
            exam_id = item.examId + '/' + item._id + '/fromCompleted';
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: exam_id });
        }
    };
    examComponent.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    examComponent.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    examComponent.prototype.AIIMSPAYROUTE = function () {
        this.__auth.notificationInfo("only for institutional users");
        // this.navCtrl.push(weekendReg)
    };
    examComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-examComponent',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/examNew/examComponent.html"*/'\n<ion-header>\n  <ion-toolbar>\n        <!-- <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button> -->\n        <ion-title>Exam Room</ion-title>\n        \n      </ion-toolbar>\n</ion-header>\n<!-- <ion-header>\n        <ion-navbar  style="color:#108fd2;">\n            <button ion-button menuToggle >\n              <ion-icon name="menu"></ion-icon>\n            </button>\n            <ion-title >Exam Room</ion-title>\n          </ion-navbar>\n    \n    </ion-header> -->\n    \n    <ion-content padding class="bgpic">\n      <div class="center" *ngIf="!completedWeekExam">\n        <ion-spinner></ion-spinner>\n        \n      </div>\n      <ion-row *ngIf="customDiv">\n          <ion-card style="height: 121px;" class="cdclr">\n              <ion-card-header>\n                <p style="font-size: 15px;\n                font-style: italic;\n                text-align: center;">If you want to write exam please create exam</p>\n\n                </ion-card-header>\n                <button class="strt-btn" ion-button outline (click)="createExam()" >Start</button>\n\n          </ion-card>\n\n        </ion-row>\n\n      <div *ngIf="!customDiv">\n      <ion-row>\n        <ion-col col-8> <h6>{{username}}\'s Exam</h6></ion-col>\n<ion-col col-4>\n  <a (click)="createExam()" style="float:right;font-size: 16px;color:#108fd2">Create Exam</a>\n</ion-col>\n      </ion-row>\n       \n    \n      <ion-card class="cdclr"  *ngFor="let item of custmizedExam | slice:0:3;   let i = index" (click)="toggleGroup(item)" >\n      <ion-card-header>\n      <ion-col col-12>\n            {{item.examName | slice:0:30}}\n            <!-- <ion-icon class="clr"  name="ios-information-circle-outline" (click)="toggleGroup(item)" ></ion-icon>&nbsp;&nbsp; -->\n\n        <ion-icon class="clr"   name="create"  *ngIf="item.pending " (click)="resumeExam(item)"></ion-icon>\n        <ion-icon class="clr" *ngIf="item.completed "   name="eye" (click)="viewResult(item)" ></ion-icon>\n\n      </ion-col>\n      </ion-card-header>\n      <div *ngIf="isGroupShown(item)">\n        <p    style="padding-left: 20px;\n        font-size: 14px;" >   {{item.examName}}</p>\n     \n        <p    style="padding-left: 20px;\n        font-size: 14px;" >{{item.createdDate | date: "yyyy/MM/dd"}}</p>\n        <p    style="padding-left: 20px;\n        font-size: 14px;" >{{item.numberOfQuestions||item.noOfQuestions}} No of questions</p>\n    </div>\n      \n    </ion-card>\n   \n    \n     <ion-row>\n          <ion-col col-12 style="text-align: right;" ><a style="color:#108fd2" (click)="getmoreCustomized()">More</a></ion-col>\n        </ion-row>\n</div>\n      \n\n\n\n        <ion-row>\n          <ion-col col-8>\n              <h6>Weekend exam</h6>\n          </ion-col>\n          <!-- <ion-col col-4><a (click)="subscribe(\'week\')" style="float:right;font-size: 16px;color:#108fd2">Pro users</a></ion-col> -->\n\n        </ion-row>\n       \n        <ion-list>\n    \n        \n          <ion-card class="cdclr" *ngFor="let items of totalWeek | slice:0:3;  let i = index" (click)="toggleGroup(items)">\n          \n              <ion-card-header >\n    \n             \n          <ion-col col-12 >\n              <span> {{items.examName | slice:0:30}}</span> \n\n             <!-- <ion-icon class="clr"  name="ios-information-circle-outline" (click)="toggleGroup(items)" ></ion-icon>&nbsp;&nbsp; -->\n    \n            <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)" ></ion-icon>\n             <ion-icon class="clr"  name="create"  *ngIf="items.examPending" (click)="resumeExam(items)"></ion-icon> \n             <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="newExamfunction(\'single\', items.examName, items)"></ion-icon> \n    \n          </ion-col>\n          </ion-card-header>\n        \n        <div *ngIf="isGroupShown(items) ">\n          \n          <p    style="padding-left: 20px;\n          font-size: 14px;" >   {{items.examName}}</p>\n           <p    style="padding-left: 20px;\n           font-size: 14px;"  [innerHtml]="items.examinformation" *ngIf="items.examinformation"></p>\n           <p style="padding-left: 20px;\n           font-size: 14px;" >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n           <p    style="padding-left: 20px;\n        font-size: 14px;"  *ngIf="items.numberOfQuestions  ">{{items.numberOfQuestions}} No of questions</p>\n\n        <!-- <p    style="padding-left: 20px;\n        font-size: 14px;"  *ngIf=" items.examType==\'daily\' && items.noOfQuestions ">{{items.noOfQuestions}} No of questions</p> -->\n        </div>\n       \n      </ion-card>\n        </ion-list>\n            <ion-row>\n              <ion-col col-12 style="text-align: right;"><a (click)="getmoreWeek()">More</a></ion-col>\n            </ion-row>\n        \n       <!-- daily exams -->\n       <div *ngIf="!dailyDiv">\n       <ion-row >\n        <ion-col col-8>\n            <h6>Daily exam</h6>\n        </ion-col>\n        <!-- <ion-col col-4><a (click)="subscribe(\'daily\')" style="float:right;font-size: 16px;color:#108fd2">Pro users</a></ion-col> -->\n\n      </ion-row>\n     \n      <ion-list>\n  \n      \n        <ion-card class="cdclr" *ngFor="let items of dailyExams | slice:0:3;  let i = index" (click)="toggleGroup(items)">\n        \n            <ion-card-header >\n  \n           \n        <ion-col col-12 >\n            <span > {{items.examName | slice:0:30}}</span> \n\n           <!-- <ion-icon class="clr"  name="ios-information-circle-outline" (click)="toggleGroup(items)" ></ion-icon>&nbsp;&nbsp; -->\n  \n          <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)" ></ion-icon>\n           <ion-icon class="clr"  name="create"  *ngIf="items.examPending" (click)="resumeExam(items)"></ion-icon> \n           <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="newExamfunction(\'single\', items.examName, items)"></ion-icon> \n  \n        </ion-col>\n        </ion-card-header>\n      \n      <div *ngIf="isGroupShown(items) ">\n        \n        <p    style="padding-left: 20px;\n        font-size: 14px;" >   {{items.examName}}</p>\n         <p    style="padding-left: 20px;\n         font-size: 14px;"  [innerHtml]="items.examinformation" *ngIf="items.examinformation"></p>\n         <p style="padding-left: 20px;\n         font-size: 14px;" >{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n         <p    style="padding-left: 20px;\n      font-size: 14px;"  *ngIf="items.numberOfQuestions  ">{{items.numberOfQuestions}} No of questions</p>\n\n      <!-- <p    style="padding-left: 20px;\n      font-size: 14px;"  *ngIf=" items.examType==\'daily\' && items.noOfQuestions ">{{items.noOfQuestions}} No of questions</p> -->\n      </div>\n     \n    </ion-card>\n      </ion-list>\n          <ion-row>\n            <ion-col col-12 style="text-align: right;"><a (click)="getDailyExams()">More</a></ion-col>\n          </ion-row>\n      </div>\n    <!--AIMS?JIPMER-->\n    <div>\n      <ion-row >\n       <ion-col col-8>\n           <h6>AIIMS/JIPMER</h6>\n       </ion-col>\n       <!-- <ion-col col-4><a (click)="subscribe(\'aiims\')" style="float:right;font-size: 16px;color:#108fd2">Pro users</a></ion-col> -->\n\n     </ion-row>\n    \n     <ion-list>\n  \n      \n      <ion-card class="cdclr" *ngFor="let items of totalAiims | slice:0:3;  let i = index" (click)="toggleGroup(items)">\n      \n          <ion-card-header >\n\n         \n      <ion-col col-12 >\n          <span > {{items.examName | slice:0:30}}</span> \n\n         <!-- <ion-icon class="clr"  name="ios-information-circle-outline" (click)="toggleGroup(items)" ></ion-icon>&nbsp;&nbsp; -->\n\n        <ion-icon class="clr" *ngIf="items.examCompleted "   name="eye" (click)="viewResult(items)" ></ion-icon>\n         <ion-icon class="clr"  name="create"  *ngIf="items.examPending" (click)="resumeExam(items)"></ion-icon> \n         <ion-icon class="clr"  name="lock"  *ngIf="items.inactiveExam" (click)="AIIMSPAYROUTE(\'single\', items.examName, items)"></ion-icon> \n\n      </ion-col>\n      </ion-card-header>\n    \n    <div *ngIf="isGroupShown(items) ">\n      \n      <p    style="padding-left: 20px;\n      font-size: 14px;" >   {{items.examName}}</p>\n       <p    style="padding-left: 20px;\n       font-size: 14px;"  *ngIf="items.examinformation">{{items.examinformation}}</p>\n       <p style="padding-left: 20px;\n       font-size: 14px;">{{items.examDate || items.examDetails.examDate | date: "yyyy/MM/dd"}}</p>\n       <p    style="padding-left: 20px;\n    font-size: 14px;"  *ngIf="items.numberOfQuestions  ">{{items.numberOfQuestions}} No of questions</p>\n\n        </div>\n   \n  </ion-card>\n    </ion-list>\n       \n         <ion-row>\n           <ion-col col-12 style="text-align: right;"><a (click)="getMoreAiims()">More</a></ion-col>\n         </ion-row>\n     </div>\n   \n    </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/examNew/examComponent.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */]])
    ], examComponent);
    return examComponent;
}());

//# sourceMappingURL=examComponent.js.map

/***/ }),

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamOver; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_exam_service__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ExamOver = /** @class */ (function () {
    function ExamOver(navCtrl, toastCtrl, navParams, examService) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.examService = examService;
        this.ExamLoad = false;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
    }
    ExamOver.prototype.ionViewDidLoad = function () {
        this.examTotCount(this.userId, this.activeUserId);
        this.ExamLoad = false;
        var toast = this.toastCtrl.create({
            message: "Please select Exam",
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    ExamOver.prototype.examTotCount = function (userId, activeUserId) {
        var _this = this;
        this.mode = 'completed';
        this.examService.fetchAllExam(this.mode).subscribe(function (res) {
            console.log(res, "1");
            _this.examList = res.data.examList;
            if (_this.examList) {
                var toast = _this.toastCtrl.create({
                    message: "Please try Exams",
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        });
        // Grand test question count
    };
    ExamOver.prototype.evaluateExam = function (tot, corr, wrng, attend, item) {
        this.ExamLoad = true;
        this.examOverTot = attend; //Total questions attended in exam
        this.examCorr = corr; //Number of correct in exam
        this.examWrong = wrng; //Number of wrong in exam
        this.examQstnCount = tot; //Total questions in exam
        if (this.examQstnCount && this.examOverTot) {
            this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.doughnutCanvas.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ["Total", "Attend"],
                    datasets: [{
                            label: '# of Votes',
                            data: [this.examQstnCount, this.examOverTot],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)'
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#36A2EB"
                            ]
                        }]
                }
            });
        }
        else {
            var toast = this.toastCtrl.create({
                message: "No data available",
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], ExamOver.prototype, "barCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('ExamGraph'),
        __metadata("design:type", Object)
    ], ExamOver.prototype, "ExamGraph", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], ExamOver.prototype, "lineCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], ExamOver.prototype, "doughnutCanvas", void 0);
    ExamOver = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-examOver',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/examOver/examOver.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Exam view</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="col-md-12" style="text-align: center;color: #0099cc;margin-bottom: 30px;">\n        <h2>Exams</h2>\n    </div>\n      <div class="scroll" style="padding:35px; background-color:#f0f0f0;border:2px,solid;  border-radius: 5px;">\n    \n        <a class="btn btn-info subject" *ngFor="let item of examList; let i = index" data-id="{{i}}" (click)="evaluateExam(item.noOfQuestions,item.examResult.noOfCorrectQue,item.examResult.noOfWrongQue,item.examResult.noOfQueAttended,item)" style= "background-color:#108fd2;color:#ffffff;padding:15px; font-size:18px;">\n            {{item.examName}} &nbsp;\n          <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown"></ion-icon>\n    \n        </a>\n    </div>\n    <!-- <div class="col-sm-12">\n        \n        <div class="col-md-12" style="text-align: center;color: #0099cc;margin-bottom: 30px;">\n            <h2>Exams</h2>\n        </div>\n<div class="bodr-1" style="margin-bottom: 25px;">\n<div class="scroll">\n      <a class="btn btn-info subject" *ngFor="let item of examList; let i = index" (click)="evaluateExam(item.noOfQuestions,item.examResult.noOfCorrectQue,item.examResult.noOfWrongQue,item.examResult.noOfQueAttended,item)">\n              {{item.examName}}\n          </a>\n  </div>\n</div>\n</div><br><br> -->\n<ion-card  [hidden]="!ExamLoad">\n  <ion-card-header >\n  Exams\n  </ion-card-header>\n  <ion-card-content>\n    <canvas #doughnutCanvas></canvas>\n  </ion-card-content>\n  <ion-item *ngIf="examWrong">\n    <ion-icon name="ios-close" item-start></ion-icon>\n    Wrong\n    <ion-badge item-end color="danger">{{examWrong}}</ion-badge>\n  </ion-item>\n  <ion-item *ngIf="examCorr">\n    <ion-icon name="ios-checkmark" item-start></ion-icon>\n    Correct\n    <ion-badge item-end color="secondary">{{examCorr}}</ion-badge>\n  </ion-item>\n\n</ion-card>\n\n\n  \n</ion-content>\n'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/examOver/examOver.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */]])
    ], ExamOver);
    return ExamOver;
}());

//# sourceMappingURL=examOver.js.map

/***/ }),

/***/ 577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LearnOver; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_question_service__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LearnOver = /** @class */ (function () {
    function LearnOver(navCtrl, toastCtrl, navParams, questionService) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.questionService = questionService;
        this.sub_percent = 0;
        this.firstLoad = false;
        this.subArray = [];
        this.subjectList = JSON.parse(localStorage.getItem('subjectData'));
        console.log("chapterListchapterListchapterListchapterList", this.chapterList);
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
    }
    LearnOver.prototype.ionViewDidLoad = function () {
        // this.DataInfo={test:"data",test2:"data2"}
        // this.firstLoad = false;
        // const toast = this.toastCtrl.create({
        //     message: "Please select subject",
        //     duration: 3000,
        //     position: 'bottom'
        //   });
        //   toast.present();
        var _this = this;
        console.log("subject list", this.subjectList);
        for (var i = 0; i < this.subjectList.length; i++) {
            this.questionService.totalQuestions(this.subjectList[i].id).subscribe(function (res) {
                //  console.log("res",res);
                if (res.status) {
                    // this.TotalCount = res.data.totalNumber;
                    // console.log("total number",this.TotalCount);
                }
                else {
                }
            }, function (err) {
            });
            this.questionService.learnPerformance(this.activeUserId, this.subjectList[i].id).subscribe(function (res) {
                console.log("rem///////mmms", res);
                _this.DataInfo = res.data;
                _this.correct = res.data.correct;
                _this.wrong = res.data.wrong;
                _this.TotalScore = _this.correct + _this.wrong;
                console.log("total score", _this.TotalScore);
                _this.TotalCount = 500;
                _this.sub_percent = (_this.TotalScore / _this.TotalCount) * 100;
                _this.subArray.push();
                if (_this.TotalScore) {
                    _this.firstLoad = true;
                    _this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](_this.doughnutCanvas.nativeElement, {
                        type: 'doughnut',
                        data: {
                            labels: ["Total", "Attend"],
                            datasets: [{
                                    label: _this.sub_percent,
                                    data: [_this.TotalCount, _this.TotalScore],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)'
                                    ],
                                    hoverBackgroundColor: [
                                        "#FF6384",
                                        "#36A2EB"
                                    ]
                                }]
                        }
                    });
                    var data = (_this.subjectList).map(function (item, i) {
                        item.graph = "doughnutCanvas" + i;
                        return item;
                    });
                    if (data) {
                        _this.subjectList = data;
                        console.log("subject", data);
                    }
                }
                else {
                    console.log("no data");
                }
            });
        }
    };
    LearnOver.prototype.evaluateLearn = function (item, count) {
        var _this = this;
        this.TotalCount = count;
        var subID = item;
        this.questionService.learnPerformance(this.activeUserId, subID).subscribe(function (res) {
            _this.DataInfo = res.data;
            _this.correct = res.data.correct;
            _this.wrong = res.data.wrong;
            _this.TotalScore = _this.correct + _this.wrong;
            if (_this.TotalScore) {
                _this.firstLoad = true;
                _this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](_this.doughnutCanvas.nativeElement, {
                    type: 'doughnut',
                    data: {
                        labels: ["Total", "Attend"],
                        datasets: [{
                                label: '# of Votes',
                                data: [_this.TotalCount, _this.TotalScore],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)'
                                ],
                                hoverBackgroundColor: [
                                    "#FF6384",
                                    "#36A2EB"
                                ]
                            }]
                    }
                });
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: "No data available",
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "barCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas0", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas6", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas7", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas8", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas9", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas10", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas11", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas12", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas13", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas14", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas15", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas16", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas17", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "doughnutCanvas18", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], LearnOver.prototype, "lineCanvas", void 0);
    LearnOver = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-learnOver',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/learnOver/learnOver.html"*/'<!--\n  Generated template for the NestedTab1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>LearnoverView</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content padding>\n    <ion-card >\n      <!-- <canvas #doughnutCanvas></canvas> -->\n    </ion-card>\n    <ion-card class="cardStyle" style="height: 112px;">\n      <ion-scroll scrollX="true" style="width:100vw;height:350px;" style="text-align: center;margin-top:0px;">\n\n          <ion-col class="scroll-item " *ngFor="let subject of subjectList; let i=index;">\n              <ion-row>\n                  <ion-col col-12>\n                      \n                    <canvas id="{{subject.graph}}" ></canvas>\n                   \n                  </ion-col>\n                  <!-- <ion-col col-12>\n                    <span>{{subject.name}} </span>\n                  </ion-col> -->\n              </ion-row>\n          </ion-col>\n      </ion-scroll>\n\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/evaluate/learnOver/learnOver.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_question_service__["a" /* QuestionService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_question_service__["a" /* QuestionService */]])
    ], LearnOver);
    return LearnOver;
}());

//# sourceMappingURL=learnOver.js.map

/***/ }),

/***/ 578:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return privacyPolicy; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var privacyPolicy = /** @class */ (function () {
    function privacyPolicy(navCtrl) {
        this.navCtrl = navCtrl;
    }
    privacyPolicy = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-privacyPolicy',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/privacyPolicy/privacyPolicy.html"*/'<ion-header>\n        <ion-navbar>\n          <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n          </button>\n          <ion-title>privacy Policy</ion-title>\n        </ion-navbar>\n      </ion-header>\n      <ion-content padding class="bgpic">\n         \n                <ion-list>\n                        <ion-list-header text-wrap>\n                                SECTION 1 - WHAT DO WE DO WITH YOUR INFORMATION?\n                        </ion-list-header>\n                        <ion-item text-wrap>When you signup to our site, app, we collect the personal information you give us such as your name, address and email addres</ion-item>\n                        <ion-item text-wrap>When you browse our store, we also automatically receive your computer’s internet protocol (IP) address in order to provide us with information that helps us learn about your browser and operating system.</ion-item>\n                        <ion-item text-wrap>Email marketing (if applicable): With your permission, we may send you emails about our store, new products and other updates.</ion-item>\n                      \n                    <ion-item text-wrap>\n                            Push notifications (if applicable): With your permission, we notify you about our store, new products and other updates.\n                    </ion-item>\n                </ion-list>\n                <ion-list>\n                        <ion-list-header text-wrap>\n                                SECTION 2 - CONSENT\n                        </ion-list-header>\n                        <ion-item text-wrap>How do you get my consent?</ion-item>\n                        <ion-item text-wrap>When you provide us with personal information to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only.</ion-item>\n                        <ion-item text-wrap>If we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.</ion-item>\n                      \n                    <ion-item text-wrap>\n                            How do I withdraw my consent?\n\n                    </ion-item>\n                    <ion-item text-wrap>\n                            If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us at contactus@nextfellow.com\n                    </ion-item>\n                </ion-list>\n                <ion-list>\n                        <ion-list-header text-wrap>\n                                SECTION 3 - DISCLOSURE\n                        </ion-list-header>\n                        <ion-item text-wrap>We may disclose your personal information if we are required by law to do so or if you violate our Terms of Service.</ion-item>\n                        <ion-item text-wrap>When you provide us with personal information to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only.</ion-item>\n                       \n                    \n                </ion-list>\n          \n                <ion-list>\n                        <ion-list-header text-wrap>\n                                SECTION 4 - PAYMENT\n                        </ion-list-header>\n                        <ion-item text-wrap>We use third party services for processing payments. We do not store your card data on their servers. The data is encrypted through the Payment Card Industry Data Security Standard (PCI-DSS) when processing payment. Your purchase transaction data is only used as long as is necessary to complete your purchase transaction. After that is complete, your purchase transaction information is not saved.</ion-item>\n                       \n                    \n                </ion-list>\n          </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/privacyPolicy/privacyPolicy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]])
    ], privacyPolicy);
    return privacyPolicy;
}());

//# sourceMappingURL=privacyPolicy.js.map

/***/ }),

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return returnPolicy; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var returnPolicy = /** @class */ (function () {
    function returnPolicy(navCtrl) {
        this.navCtrl = navCtrl;
    }
    returnPolicy = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-returnPolicy',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/returnPolicy/returnPolicy.html"*/'<ion-header>\n        <ion-navbar>\n          <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n          </button>\n          <ion-title>Return Policy</ion-title>\n        </ion-navbar>\n      </ion-header>\n      <ion-content padding class="bgpic">\n            <ion-list>\n                    \n                    <ion-item text-wrap>\n                                <p style="font-size: 14px; background-color:white;">If you are not 100% satisfied with your purchase, within 30 days from the purchase date, we will fully refund the cost of your order.</p>\n                        Voucher codes cannot be re-issued and are non-refundable. If you return an item which was purchased using a voucher code, only the final paid price (after discount) will be refunded</ion-item>\n                    <ion-item text-wrap>This is not applicable for services like and not limited to mock tests. For which no refunds will be issued after the exam date or the date of action.</ion-item>\n                   <ion-item text-wrap>\n                        Refunds (if applicable)\n                   </ion-item>\n                <ion-item text-wrap>\n                        Once your return request is received, we will notify you of the approval or rejection of your refund. If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within 10 working days.\n                </ion-item>\n                <ion-item text-wrap>\n                        Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.\n                </ion-item>\n                <ion-item text-wrap>\n                        Late or missing refunds (if applicable)\n                </ion-item>\n                <ion-item text-wrap>\n                        If you haven’t received a refund yet, first check your bank account again. Then contact your credit card company, it may take some time before your refund is officially posted. Next contact your bank. There is often some processing time before a refund is posted. If you’ve done all of this and you still have not received your refund yet, please contact us at contactus@nextfellow.com.\n                </ion-item>\n            </ion-list>\n          </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/returnPolicy/returnPolicy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]])
    ], returnPolicy);
    return returnPolicy;
}());

//# sourceMappingURL=returnPolicy.js.map

/***/ }),

/***/ 580:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return contactUs; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var contactUs = /** @class */ (function () {
    function contactUs(navCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
    }
    contactUs.prototype.Feedback = function (value) {
        console.log("value", value);
        if (value.email) {
            var email = value.email.toLowerCase();
            var toast = this.toastCtrl.create({
                message: "Thank you for your honest feedback,success",
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        }
    };
    contactUs = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contactUs',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/contactUs/contactUs.html"*/'<ion-header>\n        <ion-navbar>\n          <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n          </button>\n          <ion-title>Contact Us</ion-title>\n        </ion-navbar>\n      </ion-header>\n      <ion-content class="bgpic" >\n          <div>\n          <ion-grid>\n                <ion-card>\n                    <ion-row>\n                            <ion-col col-6 style="padding-left: 8px;\n                            padding-top: 13px;">\n                    <h2>\n                            <b>Registered Address</b>\n                    </h2>\n                    <p><b>Nellikavu,\n                            <br> Karaparamba,\n                            <br> Bilathikkulam,\n                            <br> Kozhikode,\n                            <br> Kerala - 673010</b></p>\n                        </ion-col>\n                       \n                        <ion-col col-6 style="padding-left: 20px;\n                        padding-top: 13px;">\n               <h2><b>Office Address</b></h2>\n               <p><b>KSIDC BIC,\n                   <br> UL CyberPark,\n                   <br> Kozhikode,\n                   <br> Kerala - 673016</b></p>    \n</ion-col>\n\n</ion-row>            \n                </ion-card>\n                </ion-grid>\n                </div>\n                <ion-row responsive-sm  style="margin-top: 41px;">\n         \n                        <ion-col col-3 >\n                          <a button  ion-fab style="background-color: #fff;width: 42px;\n                          height: 42px; margin-left: 23%" href="https://www.facebook.com/nextfellow">\n                            <ion-icon  name="logo-facebook" style="color: #3b5999;"></ion-icon>\n                          </a>\n                        </ion-col>\n                        <ion-col col-3   >\n                          <a button float-center ion-fab style="background-color: #fff;width: 42px;\n                          height: 42px; margin-left: 23%" href="https://plus.google.com/communities/101941017720698983479">\n                            <ion-icon name="logo-googleplus" style="color:#dd4b39;"></ion-icon>\n                          </a>\n                        </ion-col>\n                        <ion-col col-3  >\n                          <a button float-center ion-fab style="background-color: #fff;width: 42px;\n                          height: 42px; margin-left: 23%" href="https://www.linkedin.com/company/nextfellow">\n                            <ion-icon name="logo-linkedin" style="color:#0077B5;"></ion-icon>\n                          </a>\n                        </ion-col>\n                        <ion-col col-3 >\n                          <a button  ion-fab style="background-color: #fff;width: 42px;\n                          height: 42px; margin-left: 23%" href="https://twitter.com/nextfellow">\n                            <ion-icon name="logo-twitter" style="color:#55acee;"></ion-icon>\n                          </a>\n              \n                        </ion-col>\n                       \n                      </ion-row>\n                      <ion-grid>\n                        <p class="hdstyle">Want to share your valuable opinions</p>\n                        <p style="font-size: 14px;\n                        text-align: center;\n                        font-family: initial;">\n                            We are thrilled to get feedback from you, Your opinions and comments are important\n                        </p>\n                        <div style="\n                      height: 213px;\n                      width: 100%; \n                      ">\n                      <form  #subForm="ngForm" (ngSubmit)="Feedback(subForm.value)" novalidate>\n                            <ion-list>\n                        \n                              <ion-item>\n                                <ion-textarea placeholder="Message here" \n                                [(ngModel)]="message" name="message" autocomplete="on" autocorrect="on"></ion-textarea>\n                                <!-- <ion-input type="text" #email="ngModel" id="email" pattern="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$"\n                                  name="email" required ngModel> -->\n                                  <!-- <ion-input [(ngModel)]="username" name="username" type="text" #usernames="ngModel" spellcheck="false" autocapitalize="off"\n                               required> -->\n                               \n                        \n                              </ion-item>\n                              <div class="error" *ngIf="message?.errors && (message?.touched || message?.dirty)">\n                               \n                              </div>\n                            </ion-list>\n                           \n                                    <button ion-button style="float:right" class="bgcolor" type="submit"  >Send</button>\n                                 \n                            </form>\n\n                      </div>\n                    </ion-grid>\n          </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/contactUs/contactUs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */]])
    ], contactUs);
    return contactUs;
}());

//# sourceMappingURL=contactUs.js.map

/***/ }),

/***/ 581:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return timetable; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var timetable = /** @class */ (function () {
    function timetable(navCtrl, examservice) {
        this.navCtrl = navCtrl;
        this.examservice = examservice;
    }
    timetable = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-timetable',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/timetable/timetable.html"*/'<ion-header>\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Timetable</ion-title>\n    </ion-navbar>\n  </ion-header>\n  <ion-content padding class="bgpic">\n        <img src="assets/img/timetable.jpg" alt="evaluate" style="border: none; width: 100%;height: 104%;" >\n\n      </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/timetable/timetable.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]])
    ], timetable);
    return timetable;
}());

//# sourceMappingURL=timetable.js.map

/***/ }),

/***/ 582:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return moreSetting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_profile_profile__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__commen_setting_about_about__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__commen_setting_FAQ_faq__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__commen_setting_teachersPage_teachers__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_navBarService__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var moreSetting = /** @class */ (function () {
    function moreSetting(_navBar, navCtrl, platform, authservice, fireBase) {
        this._navBar = _navBar;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.authservice = authservice;
        this.fireBase = fireBase;
    }
    moreSetting.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    moreSetting.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_dashboard_home__["a" /* HomePage */]);
    };
    moreSetting.prototype.ngOnInit = function () {
        this.userData = JSON.parse(localStorage.getItem('userData'));
        console.log("userdata", this.userData);
        this.UserName = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
    };
    moreSetting.prototype.aboutPage = function () {
        console.log("kkkkkkkkkkkkkkkkkk");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__commen_setting_about_about__["a" /* About */]);
    };
    moreSetting.prototype.faq = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__commen_setting_FAQ_faq__["a" /* faq */]);
    };
    moreSetting.prototype.teachers = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__commen_setting_teachersPage_teachers__["a" /* teachers */]);
    };
    moreSetting.prototype.EditProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_profile_profile__["a" /* Profile */]);
    };
    moreSetting.prototype.logoutFromSocial = function () {
        this.fireBase.auth.signOut();
    };
    moreSetting = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-moreSettings',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/moreSettings/moreSettings.html"*/'<ion-header>\n    <ion-navbar>\n      \n      <ion-title>Menu</ion-title>\n    </ion-navbar>\n  </ion-header>\n  <ion-content class="bgpic">\n        <!-- <ion-list no-border>\n                <ion-list-header style="padding-left:0px;">\n                <ion-grid>\n                    <ion-row>\n          <ion-col col-3><img style="width:60px;height:60px;" src="assets/img/image.png"></ion-col>\n          <ion-col col-5 style="padding-top:19px;"><p class="username"><b>{{UserName}}</b></p></ion-col>\n          <ion-col col-4 style="text-align: right;">\n              <ion-icon item-end  name="ios-arrow-forward" style="font-size: 33px;\n              color: #108fd2;\n              font-weight: bold;" (click)="EditProfile()"></ion-icon>\n            </ion-col>\n          \n            </ion-row>\n                  </ion-grid>\n                  </ion-list-header>\n                  </ion-list> -->\n\n                  <ion-grid style="background-color:#108fd2; border: 2px solid #108fd2; border-radius: 15px 15px 0px 0px;">\n                  <ion-row >\n                    <ion-col col-12 style="margin-top:13px;">\n                      <img class="img-circles" src="assets/img/image-2.png" style="width: 125px;border:2px solid white;">\n                    </ion-col>\n                    <ion-col col-12>\n                      <p class="username">{{UserName}}</p>\n                    </ion-col>\n                  </ion-row>\n                  </ion-grid>\n\n<ion-grid>\n                  <ion-row>\n                    <ion-col col-12>\n                      <button ion-button outline (click)="EditProfile()" item-end  style="color: #108fd2; padding: 0px 50px 0px 50px; display: block; margin: auto;\n                   ">\n                          Edit\n                          </button>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n\n                  <ion-list style="border: 1px solid #108fd2; background-color:#108fd2;">\n\n                       \n                    <!-- <ion-list-header>\n                      <b>General</b> \n                      </ion-list-header> -->\n                         <ion-item>\n          <ion-toggle [(ngModel)]="enableNotifications" (click)="toggleNotifications()"  ></ion-toggle>\n        <ion-label>\n         Enable notification\n        </ion-label>\n        <ion-icon name=\'notifications\' style="color:#108fd2" item-start></ion-icon>\n      </ion-item>\n      <!-- <ion-item  (click)="faq()">\n            <ion-icon name=\'md-help\'  style="color:#108fd2" item-start></ion-icon>\n           FAQ\n            <ion-icon name="ios-arrow-forward-outline" item-end   style="color:#108fd2"></ion-icon>\n          </ion-item> -->\n      \n      <ion-item (click)="aboutPage()">\n        <ion-icon name=\'md-information-circle\'  style="color:#108fd2" item-start></ion-icon>\n        About us\n        <ion-icon name="ios-arrow-forward-outline"  style="color:#108fd2" item-end ></ion-icon>\n      </ion-item  >\n</ion-list>\n      </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/setting/moreSettings/moreSettings.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_9__services_navBarService__["a" /* naveBarShowSrvice */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__services_navBarService__["a" /* naveBarShowSrvice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], moreSetting);
    return moreSetting;
}());

//# sourceMappingURL=moreSettings.js.map

/***/ }),

/***/ 583:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Forgot; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__commen_login_login__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Forgot = /** @class */ (function () {
    function Forgot(navCtrl, authservice, toastCtrl) {
        this.navCtrl = navCtrl;
        this.authservice = authservice;
        this.toastCtrl = toastCtrl;
        this.login = __WEBPACK_IMPORTED_MODULE_2__commen_login_login__["a" /* Login */];
        this.spinnerLoad = false;
    }
    Forgot.prototype.Reset = function (value) {
        var _this = this;
        this.spinnerLoad = true;
        var emails = {
            email: value.email.toLowerCase()
        };
        console.log(emails, "emails");
        this.authservice.forgotPassword(emails).subscribe(function (res) {
            _this.spinnerLoad = false;
            console.log(res);
            if (res.status == 1) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__commen_login_login__["a" /* Login */]);
                var toast = _this.toastCtrl.create({
                    message: 'Reset Email Sent to user',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
            else if (res.status == 0) {
                var toast = _this.toastCtrl.create({
                    message: res.msg,
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: "OOPS! Somethng went wrong.",
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (err) {
            var toast = _this.toastCtrl.create({
                message: "OOPS! Somethng went wrong.",
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
        /* Write function to send email to users inbox */
    };
    Forgot = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forgot',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/forgot/forgot.html"*/'\n  <ion-content padding class="bgpic">\n    \n    <ion-grid>\n      <ion-row responsive-sm>\n        <ion-col>\n          <div >\n   \n            <img src="assets/img/logo-2.png" alt="logo"style="width: 130px;text-align:center;"> \n          </div>\n        </ion-col>\n   \n      </ion-row> \n     </ion-grid>\n      <ion-grid>\n        <ion-row>\n          <ion-col class="welcome">\n            <div text-center style="text-align: center;color: black;">\n                We will send a link on your registered email to reset your password.\n                \n            </div>\n            <div align="center" *ngIf="spinnerLoad">\n    \n              <ion-spinner></ion-spinner>\n    \n            </div>\n          </ion-col>\n    \n        </ion-row>\n      </ion-grid>\n      <form #loginForm="ngForm" style="margin-top: 49px;" (ngSubmit)="Reset(loginForm.value)" novalidate>\n      <ion-grid>\n        \n          <ion-row>\n            <ion-col col-12 >\n    \n              <ion-list>\n    \n                \n                <ion-item>\n                    <ion-label>Email</ion-label>\n                    <ion-input type="text"  #email="ngModel" id="email" pattern="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$" name="email" required ngModel>\n                    <!-- <ion-input [(ngModel)]="username" name="username" type="text" #usernames="ngModel" spellcheck="false" autocapitalize="off"\n                    required> -->\n                  </ion-input>\n                 \n                </ion-item>\n                <div class="error" *ngIf="email?.errors && (email?.touched || email?.dirty)">\n                    <p class="error" [hidden]="!email.errors.required"> Please enter your email. </p>\n                    <p class="error" [hidden]="!email.errors.pattern"> Please enter valid email. </p>\n                </div>\n               \n             \n    \n            \n                \n    \n              </ion-list>\n    \n    \n            </ion-col>\n    \n          </ion-row>\n       \n      </ion-grid>\n     \n      \n    \n    \n      <div style="padding:5px;" >\n    \n        <ion-row responsive-sm>\n          <ion-col>\n            <button ion-button class="bgcolor" [navPush]="login" id="loginShow" type="submit" block>Cancel</button>\n          </ion-col>\n          <ion-col>\n    \n            <button type="submit" ion-button  class="bgcolor"  color="light" [disabled]="!loginForm.form.valid"  block>Reset</button>\n          </ion-col>\n        </ion-row>\n    \n      </div>\n    </form>\n    \n   \n    \n    </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/forgot/forgot.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */]])
    ], Forgot);
    return Forgot;
}());

//# sourceMappingURL=forgot.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Details; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_question_service__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_exam_examNew_examComponent__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var Details = /** @class */ (function () {
    function Details(questionService, events, sanitizer, navCtrl, __auth, toastCtrl, platform, navParams, examService) {
        this.questionService = questionService;
        this.events = events;
        this.sanitizer = sanitizer;
        this.navCtrl = navCtrl;
        this.__auth = __auth;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.examService = examService;
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;
        this.noQuestion = false;
        this.currentIndexToShow = 0;
        this.rank = [];
        this.noPrevQuestion = false;
        this.url = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].backendUrl + '/images/';
        this.imageFlag = false;
        this.weekExam = false;
        this.qtnData = false;
        this.examDetails = {
            name: "",
            date: "",
            noOfQuestions: "",
            examResult: {
                noOfCorrectQue: "",
                noOfQueAttended: "",
                noOfWrongQue: ""
            }
        };
        this.countClick = 0;
        this.correctCountClick = 0;
        this.wrongCountClick = 0;
        this.unansweredCountClick = 0;
        this.correctcount = false;
        this.wrongcount = false;
        this.unansweredcount = false;
        this.indexArray = [];
        this.scoreDiv = false;
        this.showLeaderBoard = false;
        this.listview = false;
        this.urlRead = navParams.get('ExamIds');
        console.log("urlread", this.urlRead);
        this.events.publish('user:created', true, Date.now());
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    }
    Details.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    Details.prototype.ionViewWillLeave = function () {
        this.tabBarElement.style.display = 'flex';
    };
    Details.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    Details.prototype.ngOnInit = function () {
        var _this = this;
        this.tabBarElement.style.display = 'none';
        this.fields = this.urlRead.split('/');
        this.examId = this.fields[0];
        this.userExamId = this.fields[1];
        if (this.fields[1] == 'g') {
            this.examType = "grand";
        }
        else if (this.fields[1] == 'week') {
            this.examType = "week";
        }
        else {
            this.examType = "exam";
        }
        this.getQuiestions("correct");
        this.correctClr = true;
        this.wrongClr = false;
        this.unutendedClr = false;
        this.listClr = false;
        this.correctcount = true;
        this.showLeaderBoard = false;
        this.wrongcount = false;
        this.unansweredcount = false;
        if (this.examType == "grand") {
            this.examService.loadGrandExam(this.examId).subscribe(function (result) {
                if (result.status) {
                    _this.totalQstn = result.data.userExamObj.numberOfQuestions;
                    _this.correct = result.data.userExamObj.examResult.noOfCorrectQue;
                    if (_this.correct == 0)
                        _this.listview = false;
                    _this.wrong = result.data.userExamObj.examResult.noOfWrongQue;
                    _this.notAnswered = _this.totalQstn - result.data.userExamObj.examResult.noOfQueAttended;
                    _this.score = ((result.data.userExamObj.examResult.noOfCorrectQue * 4) + (result.data.userExamObj.examResult.noOfWrongQue * -1));
                    _this.totalScore = _this.totalQstn * 4;
                    _this.examDetails.name = result.data.userExamObj.name;
                    _this.examDetails.date = result.data.userExamObj.createdDate;
                }
            });
        }
        else if (this.examType == 'week') {
            this.userResult();
        }
        else {
            this.examService.loadExam(this.userExamId).subscribe(function (data) {
                if (data.data) {
                    console.log("data.data", data.data);
                    if (data.data.userExamObj) {
                        _this.totalQstn = data.data.userExamObj.noOfQuestions;
                    }
                    _this.correct = data.data.userExamObj.examResult.noOfCorrectQue;
                    if (_this.correct == 0)
                        _this.listview = false;
                    _this.wrong = data.data.userExamObj.examResult.noOfWrongQue;
                    _this.notAnswered = (_this.totalQstn - data.data.userExamObj.examResult.noOfQueAttended);
                    _this.score = ((data.data.userExamObj.examResult.noOfCorrectQue * 4) + (data.data.userExamObj.examResult.noOfWrongQue * -1));
                    _this.totalScore = _this.totalQstn * 4;
                    _this.examDetails.name = data.data.userExamObj.examName;
                    _this.examDetails.date = data.data.userExamObj.createdDate;
                }
            });
        }
    };
    Details.prototype.userResult = function () {
        var _this = this;
        this.examService.getUserResult(this.examId).subscribe(function (res) {
            if (res.status) {
                _this.userScore = res.data[0];
                _this.getRank(_this.userScore.examId);
                _this.totalQstn = res.data[0].examResult.totalQuestions;
                _this.correct = res.data[0].examResult.noOfCorrect;
                _this.wrong = res.data[0].examResult.noOfWrong;
                _this.notAnswered = res.data[0].examResult.noOfUnanswered;
                _this.score = res.data[0].examResult.mark;
                _this.totalScore = _this.totalQstn * 4;
            }
            else {
                _this.__auth.notificationInfo(res.message);
            }
        });
    };
    Details.prototype.getRank = function (examid) {
        var _this = this;
        this.examService.getLeaderboard(examid).subscribe(function (res) {
            if (res.status) {
                _this.rankData = res.data;
                _this.totalUsers = res.data.length;
                for (var k = 0; k < _this.rankData.length; k++) {
                    _this.indexArray.push({ "score": _this.rankData[k].examResult.mark, "name": _this.rankData[k].userDetails.name });
                }
                _this.indexArray.sort(function (a, b) {
                    return b.score - a.score;
                });
                var rank = 1;
                for (var i = 0; i < _this.indexArray.length; i++) {
                    if (i > 0 && _this.indexArray[i].score < _this.indexArray[i - 1].score) {
                        rank++;
                    }
                    _this.indexArray[i].rank = rank;
                }
                _this.userRank = _this.rankData.findIndex(function (item) {
                    return item.userId == _this.userId;
                });
                _this.topScorers = _this.rankData.slice(0, 9);
            }
            else {
                _this.__auth.notificationInfo(res.message);
            }
        });
    };
    Details.prototype.getScore = function () {
        this.scoreDiv = true;
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;
        this.qtnData = false;
        this.showLeaderBoard = false;
        this.listview = false;
    };
    Details.prototype.showLeaderborad = function () {
        this.showLeaderBoard = true;
        this.scoreDiv = false;
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;
        this.qtnData = false;
    };
    Details.prototype.toggleDetails = function (item) {
        if (this.isGroupShown(item)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = item;
        }
    };
    Details.prototype.isGroupShown = function (item) {
        return this.shownGroup === item;
    };
    Details.prototype.getQuiestions = function (category) {
        var _this = this;
        this.resultType = category;
        this.currentIndexToShow = 0;
        this.correctQstions = [];
        this.answerArray = [];
        if (this.examType == "week") {
            this.examService.getResult(this.examId, this.resultType).subscribe(function (res) {
                if (res.status) {
                    _this.qtnData = true;
                    _this.correctClr = true;
                    _this.wrongClr = false;
                    _this.unutendedClr = false;
                    _this.showLeaderBoard = false;
                    _this.scoreDiv = false;
                    if (_this.resultType == "unanswered") {
                        _this.correctQstions = res.data;
                        console.log(" this.correctQstions ", _this.correctQstions);
                        _this.correctAnswer = _this.correctQstions[_this.currentIndexToShow].answer;
                        _this.userAnswer = null;
                    }
                    else {
                        _this.correctQstions = res.data.question;
                        console.log(" this.correctQstions33 ", _this.correctQstions[42]);
                        _this.answerArray = res.data.answer;
                        var userAnswerIndex = _this.answerArray.findIndex(function (ans) {
                            return ans.questionId == _this.correctQstions[_this.currentIndexToShow]._id;
                        });
                        _this.userAnswer = _this.answerArray[userAnswerIndex].answer;
                        _this.correctAnswer = _this.correctQstions[_this.currentIndexToShow].answer;
                        if (_this.userAnswer == _this.correctAnswer) {
                            _this.userAnswer = null;
                        }
                    }
                    _this.content = _this.correctQstions[_this.currentIndexToShow].question;
                    (_this.content !== null) ? _this.Qnumber = 1 : _this.Qnumber = 0;
                    _this.options = JSON.parse(_this.correctQstions[_this.currentIndexToShow].options);
                    _this.description = _this.correctQstions[_this.currentIndexToShow].description;
                    if (_this.correctQstions[_this.currentIndexToShow].question_image) {
                        _this.imageFlag = true;
                        _this.imgUrl = _this.correctQstions[_this.currentIndexToShow].question_image;
                    }
                    else {
                        _this.imageFlag = false;
                    }
                    if (_this.correctQstions.length == 1) {
                        _this.noQuestion = true;
                        _this.noPrevQuestion = true;
                    }
                }
                else {
                    _this.correctClr = true;
                    _this.wrongClr = false;
                    _this.unutendedClr = false;
                    _this.showLeaderBoard = false;
                    _this.qtnData = false;
                    _this.__auth.notificationInfo(res.message);
                }
            });
        }
        else {
            this.examService.getQuestions(this.examId, this.examType, category).subscribe(function (res) {
                if (res.status) {
                    _this.correctClr = true;
                    _this.wrongClr = false;
                    _this.unutendedClr = false;
                    _this.scoreDiv = false;
                    _this.showLeaderBoard = false;
                    _this.noPrevQuestion = true;
                    _this.showLeaderBoard = false;
                    _this.qtnData = true;
                    _this.correctQstions = res.data;
                    console.log(" this.correctQstions2 ", _this.correctQstions);
                    _this.content = _this.correctQstions[_this.currentIndexToShow].question;
                    _this.correctAnswer = _this.correctQstions[_this.currentIndexToShow].answer;
                    _this.userAnswer = _this.correctQstions[_this.currentIndexToShow].userAnswer;
                    _this.options = JSON.parse(_this.correctQstions[_this.currentIndexToShow].options);
                    _this.description = _this.correctQstions[_this.currentIndexToShow].description;
                    (_this.content !== null) ? _this.Qnumber = 1 : _this.Qnumber = 0;
                    if (_this.correctQstions[_this.currentIndexToShow].question_image) {
                        _this.imageFlag = true;
                        _this.imgUrl = _this.correctQstions[_this.currentIndexToShow].question_image;
                    }
                    else {
                        _this.imageFlag = false;
                    }
                    if (_this.correctQstions.length == 1) {
                        _this.noQuestion = true;
                        _this.noPrevQuestion = true;
                    }
                }
                else {
                }
            });
        }
    };
    Details.prototype.nextButtonClick = function () {
        var _this = this;
        this.noPrevQuestion = false;
        this.currentIndexToShow++;
        if (this.currentIndexToShow == this.correctQstions.length - 1) {
            this.noQuestion = true;
            this.content = this.correctQstions[this.currentIndexToShow].question;
            (this.content !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
            if (this.examType == "week") {
                if (this.resultType == "unanswered") {
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    this.userAnswer = null;
                }
                else {
                    var userAnswerIndex = this.answerArray.findIndex(function (ans) {
                        return ans.questionId == _this.correctQstions[_this.currentIndexToShow]._id;
                    });
                    this.userAnswer = this.answerArray[userAnswerIndex].answer;
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    if (this.userAnswer == this.correctAnswer) {
                        this.userAnswer = null;
                    }
                }
            }
            else {
                this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
            }
            if (this.correctQstions[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
        else {
            this.content = this.correctQstions[this.currentIndexToShow].question;
            (this.content !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
            if (this.examType == "week") {
                if (this.resultType == "unanswered") {
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    this.userAnswer = null;
                }
                else {
                    var userAnswerIndex = this.answerArray.findIndex(function (ans) {
                        return ans.questionId == _this.correctQstions[_this.currentIndexToShow]._id;
                    });
                    this.userAnswer = this.answerArray[userAnswerIndex].answer;
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    if (this.userAnswer == this.correctAnswer) {
                        this.userAnswer = null;
                    }
                }
            }
            else {
                this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
            }
            if (this.correctQstions[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
    };
    Details.prototype.previousButtonClick = function () {
        var _this = this;
        this.noQuestion = false;
        this.currentIndexToShow--;
        if (this.currentIndexToShow > 0) {
            this.content = this.correctQstions[this.currentIndexToShow].question;
            (this.content !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
            if (this.examType == "week") {
                if (this.resultType == "unanswered") {
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    this.userAnswer = null;
                }
                else {
                    var userAnswerIndex = this.answerArray.findIndex(function (ans) {
                        return ans.questionId == _this.correctQstions[_this.currentIndexToShow]._id;
                    });
                    this.userAnswer = this.answerArray[userAnswerIndex].answer;
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    if (this.userAnswer == this.correctAnswer) {
                        this.userAnswer = null;
                    }
                }
            }
            else {
                this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
            }
            if (this.correctQstions[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
        else {
            this.noPrevQuestion = true;
            this.content = this.correctQstions[this.currentIndexToShow].question;
            (this.content !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
            if (this.examType == "week") {
                if (this.resultType == "unanswered") {
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    this.userAnswer = null;
                }
                else {
                    var userAnswerIndex = this.answerArray.findIndex(function (ans) {
                        return ans.questionId == _this.correctQstions[_this.currentIndexToShow]._id;
                    });
                    this.userAnswer = this.answerArray[userAnswerIndex].answer;
                    this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                    if (this.userAnswer == this.correctAnswer) {
                        this.userAnswer = null;
                    }
                }
            }
            else {
                this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
                this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
            }
            if (this.correctQstions[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
                this.imageFlag = false;
            }
        }
    };
    Details.prototype.getCorrectQstn = function (type) {
        var _this = this;
        this.listview = false;
        this.noQuestion = false;
        this.currentIndexToShow = 0;
        this.noPrevQuestion = true;
        this.resultType = type;
        if (this.examType !== "week") {
            if (this.resultType == "unanswered") {
                this.resultType = "notAnswered";
            }
            this.examService.getQuestions(this.examId, this.examType, this.resultType).subscribe(function (res) {
                if (res.status) {
                    _this.qtnData = true;
                    _this.correctQstions = res.data;
                    console.log(" this.correctQstions ", _this.correctQstions);
                    _this.content = _this.correctQstions[_this.currentIndexToShow].question;
                    (_this.content !== null) ? _this.Qnumber = 1 : _this.Qnumber = 0;
                    _this.options = JSON.parse(_this.correctQstions[_this.currentIndexToShow].options);
                    _this.description = _this.correctQstions[_this.currentIndexToShow].description;
                    _this.correctAnswer = _this.correctQstions[_this.currentIndexToShow].answer;
                    _this.userAnswer = _this.correctQstions[_this.currentIndexToShow].userAnswer;
                    if (_this.correctQstions[_this.currentIndexToShow].question_image) {
                        _this.imageFlag = true;
                        _this.imgUrl = _this.correctQstions[_this.currentIndexToShow].question_image;
                    }
                    else {
                        _this.imageFlag = false;
                    }
                    if (_this.correctQstions.length == 1) {
                        _this.noQuestion = true;
                        _this.noPrevQuestion = true;
                    }
                    if (_this.resultType == "correct") {
                        _this.correct = _this.correctQstions.length;
                        _this.correctcount = true;
                        _this.listClr = false;
                        _this.wrongcount = false;
                        _this.unansweredcount = false;
                        _this.showLeaderBoard = false;
                        _this.correctClr = true;
                        _this.wrongClr = false;
                        _this.showLeaderBoard = false;
                        _this.unutendedClr = false;
                        _this.scoreDiv = false;
                    }
                    else if (_this.resultType == "wrong") {
                        _this.wrong = _this.correctQstions.length;
                        _this.correctcount = false;
                        _this.listClr = false;
                        _this.wrongcount = true;
                        _this.showLeaderBoard = false;
                        _this.unansweredcount = false;
                        _this.showLeaderBoard = false;
                        _this.wrongClr = true;
                        _this.correctClr = false;
                        _this.unutendedClr = false;
                        _this.scoreDiv = false;
                    }
                    else {
                        _this.notAnswered = _this.correctQstions.length;
                        _this.correctcount = false;
                        _this.listClr = false;
                        _this.wrongcount = false;
                        _this.unansweredcount = true;
                        _this.showLeaderBoard = false;
                        _this.unutendedClr = true;
                        _this.wrongClr = false;
                        _this.correctClr = false;
                        _this.scoreDiv = false;
                    }
                }
                else {
                    _this.qtnData = false;
                    if (_this.resultType == "correct") {
                        _this.correct = 0;
                        _this.correctcount = true;
                        _this.listClr = false;
                        _this.wrongcount = false;
                        _this.showLeaderBoard = false;
                        _this.unansweredcount = false;
                        _this.unutendedClr = false;
                        _this.wrongClr = false;
                        _this.correctClr = true;
                    }
                    else if (_this.resultType == "wrong") {
                        _this.wrong = 0;
                        _this.correctcount = false;
                        _this.listClr = false;
                        _this.wrongcount = true;
                        _this.showLeaderBoard = false;
                        _this.unansweredcount = false;
                        _this.showLeaderBoard = false;
                        _this.unutendedClr = false;
                        _this.wrongClr = true;
                        _this.correctClr = false;
                    }
                    else {
                        _this.notAnswered = 0;
                        _this.correctcount = false;
                        _this.listClr = false;
                        _this.wrongcount = false;
                        _this.unansweredcount = true;
                        _this.showLeaderBoard = false;
                        _this.unutendedClr = true;
                        _this.showLeaderBoard = false;
                        _this.wrongClr = false;
                        _this.correctClr = false;
                    }
                    _this.listview = false;
                    _this.__auth.notificationInfo("There is no answer");
                }
            });
        }
        else {
            this.examService.getResult(this.examId, this.resultType).subscribe(function (res) {
                if (!res.status) {
                    _this.qtnData = false;
                    if (_this.resultType == "correct") {
                        _this.correct = 0;
                        _this.correctcount = true;
                        _this.showLeaderBoard = false;
                        _this.listClr = false;
                        _this.wrongcount = false;
                        _this.unansweredcount = false;
                        _this.unutendedClr = false;
                        _this.showLeaderBoard = false;
                        _this.wrongClr = false;
                        _this.correctClr = true;
                    }
                    else if (_this.resultType == "wrong") {
                        _this.wrong = 0;
                        _this.correctcount = false;
                        _this.showLeaderBoard = false;
                        _this.listClr = false;
                        _this.wrongcount = true;
                        _this.showLeaderBoard = false;
                        _this.unansweredcount = false;
                        _this.unutendedClr = false;
                        _this.wrongClr = true;
                        _this.correctClr = false;
                    }
                    else {
                        _this.notAnswered = 0;
                        _this.correctcount = false;
                        _this.listClr = false;
                        _this.showLeaderBoard = false;
                        _this.wrongcount = false;
                        _this.showLeaderBoard = false;
                        _this.unansweredcount = true;
                        _this.unutendedClr = true;
                        _this.wrongClr = false;
                        _this.correctClr = false;
                    }
                    _this.listview = false;
                    _this.__auth.notificationInfo(res.message);
                }
                else {
                    _this.noPrevQuestion = true;
                    _this.qtnData = true;
                    if (_this.resultType == "unanswered") {
                        _this.correctQstions = res.data;
                        _this.correctAnswer = _this.correctQstions[_this.currentIndexToShow].answer;
                        _this.subjectArray = _this.correctQstions[_this.currentIndexToShow].subjectArray;
                        _this.userAnswer = null;
                    }
                    else {
                        _this.correctQstions = res.data.question;
                        _this.answerArray = res.data.answer;
                        var userAnswerIndex = _this.answerArray.findIndex(function (ans) {
                            return ans.questionId == _this.correctQstions[_this.currentIndexToShow]._id;
                        });
                        _this.userAnswer = _this.answerArray[userAnswerIndex].answer;
                        _this.correctAnswer = _this.correctQstions[_this.currentIndexToShow].answer;
                        if (_this.userAnswer == _this.correctAnswer) {
                            _this.userAnswer = null;
                        }
                    }
                    _this.content = _this.correctQstions[_this.currentIndexToShow].question;
                    (_this.content !== null) ? _this.Qnumber = 1 : _this.Qnumber = 0;
                    _this.options = JSON.parse(_this.correctQstions[_this.currentIndexToShow].options);
                    _this.description = _this.correctQstions[_this.currentIndexToShow].description;
                    if (_this.correctQstions[_this.currentIndexToShow].question_image) {
                        _this.imageFlag = true;
                        _this.imgUrl = _this.correctQstions[_this.currentIndexToShow].question_image;
                    }
                    else {
                        _this.imageFlag = false;
                    }
                    if (_this.correctQstions.length == 1) {
                        _this.noQuestion = true;
                        _this.noPrevQuestion = true;
                    }
                    if (_this.resultType == "correct") {
                        // this.correct = this.correctQstions.length;
                        _this.correctClr = true;
                        _this.wrongClr = false;
                        _this.listClr = false;
                        _this.unutendedClr = false;
                        _this.showLeaderBoard = false;
                        _this.scoreDiv = false;
                        _this.showLeaderBoard = false;
                    }
                    else if (_this.resultType == "wrong") {
                        // this.wrong = this.correctQstions.length;
                        _this.wrongClr = true;
                        _this.correctClr = false;
                        _this.listClr = false;
                        _this.unutendedClr = false;
                        _this.showLeaderBoard = false;
                        _this.scoreDiv = false;
                    }
                    else {
                        //this.notAnswered = this.correctQstions.length;
                        _this.unutendedClr = true;
                        _this.wrongClr = false;
                        _this.listClr = false;
                        _this.correctClr = false;
                        _this.showLeaderBoard = false;
                        _this.scoreDiv = false;
                    }
                }
            });
        }
    };
    Details.prototype.starQuestions = function () {
        var _this = this;
        this.star = !this.star;
        this.questionService.starQuestion(this.questionId, this.subjectArray, this.star)
            .subscribe(function (res) {
            if (!res.status)
                _this.__auth.notificationInfo('OOPS! Something went wrong');
        });
    };
    Details.prototype.listView = function () {
        var _this = this;
        if (this.correctcount) {
            this.listClr = false;
            this.correctCountClick++;
            this.countClick = this.correctCountClick;
        }
        if (this.wrongcount) {
            this.listClr = false;
            this.wrongCountClick++;
            this.countClick = this.wrongCountClick;
        }
        if (this.unansweredcount) {
            this.listClr = false;
            this.unansweredCountClick++;
            this.countClick = this.unansweredCountClick;
        }
        if (this.countClick % 2 == 0) {
            this.listview = false;
            this.listClr = false;
            this.qtnData = true;
        }
        else {
            this.listview = true;
            this.listClr = true;
            this.qtnData = false;
            this.listviewData = [];
            if (this.examType == 'week') {
                this.examService.getResult(this.examId, this.resultType).subscribe(function (res) {
                    if (!res.status) {
                    }
                    else {
                        if (_this.resultType == "unanswered") {
                            _this.listviewData = res.data;
                        }
                        else {
                            _this.listviewData = res.data.question;
                        }
                        var data = (_this.listviewData).map(function (item) {
                            item.options = JSON.parse(item.options);
                            item.icon = "ios-add-circle-outline";
                            return item;
                        });
                        if (data) {
                            _this.listviewData = data;
                            console.log("listdata", _this.listviewData);
                        }
                    }
                });
            }
            else {
                this.examService.getQuestions(this.examId, this.examType, this.resultType).subscribe(function (res) {
                    if (res.status) {
                        _this.listviewData = res.data;
                        var data = (res.data).map(function (item) {
                            item.options = JSON.parse(item.options);
                            item.icon = "ios-add-circle-outline";
                            return item;
                        });
                        if (data) {
                            _this.listviewData = data;
                        }
                    }
                    else {
                        _this.listview = false;
                        _this.qtnData = false;
                        _this.__auth.notificationInfo(res.message);
                    }
                });
            }
        }
    };
    Details.prototype.backToCompletedPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    Details = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam-room/details/details.html"*/'<style>\n    .crdmd {\n        margin-left: -16px !important;\n        border-radius: 2px!important;\n        width: calc(100% - -30px)!important;\n    }\n\n    .optionStyle .label-md {\n        margin: 0px 0px 13px 0;\n    }\n\n\n    .item-md.item-block .item-inner {\n        padding-right: 2px;\n        border-bottom: 1px solid #dedede;\n    }\n</style>\n\n<ion-header>\n    <ion-toolbar>\n\n        <ion-icon name="md-arrow-back" style="font-size: 21px;\n                  margin-left: 8px;\n                  color: #fff;" (click)="backToCompletedPage()"></ion-icon>\n        &nbsp;&nbsp;\n        <span style="font-size: 18px;color: #fff;"  *ngIf="!userScore">{{examDetails.name}}</span>\n        <span style="font-size: 16px;color: #fff;"  *ngIf="userScore">{{userScore.examDetails.name}}</span>\n\n    </ion-toolbar>\n</ion-header>\n<ion-content padding #contentRef  class="bgpic">\n    \n     \n    <ion-card style="height:33px;\n        margin-left: 0px;" class="crdmd ">\n        <ion-slides [slidesPerView]="2" >\n\n            <ion-slide class="blocks" [ngClass]="(scoreDiv)?\'selected\':\'unselected\'" (click)="getScore()">\n                <span style="font-size:18px; text-align: center;">Score &rarr;</span>\n                &nbsp;\n                <span style="font-size:18px;text-align: center; ">{{score}}</span>\n\n            </ion-slide>\n            <ion-slide [ngClass]="(correctClr)?\'selected\':\'unselected\'" id="correctButton" (click)="getCorrectQstn(\'correct\')">\n                <span style="font-size:18px; text-align: center;">Correct &rarr;</span>\n               \n                <span style="font-size:18px; text-align: center;">{{correct}}</span>\n            </ion-slide>\n\n            <ion-slide [ngClass]="(wrongClr)?\'selected\':\'unselected\'" (click)="getCorrectQstn(\'wrong\')">\n                <span style="font-size:18px; text-align: center;">Wrong &rarr;</span>\n               \n                <span style="font-size:18px; text-align: center;">{{wrong}}</span>\n            </ion-slide>\n            <ion-slide [ngClass]="(unutendedClr)?\'selected\':\'unselected\'" (click)="getCorrectQstn(\'unanswered\')">\n                <span style="font-size:18px; text-align: center;">Unattended &rarr;</span>\n              \n                <span style="font-size:18px; text-align: center;">{{notAnswered}}</span>\n            </ion-slide>\n            <ion-slide class="blocks" *ngIf="userScore" [ngClass]="(showLeaderBoard)?\'selected\':\'unselected\'" (click)=showLeaderborad()>\n                <span style="font-size:18px; text-align: center;">Leaderboard &rarr;</span>\n               \n                <span style="font-size:18px; text-align: center;">{{totalUsers}}</span>\n\n            </ion-slide>\n\n\n        </ion-slides>\n    </ion-card>\n\n\n\n\n\n\n    <div *ngIf="qtnData">\n        <ion-card style="margin-top:7px;width: 100%;margin-left: 0px; padding-left:-1px;">\n            <ion-card-content style=" padding: 0px 0px 0px 0px; padding: 13px 3px;" style="background-color:#FFFEEE;margin-left:-13px!important; margin-right:-13px!important;">\n                <ion-item-group>\n\n                    <ion-item-divider text-wrap class="questionHead" style="padding:0px;background-color:#FFFEEE">\n                        <ion-grid style="padding-bottom:0px !important;">\n                            <ion-row>\n                                <ion-col col-2 style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:18px; padding-left: 0px;">\n                                    {{Qnumber}}\n                                </ion-col>\n                                <ion-col col-10 style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                                    <p style="font-size: 18px;" [innerHtml]="content "> </p>\n\n\n                                </ion-col>\n                            </ion-row>\n                        </ion-grid>\n                        <div class="col-sm-12 bdr" *ngIf="imageFlag">\n\n                            <br>\n                            <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + imgUrl\'>\n                            <br>\n                        </div>\n                    </ion-item-divider>\n\n\n\n\n                    <div style="padding:0px!important;border-bottom:none!important;">\n                        <ion-list *ngFor="let option of options ;let i= index;">\n\n                            <ion-item text-wrap class="optionStyle" style="font-weight: unset!important;" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important; "\n                                class="optionStyle" style="font-weight: unset!important;" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important; ">\n                                <div [ngClass]="{\'correct\':correctAnswer===option.id,\'wrong\':userAnswer===option.id}">\n\n\n                                    <ion-row>\n\n                                        <!-- <ion-row  > -->\n                                        <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;; margin-right: 0px;line-height:23px;">{{option.id}}.</ion-col>\n                                        <ion-col col-10 style="padding-left:5px!important;margin-left: 0px;line-height:22px; line-height:23px;font-size:18px;">\n                                            <span> {{option.text}}</span>\n                                        </ion-col>\n                                    </ion-row>\n\n                                </div>\n\n\n                            </ion-item>\n                        </ion-list>\n                    </div>\n\n                </ion-item-group>\n\n\n\n\n\n                <ion-row style="margin-top:10px;">\n                    <ion-col col-4>\n                        <button float-center ion-button color="#108fd2" style="background-color:#108fd2;" icon-left (click)="previousButtonClick()"\n                            [disabled]=noPrevQuestion>Previous</button>\n\n                    </ion-col>\n                    <ion-col col-4>\n\n                    </ion-col>\n                    <ion-col col-4>\n                        <!-- <button class="btn btn-success col-md-3 btnSize exampages " (click)="mark((questionPalette[currentPaletteId].status == \'marked\') ? \'unmarked\': \'marked\')">{{(questionPalette[currentPaletteId].status == \'marked\') ? \'Unmark\': \'Mark\'}}</button> -->\n\n\n                        <button float-center ion-button style="background-color:#108fd2;color:white; padding: 0px 30px 0px 30px;" (click)="nextButtonClick()"\n                            [disabled]=noQuestion>Next</button>\n\n                    </ion-col>\n                </ion-row>\n            </ion-card-content>\n        </ion-card>\n        <div *ngIf="description" style="background-color:#FFFEEE;">\n\n            <div style="font-weight:bold; font-size:18px;padding-left:10px;padding-top:8px;">Description</div>\n           \n            <!-- <ion-scroll scrollX="true" class="dec"> -->\n                <p class="scrollbar" [innerHTML]="sanitizer.bypassSecurityTrustHtml(description)" style="line-height: 20px;font-size:18px;padding-left:10px;padding-left:10px; padding-bottom:5px; "></p>\n            <!-- </ion-scroll> -->\n\n\n        </div>\n    </div>\n    <ion-grid *ngIf="scoreDiv" style="padding-top: 121px;">\n        <ion-row>\n            <ion-col>\n                <h2 style="text-align: center;\n                   font-size: 30px;">Total</h2>\n                <!-- <h2 style="text-align: center;font-size: 29px;">\n                    <b style="font-size: 42px;\n                      color: green; ">{{score}}</b> /{{totalScore}}</h2> -->\n                      <ion-row>\n                        <ion-col col-12>\n                            <ion-badge item-end  class="badg">{{score}}/{{totalScore}}</ion-badge>\n\n                        </ion-col>\n                       \n                          <!-- <ion-col col-4>\n                            <ion-badge item-end  class="badg">{{score}}/{{totalScore}}</ion-badge>\n\n                        </ion-col> -->\n                       \n                      </ion-row>\n            </ion-col>\n        </ion-row>\n\n    </ion-grid>\n    <ion-grid *ngIf="showLeaderBoard">\n        <ion-grid>\n            <ion-row class="tableRow">\n                <ion-col col-6>\n                    Name\n                </ion-col>\n                <ion-col col-3>\n                    Rank\n                </ion-col>\n                <ion-col col-3>\n                    Mark\n                </ion-col>\n            </ion-row>\n            <ion-row *ngFor="let item of indexArray; let i = index;" class="tableColmn">\n                <ion-col col-6>\n                    {{item.name}}\n                </ion-col>\n                <ion-col col-3 ng>\n                    {{item.rank}}\n                </ion-col>\n                <ion-col col-3>\n                    {{item.score}}\n                </ion-col>\n\n\n            </ion-row>\n        </ion-grid>\n    </ion-grid>\n    <div *ngIf="listview">\n        <div *ngFor="let item of listviewData;let i=index;">\n            <ion-row class="qsnHead">\n                <ion-grid (click)="toggleDetails(item);" [ngClass]="{active: isGroupShown(item)}">\n                    <ion-row>\n                        <ion-col col-2 style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:26px; padding-left: 0px;">\n                            <span>{{i+1}}.</span>\n                        </ion-col>\n                        <ion-col col-10 style="padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                            <span [innerHtml]="item.question"></span>\n                        </ion-col>\n                    </ion-row>\n                </ion-grid>\n\n            </ion-row>\n            <div *ngIf="isGroupShown(item)" style="background-color: #fffeee">\n\n                <div class="col-sm-12 bdr" *ngIf="imageFlag">\n                    <br>\n                    <img class="img-responsive" style="width: 400px;height: 400" [src]=\'url + item.question_image\'>\n                    <br>\n                </div>\n\n                <div *ngFor="let option of item.options">\n                    <ion-grid style="padding-bottom: 0px;\n                    padding-top: 0px;">\n                        <ion-row style=" margin:0!important;border-bottom:none;" [ngClass]="{\'correct\':item.answer===option.id,\'wrong\':item.userAnswer===option.id}">\n                            <ion-col col-2 style="padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3px !important;max-width: 13.76667%;">{{option.id}}.</ion-col>\n                            <ion-col col-10 style="padding-left:5px!important;margin-left: 0px;line-height:23px; line-height:23px;font-size:18px;" [innerHtml]="option.text"></ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n\n                <ion-row>\n                    <ion-col col-12>\n                        <p style="font-weight:bold; font-size:18px;padding-left:10px;padding-top:8px;">Description:</p>\n                        <!-- <ion-scroll scrollX="true" scrollY="true" class="dec" style="min-height:200px;height:auto;"> -->\n                            <p class="scrollbar" [innerHTML]="sanitizer.bypassSecurityTrustHtml(item.description)" style="line-height: 20px;font-size:18px;"></p>\n                        <!-- </ion-scroll> -->\n                    </ion-col>\n                </ion-row>\n\n            </div>\n        </div>\n    </div>\n</ion-content>\n<ion-footer style="padding: 0px;" *ngIf="!scoreDiv">\n    <div class="bar bar-footer bar-assertive" >\n        <ion-grid style="padding-left: 0px;padding-right: 0px; padding-top: 0px;padding-bottom:0px;">\n            <ion-col col-12 style="background-color:#cecece; ">\n                <ion-row>\n                    <!-- <ion-col col-4 style="font-size:20px;text-align:center;">\n\n                        <ion-icon ios="ios-bookmark" md="md-bookmark" [ngClass]="{\'starred\': star}"></ion-icon>\n                    </ion-col>\n                    <ion-col col-4 style="font-size:20px;text-align:center;">\n                        <ion-icon class="fabbb" ios="ios-clipboard" (click)="addNotes()" md="md-clipboard">\n                        </ion-icon>\n                    </ion-col> -->\n                    <!-- <ion-icon class="fabbb" *ngIf="!listview && !listStarView" ios="ios-clipboard" (click)="editNote()"\n                    md="md-clipboard">\n                    <ion-icon style="padding-top: 8px;" id="notifications-badge" ios="ios-checkmark" md="md-checkmark"></ion-icon>\n                  </ion-icon> -->\n                    <ion-col col-4 style="font-size:20px;text-align:center;float:right;">\n                        <ion-icon [ngClass]="(listClr)?\'blueIcon\':\'icn\'" ios="ios-list" md="md-list" (click)="listView()"></ion-icon>\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-grid>\n    </div>\n</ion-footer>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/exam-room/details/details.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5__services_question_service__["a" /* QuestionService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__services_question_service__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_exam_service__["a" /* ExamService */]])
    ], Details);
    return Details;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return naveBarShowSrvice; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var naveBarShowSrvice = /** @class */ (function () {
    function naveBarShowSrvice(http) {
        this.http = http;
        this.navebarHide = true;
        // localNavbar:boolean=true;
        this.url = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].backendUrl;
    }
    naveBarShowSrvice.prototype.OnInit = function () {
        // this.localNavbar=true;
        if (!localStorage.getItem('navBar'))
            this.showNaveBar = false;
        else
            this.showNaveBar = localStorage.getItem('navBar');
    };
    naveBarShowSrvice.prototype.show = function (index) {
        localStorage.setItem('navBar', index);
        this.showNaveBar = index;
    };
    //index pass navbar
    naveBarShowSrvice.prototype.navHide = function (index) {
        this.localNavbar = index;
        console.log(this.localNavbar, "hey its nave hiden");
    };
    naveBarShowSrvice.prototype.profileEdit = function (userId, firstname, lastname, phone, city, country) {
        var data = {
            "activeUserId": userId,
            "firstName": firstname,
            "lastName": lastname,
            "phone": phone,
            "city": city,
            "country": country
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/update-user-profile", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    naveBarShowSrvice.prototype.InviteFrds = function (userId, inviteFrds) {
        var data = {
            "activeUserId": userId,
            "email": inviteFrds
        };
        console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/invite-friend", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    naveBarShowSrvice.prototype.UserFeedback = function (userId, feedback) {
        var data = {
            "userId": userId,
            "message": feedback
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/invite-friend", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    naveBarShowSrvice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], naveBarShowSrvice);
    return naveBarShowSrvice;
}());

//# sourceMappingURL=navBarService.js.map

/***/ }),

/***/ 620:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(621);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(625);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 625:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__directives_hideheader_hideheader__ = __webpack_require__(683);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__directives_hideheader_tabsHiden__ = __webpack_require__(684);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(685);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_learn_list__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_learn_questions_questions__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_exam_exam_room_details_details__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_learn_questions_note_note_modal__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_learn_questions_editModal_editModal__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_learn_questions_report_questions_report__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_note_dailyNotes_dailynotes__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_exam_GrandExam_grantModal_grantModal__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_exam_examModal_examModal__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_exam_createexam_createExam__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_exam_examNew_examComponent__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_exam_createexam_write_exam_write_exam__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_exam_weekend_TermsAndRules_terms_component__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_exam_GrandExam_grantexam_component__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_profile_profile__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_exam_exam__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_exam_exam_room_examroom__ = __webpack_require__(1080);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_exam_weekend_weekend_component__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_evaluate_evaluate__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_evaluate_evaluateOver_evaluateOver__ = __webpack_require__(446);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_evaluate_examOver_examOver__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_evaluate_learnOver_learnOver__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_note_note__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__commen_login_login__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__commen_forgot_forgot__ = __webpack_require__(583);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__commen_setting_setting__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__commen_setting_moreSettings_moreSettings__ = __webpack_require__(582);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__commen_setting_about_about__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__commen_setting_contactUs_contactUs__ = __webpack_require__(580);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__commen_setting_privacyPolicy_privacyPolicy__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__commen_setting_returnPolicy_returnPolicy__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__commen_setting_FAQ_faq__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__commen_setting_teachersPage_teachers__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__commen_setting_timetable_timetable__ = __webpack_require__(581);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_exam_popoverPage_popoverPage__ = __webpack_require__(444);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__ionic_native_status_bar__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__ionic_native_splash_screen__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__ionic_native_file_transfer__ = __webpack_require__(442);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__ionic_native_file__ = __webpack_require__(443);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__services_navBarService__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_homeNew_homeNew__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_exam_exam_room_aiimsResult_aiimsResult__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_exam_createexam_aiimsExam_aiimsExam__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















// import { DailyNote } from '../pages/learn/questions/dailyNotes/dailynotes';






































var AppModule = /** @class */ (function () {
    function AppModule(_navBar) {
        this._navBar = _navBar;
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_51__pages_homeNew_homeNew__["a" /* homeNew */],
                __WEBPACK_IMPORTED_MODULE_9__pages_dashboard_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_exam_examNew_examComponent__["a" /* examComponent */],
                __WEBPACK_IMPORTED_MODULE_10__pages_learn_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_33__commen_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_16__pages_note_dailyNotes_dailynotes__["a" /* DailyNote */],
                __WEBPACK_IMPORTED_MODULE_25__pages_exam_exam__["a" /* Exam */],
                __WEBPACK_IMPORTED_MODULE_28__pages_evaluate_evaluate__["a" /* Evaluate */],
                __WEBPACK_IMPORTED_MODULE_32__pages_note_note__["a" /* Note */],
                __WEBPACK_IMPORTED_MODULE_11__pages_learn_questions_questions__["a" /* Questions */],
                __WEBPACK_IMPORTED_MODULE_12__pages_exam_exam_room_details_details__["a" /* Details */],
                __WEBPACK_IMPORTED_MODULE_34__commen_forgot_forgot__["a" /* Forgot */],
                __WEBPACK_IMPORTED_MODULE_13__pages_learn_questions_note_note_modal__["a" /* HintModalPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_learn_questions_editModal_editModal__["a" /* editModal */],
                __WEBPACK_IMPORTED_MODULE_15__pages_learn_questions_report_questions_report__["a" /* ReportProb */],
                __WEBPACK_IMPORTED_MODULE_35__commen_setting_setting__["a" /* Setting */],
                __WEBPACK_IMPORTED_MODULE_24__pages_profile_profile__["a" /* Profile */],
                __WEBPACK_IMPORTED_MODULE_17__pages_exam_GrandExam_grantModal_grantModal__["a" /* grantModal */],
                __WEBPACK_IMPORTED_MODULE_23__pages_exam_GrandExam_grantexam_component__["a" /* grantExam */],
                __WEBPACK_IMPORTED_MODULE_26__pages_exam_exam_room_examroom__["a" /* ExamRoom */],
                __WEBPACK_IMPORTED_MODULE_18__pages_exam_examModal_examModal__["a" /* ExamModal */],
                __WEBPACK_IMPORTED_MODULE_19__pages_exam_createexam_createExam__["a" /* CreateExam */],
                __WEBPACK_IMPORTED_MODULE_21__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */],
                __WEBPACK_IMPORTED_MODULE_43__commen_setting_timetable_timetable__["a" /* timetable */],
                __WEBPACK_IMPORTED_MODULE_29__pages_evaluate_evaluateOver_evaluateOver__["a" /* EvaluateOver */],
                __WEBPACK_IMPORTED_MODULE_30__pages_evaluate_examOver_examOver__["a" /* ExamOver */],
                __WEBPACK_IMPORTED_MODULE_31__pages_evaluate_learnOver_learnOver__["a" /* LearnOver */],
                __WEBPACK_IMPORTED_MODULE_44__pages_exam_popoverPage_popoverPage__["a" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_exam_weekend_TermsAndRules_terms_component__["a" /* termsrules */],
                __WEBPACK_IMPORTED_MODULE_27__pages_exam_weekend_weekend_component__["a" /* weekendReg */],
                __WEBPACK_IMPORTED_MODULE_6__directives_hideheader_hideheader__["a" /* HideheaderDirective */],
                __WEBPACK_IMPORTED_MODULE_7__directives_hideheader_tabsHiden__["a" /* TabHiddenDirective */],
                __WEBPACK_IMPORTED_MODULE_37__commen_setting_about_about__["a" /* About */],
                __WEBPACK_IMPORTED_MODULE_38__commen_setting_contactUs_contactUs__["a" /* contactUs */],
                __WEBPACK_IMPORTED_MODULE_39__commen_setting_privacyPolicy_privacyPolicy__["a" /* privacyPolicy */],
                __WEBPACK_IMPORTED_MODULE_40__commen_setting_returnPolicy_returnPolicy__["a" /* returnPolicy */],
                __WEBPACK_IMPORTED_MODULE_41__commen_setting_FAQ_faq__["a" /* faq */],
                __WEBPACK_IMPORTED_MODULE_42__commen_setting_teachersPage_teachers__["a" /* teachers */],
                __WEBPACK_IMPORTED_MODULE_36__commen_setting_moreSettings_moreSettings__["a" /* moreSetting */],
                __WEBPACK_IMPORTED_MODULE_52__pages_exam_exam_room_aiimsResult_aiimsResult__["a" /* aiimsResult */],
                __WEBPACK_IMPORTED_MODULE_53__pages_exam_createexam_aiimsExam_aiimsExam__["a" /* aiimsExam */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_45__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_3_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_51__pages_homeNew_homeNew__["a" /* homeNew */],
                __WEBPACK_IMPORTED_MODULE_9__pages_dashboard_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_exam_examNew_examComponent__["a" /* examComponent */],
                __WEBPACK_IMPORTED_MODULE_10__pages_learn_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_33__commen_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_14__pages_learn_questions_editModal_editModal__["a" /* editModal */],
                __WEBPACK_IMPORTED_MODULE_16__pages_note_dailyNotes_dailynotes__["a" /* DailyNote */],
                __WEBPACK_IMPORTED_MODULE_25__pages_exam_exam__["a" /* Exam */],
                __WEBPACK_IMPORTED_MODULE_28__pages_evaluate_evaluate__["a" /* Evaluate */],
                __WEBPACK_IMPORTED_MODULE_32__pages_note_note__["a" /* Note */],
                __WEBPACK_IMPORTED_MODULE_11__pages_learn_questions_questions__["a" /* Questions */],
                __WEBPACK_IMPORTED_MODULE_12__pages_exam_exam_room_details_details__["a" /* Details */],
                __WEBPACK_IMPORTED_MODULE_34__commen_forgot_forgot__["a" /* Forgot */],
                __WEBPACK_IMPORTED_MODULE_13__pages_learn_questions_note_note_modal__["a" /* HintModalPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_exam_popoverPage_popoverPage__["a" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_learn_questions_report_questions_report__["a" /* ReportProb */],
                __WEBPACK_IMPORTED_MODULE_35__commen_setting_setting__["a" /* Setting */],
                __WEBPACK_IMPORTED_MODULE_24__pages_profile_profile__["a" /* Profile */],
                __WEBPACK_IMPORTED_MODULE_43__commen_setting_timetable_timetable__["a" /* timetable */],
                __WEBPACK_IMPORTED_MODULE_17__pages_exam_GrandExam_grantModal_grantModal__["a" /* grantModal */],
                __WEBPACK_IMPORTED_MODULE_23__pages_exam_GrandExam_grantexam_component__["a" /* grantExam */],
                __WEBPACK_IMPORTED_MODULE_26__pages_exam_exam_room_examroom__["a" /* ExamRoom */],
                __WEBPACK_IMPORTED_MODULE_18__pages_exam_examModal_examModal__["a" /* ExamModal */],
                __WEBPACK_IMPORTED_MODULE_19__pages_exam_createexam_createExam__["a" /* CreateExam */],
                __WEBPACK_IMPORTED_MODULE_21__pages_exam_createexam_write_exam_write_exam__["a" /* writeExam */],
                __WEBPACK_IMPORTED_MODULE_29__pages_evaluate_evaluateOver_evaluateOver__["a" /* EvaluateOver */],
                __WEBPACK_IMPORTED_MODULE_30__pages_evaluate_examOver_examOver__["a" /* ExamOver */],
                __WEBPACK_IMPORTED_MODULE_31__pages_evaluate_learnOver_learnOver__["a" /* LearnOver */],
                __WEBPACK_IMPORTED_MODULE_22__pages_exam_weekend_TermsAndRules_terms_component__["a" /* termsrules */],
                __WEBPACK_IMPORTED_MODULE_27__pages_exam_weekend_weekend_component__["a" /* weekendReg */],
                __WEBPACK_IMPORTED_MODULE_37__commen_setting_about_about__["a" /* About */],
                __WEBPACK_IMPORTED_MODULE_38__commen_setting_contactUs_contactUs__["a" /* contactUs */],
                __WEBPACK_IMPORTED_MODULE_39__commen_setting_privacyPolicy_privacyPolicy__["a" /* privacyPolicy */],
                __WEBPACK_IMPORTED_MODULE_40__commen_setting_returnPolicy_returnPolicy__["a" /* returnPolicy */],
                __WEBPACK_IMPORTED_MODULE_41__commen_setting_FAQ_faq__["a" /* faq */],
                __WEBPACK_IMPORTED_MODULE_36__commen_setting_moreSettings_moreSettings__["a" /* moreSetting */],
                __WEBPACK_IMPORTED_MODULE_42__commen_setting_teachersPage_teachers__["a" /* teachers */],
                __WEBPACK_IMPORTED_MODULE_52__pages_exam_exam_room_aiimsResult_aiimsResult__["a" /* aiimsResult */],
                __WEBPACK_IMPORTED_MODULE_53__pages_exam_createexam_aiimsExam_aiimsExam__["a" /* aiimsExam */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_46__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_47__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_49__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_48__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_50__services_navBarService__["a" /* naveBarShowSrvice */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] }
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_50__services_navBarService__["a" /* naveBarShowSrvice */]])
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 683:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HideheaderDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the HideheaderDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
var HideheaderDirective = /** @class */ (function () {
    function HideheaderDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    HideheaderDirective.prototype.ngOnInit = function () {
        this.headerHeight = this.header.clientHeight;
        this.renderer.setElementStyle(this.header, 'webkitTransition', 'top 700ms');
        this.scrollContent = this.element.nativeElement.getElementsByClassName("scroll-content")[0];
        this.renderer.setElementStyle(this.scrollContent, 'webkitTransition', 'margin-top 700ms');
    };
    HideheaderDirective.prototype.onContentScroll = function (event) {
        if (event.scrollTop > 36) {
            this.renderer.setElementStyle(this.header, "top", "-100px");
            this.renderer.setElementStyle(this.scrollContent, "margin-top", "0px");
        }
        else {
            this.renderer.setElementStyle(this.header, "top", "0px");
            this.renderer.setElementStyle(this.scrollContent, "margin-top", "56px");
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("header"),
        __metadata("design:type", HTMLElement)
    ], HideheaderDirective.prototype, "header", void 0);
    HideheaderDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[hideheader]',
            host: {
                '(ionScroll)': 'onContentScroll($event)'
            }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */]])
    ], HideheaderDirective);
    return HideheaderDirective;
}());

//# sourceMappingURL=hideheader.js.map

/***/ }),

/***/ 684:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabHiddenDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TabHiddenDirective = /** @class */ (function () {
    function TabHiddenDirective(_el) {
        this._el = _el;
        this._tabElCache = new Map();
    }
    TabHiddenDirective.prototype.ngAfterViewChecked = function () {
        var tabId = this._el.nativeElement.id.split('-');
        tabId.shift();
        tabId.unshift('tab');
        tabId = tabId.join('-');
        var tabEl;
        if (!this._tabElCache.has(tabId)) {
            tabEl = document.querySelector('#' + tabId);
            this._tabElCache.set(tabId, tabEl);
        }
        else {
            tabEl = this._tabElCache.get(tabId);
        }
        if (tabEl) {
            if (this.tabHidden) {
                tabEl.style.display = 'none';
            }
            else {
                tabEl.style.display = 'flex';
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('tab-hidden'),
        __metadata("design:type", Boolean)
    ], TabHiddenDirective.prototype, "tabHidden", void 0);
    TabHiddenDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({ selector: '[tab-hidden]' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], TabHiddenDirective);
    return TabHiddenDirective;
}());

//# sourceMappingURL=tabsHiden.js.map

/***/ }),

/***/ 685:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__commen_login_login__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__commen_setting_setting__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_learn_list__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_evaluate_evaluate__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_note_note__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_exam_weekend_weekend_component__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_exam_examNew_examComponent__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_homeNew_homeNew__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MyApp = /** @class */ (function () {
    function MyApp(toastCtrl, platform, statusBar, splashScreen) {
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__commen_login_login__["a" /* Login */];
        this.initializeApp();
        if (localStorage.getItem("userData"))
            this.rootPage = __WEBPACK_IMPORTED_MODULE_12__pages_homeNew_homeNew__["a" /* homeNew */];
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Dashboard', component: __WEBPACK_IMPORTED_MODULE_6__pages_dashboard_home__["a" /* HomePage */], icon: 'ios-home' },
            { title: 'Learn', component: __WEBPACK_IMPORTED_MODULE_7__pages_learn_list__["a" /* ListPage */], icon: 'ios-book' },
            { title: 'Exam room', component: __WEBPACK_IMPORTED_MODULE_11__pages_exam_examNew_examComponent__["a" /* examComponent */], icon: 'ios-create' },
            { title: 'Registration', component: __WEBPACK_IMPORTED_MODULE_10__pages_exam_weekend_weekend_component__["a" /* weekendReg */], icon: 'ios-card' },
            { title: 'Evaluate', component: __WEBPACK_IMPORTED_MODULE_8__pages_evaluate_evaluate__["a" /* Evaluate */], icon: 'paper' },
            { title: 'Notes', component: __WEBPACK_IMPORTED_MODULE_9__pages_note_note__["a" /* Note */], icon: 'bookmarks' },
            { title: 'Settings', component: __WEBPACK_IMPORTED_MODULE_5__commen_setting_setting__["a" /* Setting */], icon: 'ios-cog' }
        ];
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            //  var notificationOpenedCallback = (jsonData) => { // <- Here!
            //    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
            //  };
            //  window["plugins"].OneSignal
            //    .startInit("fb35a8d2-1388-41a7-9a00-192f077e6911", "885727504851")
            //    .handleNotificationOpened(notificationOpenedCallback)
            //    .endInit();
        });
    }
    MyApp.prototype.ngOnInit = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__commen_login_login__["a" /* Login */]);
        this.username = JSON.parse(localStorage.getItem('userData'));
        if (this.username) {
            this.firstname = this.username.profile.firstName;
            this.lastName = this.username.profile.lastName;
            this.str3 = this.firstname + " " + this.lastName;
        }
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/app/app.html"*/'\n<style>\n\n.label-md{\n font-size:0 px!important;\n    }\n</style>\n\n<!-- <ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title  class="size-n"><span style="font-size: 25px;">nextfellow</span>\n      </ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content >\n\n      <ion-grid style="background-color:#f8f8f8;">\n        <ion-col col-12  ><img  src="assets/img/image.png" style="width: 45%;margin-left: 29%;"/></ion-col>\n       \n        <ion-row>\n            <ion-col col-12  style="text-align: center;font-size:20px;"><b >{{str3}}</b> </ion-col>\n          </ion-row>\n      </ion-grid>\n\n    <ion-list >\n      <button class="menu-icn" menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n          <ion-icon [name]="p.icon" style="color:#108fd2"></ion-icon> &nbsp; {{p.title}}\n      </button>\n      \n    \n\n    \n\n    </ion-list>\n  </ion-content>\n\n\n</ion-menu> -->\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="true"></ion-nav>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/app/app.html"*/,
            providers: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return weekendReg; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_student_service__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_weekend_service__ = __webpack_require__(441);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(442);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(443);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var weekendReg = /** @class */ (function () {
    function weekendReg(alertCtrl, platform, navParams, transfer, file, navCtrl, __wekklyExam, __notfiSer, StudentSer) {
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.transfer = transfer;
        this.file = file;
        this.navCtrl = navCtrl;
        this.__wekklyExam = __wekklyExam;
        this.__notfiSer = __notfiSer;
        this.StudentSer = StudentSer;
        this.details = false;
        this.bgColor1 = false;
        this.details1 = false;
        this.details2 = false;
        this.isRegistered = true;
        this.bgColor3 = false;
        this.aiimsDiv = false;
        this.updateapp = true;
        this.dailyApply = true;
        this.fullApply = true;
        this.weekendApply = true;
        this.basicButton = false;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.examCategory = navParams.get('exam');
        console.log("examCategory", this.examCategory);
        this.paymentdata = 2999;
        this.paymentdata2 = 3599;
    }
    //nidhun coupencode
    weekendReg.prototype.getMycoupenCode = function () {
        var _this = this;
        this.__wekklyExam.getallCode().subscribe(function (code) {
            _this.coupenCode = code.coupenCode;
            console.log(_this.coupenCode, "my code");
        });
    };
    weekendReg.prototype.Feedback = function (value) {
        if (value) {
            this.coupenName = value.promocode.toUpperCase();
            for (var i = 0; i < this.coupenCode.length; i++) {
                if (this.coupenName == this.coupenCode[i].coupenDetails.coupenName) {
                    this.copName = this.coupenCode[i].coupenDetails.coupenName;
                    this.copCode = this.coupenCode[i].coupenDetails.discountAmount;
                    console.log(this.copName, this.copCode, "coupen names");
                }
            }
            if (this.copName) {
                this.Namefunction(this.copName);
                this.copName = "";
                this.paymentdata2 = (this.paymentdata2) - ((this.paymentdata2) * (this.copCode)) / 100;
                this.discountAmount = this.paymentdata2;
                this.fullApply = true;
                this.__notfiSer.notificationInfo("coupon code applied");
            }
            else {
                this.paymentdata2 = 3599;
                this.__notfiSer.notificationInfo("Invalid coupon code");
            }
        }
    };
    weekendReg.prototype.basicForm = function (value) {
        if (value) {
            this.coupenName = value.promocode.toUpperCase();
            for (var i = 0; i < this.coupenCode.length; i++) {
                if (this.coupenName == this.coupenCode[i].coupenDetails.coupenName) {
                    this.copName = this.coupenCode[i].coupenDetails.coupenName;
                    this.copCode = this.coupenCode[i].coupenDetails.discountAmount;
                    console.log(this.copName, this.copCode, "coupen names");
                }
            }
            if (this.copName) {
                this.Namefunction(this.copName);
                this.copName = "";
                this.paymentdata = (this.paymentdata) - ((this.paymentdata) * (this.copCode)) / 100;
                this.discountAmount = this.paymentdata;
                this.dailyApply = true;
                this.__notfiSer.notificationInfo("coupon code applied");
            }
            else {
                this.paymentdata = 2999;
                this.__notfiSer.notificationInfo("Invalid coupon code");
            }
        }
    };
    weekendReg.prototype.Namefunction = function (name) {
        this.refcode = name;
    };
    weekendReg.prototype.ionViewDidEnter = function () {
        this.topOrBottom = this.contentHandle._tabsPlacement;
        this.contentBox = document.querySelector(".scroll-content");
        if (this.topOrBottom == "top") {
            this.tabBarHeight = this.contentBox.marginTop;
        }
        else if (this.topOrBottom == "bottom") {
            this.tabBarHeight = this.contentBox.marginBottom;
        }
    };
    weekendReg.prototype.scrollingFun = function (e) {
        if (e.scrollTop > this.contentHandle.getContentDimensions().contentHeight) {
            // document.querySelector(".tabbar")['style'].display = 'none';
            if (this.topOrBottom == "top") {
                this.contentBox.marginTop = 0;
            }
            else if (this.topOrBottom == "bottom") {
                this.contentBox.marginBottom = 0;
            }
        }
        else {
            // document.querySelector(".tabbar")['style'].display = 'flex';
            if (this.topOrBottom == "top") {
                this.contentBox.marginTop = this.tabBarHeight;
            }
            else if (this.topOrBottom == "bottom") {
                this.contentBox.marginBottom = this.tabBarHeight;
            }
        } //if else
    }; //scrollingFun
    weekendReg.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log("enter herere");
        if (this.slideIndex) {
            setTimeout(function () {
                return _this.slides.slideTo(_this.slideIndex, 1000);
            }, 1000);
        }
    };
    weekendReg.prototype.ngOnInit = function () {
        this.getMycoupenCode();
        this.Amount();
        this.checkAplay();
        this.singlePay();
        this.SUBSCRIBE();
        if (!this.examCategory) {
            this.getData1();
            this.getfirstcolor();
        }
        else if (this.examCategory == "week") {
            this.getData2();
        }
        else if (this.examCategory == "daily") {
            this.getData();
        }
        else if (this.examCategory == "aiims") {
            this.slideIndex = 3;
            console.log("this.slideIndex", this.slideIndex);
            //this.goToSlide()
            this.getData4();
        }
    };
    //pdf file download
    weekendReg.prototype.download = function () {
        var fileTransfer = this.transfer.create();
        var mime = 'application/pdf';
        var url = 'assets/img/netfellowTimetable.pdf';
        fileTransfer.download(url, this.file.dataDirectory + 'netfellowTimetable.pdf').then(function (entry) {
            console.log(entry, "dow1");
            console.log('download complete: ' + entry.toURL());
        }, function (error) {
            console.log(error, "down2");
        });
    };
    weekendReg.prototype.getfirstcolor = function () {
        this.selecteditem = true;
    };
    weekendReg.prototype.singlePay = function () {
        var _this = this;
        this.__wekklyExam.getWeekExams().subscribe(function (weekRes) {
            if (weekRes.status) {
                _this.examList = weekRes.data.inactiveExams;
                _this.examList = _this.examList.sort(function (a, b) { return new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime(); });
                console.log(_this.examList, "exam list bro");
            }
        });
    };
    //check applyed or not
    weekendReg.prototype.checkAplay = function () {
        var _this = this;
        this.__wekklyExam.checkAppliedForExam().subscribe(function (data) {
            console.log(data, "data here");
            if (data.status) {
                console.log(_this.paymentdata, "data check");
                _this.registerLabel = "Register";
                _this.isRegistered = false;
                _this.fullApply = false;
                _this.paymentDetails = data;
                _this.dailyApply = false;
            }
            else {
                _this.__wekklyExam.basicUserTest().subscribe(function (data) {
                    console.log(data, "basic nidhun1");
                    if (data.status) {
                        _this.paymentDetails = data.keyId;
                        _this.dailyApply = false;
                        _this.fullApply = true;
                        _this.basicButton = true;
                        _this.registerLabel = "Register";
                        _this.RegisterMsg = "you have offer for full package";
                    }
                    else {
                        _this.dailyApply = true;
                        _this.fullApply = true;
                        _this.registerLabel = "Already registered";
                        _this.RegisterMsg = "You are registered";
                    }
                });
            }
        }, function (err) {
            _this.__notfiSer.notificationInfo("OOPS Something went wrong.");
        });
    };
    //payment function
    weekendReg.prototype.payment = function (plan, desc, item) {
        var _this = this;
        if (plan == "basic") {
            this.myamount = this.paymentdata;
            this.amountRound = Math.round(this.myamount);
        }
        else {
            this.myamount = this.paymentdata2;
            this.amountRound = Math.round(this.myamount);
        }
        console.log(this.myamount, "amount here");
        if (item) {
            this.singleExamId = item._id;
        }
        this.plan = plan;
        if (!this.isRegistered) {
            var options_1 = {
                // key: 'rzp_test_V1RjYTq2Xll4NR',
                key: this.paymentDetails.data.keyId,
                amount: this.amountRound * 100,
                name: desc,
                prefill: {
                    name: this.userData.profile.firstName + this.userData.profile.lastName,
                    email: this.userData.username
                },
                notes: {
                    address: ""
                },
                theme: {
                    color: "#108fd2"
                }
            };
            var successCallback = function (payment_id) {
                _this.serviceCall(payment_id);
            };
            var cancelCallback = function (error) {
                console.log(error.description);
            };
            this.platform.ready().then(function () {
                RazorpayCheckout.open(options_1, successCallback, cancelCallback);
            });
        }
        else
            this.__notfiSer.notificationInfo("Already registered.");
    };
    //test
    // paymentForBasic(plan, desc,item) {
    //    this.newamouts=700;
    //     try {
    //       let options = {
    //         key: 'rzp_test_V1RjYTq2Xll4NR',
    //         // key: this.paymentDetails,
    //         amount:this.newamouts*100,
    //         name: desc,
    //         prefill: {
    //           name: this.userData.profile.firstName + this.userData.profile.lastName,
    //           email: this.userData.username
    //         },
    //         notes: {
    //           address: ""
    //         },
    //         theme: {
    //           color: "green "
    //         },
    //         handler: this.serviceCallBasic.bind(this)
    //       }
    //       let rzp1 = new this.StudentSer.nativeWindow.Razorpay(options);
    //       rzp1.open();
    //     }
    //     catch (err) {
    //       this.__notfiSer.notificationInfo("OOPS! Something went wrong");
    //     }
    // }
    // paymentResponseHander(){
    //   this.postData = {
    //     paymentId: this.paymentId,
    //     paymentStatus: "done",
    //     paymentMethod: "online",
    //     PlaneName: this.plan,
    //     refCode:(this.refcode) ? this.refcode : null,
    //     discount:this.discountAmount,
    //     updateapp:true
    //   }
    //   console.log(this.postData,"my data here")
    //   this.__wekklyExam.registerWeeklyExam(this.postData)
    //   .subscribe(
    //   res => {
    //     console.log(res,"nidhun check");
    //     if (res.status) {
    //       this.registerLabel = "Already registered"
    //       this.isRegistered = true;
    //       this.checkAplay();
    //     }else{
    //                 this.__notfiSer.notificationInfo(res.message)
    //        }
    //   }, err => {
    //     this.__notfiSer.notificationInfo("OOPS Something went wrong.")
    //   })
    // }
    //test end
    weekendReg.prototype.paymentForBasic = function (plan, desc, item) {
        var _this = this;
        this.newamouts = 700;
        var options = {
            // key: 'rzp_test_V1RjYTq2Xll4NR',
            key: this.paymentDetails,
            amount: this.newamouts * 100,
            name: desc,
            prefill: {
                name: this.userData.profile.firstName + this.userData.profile.lastName,
                email: this.userData.username
            },
            notes: {
                address: ""
            },
            theme: {
                color: "#108fd2"
            }
        };
        var successCallback = function (payment_id) {
            _this.serviceCallBasic(payment_id);
        };
        var cancelCallback = function (error) {
            console.log(error.description);
        };
        this.platform.ready().then(function () {
            RazorpayCheckout.open(options, successCallback, cancelCallback);
        });
    };
    weekendReg.prototype.serviceCallBasic = function (payment_id) {
        var _this = this;
        this.paymentId = payment_id;
        this.__wekklyExam.registerWeeklyExamBasic(this.paymentId)
            .subscribe(function (res) {
            if (res.status) {
                _this.registerLabel = "Already registered";
                _this.isRegistered = true;
                _this.dailyApply = true;
                _this.basicButton = false;
                _this.fullApply = true;
                _this.__notfiSer.notificationInfo("Register successful");
                _this.checkAplay();
            }
            else {
                _this.__notfiSer.notificationInfo(res.message);
            }
        }, function (err) {
            _this.__notfiSer.notificationInfo("OOPS Something went wrong.");
        });
    };
    weekendReg.prototype.serviceCall = function (payment_id) {
        var _this = this;
        this.paymentId = payment_id;
        if (this.singleExamId) {
            this.postData = {
                examId: this.singleExamId,
                paymentId: this.paymentId,
                paymentStatus: "done",
                paymentMethod: "online",
                PlaneName: this.plan
            };
        }
        else {
            this.postData = {
                paymentId: this.paymentId,
                paymentStatus: "done",
                paymentMethod: "online",
                PlaneName: this.plan,
                refCode: (this.refcode) ? this.refcode : null,
                discount: this.amountRound,
                updateapp: true
            };
        }
        this.__wekklyExam.registerWeeklyExam(this.postData)
            .subscribe(function (res) {
            if (res.status) {
                _this.registerLabel = "Already registered";
                _this.isRegistered = true;
                _this.dailyApply = true;
                _this.fullApply = true;
                _this.checkAplay();
            }
            else {
                _this.__notfiSer.notificationInfo(res.message);
            }
        }, function (err) {
            _this.__notfiSer.notificationInfo("OOPS Something went wrong.");
        });
    };
    weekendReg.prototype.getData = function () {
        this.details = true;
        this.details1 = false;
        this.details2 = false;
        this.details3 = false;
        this.selecteditem = false;
        this.bgColor1 = true;
        this.bgColor2 = false;
        this.aiimsDiv = false;
    };
    weekendReg.prototype.getData1 = function () {
        this.details1 = true;
        this.details = false;
        this.details2 = false;
        this.details3 = false;
        this.bgColor1 = false;
        this.bgColor2 = false;
        this.aiimsDiv = false;
        this.bgColor3 = false;
        this.selecteditem = true;
    };
    weekendReg.prototype.getData3 = function () {
        this.details3 = true;
        this.details = false;
        this.details2 = false;
        this.aiimsDiv = false;
        this.bgColor3 = false;
    };
    weekendReg.prototype.getData2 = function () {
        this.details2 = true;
        this.details1 = false;
        this.details = false;
        this.selecteditem = false;
        this.bgColor1 = false;
        this.bgColor2 = true;
        this.aiimsDiv = false;
        this.bgColor3 = false;
        if (this.RegisterMsg)
            this.__notfiSer.notificationInfo(this.RegisterMsg);
        //this.bgColor2 = '#108fd2'; 
    };
    weekendReg.prototype.getData4 = function () {
        this.aiimsDiv = true;
        this.details1 = false;
        this.details = false;
        this.details2 = false;
        this.details3 = false;
        this.bgColor1 = false;
        this.bgColor2 = false;
        this.bgColor3 = true;
        this.selecteditem = false;
    };
    //AIIMs/jipmer
    weekendReg.prototype.SUBSCRIBE = function () {
        var _this = this;
        this.__wekklyExam.__SUBSCRIBEPAY().subscribe(function (res) {
            console.log(res, "res");
            if (res.status) {
                _this.AiimsButton = true;
                _this.AIIMSKEYID = res.data.keyId;
            }
            else
                _this.AiimsButton = false;
            _this.AIIMSKEYID = res.data.keyId;
        }, function (err) {
            _this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
        });
    };
    //AIIMS amount
    weekendReg.prototype.Amount = function () {
        var _this = this;
        this.__wekklyExam.__AiimsPay().subscribe(function (res) {
            if (res.status)
                _this.AiimsAmount = res.data[0].price;
            _this.packageId = res.data[0]._id;
        }, function (err) {
            _this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
        });
    };
    weekendReg.prototype.AIIMSPAYNOW = function () {
        var _this = this;
        var options = {
            // key: 'rzp_test_V1RjYTq2Xll4NR',
            key: this.AIIMSKEYID,
            amount: this.AiimsAmount * 100,
            name: "AIIMS/JIPMER SERIES",
            prefill: {
                name: this.userData.profile.firstName + this.userData.profile.lastName,
                email: this.userData.username
            },
            notes: {
                address: ""
            },
            theme: {
                color: "#108fd2"
            }
        };
        var successCallback = function (payment_id) {
            _this.AIIMSPAYMENT_SUCCESS(payment_id);
        };
        var cancelCallback = function (error) {
            console.log(error.description);
        };
        this.platform.ready().then(function () {
            RazorpayCheckout.open(options, successCallback, cancelCallback);
        });
    };
    weekendReg.prototype.PROPOP = function () {
        var _this = this;
        this.__wekklyExam.MSGIOS().subscribe(function (res) {
            console.log(res);
            if (res.status) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Notification',
                    subTitle: 'Please check mail',
                    buttons: ['ok']
                });
                alert_1.present();
            }
            else
                _this.__notfiSer.notificationInfo("Please try again");
        });
    };
    weekendReg.prototype.AIIMSPAYMENT_SUCCESS = function (paymnetId) {
        var _this = this;
        this.AiimsButton = true;
        this.__wekklyExam.___AIIMSPAY(this.packageId, (this.MyCode) ? this.MyCode : null, paymnetId, this.AiimsAmount).subscribe(function (res) {
            if (res.status) {
                _this.SUBSCRIBE();
                _this.AiimsButton = true;
                _this.__notfiSer.notificationInfo("Registeration successful");
            }
            else {
                _this.SUBSCRIBE();
                _this.AiimsButton = false;
                _this.__notfiSer.notificationInfo(res.message);
            }
        }, function (err) {
            _this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
        });
        this.SUBSCRIBE();
        this.AiimsButton = true;
    };
    //AIIMS coupon 
    weekendReg.prototype.CouponCode = function (value) {
        var _this = this;
        console.log(value, "value data");
        this.__wekklyExam.__couponcode(this.packageId, value.couponCode).subscribe(function (res) {
            if (res.data) {
                _this.MyCode = res.data[0].couponCode.discountPercentage;
                _this.AiimsAmount = res.data[0].couponCode.price;
                _this.__notfiSer.notificationInfo("Discount applied ");
            }
            else {
                _this.__notfiSer.notificationInfo("Invalid discount code ");
            }
        }, function (err) {
            _this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
    ], weekendReg.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("contentRef"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], weekendReg.prototype, "contentHandle", void 0);
    weekendReg = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-weekendpage',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/weekend/weekend.component.html"*/'\n <style>\n    .card-md {\n       margin-left:-4px !important;\n       border-radius: 2px!important;\n       width: calc(100% - -8px)!important;   \n     }\n   </style>\n   \n   <ion-header>\n           <ion-navbar>\n               <button ion-button menuToggle>\n                   <ion-icon name="menu"></ion-icon>\n               </button>\n               <ion-title>        Pro users\n            </ion-title>\n           </ion-navbar>\n       </ion-header>\n       \n       <ion-content padding style="background-color: #efefef;" #contentRef (ionScroll)="scrollingFun($event)">\n\n            <!-- <ion-slides pager style="height:100%;background-color:white; box-shadow: 0 8px 6px -6px black; border: 1px solid #108fd2;">\n\n                    <ion-slide >\n                      <ion-row >\n                      <ion-col col-12 >\n          \n                          <img src="assets/img/girl-avatar.png" style="width:100%; ">\n          \n                      </ion-col>\n          \n                      </ion-row>\n                      <ion-row>\n                          <ion-col col-12 style="text-align:left;padding:5px;">\n          \n                           <h4> nextfellow</h4>\n                           <p>Lorem ipsum dolor sit amet, vim cu nulla dolor. Melius salutandi sententiae pro ad. Has in voluptaria interesset, at pri wisi habeo. Ius postea habemus instructior an, ei qui veri verear vivendo, ex postea persecuti philosophia nam.\n                            </p>\n                          </ion-col>\n                          </ion-row>\n                          \n                    </ion-slide>\n                  \n                    <ion-slide style="background-color: white">\n                        <ion-row >\n                            <ion-col col-12 >\n                \n                                <img src="assets/img/girl-avatar.png" style="width:100%; ">\n                \n                            </ion-col>\n                \n                            </ion-row>\n                            <ion-row>\n                                <ion-col col-12 style="text-align:left;padding:5px;">\n                \n                                 <h4> nextfellow</h4>\n                                 <p>Lorem ipsum dolor sit amet, vim cu nulla dolor. Melius salutandi sententiae pro ad. Has in voluptaria interesset, at pri wisi habeo. Ius postea habemus instructior an, ei qui veri verear vivendo, ex postea persecuti philosophia nam.\n                                  </p>\n                                </ion-col>\n                                </ion-row>\n                               \n                    </ion-slide>\n                  \n                    <ion-slide style="background-color:white">\n                        <ion-row >\n                            <ion-col col-12 >\n                \n                                <img src="assets/img/girl-avatar.png" style="width:100%; ">\n                \n                            </ion-col>\n                \n                            </ion-row>\n                            <ion-row>\n                                <ion-col col-12 style="text-align:left;padding:5px;">\n                \n                                 <h4> nextfellow</h4>\n                                 <p>Lorem ipsum dolor sit amet, vim cu nulla dolor. Melius salutandi sententiae pro ad. Has in voluptaria interesset, at pri wisi habeo. Ius postea habemus instructior an, ei qui veri verear vivendo, ex postea persecuti philosophia nam.\n                                  </p>\n                                </ion-col>\n                                </ion-row>\n                                \n                    </ion-slide>\n                 \n                  \n                  </ion-slides> -->\n              \n                   <!-- <ion-card style="height:73px;margin-left: 0px;">\n                       <ion-slides [slidesPerView]="3">\n                           <ion-slide class="blocks1">\n                               <h2 class="blockData" style="color: white;">Weekend\n                                   <br>Exam</h2>\n                           </ion-slide>\n   \n                       </ion-slides>\n                   </ion-card> -->\n                   <ion-card style="height: 63px;margin-left: 0px;\n                  ">\n                       <ion-slides  [slidesPerView]="2">\n                           <ion-slide  (click)="getData()" [ngClass]="(bgColor1)?\'selected\':\'unselected\'">\n                               <span  style="font-size: 15px;">Daily Test Series</span>\n                           </ion-slide>\n                         \n                          \n<!--                          \n                            <ion-slide  (click)="getData2()"  [ngClass]="(bgColor2)?\'selected\':\'unselected\'">\n                                    <span style="font-size: 15px;" >Weekend Test Series</span>\n                                </ion-slide> -->\n                                <ion-slide  (click)="getData1();getfirstcolor()" [ngClass]="(selecteditem)?\'selected\':\'unselected\'" id="clickYearOne">\n                                    <span style="font-size: 15px;" >NEET-PG Package</span>\n                                </ion-slide>\n                                <ion-slide  (click)="getData4()" [ngClass]="(bgColor3)?\'selected\':\'unselected\'" >\n                                    <span  style="font-size: 15px;">AIIMS/JIPMER Series</span>\n                                </ion-slide>\n                          \n                       </ion-slides>\n                   </ion-card>\n   \n                   <ion-card style="height:auto;margin-left: 0px;\n                   "   *ngIf="details">\n                       <!-- <ion-row class="BtnRow"> -->\n                           <!-- <ion-col>\n                               <button ion-button style="background-color:#108fd2">Timetables</button>\n                           </ion-col> -->\n           \n                           <!-- <ion-col class="BtnRow1">\n           \n                               <a href="assets/img/netfellowTimetable.pdf" class="button download" download>\n                                   <ion-icon name="download"></ion-icon>\n                               </a>\n                           </ion-col> -->\n                           <!-- <button ion-button (click)="downloadFile() " ><ion-icon name="download"></ion-icon></button></ion-col>\n                           -->\n                       <!-- </ion-row> -->\n                       <ion-row>\n                           <ion-col col-12>\n                               <p style="text-align: center;\n                               font-size: 17px;\n                               padding: 8px;\n                               ">Prepare with nextfellow for AIIMS & NEET PG 2019</p>\n                               <hr>\n                           </ion-col>\n                           <ion-grid>\n                           <ion-row>\n                           <ion-list>\n                               \n                                <ion-col col-12 style="text-align: center;">\n                               <ion-item style="padding-left: 30px;">\n                                    <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                                    <span style="font-size: 15px;\n                                    font-weight: bold;">Daily topic wise tests(10-25 MCQs)</span>\n                               </ion-item>\n                               \n                           \n                               <ion-item style="padding-left: 30px;">\n                                    <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                                    <span style="font-size: 15px;\n                                    font-weight: bold;">Daily high yeild notes and pearls</span>\n                               </ion-item>\n                               <ion-item style="padding-left: 30px;">\n                                    <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                                    <span style="font-size: 15px;\n                                    font-weight: bold;">Subject wise weekend tests</span>\n                               </ion-item>\n                               <ion-item style="padding-left: 30px;">\n                                    <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                                  <span style="font-size: 15px;\n                                  font-weight: bold;"> Weekly image based test(10-25 MCQs)</span>\n                               </ion-item>\n                            </ion-col>\n\n                           </ion-list>\n                           </ion-row>\n                        </ion-grid>\n<ion-grid>\n    <ion-row>\n\n                           <!-- <ion-col col-12 style="text-align: center;">\n                                <button ion-button style="color:#fff;background-color: #108fd2"  round>&#x20b9;{{paymentdata}}</button>\n                           </ion-col> -->\n                        </ion-row>\n                        </ion-grid>\n                        <ion-grid>\n                        <ion-row>\n                           <!-- <form  #subForm="ngForm" (ngSubmit)="basicForm(subForm.value)" novalidate>\n                              \n                                    <ion-row>\n                                        <ion-col col-8>\n                                    <ion-input type="text" #promocode="ngModel" id="promocode" placeholder="Enter Promo code here" \n                                      name="promocode" required ngModel></ion-input>\n    </ion-col>\n                                  <ion-col col-4>\n                                  <button ion-button  class="bgcolor" type="submit" style="background-color:black;color:#fff"  [disabled]=\'dailyApply\'>Apply</button>\n    </ion-col>\n                                  </ion-row>\n                               \n                                     \n                                </form> -->\n                            </ion-row>\n</ion-grid>\n\n                                <ion-col>\n                                        <button ion-button block round style="background-color:#108fd2;color:white; " [disabled]=\'isRegistered\' (click)="PROPOP(\'basic\',\'6 Months\',false)">Send enquiry</button>\n                                </ion-col>\n                       </ion-row>\n                       <!-- <ion-row class="BtnRow">\n                           <ion-col>\n                               <h2>\n                                   <b style="margin-left: 11px;"> 54 Subjectwise Test</b>\n                               </h2>\n                               <br>\n                               <h2>\n                                   <b style="margin-left: 5px;">54 Grand/Special Test</b>\n                               </h2>\n                           </ion-col>\n                       </ion-row>\n                       <ion-row>\n                           <ion-col>\n                               <button ion-button block style="margin-top:58px; height: 57px;background-color:#108fd2;" [disabled]=\'isRegistered\' (click)="payment(\'pro\',\'Yearly\',false)">\n                                   <h2 style="color:#fff">{{registerLabel}}</h2> &nbsp;&nbsp;\n                                   <div *ngIf="registerLabel==Register">\n                                       <h2 style="text-decoration: line-through; color:#fff">2500</h2>\n                                       <h2 style="color:#fff;margin-top: 33px;\n                         margin-left: -33px;\n                         font-size: 20px;">1750</h2>\n                                   </div>\n                               </button>\n                           </ion-col>\n                       </ion-row> -->\n           \n                   </ion-card>\n               <ion-card style="height: 427px;;margin-left: 0px;\n               "   *ngIf="details1">\n                   <!-- <ion-row class="BtnRow"> -->\n                       <!-- <ion-col>\n                           <button ion-button style="background-color:#108fd2">Timetables</button>\n                       </ion-col> -->\n       \n                       <!-- <ion-col class="BtnRow1">\n       \n                           <a href="assets/img/netfellowTimetable.pdf" class="button download" download>\n                               <ion-icon name="download"></ion-icon>\n                           </a>\n                       </ion-col> -->\n                       <!-- <button ion-button (click)="downloadFile() " ><ion-icon name="download"></ion-icon></button></ion-col>\n                       -->\n                   <!-- </ion-row> -->\n                   <ion-row>\n                       <ion-col>\n                           <p style="text-align: center;\n                           font-size: 17px;\n                           padding: 8px;\n                           ">Prepare with nextfellow for AIIMS & NEET PG 2019</p>\n                           <hr>\n                       </ion-col>\n                       <ion-grid>\n                            <ion-row>\n                       <ion-list>\n                  <ion-col col-12>\n                           <ion-item style="padding-left: 30px;">\n                                <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                                <span style="font-size: 15px;\n                                font-weight: bold;">Daily test series</span>\n                           </ion-item>\n                           <ion-item style="padding-left: 30px;">\n                                <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                              <span style="font-size: 15px;\n                              font-weight: bold;">Week end series</span>\n                           </ion-item>\n                        </ion-col>\n                       </ion-list>\n</ion-row>\n</ion-grid>\n<ion-grid>\n        <ion-row>\n    \n                       <!-- <ion-col col-12 style="text-align: center" >\n                            <button *ngIf="!basicButton" ion-button style="color:#fff;background-color: #108fd2"  round>&#x20b9;{{paymentdata2}}</button>\n\n                            <button *ngIf="basicButton" ion-button style="color:#fff;background-color: #108fd2"  round>&#x20b9;700(offer)</button>\n                       </ion-col> -->\n                      </ion-row>\n                      </ion-grid>\n                      <ion-grid>\n                            <ion-row>\n                        \n                       <!-- <form  #subForm="ngForm" (ngSubmit)="Feedback(subForm.value)" novalidate>\n                          \n                                <ion-row >\n                                    <ion-col col-8>\n                                <ion-input type="text" #promocode="ngModel" id="promocode" placeholder="Enter Promo code here" \n                                  name="promocode" required ngModel></ion-input>\n</ion-col>\n                              <ion-col col-4>\n                              <button ion-button  class="bgcolor" type="submit" style="background-color:black;color:#fff"   [disabled]=\'fullApply\'>Apply</button>\n</ion-col>\n                           \n                              </ion-row>\n                           \n                                 \n                            </form> -->\n                            </ion-row>\n                            </ion-grid>\n                            <ion-col>\n                                    <button ion-button block round style="background-color:#108fd2;color:white; " *ngIf="!basicButton" [disabled]=\'isRegistered\' (click)="PROPOP(\'pro\',\'Yearly\',false)">Send enquiry</button>\n                                    <button ion-button block round style="background-color:#108fd2;color:white; " *ngIf="basicButton"  (click)="PROPOP(\'pro\',\'Yearly\',false)">Send enquiry</button>\n\n                                </ion-col>\n                   </ion-row>\n                   <!-- <ion-row class="BtnRow">\n                       <ion-col>\n                           <h2>\n                               <b style="margin-left: 11px;"> 54 Subjectwise Test</b>\n                           </h2>\n                           <br>\n                           <h2>\n                               <b style="margin-left: 5px;">54 Grand/Special Test</b>\n                           </h2>\n                       </ion-col>\n                   </ion-row>\n                   <ion-row>\n                       <ion-col>\n                           <button ion-button block style="margin-top:58px; height: 57px;background-color:#108fd2;" [disabled]=\'isRegistered\' (click)="payment(\'pro\',\'Yearly\',false)">\n                               <h2 style="color:#fff">{{registerLabel}}</h2> &nbsp;&nbsp;\n                               <div *ngIf="registerLabel==Register">\n                                   <h2 style="text-decoration: line-through; color:#fff">2500</h2>\n                                   <h2 style="color:#fff;margin-top: 33px;\n                     margin-left: -33px;\n                     font-size: 20px;">1750</h2>\n                               </div>\n                           </button>\n                       </ion-col>\n                   </ion-row> -->\n       \n               </ion-card>\n               <ion-card style="height: 427px;;margin-left: 0px;\n               "   *ngIf="details2">\n                   <!-- <ion-row class="BtnRow"> -->\n                       <!-- <ion-col>\n                           <button ion-button style="background-color:#108fd2">Timetables</button>\n                       </ion-col> -->\n       \n                       <!-- <ion-col class="BtnRow1">\n       \n                           <a href="assets/img/netfellowTimetable.pdf" class="button download" download>\n                               <ion-icon name="download"></ion-icon>\n                           </a>\n                       </ion-col> -->\n                       <!-- <button ion-button (click)="downloadFile() " ><ion-icon name="download"></ion-icon></button></ion-col>\n                       -->\n                   <!-- </ion-row> -->\n                   <!-- <ion-row>\n                       <ion-col>\n                           <p style="text-align: center;\n                           font-size: 17px;\n                           padding: 8px;\n                           ">Prepare with nextfellow for AIIMS & NEET PG 2019</p>\n                           <hr>\n                       </ion-col>\n                       <ion-grid>\n                        <ion-row>\n                       <ion-list>\n                           <ion-col col-12>\n                           <ion-item style="padding-left: 30px;">\n                                <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                                <span style="font-size: 15px;\n                                font-weight: bold;">20 subject wise tests</span>\n                           </ion-item>\n                           <ion-item style="padding-left: 30px;">\n                                <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                                <span style="font-size: 15px;\n                                font-weight: bold;">10 mock/special tests</span>\n                           </ion-item>\n                        </ion-col>\n                       </ion-list>\n                     </ion-row>\n                    </ion-grid>\n                            <ion-grid>\n                                <ion-row>\n                       <ion-col col-12 style=" text-align: center;">\n                            <button ion-button style="color:#fff;background-color: #108fd2"round>&#x20b9;1999</button>\n                       </ion-col>\n                       </ion-row>\n                       </ion-grid>\n                       <ion-grid>\n                           <ion-row>\n                      \n                       <form  #subForm="ngForm" (ngSubmit)="Feedback(subForm.value)" novalidate>\n                          \n                                <ion-row style="margin-left: 25px;">\n                                    <ion-col col-8>\n                                <ion-input type="text" #promocode="ngModel" id="promocode" placeholder="Enter Promo code here" \n                                  name="promocode" required ngModel></ion-input>\n</ion-col>\n                              <ion-col col-4>\n                              <button ion-button  class="bgcolor" type="submit" style="background-color:black;color:#fff" [disabled]=\'weekendApply\' >Apply</button>\n</ion-col>\n                              <div class="error" *ngIf="promocode?.errors && (promocode?.touched || promocode?.dirty)">\n                               \n                              </div>\n                              </ion-row>\n                           \n                                 \n                            </form>\n                            </ion-row>\n                            </ion-grid>\n                            <ion-col>\n                                    <button ion-button block round style="color:#fff;background-color: #108fd2" [disabled]="true">Subcribe</button>\n                            </ion-col>\n                   </ion-row> -->\n                \n                   <!-- <ion-row class="BtnRow">\n                       <ion-col>\n                           <h2>\n                               <b style="margin-left: 11px;"> 54 Subjectwise Test</b>\n                           </h2>\n                           <br>\n                           <h2>\n                               <b style="margin-left: 5px;">54 Grand/Special Test</b>\n                           </h2>\n                       </ion-col>\n                   </ion-row>\n                   <ion-row>\n                       <ion-col>\n                           <button ion-button block style="margin-top:58px; height: 57px;background-color:#108fd2;" [disabled]=\'isRegistered\' (click)="payment(\'pro\',\'Yearly\',false)">\n                               <h2 style="color:#fff">{{registerLabel}}</h2> &nbsp;&nbsp;\n                               <div *ngIf="registerLabel==Register">\n                                   <h2 style="text-decoration: line-through; color:#fff">2500</h2>\n                                   <h2 style="color:#fff;margin-top: 33px;\n                     margin-left: -33px;\n                     font-size: 20px;">1750</h2>\n                               </div>\n                           </button>\n                       </ion-col>\n                   </ion-row> -->\n       \n               </ion-card>\n        \n          \n       \n       \n           <!-- <ion-grid>\n           \n            \n            \n               <ion-scroll scrollY="true" class="contentScroll" *ngIf="details2">\n                       <div class="center" *ngIf="!examList">\n                               \n                                      \n                                       <ion-spinner  ></ion-spinner>\n                                      \n                                     </div>\n                       <div  *ngIf="!isRegistered">\n                       <ion-card *ngFor="let items of examList; let i = index" style="min-height:50px;">\n                          \n                                     \n                               <p [ngClass]="{\'week\': items.examType==\'special\' || items.examType == \'week\'}">{{items.examName}} {{items.name}} <button ion-button style="float:right;" (click)="payment(\'single\',\'Single exam\', items)">payNow</button></p>\n                               <p>{{items.createdDate|date}} </p>\n                              \n                         \n                         </ion-card>\n                       </div>\n           \n                       </ion-scroll>\n           \n           </ion-grid> -->\n       \n       \n       \n       \n           <ion-card style="height: 69%; margin-left: 0px;\n          " *ngIf="details4">\n               <ion-scroll scrollY="true" class="contentScroll">\n                   <div class="center" *ngIf="!examList">\n       \n       \n                       <ion-spinner></ion-spinner>\n       \n                   </div>\n                   <div *ngIf="!isRegistered">\n                       <ion-card *ngFor="let items of examList; let i = index" style="background-color: beige">\n       <ion-row>\n       <ion-col col-8>\n       <p [ngClass]="{\'week\': items.examType==\'special\' || items.examType == \'week\'}" style="font-size: 15px;color: darkred;">{{items.examName}} {{items.name}}</p>\n               <p style="font-size: 12px;">{{items.createdDate|date}} </p> \n   \n   \n       </ion-col>\n       <ion-col col-4>\n               <button ion-button style="float:right;background-color:#108fd2" class="blockData" (click)="payment(\'single\',items.examName, items)">pay Now</button>\n   \n       </ion-col>\n       </ion-row>\n   \n                       </ion-card>\n                   </div>\n               </ion-scroll>\n           </ion-card>\n       \n     \n\n          \n\n           <ion-card style="height:424px;margin-left: 0px;"   *ngIf="aiimsDiv">\n              \n               <ion-row>\n                   <ion-col col-12>\n                       <p style="text-align: center;\n                       font-size: 17px;\n                       padding: 8px;\n                       ">Prepare with nextfellow for AIIMS & JIPMER 2019</p>\n                       <hr>\n                   </ion-col>\n                </ion-row>\n            \n                <ion-row>\n                       \n                       <ion-item style="padding-left: 30px;">\n                            <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                            <span style="font-size: 15px;\n                            font-weight: bold;">5 AIIMS mock tests</span>\n                       </ion-item>\n                       \n                   \n                       <ion-item style="padding-left: 30px;">\n                            <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                            <span style="font-size: 15px;\n                            font-weight: bold;">5 JIPMER mock tests</span>\n                       </ion-item>\n\n                       <ion-item style="padding-left: 30px;">\n                        <ion-icon name="checkmark" style="color: green;"></ion-icon>&nbsp;\n                        <span style="font-size: 15px;\n                        font-weight: bold;">1 AIIMS&JIPMER Demo Exam</span>\n                   </ion-item>\n                \n\n\n                        \n                </ion-row>\n        \n                <ion-grid>\n                    <ion-row>\n                \n                                   <!-- <ion-col col-12 style="text-align: center" >\n            \n                                        <button *ngIf="AiimsAmount"  ion-button style="color:#fff;background-color: #108fd2"  round>&#x20b9;{{AiimsAmount}}</button>\n                                   </ion-col> -->\n                                  </ion-row>\n                                  </ion-grid>\n          \n               \n                                  <ion-grid>\n                                    <ion-row>\n                                \n                               <!-- <form  #subForm="ngForm" (ngSubmit)="CouponCode(subForm.value)" novalidate>\n                                  \n                                        <ion-row >\n                                            <ion-col col-8>\n                                        <ion-input type="text" #couponCode="ngModel"  placeholder="Enter Promo code here" \n                                          name="couponCode" required ngModel></ion-input>\n        </ion-col>\n                                      <ion-col col-4>\n                                      <button ion-button  class="bgcolor" type="submit" style="background-color:black;color:#fff"   >Apply</button>\n        </ion-col>\n                                   \n                                      </ion-row>\n                                   \n                                         \n                                    </form> -->\n                                    </ion-row>\n                                    </ion-grid>\n                                    <ion-col>\n                                        <button ion-button block round style="background-color:#108fd2;color:white; " [disabled]="AiimsButton"  (click)="PROPOP()">Send enquiry</button>\n    \n                                    </ion-col>\n\n           </ion-card>\n\n   \n       </ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/weekend/weekend.component.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_weekend_service__["a" /* WeeklyService */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_2__services_student_service__["a" /* StudentService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_weekend_service__["a" /* WeeklyService */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_2__services_student_service__["a" /* StudentService */]])
    ], weekendReg);
    return weekendReg;
}());

//# sourceMappingURL=weekend.component.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__commen_forgot_forgot__ = __webpack_require__(583);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_facebook__ = __webpack_require__(1009);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_homeNew_homeNew__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_navBarService__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_firebase__ = __webpack_require__(1010);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// import { AngularFireAuth } from 'angularfire2/auth';


var Login = /** @class */ (function () {
    function Login(navCtrl, authservice, toastCtrl, fireBase, platform, fb, _navBar, appCtrl) {
        this.navCtrl = navCtrl;
        this.authservice = authservice;
        this.toastCtrl = toastCtrl;
        this.fireBase = fireBase;
        this.platform = platform;
        this.fb = fb;
        this._navBar = _navBar;
        this.appCtrl = appCtrl;
        this.temp = false;
        this.login = true;
        this.spinnerLoad = false;
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        console.log(this.tabBarElement, "login d");
        if (this.tabBarElement) {
            this.tabBarElement.style.display = 'none';
        }
        this.forgot = __WEBPACK_IMPORTED_MODULE_3__commen_forgot_forgot__["a" /* Forgot */];
    }
    Login_1 = Login;
    Login.prototype.ionViewWillEnter = function () {
        // this.mydat = JSON.parse(localStorage.getItem('navbardata'));
        // if(this.mydat){
        //   this.mydat.style.display = 'none';
        // }
        if (this.tabBarElement) {
            this.tabBarElement.style.display = 'none';
        }
    };
    Login.prototype.ngOnInit = function () {
        var _this = this;
        this.colorLog = "#108fd2";
        this.colorSign = "#fff";
        this.fontLog = "#fff";
        this.fontSign = "#108fd2";
        // firebase.auth().getRedirectResult().then(function (result) {
        //   if (result.credential) {
        //     console.log(result);
        //   }
        // }).catch(function (error) {
        //   var errorCode = error.code;
        //   var errorMessage = error.message;
        // });
        __WEBPACK_IMPORTED_MODULE_9_firebase___default.a.auth().onAuthStateChanged(function (user) {
            if (user) {
                console.log(user, "user");
                _this.firebaseLogin(user);
            }
        });
    };
    Login.prototype.ionViewDidLoad = function () {
        this.initializeBackButtonCustomHandler();
    };
    //  ionViewWillEnter() {
    //     this.tabBarElement.style.display = 'none';
    //   }
    Login.prototype.ionViewWillLeave = function () {
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
        //this.tabBarElement.style.display = 'flex';
    };
    Login.prototype.initializeBackButtonCustomHandler = function () {
        this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function (event) {
        }, 101);
    };
    Login.prototype.Submitlogin = function (value) {
        var _this = this;
        if (value.email && value.password) {
            this.spinnerLoad = true;
            var email = value.email.toLowerCase();
            var pass = value.password;
            this.authservice.login(email, pass).subscribe(function (result) {
                _this.spinnerLoad = false;
                if (result)
                    console.log(result, "ites Result");
                if (result.status) {
                    console.log(result.data, "login");
                    localStorage.setItem('userData', JSON.stringify(result.data));
                    localStorage.setItem('googledata', JSON.stringify(true));
                    //this.navCtrl.push(homeNew);
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_home__["a" /* HomePage */]);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: result.message,
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                    _this.spinnerLoad = false;
                }
            }, function (error) {
                _this.spinnerLoad = true;
            });
        }
    };
    Login.prototype.SubmitSignup = function (value) {
        var _this = this;
        if (value.email) {
            var name_1 = value.name;
            var email = value.email.toLowerCase();
            ;
            var pass = value.password;
            this.spinnerLoad = true;
            /* avoid multiple click on login button */
            // this.clicked = true;
            this.authservice.signup(name_1, email, pass).subscribe(function (res) {
                _this.spinnerLoad = false;
                if (res) {
                    console.log(res);
                    if (res.status) {
                        var toast = _this.toastCtrl.create({
                            message: res.message,
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
                        //  document.getElementById("routeLogin").click();
                        _this.navCtrl.push(Login_1);
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: res.message,
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
                    }
                }
            }, function (error) {
                var toast = _this.toastCtrl.create({
                    message: "Somthing went wrong",
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            });
        }
    };
    Login.prototype.onLogin = function () {
        this.login = true;
        this.temp = false;
        this.colorSign = "#fff";
        this.colorLog = "#108fd2";
        this.fontLog = "#fff";
        this.fontSign = "#108fd2";
    };
    Login.prototype.onSignup = function () {
        this.temp = true;
        this.login = false;
        this.colorSign = "#108fd2";
        this.colorLog = "#fff";
        this.fontLog = "#108fd2 ";
        this.fontSign = "#fff";
    };
    Login.prototype.loginWithFacebookNew = function () {
        var _this = this;
        this.fb.login(['email', 'public_profile']).then(function (response) {
            _this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(function (profile) {
                _this.userDatas = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'], providerId: "facebook.com" };
                console.log(_this.userDatas, "user data");
                if (_this.userDatas)
                    _this.facebookService(_this.userDatas);
            });
        });
    };
    Login.prototype.loginWithFacebook = function () {
        try {
            __WEBPACK_IMPORTED_MODULE_9_firebase___default.a.auth().signInWithRedirect(new __WEBPACK_IMPORTED_MODULE_9_firebase___default.a.auth.FacebookAuthProvider);
        }
        catch (error) {
            console.log(error, 'error');
        }
    };
    //   loginWithFacebookNew(){
    //   this.fb.login(['public_profile', 'user_friends', 'email'])
    //   .then((res: FacebookLoginResponse) => 
    //   console.log('Logged into Facebook!', res)
    // )
    //   .catch(e => console.log('Error logging into Facebook', e));
    //   }
    Login.prototype.loginWithGoogle = function () {
        try {
            __WEBPACK_IMPORTED_MODULE_9_firebase___default.a.auth().signInWithRedirect(new __WEBPACK_IMPORTED_MODULE_9_firebase___default.a.auth.GoogleAuthProvider);
        }
        catch (error) {
            console.log(error, 'error');
        }
        // this.googlePlus.login({})
        // .then(res => console.log('done',res))
        // .catch(err => console.error('error',err));
    };
    Login.prototype.logoutFromSocial = function () {
        this.fireBase.auth.signOut();
    };
    Login.prototype.facebookService = function (user) {
        var _this = this;
        var data = {
            email: user.email,
            firstName: user.username,
            lastName: user.first_name,
            loginType: "facebook"
        };
        this.authservice.loginWithFirebase(data).subscribe(function (res) {
            if (res.status) {
                localStorage.setItem('userData', JSON.stringify(res.data));
                // this.navCtrl.push(homeNew);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_homeNew_homeNew__["a" /* homeNew */]);
            }
            else {
                _this.navCtrl.push(Login_1);
            }
        });
    };
    Login.prototype.firebaseLogin = function (user) {
        var _this = this;
        var data;
        if (user.providerData[0].providerId == "google.com") {
            data = {
                email: user.providerData[0].email,
                firstName: user.providerData[0].displayName.split(' ', 2)[0],
                lastName: user.providerData[0].displayName.split(' ', 2)[1],
                loginType: "google"
            };
        }
        this.authservice.loginWithFirebase(data).subscribe(function (res) {
            if (res.status) {
                localStorage.setItem('userData', JSON.stringify(res.data));
                // this.appCtrl.getRootNav().push(homeNew );
                // localStorage.setItem('googledata', JSON.stringify(true));
                // this.navCtrl.push(homeNew);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_homeNew_homeNew__["a" /* homeNew */]);
            }
            else {
                _this.navCtrl.push(Login_1);
            }
        });
    };
    Login = Login_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-Login',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/login/login.html"*/'<ion-content padding  class="bgpic" style=" background-color:#efefef">\n  <ion-grid>\n    <ion-row responsive-sm>\n      <ion-col>\n        <div>\n\n          <img src="assets/img/logo-2.png" alt="logo" style="width: 130px;">\n        </div>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n\n      <ion-col col-6 style="padding-left:0px !important;">\n        <button ion-button class="LogBttn" id="routeLogin" (click)="onLogin()" [ngStyle]="{\'background-color\':  colorLog,\'color\':fontLog}">\n          Login</button>\n        </ion-col>\n\n          <ion-col col-6 >\n        <button ion-button class="signBttn" (click)="onSignup()" [ngStyle]="{\'background-color\':  colorSign,\'color\':fontSign}">Sign Up</button>\n      </ion-col>\n    </ion-row>\n\n    \n  </ion-grid>\n\n\n  <ion-grid >\n\n    <ion-row responsive-sm>\n        <ion-col col-12 style="font-style: italic;color: black;" *ngIf="login">Login with</ion-col>\n\n        <ion-col col-12 style="font-style: italic;color: black;" *ngIf="temp">Sign Up with</ion-col>\n      <ion-col col-6 style="padding-left:0px !important;">\n        <button class="socialBtn loginBtn--facebook" style="text-align: center;" (click)="loginWithFacebookNew()" >\n          <ion-icon name="logo-facebook" style="color: #fff;" style="text-align: center;"></ion-icon>  Facebook\n        </button>\n        </ion-col>  \n\n        <ion-col col-6>\n        <button class="socialBtn loginBtn--google" style="text-align: center;" (click)="loginWithGoogle()" >\n          <ion-icon name="logo-googleplus" style="color:#fff;" style="text-align: center;"></ion-icon>  Google\n        </button>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n  <p style="text-align:center;color:black">or</p>\n  <form *ngIf="login" #loginForm="ngForm" (ngSubmit)="Submitlogin(loginForm.value)" novalidate>\n    <ion-list style="padding:10px;">\n\n      <ion-item>\n        <ion-label>e-mail</ion-label>\n        <ion-input type="text" #email="ngModel" id="email" pattern="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$"\n          name="email" required ngModel>\n          <!-- <ion-input [(ngModel)]="username" name="username" type="text" #usernames="ngModel" spellcheck="false" autocapitalize="off"\n       required> -->\n        </ion-input>\n\n      </ion-item>\n      <div class="error" *ngIf="email?.errors && (email?.touched || email?.dirty)">\n        <p class="error" [hidden]="!email.errors.required"> Please enter your email. </p>\n        <p class="error" [hidden]="!email.errors.pattern"> Please enter valid email. </p>\n      </div>\n    </ion-list>\n    <ion-list style="padding: 10px;">\n      <ion-item>\n        <ion-label>Password</ion-label>\n        <ion-input type="password" class="form-control" #password="ngModel" id="password" name="password" required ngModel>\n        </ion-input>\n      </ion-item>\n      <div class="error" *ngIf="password.errors && (password.touched || password.dirty)">\n        <p class="error" [hidden]="!password.errors.required"> Please enter your password. </p>\n      </div>\n\n    </ion-list>\n    <div class="error" align="center">\n      {{msg}}\n    </div>\n\n    <ion-row responsive-sm>\n\n      <!-- <ion-col>\n       <button ion-button block class="bgcolor"(click)="onSignup()" color="light" block>Signup</button>\n     </ion-col>-->\n\n      <ion-col col-12 style="padding:10px;"> \n        <button ion-button block class="bgcolor" type="submit" [disabled]="!loginForm.form.valid " >Login</button>\n      </ion-col>\n\n    </ion-row>\n\n    <ion-grid>\n      <ion-row responsive-sm>\n        <ion-col col-12>\n          <div text-center>\n            <a style="color: black" [navPush]="forgot">Forgot Your Password</a>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n\n\n\n  <form *ngIf="temp" #signupForm="ngForm" (ngSubmit)="SubmitSignup(signupForm.value)">\n    <ion-grid>\n\n      <ion-row responsive-sm>\n        <ion-col >\n          <ion-list>\n\n            <ion-item >\n              <ion-label style="margin-right:-45px;" fixed>	&#160;Name</ion-label>\n              <ion-input type="text" class="form-control" #firstName="ngModel" pattern="[a-zA-Z][a-zA-Z ]{1,}" name="name" required ngModel></ion-input>\n\n            </ion-item>\n\n            <div class="error" *ngIf="firstName.errors && (firstName.touched || firstName.dirty)">\n              <p class="error" [hidden]="!firstName.errors.pattern"> Please enter valid name. </p>\n\n            </div>\n\n          </ion-list>\n          <ion-list >\n\n\n            <ion-item>\n              <ion-label>e-mail</ion-label>\n              <ion-input type="text" #email="ngModel" class="form-control" pattern="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$"\n                name="email" required ngModel></ion-input>\n\n\n            </ion-item>\n            <div class="error" *ngIf="email.errors && (email.touched)">\n              <p class="error" [hidden]="!email.errors.pattern"> Please enter valid email. </p>\n\n\n            </div>\n          </ion-list>\n          <ion-list>\n            <ion-item>\n              <ion-label>Password</ion-label>\n              <ion-input type="password" #password="ngModel" class="form-control" name="password" required ngModel></ion-input>\n\n\n            </ion-item>\n            <div class="error" *ngIf="password.errors && (password.touched || password.dirty)">\n              <p class="error" [hidden]="!password.errors.required"> Please enter your password. </p>\n            </div>\n\n          </ion-list>\n\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n    <div class="error" align="center">\n      {{msg}}\n    </div>\n\n    <p class="terms" style="color: black;">By Sign Up,you agree to nextfellow <b><a href="https://www.nextfellow.com/terms">\n      Terms of Use</a></b> and <b><a href="https://www.nextfellow.com/privacy">\n        Privacy Policy\n        </a></b></p>\n\n    <div>\n\n      <ion-row responsive-sm>\n\n\n        <ion-col col-12 style="padding:11px;">\n          <button ion-button block class="bgcolor" type="submit" [disabled]="!signupForm.form.valid ">Sign Up</button>\n        </ion-col>\n\n      </ion-row>\n\n\n\n    </div>\n  </form>\n\n\n  \n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/commen/login/login.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_7__services_navBarService__["a" /* naveBarShowSrvice */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_7__services_navBarService__["a" /* naveBarShowSrvice */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], Login);
    return Login;
    var Login_1;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_student_service__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_learn_questions_questions__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_dashboard_home__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, studentService, platform) {
        this.navCtrl = navCtrl;
        this.studentService = studentService;
        this.platform = platform;
        this.showSections = false;
        this.showChapter = false;
        this.walkdis = false;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
        this.userDetailsId = this.userData.userDetailsId;
        this.userId = this.userData.userId;
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.getSubject(this.userId, this.activeUserId);
        this.walkthroug();
        this.tabBarElement.style.display = 'flex';
    }
    ListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement.style.display = 'flex';
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    ListPage.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_dashboard_home__["a" /* HomePage */]);
    };
    ListPage.prototype.walkthroug = function () {
        var _this = this;
        this.studentService.dashboardInfo().subscribe(function (res) {
            if (res.data.exam)
                _this.walkdis = false;
            else
                _this.walkdis = true;
        });
    };
    ListPage.prototype.myShowFunction = function () {
    };
    ListPage.prototype.myHideFunction = function () {
    };
    ListPage.prototype.ViewSections = function (id, index) {
        this.subjectID = id;
        this.selectSub = index;
        this.selectCha = null;
        this.selectchpater = null;
        this.showSections = true;
        this.section = this.subjectList[index].children;
        this.chapter = [];
        this.showChapter = false;
    };
    ListPage.prototype.Viewchapters = function (id, index) {
        this.sectionID = id;
        this.selectCha = index;
        this.showChapter = true;
        this.chapter = this.section[index].children;
    };
    ListPage.prototype.getSubject = function (userId, activeUserId) {
        var _this = this;
        this.studentService.getSubjects(userId, activeUserId).
            subscribe(function (res) {
            _this.subjectList = res.data;
        }, function (error) { return console.log(error); });
    };
    ListPage.prototype.goLearn = function (id, i) {
        this.chapterID = id;
        this.selectchpater = i;
        this.selectSub = null;
        this.selectCha = null;
        this.tabBarElement.style.display = 'none';
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_learn_questions_questions__["a" /* Questions */], { subject: this.subjectID + "/" + this.sectionID + "/" + this.chapterID });
        this.showChapter = false;
        this.showSections = false;
    };
    ListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/list.html"*/'<style>\n\n\n.card-md {\n    margin:-15px !important;\n    border-radius: 2px!important;\n    width: calc(120% - 20px)!important;   \n}\n\n\n</style>\n\n<ion-header>\n    <ion-toolbar>\n          <!-- <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n          </button> -->\n          <ion-title>Learn</ion-title>\n          \n        </ion-toolbar>\n  </ion-header>\n    \n<ion-content padding class="bgpic">\n  \n        <div class="center" *ngIf="!subjectList">\n\n\n                <ion-spinner  ></ion-spinner>\n               \n              </div>\n\n            \n<ion-grid>\n<div *ngIf="subjectList" >\n <ion-row >\n<ion-col col-12>  <p class="head" style="padding:0px!important; margin-bottom: 15px!important;  margin-top: 0px!important">Subjects</p></ion-col>\n            \n  <ion-card  class="cardStyle" style="color:black;"  >\n       \n       <ion-row  >\n            <div class="scroll " style="padding: 34px 0px;\n        background-color: #fff;" >\n            <ion-col col-12   style="padding:0px;"    *ngFor="let subject of subjectList; let i=index;">\n<a  data-id="{{i}}" (click)="ViewSections(subject.id,i)" [ngClass]="(selectSub==i)?\'selected\':\'unselected\'">\n   {{subject.name}}\n\n  </a>\n  </ion-col>\n  </div>\n</ion-row>\n              \n    </ion-card>\n   </ion-row>\n   \n\n    <div  *ngIf="showSections">\n        <ion-row>\n            <ion-col col-12 ><p class="head" style="padding:0px!important; margin-bottom: 15px!important;  margin-top: 23px!important">Section</p></ion-col>\n                \n    <ion-card style="padding: 0px;" class="cardStyle" >\n            <ion-row>\n                    <div class="scroll " style="padding: 34px 0px;\n                background-color: #fff;" >\n                    <ion-col col-12  style="padding:0px;"  *ngFor="let sec of section; let j = index;">\n        <a   data-id="{{j}}" (click)="Viewchapters(sec.id,j)" [ngClass]="(selectCha==j)?\'selected\':\'unselected\'">\n                {{sec.name}}\n      \n          </a>\n          </ion-col>\n          </div>\n    </ion-row>\n       \n      \n      </ion-card>\n      </ion-row>\n    </div>\n\n      <div *ngIf="showChapter">\n         <ion-row >\n             \n            <ion-col col-12><p class="head" style="padding:0px!important; margin-bottom:15px!important;  margin-top: 23px!important">Chapter</p> </ion-col>\n            \n      <ion-card  class="cardStyle">\n          \n              <ion-row>\n                    <div class="scroll " style="padding: 34px 0px;\n                background-color: #fff;" >\n                    <ion-col col-12 style="padding: 0px;" *ngFor="let item of chapter; let j = index;">\n        <a   (click)="goLearn(item.id,j)" [ngClass]="(selectchpater==j)?\'selected\':\'unselected\'">\n                {{item.name}}\n      \n          </a>\n          </ion-col>\n          </div>\n    </ion-row>\n       \n         \n        </ion-card>\n       </ion-row>\n    </div>\n</div>\n</ion-grid>\n<div id="focusItem"></div>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/learn/list.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_student_service__["a" /* StudentService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_student_service__["a" /* StudentService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], ListPage);
    return ListPage;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_publishReplay__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_publishReplay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_publishReplay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





function _window() {
    // return the native window obj
    return window;
}
var StudentService = /** @class */ (function () {
    function StudentService(http) {
        this.http = http;
        this.url = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].backendUrl;
        this.subjects = null;
        var userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = userData._id;
        this.userDetailsId = userData.userDetailsId;
        this.userId = userData.userId;
    }
    /* List all subjects */
    //mock test
    StudentService.prototype.checkMockApplied = function () {
        var data = {
            'activeUserId': this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/check-mock-applied", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService.prototype.getPaymetDetails = function (code) {
        var data = {
            'activeUserId': this.activeUserId,
            'refCode': code
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        console.log(data, 'here');
        return this.http.post(this.url + "/get-payment-details", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService.prototype.applayMock = function (data) {
        data.activeUserId = this.activeUserId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/user-mock", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .do(function (data) {
            console.log('daa kitty', data);
            if (data.status) {
                alert(data.message);
                //  this.__tostr.notify(data.message,'success');
            }
            else
                alert(data.message);
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    Object.defineProperty(StudentService.prototype, "nativeWindow", {
        get: function () {
            return _window();
        },
        enumerable: true,
        configurable: true
    });
    StudentService.prototype.getUserData = function () {
        return localStorage.getItem("userData");
    };
    StudentService.prototype.getSubjects = function (userId, activeUserId) {
        var data = {
            'activeUserId': activeUserId,
            'userId': userId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        if (!this.subjects) {
            this.subjects = this.http.post(this.url + "/getSubjects", data, { headers: headers })
                .map(function (res) { return res.json(); })
                .do(function (data) { return console.log(" "); })
                .publishReplay(1)
                .refCount()
                .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        }
        return this.subjects;
    };
    /* Get first question */
    StudentService.prototype.getQuestion = function (subjectId) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "dir": "",
            "subjectId": subjectId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/getStudyQuestion", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .publishReplay(1)
            .refCount()
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService.prototype.loadLearnIDs = function (subjectId, sectionId, chapterId) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "subjectId": subjectId,
            "sectionId": sectionId,
            "chapterId": chapterId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/load-learn-total-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .publishReplay(1)
            .refCount()
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService.prototype.getLearnQuestion = function (questionId) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "questionId": questionId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/get-learn-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .publishReplay(1)
            .refCount()
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService.prototype.getListQuestion = function (subjectId) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "subjectId": subjectId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/total-questions", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .publishReplay(1)
            .refCount()
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    //get stared api 
    StudentService.prototype.getStredQuestion = function (subjectId, subarray) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "dir": "",
            "subjectId": subjectId,
            "subjectArray": subarray
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/get-staredQuestionsNidhun", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    //end stared api
    StudentService.prototype.getNextQuestion = function (subjectId, questionId) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "dir": "next",
            "questionId": questionId,
            "subjectId": subjectId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/getStudyQuestion", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService.prototype.getPreviousQuestion = function (subjectId, questionId) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "dir": "prev",
            "questionId": questionId,
            "subjectId": subjectId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/getStudyQuestion", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    /* Save the result and option selected by user */
    StudentService.prototype.saveAnswer = function (questionId, answer, result, subjectArray) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "questionId": questionId,
            "answer": answer,
            "result": result,
            "subjectArray": subjectArray
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/saveStudyAnswer", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService.prototype.dashboardInfo = function () {
        var data = {
            'activeUserId': this.activeUserId,
            'userId': this.userId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/frontPageInfos", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    StudentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], StudentService);
    return StudentService;
}());

//# sourceMappingURL=student.service.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_publishReplay__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_publishReplay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_publishReplay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var QuestionService = /** @class */ (function () {
    function QuestionService(http) {
        this.http = http;
        this.url = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].backendUrl;
        this.subjects = null;
        var userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = userData._id;
        this.userDetailsId = userData.userDetailsId;
        this.userId = userData.userId;
    }
    /* List all subjects */
    QuestionService.prototype.createProblem = function (comment, question) {
        var data = {
            comment: comment,
            question: question,
            activeUserId: this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/report-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    QuestionService.prototype.createLastQuestion = function (subjectId, numberOfQus, questionId) {
        var data = {
            subjectId: subjectId,
            questionNumber: numberOfQus,
            activeUserId: this.activeUserId,
            questionId: questionId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/last-question-attended", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    QuestionService.prototype.getLastQuestion = function (subjectId) {
        var data = {
            subjectId: subjectId,
            activeUserId: this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/get-last-attended-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    QuestionService.prototype.learnPerformance = function (activeUserId, subjectId) {
        var data = {
            subjectId: subjectId,
            activeUserId: this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/learn-question-answer-count", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    QuestionService.prototype.totalQuestions = function (subjectId) {
        var data = {
            subjectId: subjectId,
            activeUserId: this.activeUserId
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.url + "/total-number-of-question", data, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    QuestionService.prototype.starQuestion = function (questionid, subjectarray, star) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "questionId": questionid,
            "stared": star,
            "subjectArray": subjectarray
        };
        console.log(data, "to start");
        return this.http.post(this.url + "/save-staredQuestions", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    QuestionService.prototype.getAllStaredQuestion = function (subjectarray) {
        var data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId,
            "subjectArray": subjectarray
        };
        console.log(data, "to start");
        return this.http.post(this.url + "/get-staredQuestions", data)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    QuestionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], QuestionService);
    return QuestionService;
}());

//# sourceMappingURL=question.service.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Note; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_notes_service__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_learn_questions_questions__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_note_dailyNotes_dailynotes__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_dashboard_home__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Note = /** @class */ (function () {
    function Note(navCtrl, navParams, platform, alertCtrl, noteService, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.noteService = noteService;
        this.toastCtrl = toastCtrl;
        this.created = false;
        this.daily = false;
        this.shownGroup = null;
        this.recentDiv = false;
        this.miniNoteDiv = false;
        this.noteArray = [];
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.getParam = navParams.get('parms');
        console.log(" this.getParam ", this.getParam);
    }
    Note.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    Note.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_dashboard_home__["a" /* HomePage */]);
    };
    Note.prototype.ngOnInit = function () {
        this.grid = ["a", "v"];
        // console.log("this.getParam///",this.getParam);
        //if(this.getParam){
        this.create = "daily";
        //console.log("this.create",this.create);
        this.created = false;
        this.create = "daily";
        this.daily = true;
        this.loadMiniNotes();
        this.loadRecentNote();
        this.loadAddedNote();
        // }else{
        //   this.created = true;
        //   this.daily = false;
        //   this.create = "created";
        //   this.loadNotes();
        // }
        //this.loadMiniNotes();
    };
    Note.prototype.loadAddedNote = function () {
        var _this = this;
        this.noteService.fetchAllMiniNotes().subscribe(function (res) {
            if (res) {
                if (res.status) {
                    _this.miniNoteDiv = true;
                    console.log("res mininotes", res);
                    _this.noteArray = res.data.notes.sort(function (a, b) { return new Date(b.activeDate).getTime() - new Date(a.activeDate).getTime(); });
                    console.log("noteArray", _this.noteArray);
                }
            }
        });
    };
    Note.prototype.saveRecentNote = function (id, mode) {
        console.log("idkkkkkkkkkk", id, mode);
        this.noteService.saveRecentNote(id, mode).subscribe(function (res) {
            if (res) {
                console.log("recent note save ", res);
            }
        });
    };
    Note.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    Note.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    Note.prototype.loadRecentNote = function () {
        var _this = this;
        this.noteService.loadRecentNote().subscribe(function (res) {
            console.log("recent note////// ", res);
            if (res.data) {
                _this.recentDiv = true;
                _this.recentNotes = res.data;
            }
            else {
                _this.recentDiv = false;
                console.log("this.recentDiv", _this.recentDiv);
            }
        });
    };
    Note.prototype.loadMiniNotes = function () {
        var _this = this;
        this.noteService.fetchAllMiniNotes().subscribe(function (res) {
            if (res) {
                if (res.status) {
                    _this.miniNoteDiv = true;
                    console.log("res mininotes", res);
                    // this.noteArray = res.data.notes.sort((a, b) => new Date(b.activeDate).getTime() - new Date(a.activeDate).getTime());
                    //console.log("noteArray",this.noteArray);
                    if (res.data.notes.length > 0) {
                        var subjectNotes = [];
                        _this.miniNotes = res.data.notes;
                        _this.registrationId = res.data.registrationId;
                        var groups = {};
                        for (var i = 0; i < _this.miniNotes.length; i++) {
                            var groupName = _this.miniNotes[i].subjectArray[0].name;
                            if (!groups[groupName]) {
                                groups[groupName] = [];
                            }
                            groups[groupName].push(_this.miniNotes[i]);
                        }
                        _this.miniNotes = [];
                        for (var groupName in groups) {
                            console.log("gropppppppppppinsidepppppp", groups);
                            _this.miniNotes.push({ subjectName: groupName, noteTitle: groups[groupName] });
                        }
                        console.log("this.miniNotesthis.miniNotes...//", _this.miniNotes);
                        console.log("this.miniNotes", _this.miniNotes[0].noteTitle);
                    }
                }
                else {
                    _this.miniNoteDiv = false;
                    var toast = _this.toastCtrl.create({
                        message: 'Oh! You have no Notes!',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'OOPS! Something went wrong',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        });
    };
    Note.prototype.loadNotes = function () {
        var _this = this;
        var loadMore = false;
        this.noteService.fetchAllNotes(loadMore).subscribe(function (res) {
            if (res) {
                if (res.data.notes.length > 0) {
                    _this.allNotesData = res.data.notes;
                    console.log("  this.pageLoadNote", _this.allNotesData);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'Oh! You have no Notes!',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'OOPS! Something went wrong',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        });
    };
    Note.prototype.ionViewLoaded = function () {
        var rowNum = 0; //counter to iterate over the rows in the grid
        for (var i = 0; i < this.images.length; i += 2) {
            this.grid[rowNum] = Array(2); //declare two elements per row
            if (this.images[i]) {
                this.grid[rowNum][0] = this.images[i]; //insert image
            }
            if (this.images[i + 1]) {
                this.grid[rowNum][1] = this.images[i + 1];
            }
            rowNum++; //go on to the next row
        }
    };
    Note.prototype.segmentChanged = function (event) {
        if (event._value == "created") {
            this.created = true;
            this.loadNotes();
            this.create = "created";
            this.daily = false;
            this.loadNotes();
        }
        else if (event._value == "daily") {
            this.loadMiniNotes();
            this.created = false;
            this.create = "daily";
            this.daily = true;
            console.log("event", event);
        }
    };
    Note.prototype.deleteNote = function (index) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure want to delete this note?',
            buttons: [
                {
                    text: 'Yes',
                    handler: function () {
                        var noteId = _this.allNotesData[index]._id;
                        _this.noteService.deleteNote(noteId).subscribe(function (res) {
                            var toast = _this.toastCtrl.create({
                                message: 'Note deleted.',
                                duration: 3000,
                                position: 'bottom'
                            });
                            toast.present();
                        }, function (error) {
                            var toast = _this.toastCtrl.create({
                                message: 'OOPS! Something went wrong',
                                duration: 3000,
                                position: 'bottom'
                            });
                            toast.present();
                        });
                        _this.allNotesData.splice(index, 1);
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        console.log('No');
                    }
                }
            ]
        });
        alert.present();
    };
    Note.prototype.goLastStudy = function (index) {
        var loadMore = false;
        var noteId = this.allNotesData[index]._id;
        var len = this.allNotesData.length;
        for (var i = 0; i < len; i += 1) {
            if (this.allNotesData[i]["_id"] === noteId) {
                this.chapterId = this.allNotesData[i].subjectArray[2].id;
                this.sectionId = this.allNotesData[i].subjectArray[1].id;
                this.subjectId = this.allNotesData[i].subjectArray[0].id;
                this.questionId = this.allNotesData[i].questionId;
            }
        }
        this.totalIds = this.subjectId + '/' + this.sectionId + '/' + this.chapterId + '/' + this.questionId;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_learn_questions_questions__["a" /* Questions */], { subject: this.totalIds });
    };
    Note.prototype.goDailyNotes = function (index, demoflag, mode) {
        console.log("demoflag,,,,,,,,,", demoflag);
        this.tabBarElement.style.display = 'none';
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_note_dailyNotes_dailynotes__["a" /* DailyNote */], { noteId: index, RegId: this.registrationId, demoflag: demoflag, mode: mode });
    };
    Note = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-note',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/note/note.html"*/'<ion-header>\n  <ion-navbar style="color:#108fd2;">\n    <!-- <button ion-button menuToggle >\n      <ion-icon name="menu"></ion-icon>\n    </button> -->\n    <ion-title>Notes </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="bgpic">\n  <ion-segment [(ngModel)]="create" color="primary" (ionChange)="segmentChanged($event)">\n   \n    <ion-segment-button value="daily">\n      Daily Note\n    </ion-segment-button>\n    <ion-segment-button value="created">\n      Created Note\n    </ion-segment-button>\n  </ion-segment>\n  <!-- <div class="center" *ngIf="!allNotesData || !miniNotes">\n\n\n    <ion-spinner></ion-spinner>\n\n  </div> -->\n\n  <!--<ion-card *ngFor="let item of allNotesData; let i= index;" style="min-height: 21%;">\n<ion-card-header  style="background-color: #f8f8f8;\nfont-size: 19px;text-align: center;">\n  <a (click)="goLastStudy(i)" style="color: #108fd2;"> Note {{i+1}}: {{item.subjectArray[2].name|slice:0:15}}... </a>\n\n  <button ion-button  (click)="deleteNote(i)" style="float: right;\n  /* color: #108fd2; */\n  background-color: #e23d3d;\n  font-size: 15px;"><ion-icon name="ios-trash" ></ion-icon></button>  \n\n  \n    \n\n</ion-card-header>\n<ion-card-content style=" padding-top: 13px;">\n  {{item.noteContent}}\n\n</ion-card-content>\n</ion-card>-->\n  <ion-grid *ngIf="created">\n      <ion-card *ngFor="let item of allNotesData; let i= index;"  (click)="goLastStudy(i)">\n          <ion-card-header style="background-color: #F9F59F;font-size: 19px;font-weight: bold;">\n              <a (click)="goLastStudy(i)" style="color: #a94442;font-size: 19px;\n              font-weight: bold;"> Note {{i+1}}: {{item.subjectArray[2].name|slice:0:15}}... </a>\n              <button ion-fab class="pop-in" color="danger" (click)="deleteNote(i)" style="float: right;\n              width: 32px;\n              height: 32px;">\n                <ion-icon name="close"></ion-icon>\n              </button>\n            \n          </ion-card-header>\n          <ion-card-content style="background-color: #fff; height: 58px;\n          padding: 9px; border-style: solid; border-color:#faf8c0;\n          border-width: 3px;">\n               {{item.noteContent}}\n          </ion-card-content>\n          </ion-card>\n          </ion-grid>\n   \n\n  <ion-grid *ngIf="daily">\n    <div >\n      <ion-row >\n        <ion-col col-12> <h5>Recently Added Notes</h5></ion-col>\n        <ion-col col-12 *ngFor="let item of noteArray | slice:0:2; let i= index;">\n    <ion-card  (click)="toggleGroup(item)"  style="background-color: #faf8c0;height: 54px;" >\n        <ion-card-header >\n         \n          <ion-col col-12>\n      <a  style="color: #108fd2;font-size: 16px;\n        font-weight: bold;">{{item.noteTitle |slice:0:25}}... </a>\n        <ion-icon class="clr"   name="eye" (click)="goDailyNotes(item._id,item.demoFlag,\'added\')"  ></ion-icon>\n    \n      </ion-col>\n     \n      \n        </ion-card-header>\n    \n    </ion-card>\n    \n    \n    </ion-col>\n    </ion-row>\n    \n    </div>\n    \n    \n\n<div  *ngIf="recentDiv">\n    <ion-row >\n      <ion-col col-12> <h5 style="padding-top: 13px;">Recently Viewed Notes</h5></ion-col>\n      <ion-col col-12 *ngFor="let item of recentNotes | slice:0:2;  let i= index;">\n  <ion-card  (click)="toggleGroup(item)"  style="background-color: #faf8c0;height: 54px;" >\n      <ion-card-header >\n       \n        <ion-col col-12>\n    <a  style="color: #108fd2;font-size: 16px;\n      font-weight: bold;">{{item.noteTitle |slice:0:25}}... </a>\n      <ion-icon class="clr"   name="eye" (click)="goDailyNotes(item.noteId,item.demoFlag,\'recent\')"  ></ion-icon>\n\n    </ion-col>\n   \n    \n      </ion-card-header>\n\n</ion-card>\n\n\n</ion-col>\n</ion-row>\n\n</div>\n\n\n\n\n\n\n\n      <ion-row *ngIf="miniNoteDiv">\n          <ion-col col-12> <h5 style="padding-top: 13px;">Subjectwise Notes</h5></ion-col>\n          <ion-col col-12 *ngFor="let item of miniNotes; let i= index;">\n      <ion-card  (click)="toggleGroup(item)"  >\n          <ion-card-header style="background-color: #faf8c0;font-size: 16px;font-weight: bold;">\n              <ion-col col-12>\n        <a  style="color: #108fd2;font-size: 16px;\n          font-weight: bold;">{{item.subjectName}} </a>\n        \n          <ion-icon *ngIf="!isGroupShown(item)" name="add" style="color: #108fd2;font-size: 16px;\n          float: right;\n          font-weight: 700;"></ion-icon>\n          <ion-icon  *ngIf="isGroupShown(item)" name="remove" style="color: #108fd2;font-size: 16px;\n          float: right;\n          font-weight: 700;"></ion-icon>\n          </ion-col>\n          </ion-card-header>\n<div *ngIf="isGroupShown(item)"  style="background-color: #fff;height: auto;\npadding: 9px; border-style: solid; border-color:#fff;\nborder-width: 1px;">\n<!-- <ion-scroll scrollY="true" style="height: 183px;"> -->\n<ion-card  *ngFor="let items of item.noteTitle; let i= index;">\n    <ion-card-header>\n  <ion-col col-9>\n     <span style="color:#a94442">  {{items.noteTitle | slice:0:25}}...</span>\n  </ion-col>\n  <ion-col col-3>\n    <ion-icon class="clr"   name="eye" (click)="goDailyNotes(items._id,items.demoFlag,\'daily\');saveRecentNote(items._id,\'save\')"  ></ion-icon>\n\n  </ion-col>\n  </ion-card-header>\n    </ion-card>\n    <!-- </ion-scroll> -->\n</div>\n    </ion-card>\n   \n \n</ion-col>\n</ion-row>\n\n\n\n\n\n\n\n</ion-grid> \n  \n  \n\n\n</ion-content>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/note/note.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_notes_service__["a" /* NotesService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_notes_service__["a" /* NotesService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */]])
    ], Note);
    return Note;
}());

//# sourceMappingURL=note.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return writeExam; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_exam_exam__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_exam_examNew_examComponent__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_exam_exam_room_details_details__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var writeExam = /** @class */ (function () {
    function writeExam(navCtrl, events, alertCtrl, platform, __auth, navParams, examService) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.__auth = __auth;
        this.navParams = navParams;
        this.examService = examService;
        this.showNav = true;
        this.nextButtonLabel = "Next";
        this.noNextQuestion = false;
        this.qstAnswerd = false;
        this.updateFlag = false;
        this.url = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].backendUrl + '/images/';
        this.viewResult = false;
        this.testN = false;
        this.timeExamObject = [];
        this.tempQuestionPallete = [];
        this.timer = false;
        this.buttonName = false;
        this.paused = false;
        this.resumed = false;
        this.buttonToggle = false;
        this.paletteStatus = [];
        this.userExamId = navParams.get('subject');
        this.events.publish('user:created', true, Date.now());
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    }
    writeExam.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    writeExam.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    writeExam.prototype.ionViewWillLeave = function () {
        this.tabBarElement.style.display = 'flex';
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
    };
    writeExam.prototype.ngOnInit = function () {
        var _this = this;
        this.buttonName = false;
        this.pauseTitle = "Pause Timer";
        this.currentPaletteId = 0;
        this.examService.loadExam(this.userExamId).subscribe(function (res) {
            console.log("questionPalette", res);
            if (res.data.userExamObj) {
                _this.questionPalette = res.data.userExamObj.questionPalette;
            }
            console.log(_this.questionPalette, "data here");
            _this.tempQuestionPallete = _this.questionPalette; // this is temp pallete array for sorting(marked,answered,not answered)         
            if (res.data) {
                _this.examConfigData = res.data;
                _this.examId = res.data.userExamObj.examId;
                _this.currentPaletteId = 0;
                _this.timeFlag = _this.examConfigData.userExamObj.timeFlag;
                if (_this.timeFlag) {
                    _this.timerObj = _this.examConfigData.userExamObj.timeObj;
                    _this.timeExamObject = JSON.parse(localStorage.getItem('timeExamObject'));
                    if (_this.timeExamObject) {
                        _this.timeExamObject.map(function (item, i) {
                            if (item.userExamId == _this.userExamId) {
                                _this.timerIndex = i;
                                _this.previousTimer = _this.timeExamObject[_this.timerIndex];
                            }
                            ;
                        });
                    }
                    else {
                        _this.timeExamObject = [];
                        _this.previousTimer = null;
                    }
                    if (_this.previousTimer == null || !_this.previousTimer) {
                        _this.time = new Date(2017, 1, 1, _this.timerObj.hour, _this.timerObj.minute, 0);
                        _this.timer = true;
                    }
                    else {
                        _this.time = new Date(2017, 1, 1, _this.previousTimer.hours, _this.previousTimer.minutes, _this.previousTimer.seconds);
                        _this.timer = true;
                    }
                    _this._timerTick();
                }
                _this.questionId = _this.questionPalette[_this.currentPaletteId]._id;
                _this.examService.getExamQuestions(_this.userExamId, _this.questionId).subscribe(function (res) {
                    _this.playTime = _this.time;
                    _this.RealexamData = res;
                    _this.answerData = res.data.answer;
                    if (res.data.answer) {
                        _this.answered = true;
                        _this.answeredPalette = true;
                        _this.answeredPaletteId = _this.currentPaletteId;
                        _this.clickedItem = res.data.answer.answer;
                    }
                    _this.questionData = res.data.question;
                    _this.options = JSON.parse(res.data.question.options);
                });
            }
        });
    };
    writeExam.prototype._timerTick = function () {
        var _this = this;
        this.time.setSeconds(this.time.getSeconds(), -1);
        var timerObject = {
            hours: this.time.getHours(),
            minutes: this.time.getMinutes(),
            seconds: this.time.getSeconds(),
            userExamId: this.userExamId
        };
        if (this.timerIndex)
            this.timeExamObject[this.timerIndex] = timerObject;
        else {
            this.timeExamObject.push(timerObject);
            this.timeExamObject.map(function (item, i) {
                if (item.userExamId == _this.userExamId)
                    _this.timerIndex = i;
            });
        }
        localStorage.setItem('timeExamObject', JSON.stringify(this.timeExamObject));
        if (timerObject.hours === 0 && timerObject.minutes === 5 && timerObject.seconds === 0) {
            this.__auth.notificationInfo('5 Minutes Remaining');
        }
        if (timerObject.hours === 0 && timerObject.minutes === 0 && timerObject.seconds === 0) {
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
            this.statusCount();
            this.finishgrandExam();
        }
        this.x = setTimeout(function () { return _this._timerTick(); }, 1000);
    };
    writeExam.prototype.playPause = function () {
        var _this = this;
        if (this.buttonToggle) {
            this.buttonName = false;
            this.pauseTitle = "Pause Timer";
            if (this.pauseTime) {
                this.playTime = this.pauseTime;
            }
            else {
                this.playTime = this.time;
            }
            this.paused = false;
            this.resumed = true;
            var examurl = "/exam/write/" + this.userExamId;
            this.playTime.setSeconds(this.playTime.getSeconds(), -1);
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 5 && this.playTime.getSeconds() === 0) {
                this.__auth.notificationInfo('5 Minutes Remaining');
            }
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 0 && this.playTime.getSeconds() === 0) {
                window.clearInterval(this.x);
                localStorage.removeItem('timerObject');
                this.finishgrandExam();
                this.statusCount();
            }
            var x = setTimeout(function () { return _this._timerTick(); }, 1000);
        }
        else {
            this.buttonName = true;
            this.playTitle = "Resume Timer";
            this.pauseTime = this.time;
            this.resumed = false;
            this.paused = true;
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
        }
        this.buttonToggle = !this.buttonToggle;
    };
    writeExam.prototype.saveAnswer = function (answer, index) {
        var _this = this;
        if (!this.paused) {
            this.clickedItem = answer;
            var correctAnswer = this.questionData.answer;
            this.questionId = this.questionPalette[this.currentPaletteId]._id;
            if (this.answerData === null) {
                this.updateFlag = false;
            }
            else {
                this.updateFlag = true;
                this.answered = true;
                this.userExamAnwserId = this.RealexamData.data.answer._id;
                this.clickedItem = this.RealexamData.data.answer.answer;
            }
            var subjectArray = this.questionData.subjectArray;
            var result = void 0;
            if (answer === correctAnswer) {
                result = "correct";
            }
            else {
                result = "wrong";
            }
            this.qstAnswerd = true;
            console.log("questionanswerrrs", this.qstAnswerd);
            if (this.updateFlag === false) {
                this.examService.saveExamAnswer(this.currentPaletteId, this.userExamId, this.examId, this.questionId, answer, result, this.updateFlag, subjectArray).subscribe(function (res) {
                    _this.answeredPalette = true;
                    _this.answeredPaletteId = index;
                    _this.answered = true;
                    _this.updateFlag = true;
                    _this.clickedItem = answer;
                    var data = {
                        '_id': _this.questionId,
                        'status': 'answered'
                    };
                    _this.questionPalette[_this.currentPaletteId]._id = _this.questionId;
                    _this.questionPalette[_this.currentPaletteId].status = "answered";
                    console.log(_this.questionPalette[_this.currentPaletteId], "this.questionPalette[this.currentPaletteId]");
                    _this.userExamAnwserId = res.data.userExamAnswerId;
                });
            }
            else {
                this.examService.updateExamAnswer(this.currentPaletteId, answer, result, this.userExamAnwserId, this.userExamId, this.examId, this.questionId).subscribe(function (res) {
                    _this.clickedItem = answer;
                    _this.answered = true;
                    var data = {
                        '_id': _this.questionId,
                        'status': 'answered'
                    };
                    _this.questionPalette[_this.currentPaletteId]._id = _this.questionId;
                    _this.questionPalette[_this.currentPaletteId].status = "answered";
                    console.log(_this.questionPalette[_this.currentPaletteId], "this.questionPalette[this.currentPaletteId]");
                });
            }
        }
        else {
            console.log("user paused");
        }
    };
    writeExam.prototype.next = function () {
        var _this = this;
        window.scrollTo(0, 0);
        this.currentPaletteId = this.currentPaletteId + 1;
        this.questionId = this.questionPalette[this.currentPaletteId]._id;
        this.examService.getExamQuestions(this.userExamId, this.questionId).subscribe(function (res) {
            _this.RealexamData = res;
            _this.answerData = res.data.answer;
            if (_this.questionPalette.length === (_this.currentPaletteId + 1)) {
                _this.noNextQuestion = true;
                _this.nextButtonLabel = "Submit";
            }
            else {
                _this.noNextQuestion = false;
            }
            if (_this.answerData != null) {
                _this.answered = true;
                _this.clickedItem = _this.RealexamData.data.answer.answer;
            }
            else {
                _this.answered = false;
            }
            _this.questionData = res.data.question;
            _this.options = JSON.parse(res.data.question.options);
        });
    };
    writeExam.prototype.previous = function () {
        var _this = this;
        window.scrollTo(0, 0);
        this.currentPaletteId = this.currentPaletteId - 1;
        this.questionId = this.questionPalette[this.currentPaletteId]._id;
        this.examService.getExamQuestions(this.userExamId, this.questionId).subscribe(function (res) {
            _this.RealexamData = res;
            _this.answerData = res.data.answer;
            if (_this.answerData != null) {
                _this.answered = true;
                _this.clickedItem = _this.RealexamData.data.answer.answer;
            }
            else {
                _this.answered = false;
            }
            if (_this.questionPalette.length === (_this.currentPaletteId + 1)) {
                _this.noNextQuestion = true;
            }
            else {
                _this.noNextQuestion = false;
                _this.nextButtonLabel = "Next";
            }
            _this.questionData = res.data.question;
            _this.options = JSON.parse(res.data.question.options);
        });
    };
    writeExam.prototype.mark = function (status) {
        var _this = this;
        console.log(status);
        this.examService.markExam(this.userExamId, this.questionId, status).subscribe(function (res) {
            if (!res.status)
                _this.__auth.notificationInfo('OOPS! Something went wrong.');
            else {
                if (status == 'marked') {
                    _this.questionPalette[_this.currentPaletteId].marked = status;
                }
                else {
                    if (_this.answerData != null) {
                        _this.questionPalette[_this.currentPaletteId].status = 'answered';
                    }
                    else {
                        _this.questionPalette[_this.currentPaletteId].status = '';
                    }
                    _this.questionPalette[_this.currentPaletteId].marked = status;
                }
            }
        });
    };
    writeExam.prototype.goToQuestion = function (index) {
        var _this = this;
        if (!this.paused) {
            this.questionId = this.questionPalette[index]._id;
            this.examService.getGrandExamQuestions(this.userExamId, this.questionId).subscribe(function (res) {
                _this.RealexamData = res;
                _this.answerData = res.data.answer;
                if (res.data.answer) {
                    _this.answered = true;
                    _this.answeredPalette = true;
                    _this.answeredPaletteId = _this.currentPaletteId;
                    _this.clickedItem = res.data.answer.answer;
                }
                else {
                    _this.clickedItem = "";
                }
                _this.questionData = res.data.question;
                // this.currentPaletteId = index;
                _this.currentPaletteId = index;
                _this.options = JSON.parse(res.data.question.options);
                if (_this.questionPalette.length === (_this.currentPaletteId + 1)) {
                    _this.noNextQuestion = true;
                }
                else {
                    _this.noNextQuestion = false;
                }
            });
        }
        else {
            console.log("user paused");
        }
    };
    writeExam.prototype.finishExamModal = function () {
        if (!this.qstAnswerd) {
            this.__auth.notificationInfo('Please attend one question');
        }
        else {
            jQuery("#submitExamFirst").modal("show");
        }
    };
    writeExam.prototype.finishExam = function (userId) {
        var _this = this;
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
        if (!this.qstAnswerd) {
            this.__auth.notificationInfo('Please attend one question');
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Confirm submit',
                message: 'Do you want to submit exam?',
                buttons: [
                    {
                        text: 'Yes',
                        role: 'cancel',
                        handler: function () {
                            _this.examService.finishExam(_this.userExamId).subscribe(function (res) {
                                if (res.status) {
                                    var id = _this.examId + '/' + _this.userExamId;
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: id });
                                }
                                else {
                                    _this.__auth.notificationInfo('OOPS! Something went wrong.');
                                }
                            });
                        }
                    },
                    {
                        text: 'No',
                        handler: function () {
                        }
                    }
                ]
            });
            alert_1.present();
        }
    };
    writeExam.prototype.finishgrandExam = function () {
        var _this = this;
        this.examService.finishExam(this.userExamId).subscribe(function (res) {
            if (res.status) {
                window.clearInterval(_this.x);
                localStorage.removeItem('timerObject');
                var id = _this.examId + '/' + _this.userExamId;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: id });
            }
            else {
                _this.__auth.notificationInfo('OOPS! Something went wrong.');
            }
        });
    };
    writeExam.prototype.filter = function (type) {
        var _this = this;
        var mapArray = function (type) {
            var tempArray = [];
            tempArray = (_this.tempQuestionPallete).filter(function (item) {
                if (item.status == type)
                    return item;
            });
            if (tempArray.length)
                _this.questionPalette = tempArray, _this.goToQuestion(0), _this.currentPaletteId = 0;
            else
                _this.__auth.notificationInfo('No questions.');
        };
        var markedArray = function (type) {
            var tempArray = [];
            tempArray = (_this.tempQuestionPallete).filter(function (item) {
                if (item.marked == type)
                    return item;
            });
            if (tempArray.length)
                _this.questionPalette = tempArray, _this.goToQuestion(0), _this.currentPaletteId = 0;
            else
                _this.__auth.notificationInfo('No questions.');
        };
        if (type == 'm') {
            markedArray('marked');
        }
        else if (type == 'a') {
            mapArray('answered');
        }
        else if (type == 'na') {
            mapArray("");
        }
        else {
            this.questionPalette = this.tempQuestionPallete;
            this.currentPaletteId = 0;
            this.goToQuestion(0);
        }
    };
    writeExam.prototype.statusCount = function () {
        var marked = 0, unanswered = 0, answered = 0;
        this.tempQuestionPallete.filter(function (item) {
            if (item.status == 'answered')
                answered++;
            else
                unanswered++;
        });
        this.paletteStatus = [answered, marked, unanswered];
    };
    writeExam.prototype.back = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam__["a" /* Exam */]);
    };
    writeExam.prototype.findImage = function (i) {
        if (this.questionData[i].question_image) {
            this.imageFlag = true;
            this.imageUrl = this.url + this.questionData.question_image;
            console.log("imageeee", this.imageUrl);
        }
        else {
            this.imageFlag = false;
        }
    };
    writeExam.prototype.toggleDetails = function (item) {
        if (this.isGroupShown(item)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = item;
        }
    };
    writeExam.prototype.isGroupShown = function (item) {
        return this.shownGroup === item;
    };
    writeExam = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-write',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/createexam/write-exam/write-exam.html"*/'<style>\n    .card-content-md {\n        padding-left: -1px;\n    }\n\n    .card-md {\n        margin-left: -4px !important;\n        border-radius: 2px!important;\n        width: calc(100% - -8px)!important;\n    }\n\n    page-write .optionStyle {\n        border-bottom: unset!important;\n    }\n</style>\n  <ion-header #head>\n     \n  \n        <ion-toolbar>\n            <ion-navbar>\n                <ion-buttons left>\n           \n        \n                </ion-buttons>\n              </ion-navbar>\n          \n    \n    \n        </ion-toolbar>\n      </ion-header>\n\n<ion-content padding class="bgpic">\n    <!-- <ion-grid style="background-color:#b4aeae;">\n                 <ion-row>\n                     <ion-col col-6>\n                             <ion-icon name="arrow-back" style="font-size: 24px;" (click)="back()"></ion-icon>\n         \n                             &nbsp; &nbsp;  <ion-icon name="star" style="color:#ff6b01;font-size: 24px;">     </ion-icon>\n                         \n                     </ion-col>\n                     <ion-col col-2 class="align-icon">\n                         <ion-icon name="share-alt" style="font-size: 24px;"></ion-icon>\n                     </ion-col>\n                     <ion-col col-2 class="align-icon">\n                         <ion-icon name="switch" style="font-size: 24px;"></ion-icon>\n                     </ion-col>\n           s          <ion-col col-2 class="align-icon">\n                         <ion-icon name="apps" style="font-size: 24px;"></ion-icon>\n                     </ion-col>\n                 </ion-row>\n             </ion-grid> -->\n    <ion-card style="margin-top:0px;" *ngIf="questionData">\n\n        <div *ngIf="questionData">\n\n            <ion-row>\n\n                <ion-col col-12 class="count" style=" background-color:#cecece; padding: 8px!important; border-bottom:1px solid #dcd9d9;"\n                    *ngIf="timeFlag">\n                    <div class="count" style="float:left; padding-top:5px;">\n                        <div style="font-size: 15px;color: #e13838;">\n                            <span>{{ time.getHours() }}</span> :\n                            <span>{{ time.getMinutes() }}</span> :\n                            <span>{{ time.getSeconds() }}</span>\n                        </div>\n                    </div>\n\n                    <div style=" float: right!important;">\n                        <!-- <button ion-button class="playBtn" title={{pauseTitle}}  aria-hidden="true" (click)="playPause()" *ngIf="!buttonName"><ion-icon name="pause"></ion-icon>\n                                                </button> -->\n                        <ion-icon name="pause" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="!buttonName"></ion-icon>\n\n                        <!-- <button ion-button  class="playBtn" title={{playTitle}}  aria-hidden="true"  (click)="playPause()" *ngIf="buttonName"><ion-icon name="play"></ion-icon></button>   -->\n                        <ion-icon name="play" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="buttonName"></ion-icon>\n\n                    </div>\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n        <ion-card-content style=" padding: 0px 0px 0px 0px; padding: 13px 3px;" style="background-color:#FFFEEE;margin-left:-13px!important; margin-right:-13px!important;">\n\n            <ion-item-group>\n                <ion-item-divider text-wrap class="questionHead" style="padding: 0px;">\n                    <ion-grid style="padding-bottom:0px !important;" style="background-color:#FFFEEE;;; ">\n                        <ion-row style="border-bottom: 2px solid #f0f0f0;">\n                            <ion-col col-2 class="fntsze" style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:11px; padding-left:0px;">\n                                <span [innerHtml]="currentPaletteId+1">/</span>.\n                            </ion-col>\n                            <ion-col col-10 class="fntsze" style=" padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n                                <span [innerHtml]="questionData.question"></span>\n                                <!-- <span class="question"></span>&nbsp;&nbsp;<span class="question"></span> -->\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                    \n                     </ion-item-divider>\n                     <ion-list *ngFor="let item of options ;let i= index;">\n                         <ion-item text-wrap   class="optionStyle" style="font-weight: unset!important;" \n                         disabled="answered" (click)="saveAnswer(item.id,i)" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important; ">\n                         <div [ngClass]="{\'selected\': answered === true && clickedItem === item.id}">\n                                 <ion-grid style="padding:0px!important;">\n                                     <ion-row>\n                                 <ion-col col-2 style=" padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;; ">{{item.id}}.</ion-col>\n                                 <ion-col col-10  style=" font-size:18px!important;padding-left:2!important;margin-left: 0px;line-height:22px;line-height:26px;"><span> {{item.text}}</span></ion-col>\n                             </ion-row>\n                             </ion-grid>\n         \n                         </div>\n                         </ion-item>\n                         </ion-list>\n                        \n                     </ion-item-group>\n                    \n                     </ion-card-content>\n                     <ion-row style="margin-top:10px;"  >\n                        <ion-col col-4 style="text-align:center;">\n                                <button float-center ion-button color="#108fd2" style="background-color:#108fd2; " icon-left (click)="previous()" [disabled]="currentPaletteId<=0 || paused">Previous</button>\n        \n                        </ion-col>\n                        <ion-col col-4 style="text-align:center;">\n                                <!-- <button class="btn btn-success col-md-3 btnSize exampages " (click)="mark((questionPalette[currentPaletteId].status == \'marked\') ? \'unmarked\': \'marked\')">{{(questionPalette[currentPaletteId].status == \'marked\') ? \'Unmark\': \'Mark\'}}</button> -->\n                                \n                                <button  float-center ion-button color="#108fd2" icon-left style=" text-align: center; background-color:#108fd2;  padding: 0px 18px 0px 18px;margin-left:23px;" (click)="mark((questionPalette[currentPaletteId].marked == \'marked\') ? \'unmarked\': \'marked\')" [disabled]=paused>{{(questionPalette[currentPaletteId].marked == \'marked\') ? \'Unmark\': \'Mark\'}}</button>  \n                                <!-- <button  float-center ion-button color="#108fd2" icon-left style=" text-align: center; background-color:#108fd2;margin-left:22px; " (click)="mark((questionPalette[currentPaletteId].status == \'marked\') ? \'unmarked\': \'marked\')" [disabled]=paused>{{(questionPalette[currentPaletteId].status == \'marked\') ? \'Unmark\': \'Mark\'}}</button> -->\n        \n                        </ion-col>\n                        <ion-col col-4 style="text-align:center;">\n                            <button float-right ion-button style="background-color:#108fd2;color:white;margin-right:4px; " (click)="!noNextQuestion && next();noNextQuestion && finishExam(\'\')"\n                                [disabled]=paused>{{(currentPaletteId+1>=questionPalette.length)?\'Submit\':\'Next\'}}</button>\n            \n            \n                        </ion-col>\n                      </ion-row>\n                     </ion-card>\n                 \n         \n             <!-- <div class="question-wrap" style="font-size: 17px;">\n                     <div style="display: inline-flex">\n                         <span [innerHtml]="currentPaletteId+1"></span>.</div>\n                     <div style="display: inline-flex">\n                         <span class="question" [innerHtml]="questionData"> </span>\n                     </div>\n                 </div>\n                 <div class="answer-wrap">\n                         <div class="answer-main">\n                             <table class="table  table-bordered">\n                                 <tbody>\n                                     <tr *ngFor="let item of options ;let i= index;">\n                                         <td [ngClass]="{\'selected\': answered === true && clickedItem === item.id}" (click)="saveAnswer(item.id,i)">\n                                             {{item.id}}.\n                                             <span style="padding-left:10px">{{item.text}}\n                                             </span>\n                                         </td>\n                                     </tr>\n                                 </tbody>\n                             </table>\n                         </div> -->\n\n    <!-- <div style="display:flex; justify-content: space-between;margin-bottom: 20px;">\n                                \n                         </div>\n                      -->\n    <!-- <ion-card style="margin-top:0px;">\n                     <ion-card-content style="margin-top:0px;">\n                             <ion-item-group >\n                                     <ion-item-divider color="light">\n                                             <span class="question" [innerHtml]="currentPaletteId+1">/</span><span class="question" [innerHtml]="currentPaletteId+1" [innerHtml]="questionData"></span>\n                                             <span class="question"></span>&nbsp;&nbsp;<span class="question"></span>\n                                         </ion-item-divider>\n                                         <ion-list  *ngFor="let item of options ;let i= index;" [ngClass]="{\'selected\': answered === true && clickedItem === item.id}"\n                                         disabled="answered" (click)="saveAnswer(item.id,i)">\n                                                 <ion-item style="font-size: 14px;">\n                                                         {{i+1}} {{item.text}}\n                                                 </ion-item>\n                                                 </ion-list>\n                                                 <ion-row>\n                                                         <ion-col>\n                                                                 <button float-left ion-button color="primary" icon-left (click)="previous()" [disabled]="noPreviousQuestion">Previous</button>\n         \n                                                         </ion-col>\n                                                         <ion-col>\n                                                                 <button float-center ion-button color="primary" icon-left>Mark</button>\n         \n                                                            \n                                                         </ion-col>\n                                                         <ion-col>\n                                                                 <button float-right ion-button color="primary" icon-right (click)="!noNextQuestion && next();noNextQuestion && finishExamModal()">{{(noNextQuestion)?\'Submit\':\'Next\'}}</button>\n         \n                                                         </ion-col>\n                                                       </ion-row>\n                                     \n                                         </ion-item-group>\n                         </ion-card-content>\n         \n         \n             </ion-card> -->\n\n\n    <ion-grid style="height:180px;background-color: #fff;margin-top:10px!important;">\n        <p style="text-align:center;margin-top:0px!important; margin-bottom:4px!important;font-size: 18px;">Question Palette</p>\n        <ion-scroll scrollY="true" style="height:160px;">\n\n            <ion-row>\n                <ion-col col-2 col-md-4 col-xl-3 style="margin-top:0px; max-width: 12.0000%!important;" *ngFor="let item of questionPalette ;let i= index;">\n                    <ion-col class="circleStyle" [ngClass]="{\'selected\': item.status === \'answered\', \'marked\': item.marked === \'marked\'}" (click)="goToQuestion(i)">\n                        {{item.count+1||i+1}}\n\n                    </ion-col>\n                </ion-col>\n                <!-- <ion-col col-2 col-md-4 col-xl-3 style="margin-top:0px; max-width: 12.0000%!important;"  *ngFor="let item of questionPalette ;let i= index;" >\n                                 <ion-col  class="circleStyle"  [ngClass]="{\'selected\': item.status === \'answered\', \'marked\': item.status === \'marked\'}"    \n                                 (click)="goToQuestion(i)">\n                                     {{item.count+1||i+1}}\n         \n                                 </ion-col>\n        \n                             </ion-col> -->\n\n            </ion-row>\n        </ion-scroll>\n    </ion-grid>\n    <hr>\n\n    <ion-grid style="margin-top:0px;background-color:#cecece;">\n        <ion-row>\n\n            <ion-col col-6 (click)="filter(\'na\')">\n                <ion-col col-1 class="circle">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp; Not Answered\n            </ion-col>\n            <ion-col col-6 (click)="filter(\'a\')">\n                <ion-col col-1 class="circle1">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp;Answered\n            </ion-col>\n        </ion-row>\n\n        <ion-row style="padding-top: 10px;">\n            <ion-col col-6 (click)="filter(\'m\')">\n                <ion-col col-1 class="circle2">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp; Marked\n            </ion-col>\n            <ion-col col-6 (click)="filter(\'\')">\n                <ion-col col-1 class="circle">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp; All Question\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n</ion-content>\n<ion-footer>\n    <button ion-button block style="background-color:#e48684;color:white;" (click)="finishExam(\'\')">Submit Exam </button>\n</ion-footer>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/createexam/write-exam/write-exam.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]])
    ], writeExam);
    return writeExam;
}());

//# sourceMappingURL=write-exam.js.map

/***/ }),

/***/ 989:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 453,
	"./af.js": 453,
	"./ar": 454,
	"./ar-dz": 455,
	"./ar-dz.js": 455,
	"./ar-kw": 456,
	"./ar-kw.js": 456,
	"./ar-ly": 457,
	"./ar-ly.js": 457,
	"./ar-ma": 458,
	"./ar-ma.js": 458,
	"./ar-sa": 459,
	"./ar-sa.js": 459,
	"./ar-tn": 460,
	"./ar-tn.js": 460,
	"./ar.js": 454,
	"./az": 461,
	"./az.js": 461,
	"./be": 462,
	"./be.js": 462,
	"./bg": 463,
	"./bg.js": 463,
	"./bm": 464,
	"./bm.js": 464,
	"./bn": 465,
	"./bn.js": 465,
	"./bo": 466,
	"./bo.js": 466,
	"./br": 467,
	"./br.js": 467,
	"./bs": 468,
	"./bs.js": 468,
	"./ca": 469,
	"./ca.js": 469,
	"./cs": 470,
	"./cs.js": 470,
	"./cv": 471,
	"./cv.js": 471,
	"./cy": 472,
	"./cy.js": 472,
	"./da": 473,
	"./da.js": 473,
	"./de": 474,
	"./de-at": 475,
	"./de-at.js": 475,
	"./de-ch": 476,
	"./de-ch.js": 476,
	"./de.js": 474,
	"./dv": 477,
	"./dv.js": 477,
	"./el": 478,
	"./el.js": 478,
	"./en-au": 479,
	"./en-au.js": 479,
	"./en-ca": 480,
	"./en-ca.js": 480,
	"./en-gb": 481,
	"./en-gb.js": 481,
	"./en-ie": 482,
	"./en-ie.js": 482,
	"./en-il": 483,
	"./en-il.js": 483,
	"./en-nz": 484,
	"./en-nz.js": 484,
	"./eo": 485,
	"./eo.js": 485,
	"./es": 486,
	"./es-do": 487,
	"./es-do.js": 487,
	"./es-us": 488,
	"./es-us.js": 488,
	"./es.js": 486,
	"./et": 489,
	"./et.js": 489,
	"./eu": 490,
	"./eu.js": 490,
	"./fa": 491,
	"./fa.js": 491,
	"./fi": 492,
	"./fi.js": 492,
	"./fo": 493,
	"./fo.js": 493,
	"./fr": 494,
	"./fr-ca": 495,
	"./fr-ca.js": 495,
	"./fr-ch": 496,
	"./fr-ch.js": 496,
	"./fr.js": 494,
	"./fy": 497,
	"./fy.js": 497,
	"./gd": 498,
	"./gd.js": 498,
	"./gl": 499,
	"./gl.js": 499,
	"./gom-latn": 500,
	"./gom-latn.js": 500,
	"./gu": 501,
	"./gu.js": 501,
	"./he": 502,
	"./he.js": 502,
	"./hi": 503,
	"./hi.js": 503,
	"./hr": 504,
	"./hr.js": 504,
	"./hu": 505,
	"./hu.js": 505,
	"./hy-am": 506,
	"./hy-am.js": 506,
	"./id": 507,
	"./id.js": 507,
	"./is": 508,
	"./is.js": 508,
	"./it": 509,
	"./it.js": 509,
	"./ja": 510,
	"./ja.js": 510,
	"./jv": 511,
	"./jv.js": 511,
	"./ka": 512,
	"./ka.js": 512,
	"./kk": 513,
	"./kk.js": 513,
	"./km": 514,
	"./km.js": 514,
	"./kn": 515,
	"./kn.js": 515,
	"./ko": 516,
	"./ko.js": 516,
	"./ky": 517,
	"./ky.js": 517,
	"./lb": 518,
	"./lb.js": 518,
	"./lo": 519,
	"./lo.js": 519,
	"./lt": 520,
	"./lt.js": 520,
	"./lv": 521,
	"./lv.js": 521,
	"./me": 522,
	"./me.js": 522,
	"./mi": 523,
	"./mi.js": 523,
	"./mk": 524,
	"./mk.js": 524,
	"./ml": 525,
	"./ml.js": 525,
	"./mn": 526,
	"./mn.js": 526,
	"./mr": 527,
	"./mr.js": 527,
	"./ms": 528,
	"./ms-my": 529,
	"./ms-my.js": 529,
	"./ms.js": 528,
	"./mt": 530,
	"./mt.js": 530,
	"./my": 531,
	"./my.js": 531,
	"./nb": 532,
	"./nb.js": 532,
	"./ne": 533,
	"./ne.js": 533,
	"./nl": 534,
	"./nl-be": 535,
	"./nl-be.js": 535,
	"./nl.js": 534,
	"./nn": 536,
	"./nn.js": 536,
	"./pa-in": 537,
	"./pa-in.js": 537,
	"./pl": 538,
	"./pl.js": 538,
	"./pt": 539,
	"./pt-br": 540,
	"./pt-br.js": 540,
	"./pt.js": 539,
	"./ro": 541,
	"./ro.js": 541,
	"./ru": 542,
	"./ru.js": 542,
	"./sd": 543,
	"./sd.js": 543,
	"./se": 544,
	"./se.js": 544,
	"./si": 545,
	"./si.js": 545,
	"./sk": 546,
	"./sk.js": 546,
	"./sl": 547,
	"./sl.js": 547,
	"./sq": 548,
	"./sq.js": 548,
	"./sr": 549,
	"./sr-cyrl": 550,
	"./sr-cyrl.js": 550,
	"./sr.js": 549,
	"./ss": 551,
	"./ss.js": 551,
	"./sv": 552,
	"./sv.js": 552,
	"./sw": 553,
	"./sw.js": 553,
	"./ta": 554,
	"./ta.js": 554,
	"./te": 555,
	"./te.js": 555,
	"./tet": 556,
	"./tet.js": 556,
	"./tg": 557,
	"./tg.js": 557,
	"./th": 558,
	"./th.js": 558,
	"./tl-ph": 559,
	"./tl-ph.js": 559,
	"./tlh": 560,
	"./tlh.js": 560,
	"./tr": 561,
	"./tr.js": 561,
	"./tzl": 562,
	"./tzl.js": 562,
	"./tzm": 563,
	"./tzm-latn": 564,
	"./tzm-latn.js": 564,
	"./tzm.js": 563,
	"./ug-cn": 565,
	"./ug-cn.js": 565,
	"./uk": 566,
	"./uk.js": 566,
	"./ur": 567,
	"./ur.js": 567,
	"./uz": 568,
	"./uz-latn": 569,
	"./uz-latn.js": 569,
	"./uz.js": 568,
	"./vi": 570,
	"./vi.js": 570,
	"./x-pseudo": 571,
	"./x-pseudo.js": 571,
	"./yo": 572,
	"./yo.js": 572,
	"./zh-cn": 573,
	"./zh-cn.js": 573,
	"./zh-hk": 574,
	"./zh-hk.js": 574,
	"./zh-tw": 575,
	"./zh-tw.js": 575
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 989;

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return grantExam; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_exam_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_exam_exam__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_exam_examNew_examComponent__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var grantExam = /** @class */ (function () {
    function grantExam(navCtrl, platform, events, alertCtrl, __auth, navParams, examService) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.__auth = __auth;
        this.navParams = navParams;
        this.examService = examService;
        this.nextButtonLabel = "Next";
        this.noPreviousQuestion = false;
        this.noNextQuestion = false;
        this.tempQuestionPallete = [];
        this.updateFlag = false;
        this.viewResult = false;
        this.testN = false;
        this.timeGrandExamObject = [];
        this.timer = false;
        this.buttonName = false;
        this.paused = false;
        this.resumed = false;
        this.buttonToggle = false;
        this.paletteStatus = [];
        this.userExamId = navParams.get('subject');
        this.events.publish('user:created', true, Date.now());
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    }
    grantExam.prototype.ionViewWillLeave = function () {
        this.tabBarElement.style.display = 'flex';
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
    };
    grantExam.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(function () { return _this.backButtonFunc(); });
    };
    grantExam.prototype.backButtonFunc = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_exam_examNew_examComponent__["a" /* examComponent */]);
    };
    grantExam.prototype.ngOnInit = function () {
        var _this = this;
        this.currentPaletteId = 0;
        this.examService.loadGrandExam(this.userExamId).subscribe(function (res) {
            if (res.data) {
                console.log(res.data, "data here");
                _this.noOfQuest = res.data.userExamObj.questionPalette.length;
                _this.questionPalette = res.data.userExamObj.questionPalette;
                _this.tempQuestionPallete = _this.questionPalette; // this is temp pallete array for sorting(marked,answered,not answered)                   
                _this.examConfigData = res.data.userExamObj;
                _this.noPreviousQuestion = true;
                _this.examId = res.data.userExamObj._id;
                _this.currentPaletteId = 0;
                _this.timeFlag = _this.examConfigData.timeFlag;
                if (_this.timeFlag) {
                    _this.timerObj = _this.examConfigData.timeDetails;
                    _this.timeGrandExamObject = JSON.parse(localStorage.getItem('timeGrandExamObject'));
                    if (_this.timeGrandExamObject) {
                        _this.timeGrandExamObject.map(function (item, i) {
                            if (item.userExamId == _this.userExamId) {
                                _this.timerIndex = i;
                                _this.previousTimer = _this.timeGrandExamObject[_this.timerIndex];
                            }
                            ;
                        });
                    }
                    else {
                        _this.timeGrandExamObject = [];
                        _this.previousTimer = null;
                    }
                    if (_this.previousTimer == null || !_this.previousTimer) {
                        _this.time = new Date(2017, 1, 1, _this.timerObj.hour, _this.timerObj.minut, 0);
                        _this.timer = true;
                    }
                    else {
                        _this.time = new Date(2017, 1, 1, _this.previousTimer.hours, _this.previousTimer.minutes, _this.previousTimer.seconds);
                        _this.timer = true;
                    }
                    _this._timerTick();
                }
                _this.questionId = _this.questionPalette[_this.currentPaletteId]._id;
                _this.examService.getGrandExamQuestions(_this.userExamId, _this.questionId).subscribe(function (res) {
                    _this.RealexamData = res;
                    _this.answerData = res.data.answer;
                    if (res.data.answer) {
                        _this.answered = true;
                        _this.answeredPalette = true;
                        _this.clickedItem = res.data.answer.answer;
                    }
                    _this.questionData = res.data.question;
                    _this.options = JSON.parse(res.data.question.options);
                });
            }
        });
    };
    grantExam.prototype._timerTick = function () {
        var _this = this;
        this.time.setSeconds(this.time.getSeconds(), -1);
        var timerObject = {
            hours: this.time.getHours(),
            minutes: this.time.getMinutes(),
            seconds: this.time.getSeconds(),
            userExamId: this.userExamId
        };
        if (this.timerIndex)
            this.timeGrandExamObject[this.timerIndex] = timerObject;
        else {
            this.timeGrandExamObject.push(timerObject);
            this.timeGrandExamObject.map(function (item, i) {
                if (item.userExamId == _this.userExamId)
                    _this.timerIndex = i;
            });
        }
        localStorage.setItem('timeGrandExamObject', JSON.stringify(this.timeGrandExamObject));
        if (timerObject.hours === 0 && timerObject.minutes === 5 && timerObject.seconds === 0) {
            this.__auth.notificationInfo('5 Minutes Remaining');
        }
        if (timerObject.hours === 0 && timerObject.minutes === 0 && timerObject.seconds === 0) {
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
            this.statusCount();
            this.finishGrandExam("");
        }
        this.x = setTimeout(function () { return _this._timerTick(); }, 1000);
    };
    grantExam.prototype.playPause = function () {
        var _this = this;
        if (this.buttonToggle) {
            this.buttonName = false;
            this.pauseTitle = "Pause Timer";
            if (this.pauseTime) {
                this.playTime = this.pauseTime;
            }
            else {
                this.playTime = this.time;
            }
            this.paused = false;
            this.resumed = true;
            var examurl = "/exam/write/" + this.userExamId;
            this.playTime.setSeconds(this.playTime.getSeconds(), -1);
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 5 && this.playTime.getSeconds() === 0) {
                this.__auth.notificationInfo('5 Minutes Remaining');
            }
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 0 && this.playTime.getSeconds() === 0) {
                window.clearInterval(this.x);
                localStorage.removeItem('timerObject');
                this.finishGrandExam("");
                this.statusCount();
            }
            var x = setTimeout(function () { return _this._timerTick(); }, 1000);
        }
        else {
            this.buttonName = true;
            this.playTitle = "Resume Timer";
            this.pauseTime = this.time;
            this.resumed = false;
            this.paused = true;
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
        }
        this.buttonToggle = !this.buttonToggle;
    };
    grantExam.prototype.saveAnswer = function (answer, index) {
        var _this = this;
        if (!this.paused) {
            var correctAnswer = this.questionData.answer;
            this.questionId = this.questionPalette[this.currentPaletteId]._id;
            if (this.answerData === null) {
                this.updateFlag = false;
                this.questionStatus = 'answered';
            }
            else {
                this.updateFlag = true;
                this.answered = true;
                // this.userExamAnwserId = this.RealexamData.data.answer._id;
                // this.clickedItem = this.RealexamData.data.answer.answer;
                this.clickedItem = answer;
            }
            var subjectArray = this.questionData.subjectArray;
            var result = void 0;
            if (answer === correctAnswer) {
                result = "correct";
            }
            else {
                result = "wrong";
            }
            this.qstAnswerd = true;
            if (this.updateFlag === false) {
                this.examService.saveGrandExamAnswer(this.userExamId, this.currentPaletteId, this.questionStatus, this.questionId, answer, result, this.updateFlag, subjectArray).subscribe(function (res) {
                    _this.answeredPalette = true;
                    _this.answeredPaletteId = index;
                    _this.answered = true;
                    _this.updateFlag = true;
                    _this.clickedItem = answer;
                    var data = {
                        '_id': _this.questionId,
                        'status': 'answered'
                    };
                    _this.questionPalette[_this.currentPaletteId]._id = _this.questionId;
                    _this.questionPalette[_this.currentPaletteId].status = "answered";
                });
            }
            else {
                this.examService.saveGrandExamAnswer(this.userExamId, this.currentPaletteId, this.questionStatus, this.questionId, answer, result, this.updateFlag, subjectArray).subscribe(function (res) {
                    _this.clickedItem = answer;
                    _this.answered = true;
                    var data = {
                        '_id': _this.questionId,
                        'status': 'answered'
                    };
                    _this.questionPalette[_this.currentPaletteId]._id = _this.questionId;
                    _this.questionPalette[_this.currentPaletteId].status = "answered";
                });
            }
        }
        else {
            console.log("user pause");
        }
    };
    grantExam.prototype.next = function () {
        var _this = this;
        window.scrollTo(0, 0);
        this.currentPaletteId = this.currentPaletteId + 1;
        this.questionId = this.questionPalette[this.currentPaletteId]._id;
        this.examService.getGrandExamQuestions(this.userExamId, this.questionId).subscribe(function (res) {
            _this.RealexamData = res;
            _this.answerData = res.data.answer;
            _this.noPreviousQuestion = false;
            if (_this.questionPalette.length === (_this.currentPaletteId + 1)) {
                _this.noNextQuestion = true;
                _this.nextButtonLabel = "Submit";
            }
            else {
                _this.noNextQuestion = false;
            }
            if (_this.answerData != null) {
                _this.answered = true;
                _this.clickedItem = _this.answerData.answer;
            }
            else {
                _this.answered = false;
            }
            _this.questionData = res.data.question;
            _this.options = JSON.parse(res.data.question.options);
        });
    };
    grantExam.prototype.previous = function () {
        var _this = this;
        window.scrollTo(0, 0);
        this.currentPaletteId = this.currentPaletteId - 1;
        var questionId = this.questionPalette[this.currentPaletteId]._id;
        this.examService.getGrandExamQuestions(this.userExamId, questionId).subscribe(function (res) {
            _this.answerData = res.data.answer;
            if (_this.answerData != null) {
                _this.answered = true;
                _this.clickedItem = _this.answerData.answer;
            }
            else {
                _this.answered = false;
            }
            if (_this.questionPalette.length === (_this.currentPaletteId + 1)) {
                _this.noNextQuestion = true;
            }
            else {
                _this.noNextQuestion = false;
                _this.nextButtonLabel = "Next";
            }
            if (_this.currentPaletteId === 0) {
                _this.noPreviousQuestion = true;
            }
            else {
                _this.noPreviousQuestion = false;
            }
            _this.questionData = res.data.question;
            _this.options = JSON.parse(res.data.question.options);
        });
    };
    grantExam.prototype.mark = function (status) {
        var _this = this;
        this.examService.markGrandExamQuestion(this.userExamId, this.questionId, status).subscribe(function (res) {
            if (!res.status)
                _this.__auth.notificationInfo('OOPS! Something went wrong.');
            else {
                if (status == 'marked') {
                    _this.questionPalette[_this.currentPaletteId].marked = status;
                }
                else {
                    if (_this.answerData != null) {
                        _this.questionPalette[_this.currentPaletteId].status = 'answered';
                    }
                    else {
                        _this.questionPalette[_this.currentPaletteId].status = '';
                    }
                    _this.questionPalette[_this.currentPaletteId].marked = status;
                }
            }
        });
    };
    grantExam.prototype.goToQuestion = function (index) {
        var _this = this;
        if (!this.paused) {
            this.questionId = this.questionPalette[index]._id;
            this.examService.getGrandExamQuestions(this.userExamId, this.questionId).subscribe(function (res) {
                _this.RealexamData = res;
                _this.answerData = res.data.answer;
                if (res.data.answer) {
                    _this.answered = true;
                    _this.answeredPalette = true;
                    _this.answeredPaletteId = _this.currentPaletteId;
                    _this.clickedItem = res.data.answer.answer;
                }
                else {
                    _this.clickedItem = "";
                }
                _this.questionData = res.data.question;
                // this.currentPaletteId = index;
                _this.currentPaletteId = index;
                _this.options = JSON.parse(res.data.question.options);
                if (_this.questionPalette.length === (_this.currentPaletteId + 1)) {
                    _this.noNextQuestion = true;
                }
                else {
                    _this.noNextQuestion = false;
                }
            });
        }
        else {
            console.log("user paused");
        }
    };
    grantExam.prototype.finishExamModal = function () {
        if (!this.qstAnswerd) {
            this.__auth.notificationInfo('Please attend one question');
        }
    };
    grantExam.prototype.finishExam = function (userId) {
        var _this = this;
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
        if (!this.qstAnswerd) {
            this.__auth.notificationInfo('Please attend one question');
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Confirm submit',
                message: 'Do you want to submit exam?',
                buttons: [
                    {
                        text: 'Yes',
                        role: 'cancel',
                        handler: function () {
                            _this.examService.finishGrandExam(_this.userExamId).subscribe(function (res) {
                                if (res.status) {
                                    var id = _this.examId + '/' + 'g';
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: id });
                                }
                                else {
                                    _this.__auth.notificationInfo('OOPS! Something went wrong.');
                                }
                            });
                        }
                    },
                    {
                        text: 'No',
                        handler: function () {
                        }
                    }
                ]
            });
            alert_1.present();
        }
    };
    grantExam.prototype.finishGrandExam = function (userId) {
        var _this = this;
        this.examService.finishGrandExam(this.userExamId).subscribe(function (res) {
            if (res.status) {
                window.clearInterval(_this.x);
                localStorage.removeItem('timerObject');
                var id = _this.examId + '/' + 'g';
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_exam_exam_room_details_details__["a" /* Details */], { ExamIds: id });
            }
            else {
                _this.__auth.notificationInfo('OOPS! Something went wrong.');
            }
        });
    };
    grantExam.prototype.filter = function (type) {
        var _this = this;
        var mapArray = function (type) {
            var tempArray = [];
            tempArray = (_this.tempQuestionPallete).filter(function (item) {
                if (item.status == type)
                    return item;
            });
            if (tempArray.length)
                _this.questionPalette = tempArray, _this.goToQuestion(0), _this.currentPaletteId = 0;
            else
                _this.__auth.notificationInfo('No questions.');
        };
        var grandMarked = function (type) {
            var tempArray = [];
            tempArray = (_this.tempQuestionPallete).filter(function (item) {
                if (item.marked == type)
                    return item;
            });
            if (tempArray.length)
                _this.questionPalette = tempArray, _this.goToQuestion(0), _this.currentPaletteId = 0;
            else
                _this.__auth.notificationInfo('No questions.');
        };
        if (type == 'm') {
            grandMarked('marked');
        }
        else if (type == 'a') {
            mapArray('answered');
        }
        else if (type == 'na') {
            mapArray("");
        }
        else {
            this.questionPalette = this.tempQuestionPallete;
            this.currentPaletteId = 0;
            this.goToQuestion(0);
        }
    };
    grantExam.prototype.statusCount = function () {
        var marked = 0, unanswered = 0, answered = 0;
        this.tempQuestionPallete.filter(function (item) {
            if (item.status == 'answered')
                answered++;
            else
                unanswered++;
        });
        this.paletteStatus = [answered, marked, unanswered];
    };
    grantExam.prototype.back = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_exam_exam__["a" /* Exam */]);
    };
    grantExam = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-grantexam',template:/*ion-inline-start:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/GrandExam/grantexam.html"*/'\n<style>\n\n .button-ios { \n    font-size: 1rem !important;\n\n}    \n\n.card-content-md {\n    padding-left: -1px;\n  }\n  .card-md {\n    margin-left:-4px !important;\n    border-radius: 2px!important;\n    width: calc(100% - -8px)!important;   \n  }\n  \n  page-write .optionStyle {\n      border-bottom:unset!important;   \n  }\n</style>\n  <ion-header #head>\n     \n  \n        <ion-toolbar>\n            <ion-navbar>\n                <ion-buttons left>\n                    \n                </ion-buttons>\n              </ion-navbar>\n          \n    \n    \n        </ion-toolbar>\n      </ion-header>\n<ion-content padding style="background-color: #efefef;" >\n    <!-- <ion-grid style=" background-color:#b4aeae;">\n            <ion-row>\n                <ion-col col-6>\n                        <ion-icon name="arrow-back" style="font-size: 24px;" (click)="back()"></ion-icon>\n                  &nbsp; &nbsp;  <ion-icon name="star" style="color:#ff6b01;font-size: 24px;"></ion-icon>\n                </ion-col>\n                <ion-col col-2 class="align-icon">\n                    <ion-icon name="share-alt" style="font-size: 24px;"></ion-icon>\n                </ion-col>\n                <ion-col col-2 class="align-icon">\n                    <ion-icon name="switch" style="font-size: 24px;"></ion-icon>\n                </ion-col>\n                <ion-col col-2 class="align-icon">\n                    <ion-icon name="apps" style="font-size: 24px;"></ion-icon>\n                </ion-col>\n            </ion-row>\n        </ion-grid> -->\n\n    <ion-card style="margin-top:0px;" *ngIf="questionData">\n        <div *ngIf="questionData">\n\n            <ion-row>\n                    <ion-col col-12 class="count" style=" background-color:#cecece; padding: 8px!important" *ngIf="timeFlag">\n                        <div class="count" style="float:left; padding-top:5px;">\n\n                            <div style="font-size: 15px;color: #e13838;">\n                                <span>{{ time.getHours() }}</span> :\n                                <span>{{ time.getMinutes() }}</span> :\n                                <span>{{ time.getSeconds() }}</span>\n                            </div>\n                        </div>\n\n                        <div style=" float: right!important;">\n                            <!-- <button ion-button class="playBtn" title={{pauseTitle}}  aria-hidden="true" (click)="playPause()" *ngIf="!buttonName"><ion-icon name="pause"></ion-icon>\n                                        </button> -->\n                            <ion-icon name="pause" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="!buttonName"></ion-icon>\n\n                            <!-- <button ion-button  class="playBtn" title={{playTitle}}  aria-hidden="true"  (click)="playPause()" *ngIf="buttonName"><ion-icon name="play"></ion-icon></button>   -->\n                            <ion-icon name="play" class="playBtn" title={{pauseTitle}} (click)="playPause()" *ngIf="buttonName"></ion-icon>\n\n                        </div>\n                    </ion-col>\n\n            </ion-row>\n\n        </div>\n        <ion-card-content  style="background-color:#FFFEEE;margin-left:-13px!important; margin-right:-13px!important;">\n        \n                <ion-item-group>\n                    <ion-item-divider style="padding:0px;background-color:#FFFEEE;" text-wrap class="questionHead">\n                        <ion-grid style="padding-bottom:0px !important;">\n                            <ion-row style="border-bottom: 2px solid #f0f0f0;">\n                                <ion-col col-2 style="padding-right:0px!important; line-height: 19px!important; font-size:18px;width:auto!important;max-width: 13.76667%;  padding-top:11px; padding-left:0px;">\n\n                                    <span [innerHtml]="currentPaletteId+1">/</span>.\n                                </ion-col>\n\n                                <ion-col col-10 class="fntsze" style=" padding-left: 0px!important; line-height:24px !important;  color: black;font-size:18px!important;">\n\n                                    <span [innerHtml]="questionData.question"></span>\n                                </ion-col>\n                            </ion-row>\n                        </ion-grid>\n                    </ion-item-divider>\n\n                    <ion-list *ngFor="let item of options ;let i= index;">\n                        <ion-item text-wrap class="optionStyle" disabled="answered" (click)="saveAnswer(item.id,i)" style="padding-left:0px!important; min-height: 0.0rem !important; background-color:#FFFEEE!important;border-bottom: none!important; ">\n\n                            <div [ngClass]="{\'selected\': answered === true && clickedItem === item.id}">\n\n                                <ion-grid style="padding:0px!important;">\n                                    <ion-row>\n                                        <ion-col col-2 style=" padding-right:0px!important; margin-right:0px; font-size:18px!important;font-weight:unset!important;padding-left:3pxpx !important;max-width: 13.76667%;">{{item.id}}.</ion-col>\n                                        <ion-col col-10 style=" font-size:18px!important;padding-left:2!important;margin-left: 0px;line-height:22px;line-height:26px;">\n                                            <span> {{item.text}}</span>\n                                        </ion-col>\n                                    </ion-row>\n                                </ion-grid>\n\n\n\n\n                            </div>\n                        </ion-item>\n                    </ion-list>\n\n\n\n                </ion-item-group>\n           \n        </ion-card-content>\n\n        <ion-row style="margin-top:10px;">\n            <ion-col col-4>\n                <button style="text-align:left;  padding: 0px  15px 0px 15px; font-size: 20px ;background-color:#108fd2;" ion-button color="primary"  icon-left (click)="previous()" [disabled]="noPreviousQuestion || paused">Previous</button>\n\n            </ion-col>\n            <ion-col col-4 style="text-align:center;">\n\n              \n                \n                <button float-center ion-button style="margin-left: 29px;" style=" text-align: center; background-color:#108fd2; padding: 0px 15px 0px 15px; "\n                    color="primary" icon-left (click)="mark((questionPalette[currentPaletteId].marked == \'marked\') ? \'unmarked\': \'marked\')"\n                    [disabled]=paused>{{(questionPalette[currentPaletteId].marked == \'marked\') ? \'Unmark\': \'Mark\'}} </button>\n\n\n            </ion-col>\n            <ion-col col-4 style="text-align:center;">\n                <button float-right ion-button color="primary" style="background-color:#108fd2;color:white;margin-right:4px; padding: 0px 15px 0px 15px;" icon-right\n                    (click)="!noNextQuestion && next();noNextQuestion && finishExam(\'\')" [disabled]=paused>{{(noNextQuestion)?\'Submit\':\'Next\'}}</button>\n\n            </ion-col>\n        </ion-row>\n    </ion-card>\n\n\n    <!-- <ion-card style="margin-top:0px;">\n    <ion-card-content style="margin-top:0px;">\n\n        <ion-grid>\n\n            <ion-row>\n                <ion-col style="border: 1px solid black;">\n\n                    <div><span class="question" [innerHtml]="currentPaletteId+1">/</span><span class="question" [innerHtml]="currentPaletteId+1" [innerHtml]="questionData"></span>\n                        <span class="question"></span>&nbsp;&nbsp;<span class="question"></span>\n                    </div>\n                </ion-col>\n            </ion-row>\n            <ion-row *ngFor="let item of options ;let i= index;" [ngClass]="{\'selected\': answered === true && clickedItem === item.id}"\n                disabled="answered" (click)="saveAnswer(item.id,i)">\n                <ion-col style="border: 1px solid black;">\n                    {{i+1}} {{item.text}}\n                </ion-col>\n            </ion-row>\n\n\n\n            <ion-row style="border: 1px solid black;">\n                <ion-col>\n                    <button ion-button style=" background-color:#108fd2;color:white;" (click)="previous()" [disabled]="noPreviousQuestion">Previous</button>\n                </ion-col>\n                <ion-col>\n\n                    <!-- <button ion-button style="background-color:#108fd2;color:white;" (click)="mark((questionPalette[currentPaletteId].status == \'marked\') ? \'unmarked\': \'marked\')">{{(questionPalette[currentPaletteId].status == \'marked\') ? \'Unmark\': \'Mark\'}}</button> -->\n    <!-- </ion-col>\n                <ion-col>\n                    <button ion-button style="background-color:#108fd2;color:white;" (click)="!noNextQuestion && next();noNextQuestion && finishExamModal()">{{(noNextQuestion)?\'Submit\':\'Next\'}}</button>\n                </ion-col>\n            </ion-row>\n\n        </ion-grid>\n\n    </ion-card-content>\n\n</ion-card>-->\n\n    <!-- <ion-card style="height:120px;background-color: #f8f8f8;">-->\n\n    <ion-grid style="height:180px;background-color: #fff;margin-top:10px!important;">\n        <p sstyle="text-align:center; margin-bottom:4px!important;font-size: 18px;">Question Palette</p>\n\n        <ion-scroll scrollY="true" style="height:160px;">\n\n            <ion-row>\n       \n                <ion-col col-2 col-md-4 col-xl-3 style="margin-top:0px;" style="margin-top:0px; max-width: 12.0000%!important;" *ngFor="let item of questionPalette ;let i= index;">\n                    <ion-col  class="circleStyle"  [ngClass]="{\'selected\': item.status === \'answered\', \'marked\': item.marked === \'marked\'}"    \n                    (click)="goToQuestion(i)">\n                        {{item.count+1||i+1}}\n\n                    </ion-col>\n                    <!-- <ion-col class="circleStyle" [ngClass]="{\'selected\': item.status === \'answered\', \'marked\': item.marked === \'marked\'}" (click)="goToQuestion(i)">\n                        {{item.count+1||i+1}}\n\n                    </ion-col> -->\n\n\n\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n        </ion-scroll>\n    </ion-grid>\n\n    <!-- <ion-card style=" min-height:100px; margin-top:0px;background-color: #f8f8f8;">-->\n    <hr>\n    <ion-grid style="margin-top:0px;background-color:#cecece;">\n        <ion-row>\n\n            <ion-col col-6 (click)="filter(\'na\')">\n                <ion-col col-1 class="circle">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp; Not Answered\n            </ion-col>\n            <ion-col col-6 (click)="filter(\'a\')">\n                <ion-col col-1 class="circle1">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp;Answered\n            </ion-col>\n        </ion-row>\n\n        <ion-row style="padding-top: 10px;">\n            <ion-col col-6 (click)="filter(\'m\')">\n                <ion-col col-1 class="circle2">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp;&nbsp; Marked\n            </ion-col>\n            <ion-col col-6 (click)="filter(\'\')">\n                <ion-col col-1 class="circle">\n                    &nbsp;&nbsp;\n                </ion-col>\n                &nbsp;\n                 All Questions\n            </ion-col>\n\n        </ion-row>\n        <!-- <button ion-button style="background-color:#e48684;color:white; float:right;padding-top:2px;" (click)="finishExam(\'\')">Submit Exam</button> -->\n    </ion-grid>\n\n\n\n</ion-content>\n<ion-footer>\n    <button ion-button block style="background-color:#e48684;color:white;" (click)="finishExam(\'\')">Submit Exam </button>\n</ion-footer>'/*ion-inline-end:"/Users/ALLURIS/Downloads/nextfellows/iosnextfellow/src/pages/exam/GrandExam/grantexam.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_exam_service__["a" /* ExamService */]])
    ], grantExam);
    return grantExam;
}());

//# sourceMappingURL=grantexam.component.js.map

/***/ })

},[620]);
//# sourceMappingURL=main.js.map