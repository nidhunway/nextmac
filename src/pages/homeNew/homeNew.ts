import { Component,ViewChild } from '@angular/core';
import { NavController ,NavParams, Platform,Tabs} from 'ionic-angular';
import { Note } from '../../pages/note/note';
import { ListPage } from '../../pages/learn/list';
import { HomePage } from '../../pages/dashboard/home';
import { examComponent } from '../../pages/exam/examNew/examComponent';
import { weekendReg } from '../../pages/exam/weekend/weekend.component';
import { Evaluate } from '../../pages/evaluate/evaluate';
import { Setting } from '../../commen/setting/setting';
import { grantExam } from '../../pages/exam/GrandExam/grantexam.component';
import { naveBarShowSrvice } from '../../services/navBarService';
import { Events } from 'ionic-angular';


@Component({
  selector: 'page-homeNew',
  templateUrl: 'homeNew.html',
  providers:[naveBarShowSrvice]
})
export class homeNew {
  @ViewChild('myTabs') tabRef: Tabs;
    public unregisterBackButtonAction: any;
  tab1Root=HomePage;
  tab2Root=ListPage;
  tab3Root=examComponent;
  tab4Root=Note;
  tab5Root=Setting;
  seeTabs;
  navBarHide;
  indexvalus;
  indexValue:number;
  selectedTabIndex:number;
  user:boolean=false;
  selectedTab;
  public hideTabs:boolean = false;
  constructor(public navCtrl: NavController,public events: Events,public platform: Platform,public navParams: NavParams,private _navBar:naveBarShowSrvice) {
    this.selectedTabIndex=0;

    events.subscribe('user:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome', user, 'at', time);
      this.hideTabs=user;
     // alert(user);
    });

  this.navBarHide=this._navBar.localNavbar;
  
  //this.selectedTabIndex = JSON.parse(localStorage.getItem('indexdata'));
  
 }
 tabChanged(ev) {
   console.log("evvv",ev);
   
   
 
}
 indexPass(index){
  this.indexValue=index;
  console.log(this.indexValue,"vlaue here");
  //localStorage.setItem('indexdata', JSON.stringify(this.indexValue));  
}


}
