import { Component } from '@angular/core';
import { WalkthroughModule } from 'ngx-walkthrough';

import { NavController, ViewController, ModalController ,NavParams} from 'ionic-angular';
import { ListPage } from '../../pages/learn/list';
import { Exam } from '../../pages/exam/exam';
import { Note } from '../../pages/note/note';
import { CreateExam } from '../../pages/exam/createexam/createExam';
import { StudentService } from '../../services/student.service'
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { Questions } from '../../pages/learn/questions/questions';
import { weekendReg } from '../../pages/exam/weekend/weekend.component';
import { Details } from '../../pages/exam/exam-room/details/details';

import { ExamService } from '../../services/exam.service';
import { ExamRoom } from '../../pages/exam/exam-room/examroom';
// import { NestedTab1Page } from '../../pages/exam/exam-room/completeExam/nested-tab1';
import { writeExam } from '../../pages/exam/createexam/write-exam/write-exam';
import { grantModal } from '../../pages/exam/GrandExam/grantModal/grantModal';
import { Evaluate } from '../../pages/evaluate/evaluate';
import { Login } from '../../commen/login/login';
import { examComponent } from '../../pages/exam/examNew/examComponent';
import { naveBarShowSrvice } from '../../services/navBarService';
import { homeNew } from '../../pages/homeNew/homeNew';

import { Events } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [StudentService, ExamService,naveBarShowSrvice,homeNew]
})
export class HomePage {
  greeting;
  date;
  userData;
  chapterId;
  subjectId;
  live: string;
  examName;
  examId;
  chapterName;
  sectionName;
  welcomeDiv: boolean = false;
  studyonly: boolean = false;
  examonly: boolean = false;
  learnDiv: boolean = true;
  learnBig: boolean = false;
  GreenDivF: boolean = true;
  liveDiv: boolean = true;
  dashInfo;
  username;
  tabBarElement;
  googleParam:boolean;
  constructor(public params:NavParams,public events: Events,public myTabs:homeNew,public _navBar:naveBarShowSrvice,public navCtrl: NavController, private modalCtrl: ModalController, private examService: ExamService, private viewCtrl: ViewController, private http: Http, public studentService: StudentService) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    
    this.googleParam = JSON.parse(localStorage.getItem('googledata'));
 console.log(this.googleParam,"my data here just test");
 this.events.publish('user:created', false, Date.now());
//  if(this.googleParam){
//   this.navCtrl.setRoot(homeNew);
//   localStorage.removeItem('googledata');    
//   console.log(this.googleParam,"my data here just test");



// }



 
  }
  // createUser(user) {
  //   console.log('User created!')
  //   this.events.publish('user:created', user, Date.now());
  // }
  ngAfterViewInit(){
   if(this.googleParam){
     console.log('itsworking');
     
    localStorage.removeItem('googledata');    

  this.navCtrl.setRoot(homeNew);
  console.log(this.googleParam,"my data here just test");



}
  }
 

  ngOnInit() {
    
    

    this.date = new Date();
    this.userData = JSON.parse(localStorage.getItem('userData'));
    console.log(this.userData, "user data")
    if (!this.userData) {
      localStorage.removeItem('userData');
      this.navCtrl.push(Login);
    }
    this.greetingFn();
    this.username = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
      return letter.toUpperCase();
    })
    //dashbord info

    this.studentService.dashboardInfo().subscribe(
      res => {
        this.dashInfo = res.data;
        if(this.dashInfo.message.message){
          this.live = this.dashInfo.message.message;
          this.liveDiv = true;
        }else{
          this.liveDiv = false;
        }

        if (res.data.exam && res.data.study) {

          for (let item of res.data.study.subjectArray) {
            if (item.title == "chapter") this.chapterId = item.id;
            if (item.title == "subject") this.subjectId = item.id;
          }
          this.examonly = true;
          this.studyonly = true;
          this.examName = res.data.exam.examName;
          this.examId = res.data.exam._id;
          this.chapterName = res.data.study.subjectArray[2].name;
          this.sectionName = res.data.study.subjectArray[0].name;
        } else if (!res.data.exam && res.data.study) {
          this.studyonly = true;
          this.examonly = false;
          this.chapterName = res.data.study.subjectArray[2].name;
          this.sectionName = res.data.study.subjectArray[0].name;
        } else if (res.data.exam && !res.data.study) {
          this.examonly = true;
          this.studyonly = false;
          this.examName = res.data.exam.examName;
        } else {
          this.examonly = false;
          this.studyonly = false;
        }

        if ((this.chapterName) && (this.sectionName)) {
          this.learnDiv = true;
          this.learnBig = false;
          this.GreenDivF = true;
          this.welcomeDiv = false;
        } else { this.learnBig = true;this.liveDiv = false; this.learnDiv = false; this.welcomeDiv = true; this.GreenDivF = false; }


      }, error => {
        console.log(error);
        localStorage.removeItem('userData');
        this.navCtrl.push(Login);
      }
    )
    let data = {
      'activeUserId': this.userData._id,
      'userId': this.userData.userId
    }
    this.http.post(`${environment.backendUrl}/getSubjects`, data)
      .map(res => res.json())
      .subscribe(
        res => {
          localStorage.setItem('subjectData', JSON.stringify(res.data));
        },
        error => { }
      )
  }
  myShowFunction(){
  }
  myHideFunction(){
  }
  greetingFn() {
    let time = this.date.getHours();
    if (this.date.getHours() < 12 && this.date.getHours() > 0) {
      this.greeting = "Good Morning";
    } else if (this.date.getHours() >= 12 && this.date.getHours() <= 17) {
      this.greeting = "Good Afternoon";
    } else {
      this.greeting = "Good evening";
      console.log(this.date.getHours())
    }
  }
  firtChap() {
    this.navCtrl.push(ListPage);
  }
  grantTest() {
    this.navCtrl.push(CreateExam);
  }
  gotoLearn() {

    this.navCtrl.push(Questions, { subject: this.chapterId });
  }
  golive(){
    this.navCtrl.push(examComponent);
  }

  goLastExam() {
    this.examService.loadExam(this.dashInfo.exam._id).subscribe(
      res => {
        if (res.data.userExamObj) {
          (res.data.userExamObj.status == "completed") ? this.navCtrl.push(Details, { ExamIds: res.data.userExamObj.examId }) : this.navCtrl.push(writeExam, { subject: this.dashInfo.exam._id });
        }
        else console.log("OOPS ! Something went wrong.", "error");

      });

  }
  gotoLeranPage() {
    this.myTabs.indexPass(1)
    // this._navBar.indexPass(1)
    this.navCtrl.push(ListPage);
  }
  gotoExamPage() {
    this.navCtrl.push(examComponent);
  }
  gotoEvaluatePage() {
    this.navCtrl.push(Evaluate);
  }
  writeExam() {
    this.navCtrl.push(Note);
  }
  mockreg() {

    this.navCtrl.push(weekendReg);


  }
}
