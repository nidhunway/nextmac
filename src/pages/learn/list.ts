import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { StudentService } from '../../services/student.service'
import { Questions } from '../../pages/learn/questions/questions';
import { homeNew } from '../../pages/homeNew/homeNew';
import { HomePage } from '../../pages/dashboard/home';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
  providers: [StudentService]
})
export class ListPage {
  section;
  chapter;
  userData;
  subjectList;
  activeUserId: string;
  userDetailsId: string;
  userId: string;
  showSections: boolean = false;
  showChapter: boolean = false;
  chapterID;
  sectionID;
  subjectID;
  selectSub;
  selectCha;
  selectchpater;
  walkdis: boolean = false;
  tabBarElement;
  constructor(public navCtrl: NavController, private studentService: StudentService, public platform: Platform) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.activeUserId = this.userData._id;
    this.userDetailsId = this.userData.userDetailsId;
    this.userId = this.userData.userId;
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

    this.getSubject(this.userId, this.activeUserId);
    this.walkthroug()
    this.tabBarElement.style.display = 'flex';
  }
  ionViewWillEnter(): void {
    this.tabBarElement.style.display = 'flex';

    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
 
  private backButtonFunc(): void {
    this.navCtrl.setRoot(HomePage);
  }
  walkthroug(){
    this.studentService.dashboardInfo().subscribe(
      res => {
        if (res.data.exam) this.walkdis = false;
        else this.walkdis = true;

      })
  }

  myShowFunction() {
  }
  myHideFunction() {
  }
  ViewSections(id, index) {
    this.subjectID = id;
    this.selectSub = index;
    this.selectCha = null;
    this.selectchpater = null;
    this.showSections = true;
    this.section = this.subjectList[index].children;
    this.chapter = [];
    this.showChapter = false;

  }

  Viewchapters(id, index) {
    this.sectionID = id;
    this.selectCha = index;
    this.showChapter = true;
    this.chapter = this.section[index].children;
  }

  getSubject(userId, activeUserId) {
    this.studentService.getSubjects(userId, activeUserId).
      subscribe(
        res => {
          this.subjectList = res.data;
        },
        error => console.log(error)
      )
  }

  goLearn(id, i) {
    this.chapterID = id;
    this.selectchpater = i;
    this.selectSub = null;
    this.selectCha = null;
    this.tabBarElement.style.display = 'none';

    this.navCtrl.push(Questions, { subject: this.subjectID + "/" + this.sectionID + "/" + this.chapterID });

    this.showChapter = false;
    this.showSections = false;
  }


}
