import { Component } from '@angular/core';
import { NavParams, ViewController, IonicPage, Platform } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'note.modal.html'
})
export class HintModalPage {
  myParam: string;
  text;
  note;
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public platform: Platform
  ) {
    this.myParam = params.get('myParam');
    console.log("myparams", this.myParam)
  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }

  private backButtonFunc(): void {
    this.viewCtrl.dismiss();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  saveNote(data) {

    this.viewCtrl.dismiss(data);

  }

}
