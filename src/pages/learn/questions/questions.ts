import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform } from 'ionic-angular';
import { StudentService } from '../../../services/student.service'
import { NotesService } from '../../../services/notes.service';
import { QuestionService } from '../../../services/question.service';
import { HintModalPage } from '../../../pages/learn/questions/note/note.modal';
import { editModal } from '../../../pages/learn/questions/editModal/editModal';
import { ReportProb } from '../../../pages/learn/questions/report-questions/report';
import { ListPage } from '../../../pages/learn/list';
import { ToastController } from 'ionic-angular';
import { AuthService } from '../../../services/auth.service';
import { Content } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { environment } from '../../../environments/environment'

import { Events } from 'ionic-angular';

@Component({
  selector: 'page-questions',
  templateUrl: 'questions.html',
  providers: [StudentService, NotesService, QuestionService, AuthService]

})
export class Questions {

  @ViewChild("contentRef") contentHandle: Content;
  url = environment.backendUrl + '/images/';
  private tabBarHeight;
  private topOrBottom: string;
  private contentBox;
  public subjectId;
  public sectionId;
  public chapterId;
  activeUserId: string;
  userDetailsId: string;
  answerForClass;
  userId: string;
  noteId;
  star: boolean = false;
  quickview: boolean = false;
  listview: boolean = false;
  listStarView: boolean = false;
  starredView: boolean = false;
  questionData;
  questionDetails: string = "";
  questionName: string = "";
  options: any;
  oldContent;
  qsNumber: number;
  firstLearnId;
  quickStarredView;
  qstnNumber: number;
  totalNumber: number;
  clickedItem: string = null;
  questionId: string;
  correctAnswer: string;
  corectQuickAnswer;
  subjectArray;
  answerStatus;
  pageLoadNote;
  questionquickData;
  noteLength: number;
  newData;
  quickOptions;
  shownGroup;
  allNotesData;
  questionquickName;
  resultData: Array<any> = [];
  count;
  starclr: boolean;
  countClick: number = 0;
  quickStarCount: number = 0;
  listStarCount: number = 0;
  lastLearnQuestionNumber: number = 0;
  listCount: number = 0;
  questionlistData;
  learnQuestionNumber;
  questionStarData;
  imageFlag: boolean;
  questionlistStarData;
  imageUrl;
  quickclr: boolean;
  listClr: boolean;
  noData: boolean;
  userAnswer;
  currentIndexToShow: number = 0;
  questions;
  Qnumber: number;
  learnQuestionPalette;
  answers;
  description;
  question_image;
  imgUrl;
  totalId;
  questionID;
  lastLearnQuestionId;
  fields;
  noQuestion: boolean = false;
  noPrevQuestion: boolean = false;
  tabBarElement: any;

  constructor(public navCtrl: NavController,public events: Events, public platform: Platform, private __auth: AuthService, public modalCtrl: ModalController, public toastCtrl: ToastController, public alertCtrl: AlertController, private studentService: StudentService, public navParams: NavParams, private questionService: QuestionService, private noteService: NotesService) {
    this.totalId = navParams.get('subject')
    this.fields = this.totalId.split('/');
    this.subjectId = this.fields[0]
    this.sectionId = this.fields[1];
    this.chapterId = this.fields[2];
    this.questionId = this.fields[3];
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    if (!this.chapterId) {
      this.navCtrl.push(ListPage);
    }
    this.events.publish('user:created', true, Date.now());

  }
  // createUser(user) {
  //   console.log('User created!')
  //   this.events.publish('user:created', user, Date.now());
  // }


  ionViewWillEnter(): void {
    this.tabBarElement.style.display = 'none';
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }
  private backButtonFunc(): void {
    this.navCtrl.setRoot(ListPage);
  }
  staredtoogle(event) {
    if (event._value) {
      this.studentService.getStredQuestion(this.chapterId, this.subjectArray).subscribe(
        res => {
        }, err => {
        })
    }
  }
  ngOnInit() {
    this.tabBarElement.style.display = 'none';
    let userData = JSON.parse(localStorage.getItem('userData'));
    this.activeUserId = userData._id;
    this.userDetailsId = userData.userDetailsId;
    this.userId = userData.userId;
    this.loadLearn();
    if (this.questionId)
      this.getFirstLearn();
  }

  loadLearn() {
    this.studentService.loadLearnIDs(this.subjectId, this.sectionId, this.chapterId).subscribe(
      res => {
        if (res.status) {
          this.totalNumber = res.data.questionPalette.length;
          this.learnQuestionPalette = res.data.questionPalette;
          if (this.totalNumber == 0) {
            this.__auth.notificationInfo("No question found !")
            this.navCtrl.push(ListPage);
          } else {
            this.questionService.getLastQuestion(this.chapterId).subscribe(
              res => {
                if ((res.status) && (res.data.question)) {
                  this.questionData = res.data;
                  this.lastLearnQuestionId = this.questionData.question._id;
                  for (var k = 0; k < this.learnQuestionPalette.length; k++) {
                    if (this.learnQuestionPalette[k] === this.lastLearnQuestionId) {
                      this.lastLearnQuestionNumber = k + 1;
                    }
                  }
                  console.log("lasteeeeeeeeeee", this.lastLearnQuestionNumber);
                  console.log("lasteeeeedddddddddddeeeeee", this.learnQuestionPalette.length);
                  if (this.lastLearnQuestionNumber == this.learnQuestionPalette.length) {
                    this.getFirstLearn();
                  } else {
                    this.qsNumber = this.lastLearnQuestionNumber;
                    this.continueQus();
                    this.loadnotes();
                  }
                }
                else {
                  this.getFirstLearn();
                }
              });
          }
        }
        else {
          this.__auth.notificationInfo("No question found !")
          this.navCtrl.push(ListPage);
        }
      });

  }
  getFirstLearn() {
    this.studentService.loadLearnIDs(this.subjectId, this.sectionId, this.chapterId).subscribe(
      res => {
        if (res) {
          this.learnQuestionPalette = res.data.questionPalette;
          this.firstLearnId = this.learnQuestionPalette[0];
          if (this.questionId) {
            for (var k = 0; k < this.learnQuestionPalette.length; k++) {
              if (this.learnQuestionPalette[k] === this.questionId) {
                this.learnQuestionNumber = k + 1;
              }
            }
          }
          if (this.questionId) {
            this.firstLearnId = this.questionId;
          } else {
            this.firstLearnId;
          }
          this.studentService.getLearnQuestion(this.firstLearnId).subscribe(
            res => {
              if (res) {
                this.questionData = res.data;
                if (this.questionId) {
                  this.qsNumber = this.learnQuestionNumber;
                } else {
                  (this.questionData.question !== null) ? this.qsNumber = 1 : this.qsNumber = 0;
                }
                this.continueQus();
                this.loadnotes();
              } else {
                this.__auth.notificationInfo("No question found !")
                this.navCtrl.push(ListPage);
              }
            });
        }
        else {
          this.__auth.notificationInfo("No question found !")
          this.navCtrl.push(ListPage);
        }
      });
  }

  continueQus() {
    this.questionDetails = this.questionData.question;
    this.questionName = this.questionData.question.question;
    if (this.quickview) {
      this.questionquickName = this.questionquickData.question.question;
    }
    this.questionData.question.options ? this.options = JSON.parse(this.questionData.question.options) : this.options = [];
    this.questionId = this.questionData.question._id;
    this.subjectArray = this.questionData.question.subjectArray;
    this.correctAnswer = this.questionData.question.answer;
    if (this.questionData.answer !== null) {
      /* when user already answered this question */
      this.answerForClass = this.questionData.question.answer;
      this.clickedItem = this.questionData.answer.answer;
    } else {
      this.clickedItem = null;
      this.answerForClass = null;
    }
    if (this.questionData.notes != null) {
    }
    if (this.questionData.staredData[0]) {
      this.star = this.questionData.staredData[0].stared;
    } else {
      this.star = false
    }
  }
  previous(questionId) {
    let questionid = this.learnQuestionPalette[questionId - 2]
    if (questionid) {
      this.studentService.getLearnQuestion(questionid).subscribe(
        res => {
          if (res.data.question._id) {
            this.questionData = res.data;
            this.questionDetails = res.data.question;
            this.questionName = this.questionData.question.question;
            this.options = JSON.parse(res.data.question.options);
            (this.questionData.question !== null) ? this.qsNumber-- : this.qsNumber = 0;
            this.questionId = this.questionData.question._id;
            this.subjectArray = this.questionData.question.subjectArray;
            this.correctAnswer = this.questionData.question.answer;
            if (this.quickview) {
              this.questionquickData = res.data;
              if (this.questionquickData.staredData[0]) {
                this.star = this.questionquickData.staredData[0].stared;
              } else {
                this.star = false
              }
              this.corectQuickAnswer = this.questionquickData.question.answer;
              this.questionquickName = this.questionquickData.question.question;
              (this.questionquickData.question !== null) ? this.qstnNumber-- : this.qstnNumber = 0;
              if (this.questionquickData.question.question_image) {
                this.imageFlag = true;
                this.imgUrl = this.url + this.questionquickData.question.question_image;
              }
              else {
                this.imageFlag = false;
              }
            }
            if (this.questionData.answer !== null) {
              /* when user already answered this question */
              this.answerForClass = this.questionData.question.answer;
              this.clickedItem = this.questionData.answer.answer;
            } else {
              this.clickedItem = null;
              this.answerForClass = null;
            }
            if (this.questionData.notes !== null) {
              // this.notesData = this.questionData.notes;
            }
            if (this.questionData.staredData[0]) {
              this.star = this.questionData.staredData[0].stared;
            } else {
              this.star = false
            }
          }
          this.loadnotes();
        });
    }
  }
  next(questionId) {
    let questionid = this.learnQuestionPalette[questionId]
    if (questionid) { // Saving last question attended
      if (!this.quickview) {
        this.questionService.createLastQuestion(this.chapterId, this.qsNumber, questionid).subscribe(
          res => {
          });
      }
      this.studentService.getLearnQuestion(questionid).subscribe(
        res => {
          if (res.data.question.question) {
            this.questionData = res.data;
            this.questionDetails = res.data.question;
            this.questionName = this.questionData.question.question;
            (this.questionData.question !== null) ? this.qsNumber++ : this.qsNumber = 0;
            if (res.data.question.options) {
              this.options = JSON.parse(res.data.question.options);
            }
            if (this.quickview) {
              this.questionquickData = res.data;
              if (this.questionquickData.staredData[0]) {
                this.star = this.questionquickData.staredData[0].stared;
              } else {
                this.star = false
              }
              if (res.data.question.options) {
                this.quickOptions = JSON.parse(res.data.question.options);
              }
              this.corectQuickAnswer = this.questionquickData.question.answer;
              this.questionquickName = this.questionquickData.question.question;
              (this.questionquickData.question !== null) ? this.qstnNumber++ : this.qstnNumber = 0;
              if (this.questionquickData.question.question_image) {
                this.imageFlag = true;
                this.imgUrl = this.url + this.questionquickData.question.question_image;
              }
              else {
                this.imageFlag = false;
              }
            }
            this.questionId = this.questionData.question._id;
            this.subjectArray = this.questionData.question.subjectArray;
            this.correctAnswer = this.questionData.question.answer;
            if (this.questionData.answer !== null) {
              /* when user already answered this question */
              this.answerForClass = this.questionData.question.answer;
              this.clickedItem = this.questionData.answer.answer;
            } else {
              this.clickedItem = null;
              this.answerForClass = null;
            }
            if (this.questionData.notes !== null) {
              // this.notesData = this.questionData.notes;
            }

            if (this.questionData.staredData[0]) {
              this.star = this.questionData.staredData[0].stared;
            } else {
              this.star = false
            }
          }
          else {
            this.__auth.notificationInfo("No question found !")
            this.navCtrl.push(ListPage);
          }
          this.loadnotes();
        });
    }
  }

  quickView() {
    this.countClick++;
    this.listview = false;
    if (this.countClick % 2 == 0) {
      this.quickclr = false;
      this.quickview = false;
      this.starredView = false;
      this.listClr = false;
      this.starclr = false;
      this.listview = false;
      this.loadLearn();
    } else {
      this.quickclr = true;
      this.starclr = false;
      this.listClr = false;
      this.starredView = false;
      this.quickview = true;
      this.listview = false;
      this.studentService.getQuestion(this.chapterId).subscribe(
        res => {
          if (res.data.question.question) {
            this.questionquickData = res.data;
            this.questionquickName = this.questionquickData.question.question;
            this.corectQuickAnswer = this.questionquickData.question.answer;

            (this.questionquickData.question !== null) ? this.qstnNumber = 1 : this.qstnNumber = 0;
            if (this.questionquickData.question.question_image) {
              this.imageFlag = true;
              this.imgUrl = this.url + this.questionquickData.question.question_image;
            }
            else {
              this.imageFlag = false;
            }
            if (this.questionquickData.staredData[0]) {
              this.star = this.questionquickData.staredData[0].stared;
            } else {
              this.star = false
            }
            this.loadnotes();
          }
          else {
            this.__auth.notificationInfo("No question found !")
            this.navCtrl.push(ListPage);
          }
        });
    }
  }
  listView() {
    this.listCount++;
    this.quickview = false;
    this.starredView = false;
    this.listview = false;
    if (this.listCount % 2 == 0) {
      this.listClr = false;
      this.quickclr = false;
      this.starclr = false;
      this.listview = false;
      this.loadLearn();
    } else {
      this.listClr = true;
      this.quickclr = false;
      this.starclr = false;
      this.listview = true;
      this.quickview = false;
      this.starredView = false;
      this.studentService.getListQuestion(this.chapterId).subscribe(
        res => {
          if (res.data) {
            this.questionlistData = res.data;
            let data = (res.data).map(item => {
              item.options = JSON.parse(item.options);
              return item;
            });
            if (data) {
              this.questionlistData = data;
            }
          }
          else {
            this.__auth.notificationInfo("No question found !")
            this.navCtrl.push(ListPage);
          }
        });
    }
  }
  listStarview() {
    this.listStarCount++;
    this.quickview = false;
    this.starredView = false;
    this.listStarView = false;
    this.listview = false;
    if (this.listStarCount % 2 == 0) {
      this.listClr = false;
      this.quickclr = false;
      this.starclr = false;
      this.listview = false;
      this.listStarView = false;
      this.quickStarredView = false;
      this.logEvent("liststar");
    } else {
      this.listClr = true;
      this.quickclr = false;
      this.starclr = false;
      this.listview = false;
      this.quickview = false;
      this.starredView = true;
      this.listStarView = true;
      this.quickStarredView = false;
      this.resultData.length = 0;
      this.questionService.getAllStaredQuestion(this.subjectArray).subscribe(
        res => {
          if (res.data.length > 0) {
            this.resultData.length = 0;
            this.questionlistStarData = res.data;
            let data = (res.data).map(item => {
              item.options = JSON.parse(item.question[0].options);
              item.questions = item.question[0].question;
              item.description = item.question[0].description;
              item.answers = item.question[0].answer;
              item.question_image = item.question[0].question_image;
              return item;
            });
            if (data) {
              this.questionlistStarData = data;
            }
          } else if (res.data.length == 0) {
            this.resultData.length = 0;
            this.questionlistStarData = res.data;
            this.__auth.notificationInfo("No question found !")
          }
          else {
            this.__auth.notificationInfo("No question found !")
            this.navCtrl.push(ListPage);
          }
        });
    }
  }
  quickStarView(e) {
    if (e == "quickPage") {
      this.quickStarCount;
    } else {
      this.quickStarCount++;
    }
    this.quickview = false;
    this.starredView = false;
    this.listStarView = false;
    this.listview = false;
    if (this.quickStarCount % 2 == 0) {
      this.listClr = false;
      this.quickclr = false;
      this.starclr = false;
      this.listview = false;
      this.quickStarredView = false;
      this.listStarView = false;
      this.logEvent("liststar");
    } else {
      this.listClr = false;
      this.quickclr = true;
      this.starclr = false;
      this.listview = false;
      this.quickview = false;
      this.starredView = true;
      this.listStarView = false;
      this.quickStarredView = true;
      this.resultData.length = 0;
      this.questionService.getAllStaredQuestion(this.subjectArray).subscribe(
        res => {
          this.currentIndexToShow = 0;
          if (res.data.length == 1) {
            this.noQuestion = true;
            this.noPrevQuestion = true;
          }
          if (res.data.length > 0) {
            this.noData = false;
            this.questionStarData = res.data;
            this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
            (this.questions !== null) ? this.Qnumber = 1 : this.Qnumber = 0;
            this.noPrevQuestion = true;
            if (this.Qnumber == res.data.length) {
              this.noQuestion = true;
            } else {
              this.noQuestion = false;
            }
            this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
            this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
            this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
            if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
              this.imageFlag = true;
              this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            }
            else {
              this.imageFlag = false;
            }
            this.star = true;
            this.loadnotes();
          } else if (res.data.length == 0) {
            this.noData = true;
            this.resultData.length = 0;
            this.star = false;
            this.__auth.notificationInfo("No question found !")
          }
          else {
            this.noData = true;
            this.star = false;
            this.resultData.length = 0;
            this.__auth.notificationInfo("No question found !")
            this.navCtrl.push(ListPage);
          }
        });
    }
  }
  logEvent(e) {
    if (e._value == true || e == true || e == "liststar") {
      this.quickview = false;
      this.starredView = true;
      this.quickStarredView = false;
      this.listStarView = false;
      this.listview = false;
      this.listClr = false;
      this.quickclr = false;
      this.starclr = true;
      this.noData = false;
      this.questionService.getAllStaredQuestion(this.subjectArray).subscribe(
        res => {
          this.currentIndexToShow = 0;
          if (res.data.length == 1) {
            this.noQuestion = true;
            this.noPrevQuestion = true;
          }
          if (res.data.length > 0) {
            this.noData = false;
            this.questionStarData = res.data;
            this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
            if (this.questionStarData[this.currentIndexToShow].answer != null) {
              this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
            }
            (this.questions !== null) ? this.Qnumber = 1 : this.Qnumber = 0;
            this.noPrevQuestion = true;
            if (this.Qnumber == res.data.length) {
              this.noQuestion = true;
            } else {
              this.noQuestion = false;
            }
            this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
            this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
            this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
            if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
              this.imageFlag = true;
              this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
            }
            else {
              this.imageFlag = false;
            }
            this.star = true;
            this.loadnotes();
          } else if (res.data.length == 0) {
            this.noData = true;
            this.resultData.length = 0;
            this.star = false;
            this.__auth.notificationInfo("No question found !")
          }
          else {
            this.noData = true;
            this.star = false;
            this.resultData.length = 0;
            this.__auth.notificationInfo("No question found !")
            this.navCtrl.push(ListPage);
          }
        });
    } else {
      this.quickview = false;
      this.starredView = false;
      this.listStarView = false;
      this.quickStarredView = false;
      this.listview = false;
      this.listClr = false;
      this.quickclr = false;
      this.starclr = false;
      this.loadLearn();
    }
  }
  nextButtonClick() {
    this.noPrevQuestion = false;
    this.currentIndexToShow++;
    if (this.currentIndexToShow == this.questionStarData.length - 1) {
      this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
      this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
      this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
      if (this.questionStarData[this.currentIndexToShow].answer != null) {
        this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
      }
      this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
      (this.questions !== null) ? this.Qnumber++ : this.Qnumber = 0;
      this.noQuestion = true;
      if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      }
      else {
        this.imageFlag = false;
      }
      this.loadnotes();
    } else {
      this.noQuestion = false;
      this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
      this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
      if (this.questionStarData[this.currentIndexToShow].answer != null) {
        this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
      }
      (this.questions !== null) ? this.Qnumber++ : this.Qnumber = 0;
      this.description = this.questionStarData[this.currentIndexToShow].question[0].description;
      this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
      if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      }
      else {
        this.imageFlag = false;
      }
      this.loadnotes();
    }
  }
  previousButtonClick() {
    this.noQuestion = false
    this.currentIndexToShow--;
    if (this.currentIndexToShow > 0) {
      this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
      this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
      if (this.questionStarData[this.currentIndexToShow].answer != null) {
        this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
      }
      (this.questions !== null) ? this.Qnumber-- : this.Qnumber = 0;
      this.description = this.questionStarData[this.currentIndexToShow].question[0].description;

      this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
      if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      }
      else {
        this.imageFlag = false;
      }
      this.loadnotes();
    } else {
      this.noPrevQuestion = true;
      this.questions = this.questionStarData[this.currentIndexToShow].question[0].question;
      this.answers = this.questionStarData[this.currentIndexToShow].question[0].answer;
      if (this.questionStarData[this.currentIndexToShow].answer != null) {
        this.userAnswer = this.questionStarData[this.currentIndexToShow].answer.answer;
      }
      (this.questions !== null) ? this.Qnumber-- : this.Qnumber = 0;
      this.description = this.questionStarData[this.currentIndexToShow].question[0].description;

      this.question_image = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      this.options = JSON.parse(this.questionStarData[this.currentIndexToShow].question[0].options);
      if (this.questionStarData[this.currentIndexToShow].question[0].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.questionStarData[this.currentIndexToShow].question[0].question_image;
      }
      else {
        this.imageFlag = false;
      }
      this.loadnotes();
    }
  }
  findImage(i) {
    if (this.listStarview) {
      if (this.questionlistStarData[i].question_image.length > 0) {
        this.imageFlag = true;
        this.imageUrl = this.questionlistStarData[i].question_image;
      } else {
        this.imageFlag = false;
      }
    } else {
      if (this.questionlistData[i].question_image.length > 0) {
        this.imageFlag = true;
        this.imageUrl = this.questionlistData[i].question_image;
      } else {
        this.imageFlag = false;
      }
    }
  }
  loadnotes() {
    if (this.starredView) {
      this.resultData.length = 0;
      if (this.questionStarData[this.currentIndexToShow].notes.length > 0) {
        this.resultData.length = 0;
        let loadMore = false;
        this.noteService.fetchAllNotes(loadMore).subscribe(
          res => {
            if (res) {
              if (res.data.notes.length > 0) {
                this.pageLoadNote = res.data.notes
                for (let i = 0; i < res.data.notes.length; i++) {
                  this.newData = (res.data.notes).map(item => {
                    return item
                  });
                }
                this.allNotesData = this.newData;
                this.resultData.length = 0;
                for (var k = 0; k < this.allNotesData.length; k++) {
                  if (this.starredView) {
                    if (this.noData) {
                      this.resultData.length = 0;
                    } else if (this.allNotesData[k]["questionId"] === this.questionStarData[this.currentIndexToShow].question[0]._id) {
                      this.resultData.push(this.allNotesData[k]);
                      this.noteLength = this.resultData.length;
                    }
                  }
                }
              } else {
                this.resultData.length = 0;
              }
            }
          });
      }
    } else
      if (this.questionData.notes.length > 0) {
        this.resultData.length = 0;
        let loadMore = false;
        this.noteService.fetchAllNotes(loadMore).subscribe(
          res => {
            if (res) {
              if (res.data.notes.length > 0) {
                this.pageLoadNote = res.data.notes
                for (let i = 0; i < res.data.notes.length; i++) {
                  this.newData = (res.data.notes).map(item => {
                    return item
                  });
                }
                this.allNotesData = this.newData;
                this.resultData.length = 0;
                for (var k = 0; k < this.allNotesData.length; k++) {
                  if (this.quickview) {
                    if (this.allNotesData[k]["questionId"] === this.questionquickData.question._id) {
                      this.resultData.push(this.allNotesData[k]);
                      this.noteLength = this.resultData.length;
                    }
                  } else if (this.starredView) {
                    if (this.noData) {
                      this.resultData.length = 0;
                    } else if (this.allNotesData[k]["questionId"] === this.questionStarData[this.currentIndexToShow].question[0]._id) {
                      this.resultData.push(this.allNotesData[k]);
                      this.noteLength = this.resultData.length;
                    }
                  } else {
                    if (this.allNotesData[k]["questionId"] === this.questionData.question._id) {
                      this.resultData.push(this.allNotesData[k]);
                      this.noteLength = this.resultData.length;
                    }
                  }
                }
              } else {
                this.resultData.length = 0;
              }
            }
          });
      } else {
        this.resultData.length = 0;
      }
  }
  addNotes() {
    let chooseModal = this.modalCtrl.create(HintModalPage, null, { cssClass: 'inset-modalNote' });
    chooseModal.onDidDismiss(content => {
      if (content) {
        if (this.quickview) {
          this.noteService.createNote(content, this.questionquickData.question._id, this.subjectArray).subscribe(
            res => {
              if (res.status === 1) {
                this.resultData.length = this.resultData.length + 1;
              } else {
                this.__auth.notificationInfo('OOPS! Something went wrong')
              }
            });
        } else if (this.starredView) {
          this.noteService.createNote(content, this.questionStarData[this.currentIndexToShow].question[0]._id, this.subjectArray).subscribe(
            res => {
              if (res.status === 1) {
                this.resultData.length = this.resultData.length + 1;
              } else {
                this.__auth.notificationInfo('OOPS! Something went wrong')
              }
            });
        } else {
          this.noteService.createNote(content, this.questionData.question._id, this.subjectArray).subscribe(
            res => {
              if (res.status === 1) {
                this.resultData.length = this.resultData.length + 1;
              } else {
                this.__auth.notificationInfo('OOPS! Something went wrong')
              }
            });
        }
      }
    })
    chooseModal.present();
  }

  editNote() {
    let loadMore = false
    this.noteService.fetchAllNotes(loadMore).subscribe(
      res => {
        this.pageLoadNote = res.data.notes
        if (res.data.notes.length > 0) {
          for (let i = 0; i < res.data.notes.length; i++) {
            this.newData = (res.data.notes).map(item => {
              return item
            });
          }
        }
        this.allNotesData = this.newData;
        this.resultData.length = 0;
        for (var k = 0; k < this.allNotesData.length; k++) {
          if (this.quickview) {
            if (this.allNotesData[k]["questionId"] === this.questionquickData.question._id) {
              this.resultData.push(this.allNotesData[k]);
              this.oldContent = this.resultData[0].noteContent;
              this.noteId = this.resultData[0]._id;
            }
          } else if (this.starredView) {
            if (this.allNotesData[k]["questionId"] === this.questionStarData[this.currentIndexToShow].question[0]._id) {
              this.resultData.push(this.allNotesData[k]);
              this.oldContent = this.resultData[0].noteContent;
              this.noteId = this.resultData[0]._id;
            }
          } else {
            if (this.allNotesData[k]["questionId"] === this.questionData.question._id) {
              this.resultData.push(this.allNotesData[k]);
              this.oldContent = this.resultData[0].noteContent;
              this.noteId = this.resultData[0]._id;
            }
          }
        }
        let chooseEditModal = this.modalCtrl.create(editModal, { oldContent: this.oldContent, noteId: this.noteId }, { cssClass: 'inset-modal' });
        chooseEditModal.onDidDismiss(content => {
          if (content == "dataDeletedSuccessfully") {
            this.resultData.length = 0;
          } else {
            let noteId = this.resultData[0]._id;
            if (content) {
              this.noteService.editNote(noteId, content, this.subjectArray).subscribe(
                res => {
                  if (res.status === 1) {
                  } else {
                    this.__auth.notificationInfo('OOPS! Something went wrong')
                  }
                })
            }
          }
        })
        chooseEditModal.present();
      });
  }

  starQuestions() {
    if (this.starredView) {
      if (this.noData) {
        this.star = false;
      } else {
        this.star = !this.star;
      }
      this.questionService.starQuestion(this.questionStarData[this.currentIndexToShow].question[0]._id, this.subjectArray, this.star)
        .subscribe(
          res => {
            if (!res.status) this.__auth.notificationInfo('OOPS! Something went wrong')
          }
        )
      if (this.quickStarredView) {
        this.quickStarView("quickPage");
      } else {
        this.logEvent(true);
      }
    } else if (this.quickview) {
      this.star = !this.star;
      this.questionService.starQuestion(this.questionquickData.question._id, this.subjectArray, this.star)
        .subscribe(
          res => {
            if (!res.status) this.__auth.notificationInfo('OOPS! Something went wrong')
          }
        )
    }
    else {
      this.star = !this.star;
      this.questionService.starQuestion(this.questionId, this.subjectArray, this.star)
        .subscribe(
          res => {
            if (!res.status) this.__auth.notificationInfo('OOPS! Something went wrong')
          }
        )
    }
  }

  ReportQue() {
    let chooseModal = this.modalCtrl.create(ReportProb, null, { cssClass: 'inset-modal' });
    chooseModal.onDidDismiss(data => {
      if (data) {
        this.questionService.createProblem(data, this.questionDetails).subscribe(
          res => {
            if (res) {
              if (res.status) {
                this.__auth.notificationInfo(res.message)
              } else {
                this.__auth.notificationInfo(res.message)
              }
            }
            else {
              this.__auth.notificationInfo('OOPS! Something went wrong')
            }
          });
      }
    });
    chooseModal.present();
  }

  toggleDetails(item) {
    if (this.isGroupShown(item)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = item;
    }
  }

  isGroupShown(item) {
    return this.shownGroup === item;
  }

  ionViewDidEnter() {
    this.topOrBottom = this.contentHandle._tabsPlacement;
    this.contentBox = document.querySelector(".scroll-content");

    if (this.topOrBottom == "top") {
      this.tabBarHeight = this.contentBox.marginTop;
    } else if (this.topOrBottom == "bottom") {
      this.tabBarHeight = this.contentBox.marginBottom;
    }
  }

  scrollingFun(e) {
    if (e.scrollTop > this.contentHandle.getContentDimensions().contentHeight) {
      if (this.topOrBottom == "top") {
        this.contentBox.marginTop = 0;
      } else if (this.topOrBottom == "bottom") {
        this.contentBox.marginBottom = 0;
      }

    } else {
      if (this.topOrBottom == "top") {
        this.contentBox.marginTop = this.tabBarHeight;
      } else if (this.topOrBottom == "bottom") {
        this.contentBox.marginBottom = this.tabBarHeight;
      }
    }
  }

  goBackto() {
    this.navCtrl.push(ListPage);
    this.chapterId = "";
  }

  finishLearn() {
    this.navCtrl.push(ListPage);
  }

  selectedOption(id) {
    if (id === this.correctAnswer) {
      this.answerStatus = "correct";
    } else {
      this.answerStatus = "wrong";
    }
    if (this.questionData.answer === null) {
      this.studentService.saveAnswer(this.questionId, id, this.answerStatus, this.subjectArray).subscribe(
        res => {
        }
      );
    }
    this.answerStatus = "attended";
    this.answerForClass = this.correctAnswer;
    this.clickedItem = id;
  }
}
