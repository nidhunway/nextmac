import { Component } from '@angular/core';
import { NavParams, ViewController, IonicPage, AlertController, Platform} from 'ionic-angular';
import { NotesService } from '../../../../services/notes.service';
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'editModal',
  templateUrl: 'editModal.html',
  providers: [NotesService]

})
export class editModal {
  myParam: string;
  text;
  note;
  myParamId;
  constructor(
   public toastCtrl: ToastController,public noteService: NotesService,
    private alertCtrl:AlertController,
    public viewCtrl: ViewController,
    params: NavParams,
    public platform: Platform
  ) {
    this.myParam = params.get('oldContent');
    this.myParamId = params.get('noteId');
    console.log("myparams", this.myParamId)
    if(this.myParam){
    this.note=this.myParam;
    }
  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }

  private backButtonFunc(): void {
    this.viewCtrl.dismiss();
  }
  deleteNote() {
    const alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure want to delete this note?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            // let noteId = this.allNotesData[index]._id;
            this.noteService.deleteNote(this.myParamId).subscribe(
              res => {
                const toast = this.toastCtrl.create({
                  message: 'Note deleted.',
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
              },
              error => {
                const toast = this.toastCtrl.create({
                  message: 'OOPS! Something went wrong',
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
              });
              this.viewCtrl.dismiss("dataDeletedSuccessfully");
          }
         
        },
        {
          text: 'No',
          handler: () => {
            console.log('No');
          }
        }
      ]
    });

    alert.present();
 
}
  dismiss() {
    this.viewCtrl.dismiss();
  }
  saveNote(data) {
    this.viewCtrl.dismiss(data);

  }

}
