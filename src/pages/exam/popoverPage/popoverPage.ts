import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { ExamService } from '../../../services/exam.service';
import { AuthService } from '../../../services/auth.service';
import { HomePage } from '../../../pages/dashboard/home';

@Component({
  selector: 'page-popoverPage',
  templateUrl: 'popoverPage.html',
  providers: [ExamService, AuthService]

})

export class PopoverPage {
  custmizedExam = [];
  mode;
  myParam;
  customized: boolean = false;
  week: boolean = false;
  daily: boolean = false;
  demoExam: boolean = false;
  completedWeekExam = [];
  pendingWeekexam = [];
  demoExams = [];
  dailyExams = [];
  totalWeek = [];
  noOfQstn;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public platform: Platform, private examService: ExamService, public __auth: AuthService, public params: NavParams, ) {
    // this.myParam = params.get('ev');
    this.myParam = params.get('myEvent');


    if (this.myParam == "week") {
      this.week = true;
    }
    else if (this.myParam == "customized") {
      this.customized = true;

    } else {
      this.daily = true;
    }

  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
  private backButtonFunc(): void {
    this.viewCtrl.dismiss();
    this.navCtrl.setRoot(HomePage);

  }
  close() {
    this.viewCtrl.dismiss();
  }
  allExam(event) {

    if (event == "pending") {
      // this.custmizedExam = [];
      this.examService.fetchAllExam(event).subscribe(
        res => {

          if (res.data.examList.length > 0) {
            let customizedPending = [];
            customizedPending = res.data.examList;
            let exam = (customizedPending).map((item) => {
              item.customizedExam = true;
              item.pending = true;
              return item;

            });
            customizedPending = exam;


            this.custmizedExam = (customizedPending.length > 0) ? this.custmizedExam.concat(customizedPending) : this.custmizedExam;

          }


        })
      this.examService.fetchGrandExam().subscribe(
        res => {

          if (res.data.pendingExams.length > 0) {
            let pendingGexam = [];
            pendingGexam = (res.data.pendingExams).map((item) => {
              item.examType = "grand";
              item.examName = item.name;
              item.pending = true;
              return item;
            })

            //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            this.custmizedExam = (pendingGexam.length > 0) ? this.custmizedExam.concat(pendingGexam) : this.custmizedExam;

          }
          this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          if (this.custmizedExam.length == 0) {
            this.__auth.notificationInfo("There is no pending exams found!");
          }
          this.viewCtrl.dismiss(this.custmizedExam, event);

        })

    } else if (event == "completed") {
      // this.custmizedExam = [];
      this.examService.fetchAllExam(event).subscribe(
        res => {
          if (res.data.examList.length > 0) {
            this.custmizedExam = res.data.examList;
            let exam = (this.custmizedExam).map((item) => {
              item.customizedExam = true;
              item.completed = true;
              return item;

            });
            this.custmizedExam = exam;
            //this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          }

        }
      )
      this.examService.fetchGrandExam().subscribe(
        res => {

          let completedGexam = [];
          if (res.data.completedExam.length > 0) {
            completedGexam = (res.data.completedExam).map((item) => {
              item.examType = "grand";
              item.examName = item.name;
              item.completed = true;
              return item;
            })

            // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            this.custmizedExam = (completedGexam.length > 0) ? this.custmizedExam.concat(completedGexam) : this.custmizedExam;

          }
          this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          if (this.custmizedExam.length == 0) {
            this.__auth.notificationInfo("There is no completed exams found!");
          }
          this.viewCtrl.dismiss(this.custmizedExam, event);

        })
    } else {
      this.getAllCustomized();

    }


  }
  allWeekExam(category) {

    if (category == "pending") {
      this.demoExam = false;
      this.completedWeekExam = [];
      this.examService.getWeekExams().subscribe(
        weekRes => {
          if (weekRes.data) {

            if (weekRes.data.pendingExams.length > 0) {
              //let  pendingWeek=[];
              this.pendingWeekexam = weekRes.data.pendingExams;
              let exam = (this.pendingWeekexam).map((item) => {
                item.examName = item.examDetails.name;
                item.examPending = true;

                if (item.numberOfQuestions) {
                  item.numberOfQuestions = item.numberOfQuestions;
                } else if (item.noOfQuestions) {
                  item.numberOfQuestions = item.noOfQuestions;

                }
                //item.examType = "week";
                // this.pendingWeekExaminactive.push(item);

                return item;

              });
              this.pendingWeekexam = exam;

              // this.pendingWeekexam = this.pendingWeekexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
              this.completedWeekExam = this.pendingWeekexam;


            }

            if (weekRes.data.activeExams.length > 0) {
              let activeWeekExam = [];
              // let actWeek=[];
              activeWeekExam = weekRes.data.activeExams;
              let exam = (activeWeekExam).map((item) => {
                //  item.examName =item.examDetails.name;
                item.examPending = true;
                // if(item.examType=="week"){
                //   item.examType = "week";

                // }else if( item.examType =="daily"){
                //   item.examType = "daily";
                // }else{
                //   item.examType = "special";
                // }
                if (item.numberOfQuestions) {
                  item.numberOfQuestions = item.numberOfQuestions;
                } else if (item.noOfQuestions) {
                  item.numberOfQuestions = item.noOfQuestions;

                }
                // item.examType = "week";

                return item;

              });
              activeWeekExam = exam;
              this.completedWeekExam = (activeWeekExam.length > 0) ? this.completedWeekExam.concat(activeWeekExam) : this.completedWeekExam;

              // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

            }
            if (weekRes.data.inactiveExams.length > 0) {
              // let inactiveExam=[];
              let pendingWeekExaminactive = [];
              pendingWeekExaminactive = weekRes.data.inactiveExams;
              let exam = (pendingWeekExaminactive).map((item) => {
                item.inactiveExam = true;
                // if(item.examType=="week"){
                //   item.examType = "week";

                // }else if( item.examType =="daily"){
                //   item.examType = "daily";
                // }else{
                //   item.examType = "special";
                // }
                // item.examType = "week";
                if (item.numberOfQuestions) {
                  item.numberOfQuestions = item.numberOfQuestions;
                } else if (item.noOfQuestions) {
                  item.numberOfQuestions = item.noOfQuestions;

                }
                return item;

              });
              pendingWeekExaminactive = exam;


              this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? this.completedWeekExam.concat(pendingWeekExaminactive) : this.completedWeekExam;

              this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

            }
            let exam = (this.completedWeekExam).map((item, index) => {

              if (item.examType == "daily") {
                item.dailyExam = true;

                this.dailyExams.push(item);

              } else {
                item.dailyExam = false;
                this.totalWeek.push(item);

              }


              return item;

            });
            this.dailyExams = this.dailyExams.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            this.totalWeek = this.totalWeek.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());




            if (this.myParam == "week") {
              if (this.totalWeek.length == 0) {
                this.__auth.notificationInfo("There is no pending exams found!");
              }
              this.viewCtrl.dismiss(this.totalWeek, category);

            } else if (this.myParam == "daily") {
              if (this.dailyExams.length == 0) {
                this.__auth.notificationInfo("There is no pending exams found!");
              }
              this.viewCtrl.dismiss(this.dailyExams, category);

            }




          }
        });
    } else if (category == "completed") {
      this.demoExam = false;
      this.completedWeekExam = [];
      this.examService.getWeekExams().subscribe(
        weekRes => {
          if (weekRes.data) {

            if (weekRes.data.completedExams.length > 0) {

              this.completedWeekExam = weekRes.data.completedExams;
              let exam = (this.completedWeekExam).map((item) => {
                item.examName = item.examDetails.name;
                item.examCompleted = true;
                //item.examType = "week";
                // if(item.examType=="week"){
                //   item.examType = "week";

                // }else if( item.examType =="daily"){
                //   item.examType = "daily";
                // }else{
                //   item.examType = "special";
                // }
                if (item.numberOfQuestions) {
                  item.numberOfQuestions = item.numberOfQuestions;
                } else if (item.noOfQuestions) {
                  item.numberOfQuestions = item.noOfQuestions;

                } else if (item.examResult.totalQuestions) {
                  item.numberOfQuestions = item.examResult.totalQuestions;

                }
                if (item.examType == "daily") {
                  item.dailyExam = true;


                  this.dailyExams.push(item);

                } else {
                  item.dailyExam = false;
                  this.totalWeek.push(item);

                }

                return item;

              });
              this.completedWeekExam = exam;
              this.totalWeek = this.totalWeek.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
              this.dailyExams = this.dailyExams.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

              if (this.myParam == "week") {
                if (this.totalWeek.length == 0) {
                  this.__auth.notificationInfo("There is no completed exams found!");

                }

                this.viewCtrl.dismiss(this.totalWeek, category);


              } else if (this.myParam == "daily") {
                if (this.dailyExams.length == 0) {
                  this.__auth.notificationInfo("There is no completed exams found!");

                }
                this.viewCtrl.dismiss(this.dailyExams, category);

              }







            }

            else {
              this.__auth.notificationInfo("There is no completed exams found!");
            }

          }
        });
    } else if (category == "demo") {
      this.demoExam = true;


      this.examService.getWeekExams().subscribe(
        weekRes => {

          if (weekRes.data) {
            console.log("weekRes.data0 demo", weekRes.data);

            if (weekRes.data.activeExams.length > 0) {
              let exam = (weekRes.data.activeExams).map((item) => {
                if (item.examCategory == "demo") {
                  item.examPending = true;
                  // if(item.examType=="week"){
                  //   item.examType = "week";

                  // }else if( item.examType =="daily"){
                  //   item.examType = "daily";
                  // }else{
                  //   item.examType = "special";
                  // }
                  this.demoExams.push(item)
                }
                return item;

              });
              //             for(let i=0;i<weekRes.data.activeExams.length;i++){
              //               if(weekRes.data.activeExams[i].examCategory=="demo"){
              //                if(weekRes.data.activeExams[i].numberOfQuestions){
              //                 this.noOfQstn=weekRes.data.activeExams[i].numberOfQuestions;
              //                    }else if(weekRes.data.activeExams[i].noOfQuestions){
              //                     this.noOfQstn =weekRes.data.activeExams[i].noOfQuestions;

              //                    }else if(weekRes.data.activeExams[i].examResult.totalQuestions ){
              //                     this.noOfQstn=weekRes.data.activeExams[i].examResult.totalQuestions;

              //                    }
              // this.demoExams.push({"examName":weekRes.data.activeExams[i].examName,"examDate":weekRes.data.activeExams[i].examDate,
              // "examinformation":weekRes.data.activeExams[i].examinformation,"examPending":true,
              // "examId":weekRes.data.activeExams[i].examConfig[0].id,
              // "examType":weekRes.data.activeExams[i].examType,"numberOfQuestions":this.noOfQstn});
              //               }
              //             }

            }
            let demoCompleted = [];
            if (weekRes.data.completedExams.length > 0) {
              let exam = (weekRes.data.completedExams).map((item) => {
                if (item.examCategory == "demo") {
                  item.examCompletd = true;
                  item.examName = item.examDetails.name;
                  item.examType = item.examDetails.examType;
                  // if(item.examType=="week"){
                  //   item.examType = "week";

                  // }else if( item.examType =="daily"){
                  //   item.examType = "daily";
                  // }else{
                  //   item.examType = "special";
                  // }
                  this.demoExams.push(item)
                }
                return item;

              });
              //             for(let i=0;i<weekRes.data.completedExams.length;i++){
              //               if(weekRes.data.completedExams[i].examCategory=="demo"){
              //                 if(weekRes.data.completedExams[i].numberOfQuestions){
              //                   this.noOfQstn=weekRes.data.completedExams[i].numberOfQuestions;
              //                      }else if(weekRes.data.completedExams[i].noOfQuestions){
              //                       this.noOfQstn =weekRes.data.completedExams[i].noOfQuestions;

              //                      }else if(weekRes.data.completedExams[i].examResult.totalQuestions ){
              //                       this.noOfQstn=weekRes.data.completedExams[i].examResult.totalQuestions;

              //                      }
              // demoCompleted.push({"examName":weekRes.data.completedExams[i].examName,"examDate":weekRes.data.completedExams[i].examDate,
              // "examinformation":weekRes.data.completedExams[i].examinformation,"examCompleted":true,
              // "examId":weekRes.data.completedExams[i].examConfig[0].id,

              // "examType":weekRes.data.completedExams[i].examType,"numberOfQuestions":this.noOfQstn});
              //               }

              //             }
              //             this.demoExams = (demoCompleted.length > 0) ? this.demoExams.concat(demoCompleted) : this.demoExams;

            }
            let demoInactive = [];
            if (weekRes.data.inactiveExams.length > 0) {
              let exam = (weekRes.data.inactiveExams).map((item) => {
                if (item.examCategory == "demo") {
                  item.inactiveExam = true;
                  // if(item.examType=="week"){
                  //   item.examType = "week";

                  // }else if( item.examType =="daily"){
                  //   item.examType = "daily";
                  // }else{
                  //   item.examType = "special";
                  // }
                  this.demoExams.push(item)
                }
                return item;

              });
              // for(let i=0;i<weekRes.data.inactiveExams.length;i++){
              //   if(weekRes.data.inactiveExams[i].examCategory=="demo"){
              //     if(weekRes.data.inactiveExams[i].numberOfQuestions){
              //       this.noOfQstn=weekRes.data.inactiveExams[i].numberOfQuestions;
              //          }else if(weekRes.data.inactiveExams[i].noOfQuestions){
              //           this.noOfQstn =weekRes.data.inactiveExams[i].noOfQuestions;

              //          }else if(weekRes.data.inactiveExams[i].examResult.totalQuestions ){
              //           this.noOfQstn=weekRes.data.inactiveExams[i].examResult.totalQuestions;

              //          }
              //     demoInactive.push({"examName":weekRes.data.inactiveExams[i].examName,
              //     "examId":weekRes.data.inactiveExams[i].examConfig[0].id,
              //     "numberOfQuestions":this.noOfQstn,"examDate":weekRes.data.inactiveExams[i].examDate,"examinformation":weekRes.data.inactiveExams[i].examinformation,"inactiveExam":true,"examType":weekRes.data.inactiveExams[i].examType});
              //   }

              // }
              // this.demoExams = (demoInactive.length > 0) ? this.demoExams.concat(demoInactive) : this.demoExams;

            }
            let demoPending = [];
            if (weekRes.data.pendingExams.length > 0) {
              // for(let i=0;i<weekRes.data.pendingExams.length;i++){
              //   if(weekRes.data.pendingExams[i].examCategory=="demo"){
              //     if(weekRes.data.pendingExams[i].numberOfQuestions){
              //       this.noOfQstn=weekRes.data.pendingExams[i].numberOfQuestions;
              //          }else if(weekRes.data.pendingExams[i].noOfQuestions){
              //           this.noOfQstn =weekRes.data.pendingExams[i].noOfQuestions;

              //          }else if(weekRes.data.pendingExams[i].examResult.totalQuestions ){
              //           this.noOfQstn=weekRes.data.pendingExams[i].examResult.totalQuestions;

              //          }

              //     demoPending.push({"examName":weekRes.data.pendingExams[i].examName,"examId":weekRes.data.pendingExams[i].examConfig[0].id,"numberOfQuestions":this.noOfQstn,"examDate":weekRes.data.pendingExams[i].examDate,"examinformation":weekRes.data.pendingExams[i].examinformation,"examPending":true,"examType":weekRes.data.pendingExams[i].examType});
              //   }

              // }
              let exam = (weekRes.data.pendingExams).map((item) => {
                if (item.examCategory == "demo") {
                  item.examPending = true;
                  item.examName = item.examDetails.name;
                  item.examType = item.examDetails.examType;
                  // if(item.examType=="week"){
                  //   item.examType = "week";

                  // }else if( item.examType =="daily"){
                  //   item.examType = "daily";
                  // }else{
                  //   item.examType = "special";
                  // }
                  this.demoExams.push(item)

                }
                return item;

              });
              // this.demoExams = (demoPending.length > 0) ? this.demoExams.concat(demoPending) : this.demoExams;
              console.log("demo exams", this.demoExams);

            }
            let exam = (this.demoExams).map((item, index) => {

              if (item.examType == "daily") {
                item.dailyExam = true;

                this.dailyExams.push(item);

              } else {
                item.dailyExam = false;
                this.totalWeek.push(item);

              }


              return item;

            });

            this.completedWeekExam = exam;



            this.demoExams = this.demoExams.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());


            this.dailyExams = this.dailyExams.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            this.totalWeek = this.totalWeek.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());


            if (this.myParam == "week") {

              this.viewCtrl.dismiss(this.totalWeek, category);
              if (this.totalWeek.length == 0) {
                this.__auth.notificationInfo("There is no demo exams found!");
              }

            } else if (this.myParam == "daily") {
              this.viewCtrl.dismiss(this.dailyExams, category);
              if (this.dailyExams.length == 0) {
                this.__auth.notificationInfo("There is no demo exams found!");
              }

            }
          }
        });
    }
    else {

      this.demoExam = false;
      this.getAllWeek();
    }
  }
  getAllWeek() {
    this.examService.getWeekExams().subscribe(
      weekRes => {

        if (weekRes.data) {
          if (weekRes.data.completedExams.length > 0) {
            this.completedWeekExam = weekRes.data.completedExams;
            let exam = (this.completedWeekExam).map((item) => {
              item.examName = item.examDetails.name;
              item.examCompleted = true;
              item.examType = item.examDetails.examType;
              // if(item.examType=="week"){
              //   item.examType = "week";

              // }else if( item.examType =="daily"){
              //   item.examType = "daily";
              // }else{
              //   item.examType = "special";
              // }
              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              } else if (item.examResult.totalQuestions) {
                item.numberOfQuestions = item.examResult.totalQuestions;

              }
              // item.examType = "week";
              return item;
            });
            this.completedWeekExam = exam;
            //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          }

          if (weekRes.data.activeExams.length > 0) {
            let activeWeekExam = [];

            activeWeekExam = weekRes.data.activeExams;

            let exam = (activeWeekExam).map((item) => {
              item.examPending = true;
              //item.examType = "week";
              // if(item.examType=="week"){
              //   item.examType = "week";

              // }else if( item.examType =="daily"){
              //   item.examType = "daily";
              // }else{
              //   item.examType = "special";
              // }
              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;

            });
            activeWeekExam = exam;

            this.completedWeekExam = (activeWeekExam.length > 0) ? this.completedWeekExam.concat(activeWeekExam) : this.completedWeekExam;
            //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          }
          if (weekRes.data.inactiveExams.length > 0) {
            let pendingWeekExaminactive = [];
            pendingWeekExaminactive = weekRes.data.inactiveExams;
            let exam = (pendingWeekExaminactive).map((item) => {
              item.inactiveExam = true;
              // item.examType = "week";
              //  if(item.examType=="week"){
              //   item.examType = "week";

              // }else if( item.examType =="daily"){
              //   item.examType = "daily";
              // }else{
              //   item.examType = "special";
              // }
              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;

            });
            pendingWeekExaminactive = exam;
            this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? this.completedWeekExam.concat(pendingWeekExaminactive) : this.completedWeekExam;
            // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          }

          if (weekRes.data.pendingExams.length > 0) {
            let pendingWeekexam = [];
            pendingWeekexam = weekRes.data.pendingExams;
            let exam = (pendingWeekexam).map((item) => {
              item.examName = item.examDetails.name;
              item.examPending = true;
              item.examType = item.examDetails.examType;
              // if(item.examType=="week"){
              //   item.examType = "week";

              // }else if( item.examType =="daily"){
              //   item.examType = "daily";
              // }else{
              //   item.examType = "special";
              // }
              //item.examType = "week";
              // this.pendingWeekExaminactive.push(item);
              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;

            });
            pendingWeekexam = exam;
            pendingWeekexam = pendingWeekexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            this.completedWeekExam = (pendingWeekexam.length > 0) ? this.completedWeekExam.concat(pendingWeekexam) : this.completedWeekExam;
            this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          }
          let exam = (this.completedWeekExam).map((item, index) => {

            if (item.examType == "daily") {
              item.dailyExam = true;

              this.dailyExams.push(item);

            } else {
              item.dailyExam = false;
              this.totalWeek.push(item);

            }


            return item;

          });

          this.completedWeekExam = exam;
          this.totalWeek = this.totalWeek.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          this.dailyExams = this.dailyExams.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          if (this.myParam == "week") {
            if (this.totalWeek.length == 0) {
              this.__auth.notificationInfo("There is no Exams found!");

            }
            this.viewCtrl.dismiss(this.totalWeek, "All");

          } else if (this.myParam == "daily") {
            if (this.dailyExams.length == 0) {
              this.__auth.notificationInfo("There is no Exams found!");

            }

            this.viewCtrl.dismiss(this.dailyExams, "All");

          }


        } else {
          this.__auth.notificationInfo("There is no data found!");
        }
      });
  }
  getAllCustomized() {
    this.mode = 'completed';
    this.examService.fetchAllExam(this.mode).subscribe(
      res => {
        if (res.data.examList.length > 0) {
          this.custmizedExam = res.data.examList;
          let exam = (this.custmizedExam).map((item) => {
            item.customizedExam = true;
            item.completed = true;
            return item;

          });
          this.custmizedExam = exam;
        }
        // this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
        this.mode = 'pending';
        this.examService.fetchAllExam(this.mode).subscribe(
          res => {
            let customizedPending = [];
            if (res.data.examList.length > 0) {
              customizedPending = res.data.examList;
              let exam = (customizedPending).map((item) => {
                item.customizedExam = true;
                item.pending = true;
                return item;

              });
              customizedPending = exam;

              // customizedPending = customizedPending.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
              this.custmizedExam = (customizedPending.length > 0) ? this.custmizedExam.concat(customizedPending) : this.custmizedExam;
            }
            this.examService.fetchGrandExam().subscribe(
              res => {

                let completedGexam = [];
                if (res.data.completedExam.length > 0) {
                  completedGexam = (res.data.completedExam).map((item) => {
                    item.examType = "grand";
                    item.examName = item.name;
                    item.completed = true;
                    return item;
                  })

                  // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                  this.custmizedExam = (completedGexam.length > 0) ? this.custmizedExam.concat(completedGexam) : this.custmizedExam;

                }
                if (res.data.pendingExams.length > 0) {
                  let pendingGexam = [];
                  pendingGexam = (res.data.pendingExams).map((item) => {
                    item.examType = "grand";
                    item.examName = item.name;
                    item.pending = true;
                    return item;
                  })
                  //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                  this.custmizedExam = (pendingGexam.length > 0) ? this.custmizedExam.concat(pendingGexam) : this.custmizedExam;

                }
                this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

                if (this.custmizedExam.length == 0) {
                  this.__auth.notificationInfo("There is no exams found!");
                }

                this.viewCtrl.dismiss(this.custmizedExam, "All");
              });
          });
      });
  }

}
