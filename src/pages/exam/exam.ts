import { Component } from '@angular/core';
import { NavController, NavParams, Platform, PopoverController, ViewController } from 'ionic-angular';
import { ExamService } from '../../services/exam.service';
import { AuthService } from '../../services/auth.service';
import { CreateExam } from '../../pages/exam/createexam/createExam';
import { writeExam } from '../../pages/exam/createexam/write-exam/write-exam';
import { grantExam } from '../../pages/exam/GrandExam/grantexam.component';
import { termsrules } from '../../pages/exam/weekend/TermsAndRules/terms.component';
import { Details } from '../../pages/exam/exam-room/details/details';
import { aiimsResult } from '../../pages/exam/exam-room/aiimsResult/aiimsResult';

import { PopoverPage } from '../../pages/exam/popoverPage/popoverPage';
import { weekendReg } from '../../pages/exam/weekend/weekend.component';
import { HomePage } from '../../pages/dashboard/home';
import { aiimsExam } from '../../pages/exam/createexam/aiimsExam/aiimsExam';


@Component({
  selector: 'page-exam',
  templateUrl: 'exam.html',
  providers: [ExamService, AuthService]

})
export class Exam {
  examType;
  mode;
  userData;
  registerLabel;
  singleExamId;
  paymentId;
  postData;
  demoExams = [];
  demoExam: boolean = false;
  plan;
  isRegistered: boolean = false;
  paymentDetails;
  RegisterMsg;
  custmizedExam = [];
  examTypes;
  completedWeekExam = [];
  customized: boolean = false;
  week: boolean = false;
  pendingWeekexam = [];
  totalAiims=[];
  selectedExam;
  username;
  shownGroup = null;
  type;
  dailyExams = [];
  totalWeek = [];
  daily: boolean = false;
  aiims:boolean=false;
  aiimsdemoExam:boolean=false;
  constructor(public platform: Platform, public popoverCtrl: PopoverController, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, private examService: ExamService, public __auth: AuthService) {
    this.examType = navParams.get('examType');
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.type = "All"
    if (this.examType == "cutomized") {
      this.examTypes = "customized";
    } else if (this.examType == "week") {
      this.examTypes = "week";
    } else if (this.examType == "daily") {
      this.examTypes = "daily";
    }else{
      this.examTypes = "aiims";
    }
  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
  private backButtonFunc(): void {
    this.navCtrl.setRoot(HomePage);
    //this.viewCtrl.dismiss();
  }
  ngOnInit() {
    if (this.examType == "cutomized") {
      this.customized = true;
      this.week = false;
      this.daily = false;
      this.aiims=false;

      this.getAllCustomized();
    } else if (this.examType == "week") {
      this.week = true;
      this.customized = false;
      this.daily = false;
      this.aiims=false;

      this.getAllWeek();
    } else if (this.examType == "daily"){
      this.week = false;
      this.customized = false;
      this.daily = true;
      this.aiims=false;
      this.getAllDaily();
    }else{
      this.aiims=true
      this.week = false;
      this.customized = false;
      this.daily = false;
      this.getAllAiims();
      
    }
    this.checkAplay();
    this.username = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
      return letter.toUpperCase();
    })
  }
  getAllAiims(){
    
    this.examService.getSubscribeExam().subscribe(
      res => {
        console.log("res subcribed exams",res);
        if(res.data){
          if(res.data.activeExams.length>0){
            let exam = (res.data.activeExams).map((item) => {
              item.examPending = true;
              item.examId=item._id;
              return item;
            });
            this.totalAiims=exam;
          }
          if(res.data.completedExams.length>0){
            let exam = (res.data.completedExams).map((item) => {
              item.examCompleted = true;
              item.examName=item.examDetails.name;
              item.examType=item.examDetails.examType;
              item.examDate=item.examDetails.examDate;
             item.numberOfQuestions= item.examResult.total;
            item.examinformation= item.examDetails.description;
              return item;
            });
            this.totalAiims = (exam.length > 0) ? this.totalAiims.concat(exam) : this.totalAiims;

          }
          if(res.data.inactiveExams.length>0){

       let exam = (res.data.inactiveExams).map((item) => {
        item.inactiveExam = true;
              return item;
            });
            this.totalAiims = (exam.length > 0) ? this.totalAiims.concat(exam) : this.totalAiims;

          }
          if(res.data.pendingExams.length>0){
            let exam = (res.data.pendingExams).map((item) => {
              item.examPending = true;
              item.examName=item.examDetails.name;
              item.examType=item.examDetails.examType;
              item.examinformation=item.examDetails.description;
              item.packageId=item.examDetails.packageId;
              item.examDate=item.examDetails.examDate;
                    return item;
                  });
                  this.totalAiims = (exam.length > 0) ? this.totalAiims.concat(exam) : this.totalAiims;
                  this.totalAiims = this.totalAiims.sort((a, b) => new Date(a.examDate).getTime() - new Date(b.examDate).getTime());

          }
          console.log("total aiims exam", this.totalAiims);
          

        }else {

          this.__auth.notificationInfo("There is no data found!");
        }
        
      });
      
  }
  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };
  presentPopover(myEvent) {

    let popover = this.popoverCtrl.create(PopoverPage, { myEvent }, { enableBackdropDismiss: false });
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss((data, event) => {
      if (event == "pending") {
        event = "Pending";
      } else if (event == "completed") {
        event = "Completed";
      } else if (event == "demo") {

        event = "Demo";
      }
      if (myEvent == "customized") {
        this.custmizedExam = data;
        this.type = event;


      } else if (myEvent == "week") {
        this.totalWeek = data;

        this.type = event;
        if (event == "demo") {
          this.demoExam = true;
        } else {
          this.demoExam = false;
        }
      } else if(myEvent == "daily") {
        this.dailyExams = data;

        this.type = event;
        if (event == "demo") {
          this.aiimsdemoExam = true;
        } else {
          this.aiimsdemoExam = false;
        }
      }else{
        
        
        this.type = event;
        if (event == "demo") {
          this.demoExam = true;
    this.demoExams=data;
        } else {
          this.demoExam = false;
          this.totalAiims=data;
        }
      }

      // Navigate to new page.  Popover should be gone at this point completely
    });
  }
  register(category) {
    this.navCtrl.push(weekendReg,{exam:category});
  } checkAplay() {
    this.examService.checkAppliedForExam().subscribe(data => {
      if (data.status) {
        this.registerLabel = "Register"
        this.isRegistered = false;
        this.paymentDetails = data;
      }
      else {
        this.registerLabel = "Already registered"
        this.RegisterMsg = "You are registered"
      }
    }, err => {
      this.__auth.notificationInfo("OOPS Something went wrong.")
    });
  }

  newExamfunction(plan, desc, item) {
    this.__auth.notificationInfo("only for institutional users");

    // if (item) {
    //   this.singleExamId = item._id;
    // }
    // this.plan = plan;
    // if (!this.isRegistered) {
    //   let options = {
    //     // key: 'rzp_test_V1RjYTq2Xll4NR',
    //     key: this.paymentDetails.data.keyId,
    //     amount: (this.paymentDetails.data.weeklyDetails[plan].amount) * 100,
    //     name: desc,
    //     prefill: {
    //       name: this.userData.profile.firstName + this.userData.profile.lastName,
    //       email: this.userData.username
    //     },
    //     notes: {
    //       address: ""
    //     },
    //     theme: {
    //       color: "#108fd2"
    //     }
    //   }
    //   var successCallback = (payment_id) => {
    //     this.callApi(payment_id);
    //   };
    //   var cancelCallback = (error) => {
    //   };
    //   this.platform.ready().then(() => {
    //     RazorpayCheckout.open(options, successCallback, cancelCallback);
    //   })
    // }
    // else this.__auth.notificationInfo('Somthing went wrong.')
  }

  callApi(payment_id) {
    this.paymentId = payment_id;
    if (this.singleExamId) {
      this.postData = {
        examId: this.singleExamId,
        paymentId: this.paymentId,
        paymentStatus: "done",
        paymentMethod: "online",
        PlaneName: this.plan
      }
    } else {
      this.postData = {
        paymentId: this.paymentId,
        paymentStatus: "done",
        paymentMethod: "online",
        PlaneName: this.plan
      }
    }

    this.examService.SinglePaymentExam(this.postData)
      .subscribe(
        res => {
          if (res.status) {
            this.checkAplay();
            this.getAllWeek();
          }
        }, err => {
          this.__auth.notificationInfo('Somthing went wrong.')
        })

  }
  viewResult(item) {
    let exam_id;
    if (item.examType == "grand") {
      exam_id = item._id + '/g' + '/fromCompleted'
      this.navCtrl.push(Details, { ExamIds: exam_id });
    } else if (item.examType === "week" || item.examType === "special" || item.examType === "daily") {
      this.navCtrl.push(Details, { ExamIds: item._id + '/week' });
    } else if(item.examType == "AIIMS" || item.examType == "JIPMER"){
      this.navCtrl.push(aiimsResult, { ExamIds: item.examId +  '/' + item._id  + '/' + item.examType });
    }else {
      exam_id = item.examId + '/' + item._id + '/fromCompleted';
      this.navCtrl.push(Details, { ExamIds: exam_id });
    }
  }

  segmentChanged(event) {

    this.type = "All"
    if (event._value == "week") {
      this.week = true;
      this.customized = false;
      this.daily = false;
      this.aiims=false;
      this.getAllWeek();
    } else if (event._value == "customized") {
      this.customized = true;
      this.week = false;
      this.daily = false;
      this.aiims=false;
      this.getAllCustomized();

    } else if (event._value == "daily") {
     
      
      this.week = false;
      this.customized = false;
      this.daily = true;
      this.aiims=false;
      this.getAllDaily();
    }
    else{
      this.aiims=true
      this.week = false;
      this.customized = false;
      this.daily = false;
      this.getAllAiims();
    }
  }
  getAllDaily() {
    this.examService.getWeekExams().subscribe(
      weekRes => {

        if (weekRes.data) {
          if (weekRes.data.completedExams.length > 0) {
            this.completedWeekExam = weekRes.data.completedExams;
            let exam = (this.completedWeekExam).map((item) => {
              item.examName = item.examDetails.name;
              item.examType = item.examDetails.examType;
              item.examDate = item.examDetails.examDate;
              item.examCompleted = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              } else if (item.examResult.totalQuestions) {
                item.numberOfQuestions = item.examResult.totalQuestions;

              }
              return item;
            });
            this.completedWeekExam = exam;
            //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          }

          if (weekRes.data.activeExams.length > 0) {
            let activeWeekExam = [];

            activeWeekExam = weekRes.data.activeExams;

            let exam = (activeWeekExam).map((item) => {
              item.examPending = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;

            });
            activeWeekExam = exam;

            this.completedWeekExam = (activeWeekExam.length > 0) ? this.completedWeekExam.concat(activeWeekExam) : this.completedWeekExam;
            //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          }
          if (weekRes.data.inactiveExams.length > 0) {

            let pendingWeekExaminactive = [];
            pendingWeekExaminactive = weekRes.data.inactiveExams;
            let exam = (pendingWeekExaminactive).map((item) => {
              item.inactiveExam = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;

            });
            pendingWeekExaminactive = exam;
            this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? this.completedWeekExam.concat(pendingWeekExaminactive) : this.completedWeekExam;
            // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          }

          if (weekRes.data.pendingExams.length > 0) {
            let pendingWeekexam = [];
            pendingWeekexam = weekRes.data.pendingExams;
            let exam = (pendingWeekexam).map((item) => {
              item.examName = item.examDetails.name;
              item.examType = item.examDetails.examType;
              item.examDate = item.examDetails.examDate;
              item.examPending = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }

              return item;

            });
            pendingWeekexam = exam;
            this.completedWeekExam = (pendingWeekexam.length > 0) ? this.completedWeekExam.concat(pendingWeekexam) : this.completedWeekExam;

          }
          let exam = (this.completedWeekExam).map((item, index) => {

            if (item.examType == "daily") {
              item.dailyExam = true;

              this.dailyExams.push(item);

            }


            return item;

          });

          //this.completedWeekExam = exam;
          console.log("//////////////",this.dailyExams);

          this.dailyExams = this.dailyExams.sort((a, b) => new Date(b.examDate).getTime() - new Date(a.examDate).getTime());

          if (this.dailyExams.length == 0) {
            this.daily = false;
            this.__auth.notificationInfo("There is no daily exams found!");

          }


        } else {
          this.__auth.notificationInfo("There is no data found!");
        }
      });
  }
  getAllWeek() {
    this.examService.getWeekExams().subscribe(
      weekRes => {

        if (weekRes.data) {
          if (weekRes.data.completedExams.length > 0) {
            this.completedWeekExam = weekRes.data.completedExams;
            let exam = (this.completedWeekExam).map((item) => {
              item.examName = item.examDetails.name;
              item.examType = item.examDetails.examType;
              item.examDate = item.examDetails.examDate;
              item.examCompleted = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              } else if (item.examResult.totalQuestions) {
                item.numberOfQuestions = item.examResult.totalQuestions;

              }
              return item;
            });
            this.completedWeekExam = exam;
            //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          }

          if (weekRes.data.activeExams.length > 0) {
            let activeWeekExam = [];

            activeWeekExam = weekRes.data.activeExams;

            let exam = (activeWeekExam).map((item) => {
              item.examPending = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;

            });
            activeWeekExam = exam;

            this.completedWeekExam = (activeWeekExam.length > 0) ? this.completedWeekExam.concat(activeWeekExam) : this.completedWeekExam;
            //this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          }
          if (weekRes.data.inactiveExams.length > 0) {

            let pendingWeekExaminactive = [];
            pendingWeekExaminactive = weekRes.data.inactiveExams;
            let exam = (pendingWeekExaminactive).map((item) => {
              item.inactiveExam = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;

            });
            pendingWeekExaminactive = exam;
            this.completedWeekExam = (pendingWeekExaminactive.length > 0) ? this.completedWeekExam.concat(pendingWeekExaminactive) : this.completedWeekExam;
            // this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          }

          if (weekRes.data.pendingExams.length > 0) {
            let pendingWeekexam = [];
            pendingWeekexam = weekRes.data.pendingExams;
            let exam = (pendingWeekexam).map((item) => {
              item.examName = item.examDetails.name;
              item.examType = item.examDetails.examType;
              item.examDate = item.examDetails.examDate;

              item.examPending = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }

              return item;

            });
            pendingWeekexam = exam;
            //pendingWeekexam = pendingWeekexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
            this.completedWeekExam = (pendingWeekexam.length > 0) ? this.completedWeekExam.concat(pendingWeekexam) : this.completedWeekExam;
            this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          }
          console.log(" this.completedWeekExam", this.completedWeekExam);
          
          let exam = (this.completedWeekExam).map((item, index) => {

            if (item.examType == "daily") {
              //item.dailyExam=true;

              // this.dailyExams.push(item);

            } else {
              item.dailyExam = false;
              this.totalWeek.push(item);

            }


            return item;

          });
          console.log("this.totalWeek",this.totalWeek);
          

          //this.completedWeekExam = exam;
          if (this.totalWeek.length == 0) {
            this.week = false;
            this.__auth.notificationInfo("There is no week exams found!");

          }


        } else {
          this.__auth.notificationInfo("There is no data found!");
        }
      });
  }
  resumeExam(exam) {
console.log("exaaaaaaaaaam weeeeeeeeeeee",exam);

    if (!exam.examType) {
      if (exam.examId) {
        this.navCtrl.push(writeExam, { subject: exam._id });
      }
      else {
        this.examService.startExam(exam._id).subscribe(
          res => {
            let userExamId = res.data.userExamId;
            this.navCtrl.push(writeExam, { subject: userExamId });
          });
      }
    } else if (exam.examType == "grand") {
      this.navCtrl.push(grantExam, { subject: exam._id });
    } else if (exam.examType == "week" || exam.examType == "special" || exam.examType == "daily") {
      this.navCtrl.push(termsrules, { subject: (exam.examId) ? exam.examId : exam._id });
    }else  if(exam.examType == "AIIMS" ){
      this.navCtrl.push(aiimsExam, { subject: exam.examId,package:exam.packageId });

    }
    else if(exam.examType == "JIPMER" ){
      console.log("exam.examId",exam.examId);
      this.navCtrl.push(aiimsExam, { subject: exam.examId,package:exam.packageId });

    }
  }
  getAllCustomized() {
    this.mode = 'completed';
    this.examService.fetchAllExam(this.mode).subscribe(
      res => {
        if (res.data.examList.length > 0) {
          this.custmizedExam = res.data.examList;
          let exam = (this.custmizedExam).map((item) => {
            item.customizedExam = true;
            item.completed = true;
            return item;

          });
          this.custmizedExam = exam;
        }
        // this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
        this.mode = 'pending';
        this.examService.fetchAllExam(this.mode).subscribe(
          res => {
            let customizedPending = [];
            if (res.data.examList.length > 0) {
              customizedPending = res.data.examList;
              let exam = (customizedPending).map((item) => {
                item.customizedExam = true;
                item.pending = true;
                return item;

              });
              customizedPending = exam;

              // customizedPending = customizedPending.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
              this.custmizedExam = (customizedPending.length > 0) ? this.custmizedExam.concat(customizedPending) : this.custmizedExam;
            }
            this.examService.fetchGrandExam().subscribe(
              res => {

                let completedGexam = [];
                if (res.data.completedExam.length > 0) {
                  completedGexam = (res.data.completedExam).map((item) => {
                    item.examType = "grand";
                    item.examName = item.name;
                    item.completed = true;
                    return item;
                  })

                  // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                  this.custmizedExam = (completedGexam.length > 0) ? this.custmizedExam.concat(completedGexam) : this.custmizedExam;

                }
                if (res.data.pendingExams.length > 0) {
                  let pendingGexam = [];
                  pendingGexam = (res.data.pendingExams).map((item) => {
                    item.examType = "grand";
                    item.examName = item.name;
                    item.pending = true;
                    return item;
                  })
                  //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                  this.custmizedExam = (pendingGexam.length > 0) ? this.custmizedExam.concat(pendingGexam) : this.custmizedExam;

                }
                this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                if (this.custmizedExam.length == 0) {
                  this.customized = false;
                  this.__auth.notificationInfo("There is no data found!");
                }
              });
          });
      });
  }
  AIIMSPAYROUTE(){
    this.__auth.notificationInfo("only for institutional users");

    // this.navCtrl.push(weekendReg)
  }
  writeExam() {
    this.navCtrl.push(CreateExam);
  }

}
