import { Component } from '@angular/core';
import { NavParams, NavController, ModalController } from 'ionic-angular';
import { ExamModal } from '../../../pages/exam/examModal/examModal';
import { ExamService } from '../../../services/exam.service';
import { ToastController } from 'ionic-angular';
import { writeExam } from '../../../pages/exam/createexam/write-exam/write-exam';
import { AuthService } from '../../../services/auth.service'
import { grantModal } from '../../../pages/exam/GrandExam/grantModal/grantModal';
import { grantExam } from '../../../pages/exam/GrandExam/grantexam.component';
import { Exam } from '../../../pages/exam/exam';
import { StudentService } from '../../../services/student.service'
import { examComponent } from '../../../pages/exam/examNew/examComponent';

@Component({
  selector: 'page-createExam',
  templateUrl: 'createExam.html',
  providers: [ExamService, AuthService, StudentService]
})
export class CreateExam {
  myParam;
  userData;
  activeUserId;
  userId;
  subjectList;
  section;
  chapter;
  cart: Array<any> = [];
  selectedSubject;
  selectedSection;
  shortname;
  totalQuestionsCount;
  numOfQuestion;
  examName;
  cartLength;
  dueFlag: boolean = false;
  timeFlag: boolean = false;
  examconfig: Array<any> = [];
  dueObj: object;
  defMint;
  hourss;
  showSection: boolean;
  showChapter: boolean;
  time;
  examType;
  viewData: boolean;
  viewPlusSection: boolean;
  sectionArray: Array<any> = [];
  subjectArray: Array<any> = [];
  index;
  removingIndex;
  walkdis: boolean = false;

  constructor(public navCtrl: NavController, private studentService: StudentService, params: NavParams, public __auth: AuthService, private toastCtrl: ToastController, private examService: ExamService, public modalCtrl: ModalController) {
    // this.showSection = false;
    // this.showChapter = false;
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.activeUserId = this.userData._id;
    this.userId = this.userData.userId;
    this.getSubject(this.userId, this.activeUserId);
    this.walkthroug();
  }

  getSubject(userId, activeUserId) {
    this.subjectList = JSON.parse(localStorage.getItem('subjectData'));
  }

  walkthroug() {
    this.studentService.dashboardInfo().subscribe(
      res => {
        if (res.data.exam) this.walkdis = false;
        else this.walkdis = true;
      })
  }

  ViewSections(index) {
    this.showSection = true;
    this.chapter = null;
    this.index = index;
    this.viewData = true;
    this.section = this.subjectList[index].children;
  }

  Viewchapters(index) {
    this.showChapter = true;
    if (this.section == []) {
      this.chapter = [];
    } else {
      this.chapter = this.section[index].children;
    }
  }

  selectSubject(index) {
    this.selectedSubject = this.subjectList[index];
    /* Check for double entry in array */
    var status;
    var kstatus;
    if (this.cart.length != 0) {
      for (let i = 0; i < this.cart.length; i++) {
        /* double entry check */
        if (this.selectedSubject.id === this.cart[i].id) {
          status = 1;
          break;
        } else {
          status = 0;
        }
        if (this.cart[i].parentArray) {
          for (let j = 0; j < this.cart.length; j++) {
            if (this.selectedSubject.id === this.cart[i].parentArray[0]) {
              kstatus = 0;
              this.cart.splice(i, 1);
              j--;
            } else {
              kstatus = 0;
            }
          }
        }
      }
      if (status === 0 && kstatus == 0) {
        this.cart.push(this.selectedSubject);
      }
    } else {
      this.cart.push(this.selectedSubject);
    }
  }

  selectSection(index) {
    //this.showChapter = true;
    this.selectedSection = this.section[index];
    /* Checking section for double entry */
    var pstatus = 1;
    var kstatus = 1;
    var stat = 1;
    if (this.cart.length != 0) {
      for (let i = 0; i < this.cart.length; i++) {
        if (this.section[index].id === this.cart[i].id) {
          stat = 1;
          break;
        } else {
          stat = 0;
        }
        if (this.selectedSection.imParentObj.id === this.cart[i].id) {
          pstatus = 1;
          break;
        } else {
          pstatus = 0;
        }
        if (this.selectedSection.id === this.cart[i].parentArray[1]) {
          this.cart.splice(i, 1);
          kstatus = 0;
        } else {
          kstatus = 0;
        }
      }
      if (stat === 0 && pstatus === 0 && kstatus === 0) {
        this.cart.push(this.section[index]);
      } else {
        console.log("onnu podo")
      }

    } else {
      this.cart.push(this.selectedSection);
    }
  }

  selectChapter(index) {
    var cstatus;
    var lstatus;
    var gstatus;
    if (this.cart.length != 0) {
      /* check chapter double entry */
      for (let k = 0; k < this.cart.length; k++) {

        /* Check for grand parent */
        for (let g = 0; g < this.chapter[index].parentArray.length; g++) {
          if (this.chapter[index].parentArray[g] === this.cart[k].id) {
            console.log("Kids, Grandpa here ! ESCAPE!!!");
            gstatus = 1;
            break;
          } else {
            gstatus = 0;
          }
        }
        if (this.chapter[index].id === this.cart[k].id) {
          cstatus = 1;
          break;
        } else {
          cstatus = 0;
        }
        if (this.chapter[index].imParentObj.id === this.cart[k].id) {
          console.log("Found you father! Kid, Dn't go there!");
          lstatus = 1;
          break;
        } else {
          lstatus = 0;
        }
      }
      /* If kids,parents,grandpa are not there in cart */
      if (cstatus === 0 && lstatus == 0 && gstatus === 0) {
        this.cart.push(this.chapter[index]);
      }
    }
    /* if cart is empty */
    else {
      this.cart.push(this.chapter[index]);
    }

  }
  dltSubjct(index) {
    this.removingIndex = index;
    if (!this.viewData) {
      this.subjectList.splice(index, 1);
    } else {
      this.subjectList.splice(index, 1);
      this.showSection = false;
      this.showChapter = false;
      this.section = [];
      this.chapter = [];
    }
  }

  dltSection(index) {
    this.removingIndex = index;
    this.section.splice(index, 1);
    //this.section[index].children=[];
    this.chapter = this.section[index].children;
    // this.chapter.splice(index, 1);
    this.chapter = [];
    this.showChapter = false;
    if (this.chapter.length == 0) {
      this.showChapter = false;
    }
  }

  dltChapter(index) {
    this.removingIndex = index;
    //this.section.splice(index, 1);
    this.chapter.splice(index, 1);
    if (this.chapter.length == 0) {
      this.showChapter = false;
    }

  }
  removeFromCart(index) {
    // this.cart.splice(index, 1);
    if (this.cart[index].title == "subject") {
      // this.subjectList.push(this.cart[index])
      this.subjectList.splice(this.removingIndex, 0, this.cart[index]);
      this.cart.splice(index, 1);
    }
    else if (this.cart[index].title == "section") {
      //this.section.push(this.cart[index]);
      this.section.splice(this.removingIndex, 0, this.cart[index]);

      console.log("section remove222222222", this.section);
      this.cart.splice(index, 1);

    } else {
      console.log("chapter remove", this.removingIndex);
      //  this.chapter.push(this.cart[index]);
      this.chapter.splice(this.removingIndex, 0, this.cart[index]);

      this.cart.splice(index, 1);
    }
  }

  backToeaxam() {
    this.navCtrl.push(examComponent);
  }
  createExam() {

    this.section = "";
    this.chapter = "";
    this.shortname = '';
    this.totalQuestionsCount = 0;
    for (let i = 0; i < this.cart.length; i++) {
      this.totalQuestionsCount = this.totalQuestionsCount + this.cart[i].count;
      let shortedname: string;
      let nameShort = this.cart[i].name.split(" ")[0];
      if (this.shortname == '') {
        this.shortname = this.shortname.concat(nameShort);
      }
      else {

        this.shortname = this.shortname.concat("-", nameShort);
      }
    }
    let data = {
      'shortname': this.shortname,
      "totalQuestionsCount": this.totalQuestionsCount
    };

    let profileModal = this.modalCtrl.create(ExamModal, { data: data }, { cssClass: 'inset-modal' });
    profileModal.onDidDismiss(value => {
      if (value) {
        if (value.names && value.questionNumber) {

          this.numOfQuestion = value.questionNumber;
          this.examName = value.names;
          this.defMint = value.minute;
          this.hourss = value.hour;
          this.cartLength = this.cart.length;
          let averageNumber = this.numOfQuestion / this.cartLength;
          let averageNumberRound = Math.floor(averageNumber);
          let decimal = averageNumber % 1;
          let needMore = this.numOfQuestion - (averageNumberRound * this.cartLength);
          let questionsWanted;
          for (let i = 0; i < this.cartLength; i++) {
            let currentId = this.cart[i].id;
            let currentName = this.cart[i].name;
            if (i == 0) {
              questionsWanted = averageNumberRound + needMore;
            } else {
              questionsWanted = averageNumberRound;
            }
            let data = {
              "id": currentId,
              "name": currentName,
              "noOfQuestions": questionsWanted
            };
            this.examconfig.push(data);
          }
          if (this.hourss) {
            this.timeFlag = true;
          } else {
            this.timeFlag = false;
          }

          if (value.box == true) {
            this.dueFlag = true;
            this.dueObj = value.date;
          } else {
            this.dueFlag = false
          }



          let data = {
            'userId': this.userId,
            'examName': this.examName,
            'examConfig': this.examconfig,
            'noOfQuestions': this.numOfQuestion,
            'dueFlag': this.dueFlag,
            'timeFlag': this.timeFlag,
            'dueObj': this.dueObj
          };



          this.examService.createExam(this.examName, this.examconfig, this.numOfQuestion, this.dueFlag, this.dueObj, this.timeFlag, this.defMint, this.hourss).subscribe(
            res => {

              let examId = res.data.examId;

              if (res.status === 1) {
                if (this.dueFlag == true) {
                  this.examService.startExam(examId).subscribe(
                    res => {
                      this.__auth.notificationInfo('Exam saved successfully')

                    }
                  )
                }
                else {
                  this.examService.startExam(examId).subscribe(
                    res => {
                      if (res.data.userExamId) {

                        this.navCtrl.push(writeExam, { subject: res.data.userExamId });
                        this.cart = [];
                      } else {

                      }

                    }
                  )
                }
              }
            }
          );
        }
      }
    })
    profileModal.present();
  }
  //grand exam
  grandExam() {
    let chooseModal = this.modalCtrl.create(grantModal, null, { cssClass: 'inset-modal' });
    chooseModal.onDidDismiss(value => {
      console.log(value, "fvalue")
      if (value.names && value.nofoq) {
        console.log(value, "2value")

        this.numOfQuestion = value.nofoq;
        this.examName = value.names;
        this.defMint = value.minutes;
        this.hourss = value.hour;
        this.time = value.time;
        this.examType = "grand"
        if (this.hourss) {
          this.timeFlag = true;
        } else {
          this.timeFlag = false;
        }


        if (value.box == true) {
          this.dueFlag = true;
          this.dueObj = value.date;
        } else {
          this.dueFlag = false
        }
        this.examService.createGrandExam(this.examName, this.examconfig, this.numOfQuestion, this.dueFlag, this.timeFlag, this.defMint, this.hourss, this.dueObj, this.examType).subscribe(
          res => {
            console.log(res.data, "first")
            let examId = res.data.exam_id;
            if (res.status === true) {
              if (this.dueFlag == true) {
                this.examService.loadGrandExam(examId).subscribe(
                  res => {
                    this.__auth.notificationInfo('Grant Test saved successfully')
                    // this.router.navigate(['/exam/room'])
                  }
                )
              }
              else {
                this.examService.loadGrandExam(examId).subscribe(
                  res => {
                    console.log(res.data, "secnd")
                    if (res.data.userExamObj) {
                      this.navCtrl.push(grantExam, { subject: examId });
                      // this.router.navigate(['/grand', examId]);
                    } else {
                      console.log("Can't find user Exam id");
                    }
                  }
                )
              }
            }
          })
      }




    });
    chooseModal.present();
  }

}