import { Component } from '@angular/core';
import { NavController, NavParams ,Platform} from 'ionic-angular';
import { ExamService } from '../../../../services/exam.service';
import { AuthService } from '../../../../services/auth.service'
import { AlertController } from 'ionic-angular';
import { Exam } from '../../../../pages/exam/exam';
import { examComponent } from '../../../../pages/exam/examNew/examComponent';

import { environment } from '../../../../environments/environment'
import { Details } from '../../../../pages/exam/exam-room/details/details';
import { aiimsResult } from '../../../../pages/exam/exam-room/aiimsResult/aiimsResult';

import { Events } from 'ionic-angular';
//declare var jQuery: any;

@Component({
    selector: 'page-aiimsExam',
    templateUrl: 'aiimsExam.html',
    providers: [ExamService, AuthService]
})
export class aiimsExam {
    userExamId;
    packageId;
    pauseTime;
  playTime;
  timeFlag: boolean;
  zeroTime: boolean = false;
  buttonName: boolean = false;
  paused: boolean = false;
  resumed: boolean = false;
  marked:boolean = false;
  buttonToggle: boolean = false;
  playTitle;
  pauseTitle;
    questionPalette: Array<any> = [];
    currentPaletteId: number = 0;
    question: string;
    options = [];
    slNumber: number;
    questionId: string;
    questionData: any;
    clickedItem: string;
    examId: string;
    subjectArray: Array<any> = [];
    answerData;
    answered: boolean;
    imageFlag: boolean;
    imageUrl: string;
    qPallete; //for sorting and marking, copy of questionPalette
    url = environment.backendUrl + '/images/';
    paletteStatus: Array<number> = [];
    time;
    previousTimer;
    x;
    timerObj;
    timer = false;
    testObject: Array<any> = [];
    timerIndex;
    timerValues;
    examType;
    countClick:number=0;
    tabBarElement;
    constructor(public navCtrl: NavController, public events: Events,private alertCtrl: AlertController,public platform: Platform, public __auth: AuthService, public navParams: NavParams, private examService: ExamService) {
        this.events.publish('user:created', true, Date.now());
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');        
        if(this.tabBarElement){this.tabBarElement.style.display = 'none';  }
    this.userExamId = navParams.get('subject');
    this.packageId=navParams.get('package');
    console.log(" this.userExamId", this.userExamId);
    
    }
    ngOnInit() {
        this.examService.loadPackageExams(this.userExamId,this.packageId).subscribe(
            res => {
                console.log("res....exam",res);
                
                let examDetails = res.data;
                let count = 1;
                this.examId = examDetails.examId;
                console.log("exam details",examDetails);
                this.examType=examDetails.examDetails.examType;
                this.userExamId = examDetails.userExamId;
                console.log(" this.userExamId ", this.userExamId );
                
                this.questionPalette = (examDetails.questionPalette).filter(item => {
                  item.number = count;
                  count++;
                  return item;
                });
    console.log("this.questionPalette",this.questionPalette);
    
                this.qPallete = this.questionPalette;
                this.timeFlag = examDetails.examDetails.timeFlag;
                if (examDetails.examDetails.timeFlag == true) {
                  this.timerObj = examDetails.examDetails.timeObject;
                  this.testObject = JSON.parse(localStorage.getItem('testObject'));
                  if (this.testObject) {
                    this.testObject.map((item, i) => {
                      if (item.userExamId == this.userExamId) {
                        this.timerIndex = i;
                        this.previousTimer = this.testObject[this.timerIndex];
                      };
                    })
                  } else {
                    this.testObject = [];
                    this.previousTimer = null;
                  }
                  if (this.previousTimer == null || !this.previousTimer) {
                    this.time = new Date(2017, 1, 1, this.timerObj.hour, this.timerObj.min, 0);
                    this.timer = true;
                  } else {
                    this.time = new Date(2017, 1, 1, this.previousTimer.hours, this.previousTimer.minutes, this.previousTimer.seconds);
                    this.timer = true;
                  }
                  this._timerTick();
                }
                this.getQuestion(this.currentPaletteId);
              
            });

}

_timerTick() {
  this.time.setSeconds(this.time.getSeconds(), -1);
  let timerObject = {
    hours: this.time.getHours(),
    minutes: this.time.getMinutes(),
    seconds: this.time.getSeconds(),
    userExamId: this.userExamId
  }
  if (this.timerIndex) this.testObject[this.timerIndex] = timerObject;
  else {
    this.testObject.push(timerObject);
    this.testObject.map((item, i) => {
      if (item.userExamId == this.userExamId) this.timerIndex = i;
    })
  }
  localStorage.setItem('testObject', JSON.stringify(this.testObject));
  if (timerObject.hours === 0 && timerObject.minutes === 5 && timerObject.seconds === 0) {
    this.__auth.notificationInfo('5 Minutes Remaining');
  }
  if (timerObject.hours === 0 && timerObject.minutes === 0 && timerObject.seconds === 0) {
    window.clearInterval(this.x);
    localStorage.removeItem('timerObject');
    this.statusCount();

    this.finishExam("");
  }
  this.x = setTimeout(() => this._timerTick(), 1000);
}

playPause() {
  if (this.buttonToggle) {
    this.buttonName = false;
    this.pauseTitle = "Pause Timer"
    if (this.pauseTime) {
      this.playTime = this.pauseTime;
    }
    else {
      this.playTime = this.time;
    }
    this.paused = false;
    this.resumed = true;
    this.playTime.setSeconds(this.playTime.getSeconds(), -1);
    if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 5 && this.playTime.getSeconds() === 0) {
      this.__auth.notificationInfo('5 Minutes Remaining')
    }
    if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 0 && this.playTime.getSeconds() === 0) {
      window.clearInterval(this.x);
      localStorage.removeItem('timerObject');
      this.finishExam("");
      this.statusCount();
    }
    var x = setTimeout(() => this._timerTick(), 1000);
  }
  else {
    this.buttonName = true;
    this.playTitle = "Resume Timer"
    this.pauseTime = this.time;
    this.resumed = false;
    this.paused = true;
    window.clearInterval(this.x);
    localStorage.removeItem('timerObject');
  }
  this.buttonToggle = !this.buttonToggle
}

  getQuestion(index) {
    this.currentPaletteId = index;
    console.log("this.questionPalette[this.currentPaletteId]",this.questionPalette[this.currentPaletteId]);
    
    this.questionId = this.questionPalette[this.currentPaletteId]._id;
    console.log("this.questionId///////////",this.questionId);
    console.log("this.userExamId//////",this.userExamId);
    
    this.examService.getAiimsQuestions(this.userExamId,this.questionId)
      .subscribe(
        res => {
            console.log("aiims",res);
            
          if (!res.status)  this.__auth.notificationInfo("OOPS! Something went wrong.");
          else {
            this.questionData = (res.data.question[0]) ? res.data.question[0] : {};
            this.answerData = (res.data.userAnswer[0]) ? res.data.userAnswer[0] : {};
            console.log("this.questionData",this.questionData);
            console.log("this.questionPalette",this.questionPalette);
            
            this.question = this.questionData.question;
            this.options = JSON.parse(this.questionData.options);
            this.slNumber = this.questionPalette[this.currentPaletteId].number;
            this.subjectArray = this.questionData.subjectArray;
            if (this.questionData.question_image) {
              this.imageFlag = true;
              this.imageUrl = this.url + this.questionData.question_image;
            } else {
              this.imageFlag = false;
            }
            if (this.answerData.answer) {
              this.answered = true;
              this.clickedItem = this.answerData.answer;
            } else {
              this.answered = false;
              this.clickedItem = '';
            }
          }

        }
      )
  }

  nextQuestion() {
    if (this.questionPalette.length === (this.currentPaletteId + 1)) {
      this.statusCount();
      this.finishExam("");
      
    
    } else {
      this.currentPaletteId++;
      this.getQuestion(this.currentPaletteId);
    }
  }
  statusCount(){
    let marked = 0, unanswered = 0, answered = 0;
    this.qPallete.filter((item) => {
      if (item.status == 'answered') answered++;
      else if (item.status == 'marked') marked++;
      else unanswered++;
    })
    this.paletteStatus = [answered, marked, unanswered];
  }
  previousQuestion() {
    this.currentPaletteId--;
    this.getQuestion(this.currentPaletteId);
  }
  goToQuestion(index) {
    this.currentPaletteId = index;
    this.getQuestion(index)
  }
  filter(type) {
    let mapArray = (type) => {
      let tempArray = []
      tempArray = (this.qPallete).filter(item => {
        if (item.status == type) return item;
      })
      if (tempArray.length) this.questionPalette = tempArray, this.getQuestion(0), this.currentPaletteId = 0;
      else  this.__auth.notificationInfo("No questions!");
    }
    if (type == 'm') {
      mapArray('marked')
    }
    else if (type == 'a') {
      mapArray('answered')
    }
    else if (type == 'na') {
      mapArray('')
    }
    else {
      this.questionPalette = this.qPallete;
      this.currentPaletteId = 0;
      this.getQuestion(0)
    }

  }
  finishExam(userId) {
   window.clearInterval(this.x);
    localStorage.removeItem('timerObject');
    let alert = this.alertCtrl.create({
      title: 'Confirm submit',
      message: 'Do you want to submit exam?',
      buttons: [
        {
          text: 'Yes',
          role: 'cancel',
          handler: () => {
            this.examService.finishAiimsExam(this.userExamId,this.examId,this.examType).subscribe(res => {
              console.log("resss",res);
              
              if (res.status) {
                this.navCtrl.push(aiimsResult, { ExamIds: this.examId +  '/' + this.userExamId  + '/' + this.examType});

              } else {
                this.__auth.notificationInfo('OOPS! Something went wrong.')

              }
            });
          }
        },
        {
          text: 'No',
          handler: () => {
          }


        }
      ]
    });
    alert.present();

  }
   saveAnswer(answer, index) {
    //this.countClick++;
   
      let result = null;
      let answerStatus;
      if(this.examType == "AIIMS"){
        if (this.questionPalette[this.currentPaletteId].status == "marked") {
          this.marked = true;
        } else {
          this.marked = false;
        }
      }else{
        this.marked = false;
      }
      console.log("this.clickedItem",this.clickedItem);
      
      if (this.clickedItem && this.clickedItem == answer) {
        console.log("haaaai");
        
        this.clickedItem = 'ABCD';
        answerStatus = "unanswered"
        result = "unanswered"
        this.answered = false;

      } else {
        console.log("..................................");
        this.answered = true;
        this.clickedItem = answer;
        let correctAnswer = this.questionData.answer;
        if (answer == correctAnswer) {
          result = 'correct';
        } else {
          result = 'wrong';
        }
        answerStatus = "answered"

      }
    console.log("answerStatus",answerStatus);
    
    this.examService.saveAiimsExamAnswer(this.questionId, this.examId, this.userExamId, result, this.clickedItem,this.examType, this.marked )
    .subscribe(
      res => {
        console.log("ressss",res);
        
            console.log("ressss",res);
            
            if (!res.status) this.__auth.notificationInfo("OOPS! Something went wrong.");
            else {
              if(this.examType == "AIIMS"){
                if (this.questionPalette[this.currentPaletteId].status == "marked" && this.answered) {
                  this.questionPalette[this.currentPaletteId].status = "marked";
                  this.qPallete[this.currentPaletteId].status = "marked";
                } else if (answerStatus == 'unanswered') {
                  this.questionPalette[this.currentPaletteId].status = "";
                  this.qPallete[this.currentPaletteId].status = "";
                } else {
                  this.questionPalette[this.currentPaletteId].status = "answered";
                  this.qPallete[this.currentPaletteId].status = "answered";
                }
              }else{
                if (answerStatus == 'unanswered') {
                  this.questionPalette[this.currentPaletteId].status = "";
                  this.qPallete[this.currentPaletteId].status = "";
                } else {
                  this.questionPalette[this.currentPaletteId].status = "answered";
                  this.qPallete[this.currentPaletteId].status = "answered";
                }
              }
              }
            })
      }
    
  markQuestion(type) {
    console.log(type, "type da")
    this.examService.markAiimsQuestions(this.questionId, this.userExamId, type)
      .subscribe(
        res => {
          console.log("mark",res);
          
          if (!res.status) this.__auth.notificationInfo("OOPS! Something went wrong.");
          else {
            if (type == "mark") {
              type = "marked"
            } else {
              type = "unmarked"
            }
            if (this.examType == "JIPMER") {
              if ((type == 'marked') && this.answered) {
                this.questionPalette[this.currentPaletteId].status = 'marked'
                this.qPallete[this.currentPaletteId].status = "marked";
              } else if ((type == 'unmarked') && this.answered) {
                this.questionPalette[this.currentPaletteId].status = 'answered';
                this.qPallete[this.currentPaletteId].status = 'answered';
              }else if ((type == 'unmarked') && !this.answered) {
                this.questionPalette[this.currentPaletteId].status = '';
                this.qPallete[this.currentPaletteId].status = '';
              } else {
                this.questionPalette[this.currentPaletteId].status = type;
                this.qPallete[this.currentPaletteId].status = type;
              }
            } else {
              if ((type == 'marked') && this.answered) {
                this.questionPalette[this.currentPaletteId].status = 'marked'
                this.qPallete[this.currentPaletteId].status = "marked";
              }else if ((type == 'unmarked') && this.answered) {
                this.questionPalette[this.currentPaletteId].status = 'answered';
                this.qPallete[this.currentPaletteId].status = 'answered';
              } else if ((type == 'unmarked') && !this.answered) {
                this.questionPalette[this.currentPaletteId].status = '';
                this.qPallete[this.currentPaletteId].status = '';
              } else {
                this.questionPalette[this.currentPaletteId].status = type;
                this.qPallete[this.currentPaletteId].status = type;
              }
            }
          }
        })
  }

//   finishExam(userId) {
//     window.clearInterval(this.x);
//     localStorage.removeItem('timerObject');
//     let alert = this.alertCtrl.create({
//       title: 'Confirm submit',
//       message: 'Do you want to submit exam?',
//       buttons: [
//         {
//           text: 'Yes',
//           role: 'cancel',
//           handler: () => {
//             this.weeklyService.submitExam(this.userExamId).subscribe(res => {
//               if (res.status) {
//                 this.navCtrl.push(Details, { ExamIds: this.userExamId + '/week' });
//               } else {
//                 this.__auth.notificationInfo('OOPS! Something went wrong.')

//               }
//             });
//           }
//         },
//         {
//           text: 'No',
//           handler: () => {
//           }


//         }
//       ]
//     });
//     alert.present();

//   }

}