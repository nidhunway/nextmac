import { Component } from '@angular/core';
import { NavController, NavParams ,Platform} from 'ionic-angular';
import { ExamService } from '../../../../services/exam.service';
import { AuthService } from '../../../../services/auth.service'
import { AlertController } from 'ionic-angular';
import { Exam } from '../../../../pages/exam/exam';
import { examComponent } from '../../../../pages/exam/examNew/examComponent';

import { environment } from '../../../../environments/environment'
import { Details } from '../../../../pages/exam/exam-room/details/details';
import { Events } from 'ionic-angular';

declare var jQuery: any;

@Component({
    selector: 'page-write',
    templateUrl: 'write-exam.html',
    providers: [ExamService, AuthService]
})
export class writeExam {
    tabBarElement: any;
    public userExamId
    showNav = true;
    questionPalette;
    questionData;
    options;
    nextButtonLabel: string = "Next";
    noNextQuestion: boolean = false;
    qstAnswerd: boolean = false
    imageFlag;
    imageUrl;
    shownGroup;
    examConfigData;
    RealexamData;
    answerData;
    examId;
    currentPaletteId: number;
    questionId;
    updateFlag: boolean = false;
    answered;
    marked;
    clickedItem;
    url = environment.backendUrl + '/images/';
    userExamAnwserId: string;
    answeredPalette: boolean;
    answeredPaletteId: number;
    viewResult: boolean = false;
    noOfCorrectAnswer: number;
    wrongAnswers: number;
    totalQuestionAttended: number;
    percentage: number;
    examHours: number;
    examMinutes: number;
    testNidhun: any;
    testN: boolean = false;
    timeFlag: boolean;
    time;
    totalQuestions: number;
    totalWrong: number;
    timeExamObject: Array<any> = [];
    helloww: any;
    greeting: string;
    tempQuestionPallete = []
    previousTimer;
    x;
    timerObj;
    timer = false;
    timerIndex;
    timerValues;
    pauseTime;
    playTime;
    buttonName: boolean = false;
    paused: boolean = false;
    resumed: boolean = false;
    buttonToggle: boolean = false;
    playTitle;
    pauseTitle;
    paletteStatus = [];
    MrakedOrNot;
    constructor(public navCtrl: NavController, public events: Events,private alertCtrl: AlertController,public platform: Platform, public __auth: AuthService, public navParams: NavParams, private examService: ExamService) {
        this.userExamId = navParams.get('subject');
        this.events.publish('user:created', true, Date.now());

        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');        

    }
    ionViewWillEnter(): void {
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(() => this.backButtonFunc());
      }
     
      private backButtonFunc(): void {
        this.navCtrl.setRoot(examComponent);
      }
    ionViewWillLeave() {
        this.tabBarElement.style.display = 'flex';
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');

    }

    ngOnInit() {

        this.buttonName = false;
        this.pauseTitle = "Pause Timer";
        this.currentPaletteId = 0;
        this.examService.loadExam(this.userExamId).subscribe(
            res => {
                console.log("questionPalette", res);
                if(res.data.userExamObj){
                this.questionPalette = res.data.userExamObj.questionPalette;
                }
                console.log(this.questionPalette,"data here")
                this.tempQuestionPallete = this.questionPalette; // this is temp pallete array for sorting(marked,answered,not answered)         
                if (res.data) {
                    this.examConfigData = res.data;
                    this.examId = res.data.userExamObj.examId;
                    this.currentPaletteId = 0;
                    this.timeFlag = this.examConfigData.userExamObj.timeFlag;
                    if (this.timeFlag) {
                        this.timerObj = this.examConfigData.userExamObj.timeObj;
                        this.timeExamObject = JSON.parse(localStorage.getItem('timeExamObject'));
                        if (this.timeExamObject) {
                            this.timeExamObject.map((item, i) => {
                                if (item.userExamId == this.userExamId) {
                                    this.timerIndex = i;
                                    this.previousTimer = this.timeExamObject[this.timerIndex];
                                };
                            })
                        } else {
                            this.timeExamObject = [];
                            this.previousTimer = null;
                        }

                        if (this.previousTimer == null || !this.previousTimer) {
                            this.time = new Date(2017, 1, 1, this.timerObj.hour, this.timerObj.minute, 0);
                            this.timer = true;
                        } else {
                            this.time = new Date(2017, 1, 1, this.previousTimer.hours, this.previousTimer.minutes, this.previousTimer.seconds);
                            this.timer = true;
                        }
                        this._timerTick();
                    }
                    this.questionId = this.questionPalette[this.currentPaletteId]._id;
                    this.examService.getExamQuestions(this.userExamId, this.questionId).subscribe(
                        res => {
                            this.playTime = this.time;
                            this.RealexamData = res;
                            this.answerData = res.data.answer;
                            if (res.data.answer) {
                                this.answered = true;
                                this.answeredPalette = true;
                                this.answeredPaletteId = this.currentPaletteId;
                                this.clickedItem = res.data.answer.answer;
                            }
                            this.questionData = res.data.question;
                            this.options = JSON.parse(res.data.question.options);
                        }
                    )
                }
            }
        )
    }

    _timerTick() {
        this.time.setSeconds(this.time.getSeconds(), -1);
        let timerObject = {
            hours: this.time.getHours(),
            minutes: this.time.getMinutes(),
            seconds: this.time.getSeconds(),
            userExamId: this.userExamId
        }
        if (this.timerIndex) this.timeExamObject[this.timerIndex] = timerObject;
        else {
            this.timeExamObject.push(timerObject);
            this.timeExamObject.map((item, i) => {
                if (item.userExamId == this.userExamId) this.timerIndex = i;
            })
        }
        localStorage.setItem('timeExamObject', JSON.stringify(this.timeExamObject));
        if (timerObject.hours === 0 && timerObject.minutes === 5 && timerObject.seconds === 0) {
            this.__auth.notificationInfo('5 Minutes Remaining');
        }
        if (timerObject.hours === 0 && timerObject.minutes === 0 && timerObject.seconds === 0) {
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
            this.statusCount();
            this.finishgrandExam();
        }
        this.x = setTimeout(() => this._timerTick(), 1000);
    }

    playPause() {
        if (this.buttonToggle) {
            this.buttonName = false;
            this.pauseTitle = "Pause Timer"
            if (this.pauseTime) {
                this.playTime = this.pauseTime;
            }
            else {
                this.playTime = this.time;
            }
            this.paused = false;
            this.resumed = true;
            let examurl = "/exam/write/" + this.userExamId;
            this.playTime.setSeconds(this.playTime.getSeconds(), -1);
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 5 && this.playTime.getSeconds() === 0) {
                this.__auth.notificationInfo('5 Minutes Remaining')
            }
            if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 0 && this.playTime.getSeconds() === 0) {
                window.clearInterval(this.x);
                localStorage.removeItem('timerObject');
                this.finishgrandExam();
                this.statusCount();
            }
            var x = setTimeout(() => this._timerTick(), 1000);
        }
        else {
            this.buttonName = true;
            this.playTitle = "Resume Timer"
            this.pauseTime = this.time;
            this.resumed = false;
            this.paused = true;
            window.clearInterval(this.x);
            localStorage.removeItem('timerObject');
        }
        this.buttonToggle = !this.buttonToggle
    }
    saveAnswer(answer, index) {
        if (!this.paused) {
            this.clickedItem = answer

            let correctAnswer = this.questionData.answer;
            this.questionId = this.questionPalette[this.currentPaletteId]._id;
            if (this.answerData === null) {
                this.updateFlag = false;
            } else {

                this.updateFlag = true;
                this.answered = true;
                this.userExamAnwserId = this.RealexamData.data.answer._id;
                this.clickedItem = this.RealexamData.data.answer.answer;

            }

            let subjectArray = this.questionData.subjectArray;
            let result;
            if (answer === correctAnswer) {
                result = "correct";
            } else {
                result = "wrong";
            }
            this.qstAnswerd = true;
            console.log("questionanswerrrs", this.qstAnswerd);

            if (this.updateFlag === false) {

                this.examService.saveExamAnswer(this.currentPaletteId, this.userExamId, this.examId, this.questionId, answer, result, this.updateFlag, subjectArray).subscribe(
                    res => {
                        this.answeredPalette = true;
                        this.answeredPaletteId = index;
                        this.answered = true;
                        this.updateFlag = true;
                        this.clickedItem = answer;


                        let data = {
                            '_id': this.questionId,
                            'status': 'answered'
                        };
                        this.questionPalette[this.currentPaletteId]._id = this.questionId;
                        this.questionPalette[this.currentPaletteId].status ="answered";
                        console.log(this.questionPalette[this.currentPaletteId],"this.questionPalette[this.currentPaletteId]");
                        
                        this.userExamAnwserId = res.data.userExamAnswerId;
                    }
                )
            } else {
                this.examService.updateExamAnswer(this.currentPaletteId, answer, result, this.userExamAnwserId, this.userExamId, this.examId, this.questionId).subscribe(
                    res => {
                        this.clickedItem = answer;
                        this.answered = true;
                        let data = {
                            '_id': this.questionId,
                            'status': 'answered'
                        };
                        this.questionPalette[this.currentPaletteId]._id = this.questionId;
                        this.questionPalette[this.currentPaletteId].status ="answered";
                        
                        console.log(this.questionPalette[this.currentPaletteId],"this.questionPalette[this.currentPaletteId]");
                        
                    }
                )
            }
        } else {
            console.log("user paused");

        }
    }


    next() {
        window.scrollTo(0, 0);
        this.currentPaletteId = this.currentPaletteId + 1;
        this.questionId = this.questionPalette[this.currentPaletteId]._id;
        this.examService.getExamQuestions(this.userExamId, this.questionId).subscribe(
            res => {
                this.RealexamData = res;
                this.answerData = res.data.answer;

                if (this.questionPalette.length === (this.currentPaletteId + 1)) {
                    this.noNextQuestion = true;
                    this.nextButtonLabel = "Submit";
                } else {
                    this.noNextQuestion = false;
                }

                if (this.answerData != null) {
                    this.answered = true;
                    this.clickedItem = this.RealexamData.data.answer.answer;
                } else {
                    this.answered = false;
                }

                this.questionData = res.data.question;
                this.options = JSON.parse(res.data.question.options);
            }
        )
    }

    previous() {
        window.scrollTo(0, 0);
        this.currentPaletteId = this.currentPaletteId - 1;
        this.questionId = this.questionPalette[this.currentPaletteId]._id;
        this.examService.getExamQuestions(this.userExamId, this.questionId).subscribe(
            res => {
                this.RealexamData = res;
                this.answerData = res.data.answer;
                if (this.answerData != null) {
                    this.answered = true;
                    this.clickedItem = this.RealexamData.data.answer.answer;
                } else {
                    this.answered = false;
                }

                if (this.questionPalette.length === (this.currentPaletteId + 1)) {
                    this.noNextQuestion = true;
                } else {
                    this.noNextQuestion = false;
                    this.nextButtonLabel = "Next";
                }
                this.questionData = res.data.question;
                this.options = JSON.parse(res.data.question.options);
            }
        )
    }

    mark(status) {
        console.log(status)
        this.examService.markExam(this.userExamId, this.questionId, status).subscribe(
            res => {
                if (!res.status) this.__auth.notificationInfo('OOPS! Something went wrong.')
                else {
                    if (status == 'marked') {
                        this.questionPalette[this.currentPaletteId].marked = status;
                    } else  {
                        if(this.answerData != null){
            this.questionPalette[this.currentPaletteId].status = 'answered'

                        }else {
                             this.questionPalette[this.currentPaletteId].status = '';
                            }
                    
                        this.questionPalette[this.currentPaletteId].marked = status;
                        
                    }
                }
            }
        )
    }

    goToQuestion(index) {
        if (!this.paused) {
            this.questionId = this.questionPalette[index]._id;
            this.examService.getGrandExamQuestions(this.userExamId, this.questionId).subscribe(
                res => {
                    this.RealexamData = res;
                    this.answerData = res.data.answer;
                    if (res.data.answer) {
                        this.answered = true;
                        this.answeredPalette = true;
                        this.answeredPaletteId = this.currentPaletteId;
                        this.clickedItem = res.data.answer.answer;
                    } else {
                        this.clickedItem = "";
                    }
                    this.questionData = res.data.question;
                    // this.currentPaletteId = index;
                    this.currentPaletteId = index;
                    this.options = JSON.parse(res.data.question.options);
                    if (this.questionPalette.length === (this.currentPaletteId + 1)) {
                        this.noNextQuestion = true;
                    } else {
                        this.noNextQuestion = false;
                    }

                }
            )
        } else {
            console.log("user paused");

        }
    }

    finishExamModal() {
        if (!this.qstAnswerd) {
            this.__auth.notificationInfo('Please attend one question')
        }
        else {
            jQuery("#submitExamFirst").modal("show");
        }

    }



    finishExam(userId) {
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
        if (!this.qstAnswerd) {
            this.__auth.notificationInfo('Please attend one question')

        } else {

            let alert = this.alertCtrl.create({
                title: 'Confirm submit',
                message: 'Do you want to submit exam?',
                buttons: [
                    {
                        text: 'Yes',
                        role: 'cancel',
                        handler: () => {
                            this.examService.finishExam(this.userExamId).subscribe(res => {
                                if (res.status) {
                                    let id = this.examId + '/' + this.userExamId;
                                    this.navCtrl.push(Details, { ExamIds: id });
                                } else {
                                    this.__auth.notificationInfo('OOPS! Something went wrong.')

                                }
                            });
                        }
                    },
                    {
                        text: 'No',
                        handler: () => {
                        }


                    }
                ]
            });
            alert.present();




        }

    }

    finishgrandExam() {
        this.examService.finishExam(this.userExamId).subscribe(res => {
            if (res.status) {
                window.clearInterval(this.x);
                localStorage.removeItem('timerObject');
                let id = this.examId + '/' + this.userExamId;
                this.navCtrl.push(Details, { ExamIds: id });

            } else {
                this.__auth.notificationInfo('OOPS! Something went wrong.');
            }
        });


    }
    

    filter(type) {
        let mapArray = (type) => {
            let tempArray = []
            tempArray = (this.tempQuestionPallete).filter(item => {
                if (item.status == type) return item;
            })

            if (tempArray.length) this.questionPalette = tempArray, this.goToQuestion(0), this.currentPaletteId = 0;
            else this.__auth.notificationInfo('No questions.')
        }
        let markedArray = (type) => {
            let tempArray = []
            tempArray = (this.tempQuestionPallete).filter(item => {
                if (item.marked == type) return item;
            })

            if (tempArray.length) this.questionPalette = tempArray, this.goToQuestion(0), this.currentPaletteId = 0;
            else this.__auth.notificationInfo('No questions.')
        }
        if (type == 'm') {
            markedArray('marked')
        }
        else if (type == 'a') {
            mapArray('answered')
        }
        else if (type == 'na') {
            mapArray("")
        }
        else {
            this.questionPalette = this.tempQuestionPallete;
            this.currentPaletteId = 0;
            this.goToQuestion(0)
        }
    }

    statusCount() {
        let marked = 0, unanswered = 0, answered = 0;
        this.tempQuestionPallete.filter((item) => {
            if (item.status == 'answered') answered++;
            else unanswered++;
        })
        this.paletteStatus = [answered, marked, unanswered];
    }
    back() {
        this.navCtrl.push(Exam);


    }
    findImage(i) {
        if (this.questionData[i].question_image) {
            this.imageFlag = true;
            this.imageUrl = this.url + this.questionData.question_image;
            console.log("imageeee", this.imageUrl);

        } else {
            this.imageFlag = false;
        }

    }
    toggleDetails(item) {
        if (this.isGroupShown(item)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = item;
        }
    }
    isGroupShown(item) {
        return this.shownGroup === item;
    }

}
