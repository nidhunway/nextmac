import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ExamService } from '../../../services/exam.service';
import { AuthService } from '../../../services/auth.service';
import { ToastController } from 'ionic-angular';
import { Details } from '../../../pages/exam/exam-room/details/details';
import { aiimsResult } from '../../../pages/exam/exam-room/aiimsResult/aiimsResult';

import { Exam } from '../../exam/exam';
import { homeNew } from '../../../pages/homeNew/homeNew';
import { writeExam } from '../../../pages/exam/createexam/write-exam/write-exam';
import { aiimsExam } from '../../../pages/exam/createexam/aiimsExam/aiimsExam';

import { CreateExam } from '../../../pages/exam/createexam/createExam';

import { grantExam } from '../../../pages/exam/GrandExam/grantexam.component';
import { termsrules } from '../../../pages/exam/weekend/TermsAndRules/terms.component';
import { HomePage } from '../../../pages/dashboard/home';
import { weekendReg } from '../../../pages/exam/weekend/weekend.component';

@Component({
  selector: 'page-examComponent',
  templateUrl: 'examComponent.html',
  providers: [ExamService, AuthService]
})
export class examComponent {
  userData;
  customDiv: boolean;
  mode;
  examListComplete;
  registerLabel;
  singleExamId;
  paymentId;
  postData;
  plan;
  isRegistered: boolean = false;
  paymentDetails;
  RegisterMsg;
  spinnerLoad: boolean;
  completedWeekExam = [];
  pendingWeekExam = [];
  weekExam = [];
  pendingWeekExaminactive = [];
  pendingWeekexam = [];
  activeWeekExam = [];
  custmizedExam = [];
  username;
  shownGroup = null;
  dailyExams = [];
  totalWeek = [];
  dailyDiv: boolean = false;
  weekDiv: boolean = false;
  totalAiims=[];
  constructor(public platform: Platform, public navCtrl: NavController, private examService: ExamService, public __auth: AuthService, public navParams: NavParams, public toastCtrl: ToastController) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
  }
  ngOnInit() {
    this.getWeek();
    this.getCustomizedExam();
    this.checkAplay();
    this.getAiims();
    //this.getDailyExams();
    this.username = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
      return letter.toUpperCase();
    })

  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
  private backButtonFunc(): void {
    this.navCtrl.setRoot(HomePage);
  }
  subscribe(category) {
    this.navCtrl.push(weekendReg,{exam:category});
  }
  createExam() {
    this.navCtrl.push(CreateExam);

  }
  writeAiims(){
    this.navCtrl.push(aiimsExam);
  }
  getAiims(){
    
    this.examService.getSubscribeExam().subscribe(
      res => {
        console.log("res subcribed exams",res);
        if(res.data){
          if(res.data.activeExams.length>0){
            let exam = (res.data.activeExams).map((item) => {
              item.examPending = true;
              item.examId=item._id;

              return item;
            });
            this.totalAiims=exam;
          }
          if(res.data.completedExams.length>0){
            let completedAiims=[];
            let exam = (res.data.completedExams).map((item) => {
              item.examCompleted = true;
              item.examName=item.examDetails.name;
              item.examDate=item.examDetails.examDate;

              item.examType=item.examDetails.examType;
              item.examDate=item.examDetails.examDate;
             item.numberOfQuestions= item.examResult.total;
            item.examinformation= item.examDetails.description;
              return item;
            });
            completedAiims=exam;
            console.log("res.data.completedExams111111111111",this.totalAiims);
            this.totalAiims = (completedAiims.length > 0) ? this.totalAiims.concat(completedAiims) : this.totalAiims;

         
            console.log("res.data.completedExams",this.totalAiims);

          }
          if(res.data.inactiveExams.length>0){

       let exam = (res.data.inactiveExams).map((item) => {
        item.inactiveExam = true;
              return item;
            });
            this.totalAiims = (exam.length > 0) ? this.totalAiims.concat(exam) : this.totalAiims;

          }
          if(res.data.pendingExams.length>0){
            let exam = (res.data.pendingExams).map((item) => {
              item.examPending = true;
              item.examName=item.examDetails.name;
              item.examType=item.examDetails.examType;
              item.examDate=item.examDetails.examDate;

              item.examinformation=item.examDetails.description;
              item.packageId=item.examDetails.packageId;
              item.examDate=item.examDetails.examDate;
                    return item;
                  });
                  this.totalAiims = (exam.length > 0) ? this.totalAiims.concat(exam) : this.totalAiims;
                  console.log("before sorting",this.totalAiims);
                  
                  this.totalAiims = this.totalAiims.sort((a, b) => new Date(a.examDate).getTime() - new Date(b.examDate).getTime());
                  console.log("after sorting",this.totalAiims);

          }
          console.log("total aiims exam", this.totalAiims);
          

        }else {

          this.__auth.notificationInfo("There is no data found!");
        }
        
      });
  }
  getWeek() {
    this.examService.getWeekExams().subscribe(
      weekRes => {
        console.log("weeeekres", weekRes);

        if (weekRes.data) {
          if (weekRes.data.completedExams.length > 0) {
            this.completedWeekExam = weekRes.data.completedExams;
            let exam = (this.completedWeekExam).map((item) => {
              item.examName = item.examDetails.name;
              item.examType = item.examDetails.examType;
              item.examDate = item.examDetails.examDate;
              item.examCompleted = true;
              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              } else if (item.examResult.totalQuestions) {
                item.numberOfQuestions = item.examResult.totalQuestions;

              }
              return item;
            });
            this.completedWeekExam = exam;
            console.log(" this.completedWeekExam", this.completedWeekExam);
            
          }
          if (weekRes.data.activeExams.length > 0) {
            this.activeWeekExam = weekRes.data.activeExams;
            console.log("this.activeWeekExam", this.activeWeekExam);

            let exam = (this.activeWeekExam).map((item) => {
              //  item.examName =item.examDetails.name;

              item.examPending = true;
              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;
            });
            this.activeWeekExam = exam;
            console.log(" this.activeWeekExam", this.activeWeekExam);
            
            this.completedWeekExam = (this.activeWeekExam.length > 0) ? this.completedWeekExam.concat(this.activeWeekExam) : this.completedWeekExam;
          }
          if (weekRes.data.inactiveExams.length > 0) {
            this.pendingWeekExaminactive = weekRes.data.inactiveExams;
            let exam = (this.pendingWeekExaminactive).map((item) => {
              item.inactiveExam = true;

              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;
            });
            this.pendingWeekExaminactive = exam;
            console.log("this.pendingWeekExaminactive",this.pendingWeekExaminactive);
            
            this.completedWeekExam = (this.pendingWeekExaminactive.length > 0) ? this.completedWeekExam.concat(this.pendingWeekExaminactive) : this.completedWeekExam;

          }
          if (weekRes.data.pendingExams.length > 0) {
            this.pendingWeekexam = weekRes.data.pendingExams;
            let exam = (this.pendingWeekexam).map((item) => {
              item.examName = item.examDetails.name;
              item.examType = item.examDetails.examType;
              item.examDate = item.examDetails.examDate;
              item.examPending = true;
              if (item.numberOfQuestions) {
                item.numberOfQuestions = item.numberOfQuestions;
              } else if (item.noOfQuestions) {
                item.numberOfQuestions = item.noOfQuestions;

              }
              return item;
            });
            this.pendingWeekexam = exam;
            console.log(" this.pendingWeekexam", this.pendingWeekexam);
            
            this.completedWeekExam = (this.pendingWeekexam.length > 0) ? this.completedWeekExam.concat(this.pendingWeekexam) : this.completedWeekExam;
          }

          
        //  this.completedWeekExam = this.completedWeekExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());

          let exam = (this.completedWeekExam).map((item, index) => {


            if (item.examType == "daily") {
              item.dailyExam = true;

              this.dailyExams.push(item);


            } else {
              item.dailyExam = false;
              this.totalWeek.push(item);

            }


            return item;

          });

          this.completedWeekExam = exam;
          console.log("qqqqqqqqqqqqqqqqqq",this.totalWeek );
          
          
          this.dailyExams = this.dailyExams.sort((a, b) => new Date(b.examDate).getTime() - new Date(a.examDate).getTime());
          this.totalWeek = this.totalWeek.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          console.log("wwwwwww", this.dailyExams);
          if (this.dailyExams.length == 0) {
            this.dailyDiv = true;
            this.__auth.notificationInfo("There is no daily exams found!");

          } else {
            this.dailyDiv = false;
          }
          if (this.totalWeek.length == 0) {
            this.weekDiv = true;
            this.__auth.notificationInfo("There is no daily exams found!");

          } else {
            this.weekDiv = false;
          }
         



        } else {

          this.__auth.notificationInfo("There is no data found!");
        }
      });
  }
  getCustomizedExam() {

    this.mode = 'completed';
    this.examService.fetchAllExam(this.mode).subscribe(
      res => {
        console.log("res info////", res);
        if (res.data) {

          this.custmizedExam = res.data.examList;
          let exam = (this.custmizedExam).map((item) => {
            item.customizedExam = true;
            item.completed = true;
            return item;

          });
          this.custmizedExam = exam;
          // this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
          this.mode = 'pending';
          this.examService.fetchAllExam(this.mode).subscribe(
            res => {
              let customizedPending = [];
              customizedPending = res.data.examList;
              let exam = (customizedPending).map((item) => {
                item.customizedExam = true;
                item.pending = true;
                return item;

              });
              customizedPending = exam;

              // customizedPending = customizedPending.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
              this.custmizedExam = (customizedPending.length > 0) ? this.custmizedExam.concat(customizedPending) : this.custmizedExam;

              this.examService.fetchGrandExam().subscribe(
                res => {

                  let completedGexam = [];
                  if (res.data.completedExam.length > 0) {
                    completedGexam = (res.data.completedExam).map((item) => {
                      item.examType = "grand";
                      item.examName = item.name;
                      item.completed = true;
                      return item;
                    })

                    // completedGexam = completedGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    this.custmizedExam = (completedGexam.length > 0) ? this.custmizedExam.concat(completedGexam) : this.custmizedExam;

                  }
                  if (res.data.pendingExams.length > 0) {
                    let pendingGexam = [];
                    pendingGexam = (res.data.pendingExams).map((item) => {
                      item.examType = "grand";
                      item.examName = item.name;
                      item.pending = true;
                      return item;
                    })
                    //pendingGexam = pendingGexam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                    this.custmizedExam = (pendingGexam.length > 0) ? this.custmizedExam.concat(pendingGexam) : this.custmizedExam;

                  }
                  this.custmizedExam = this.custmizedExam.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.createdDate).getTime());
                  console.log("get all custm", this.custmizedExam);
                  if (this.custmizedExam.length == 0) {
                    this.customDiv = true;
                  } else {
                    this.customDiv = false;
                  }

                });
            });
        } else {
          this.__auth.notificationInfo("There is no data found!");

        }
      });

  }

  resumeExam(exam) {
    console.log("exammmm", exam);

    if (!exam.examType) {
      if (exam.examId) {

        this.navCtrl.push(writeExam, { subject: exam._id });
      }
      else {
        this.examService.startExam(exam._id).subscribe(
          res => {
            let userExamId = res.data.userExamId;
            this.navCtrl.push(writeExam, { subject: userExamId });
          });
      }
    } else if (exam.examType == "grand") {
      this.navCtrl.push(grantExam, { subject: exam._id });
    } else if (exam.examType == "week" || exam.examType == "special" || exam.examType == "daily") {
      console.log("exam._id ", exam._id, exam.examId);

      this.navCtrl.push(termsrules, { subject: (exam.examId) ? exam.examId : exam._id });
    }else if(exam.examType == "AIIMS"  ){
      this.navCtrl.push(aiimsExam, { subject: exam.examId,package:exam.packageId });

    }
    else if(exam.examType == "JIPMER" ){
     
      
      this.navCtrl.push(aiimsExam, { subject: exam.examId,package:exam.packageId });

    }
  }
  getmoreWeek() {
    this.navCtrl.push(Exam, { examType: "week" });

  }
  getDailyExams() {
    this.navCtrl.push(Exam, { examType: "daily" });
  }
  getMoreAiims(){
    this.navCtrl.push(Exam, { examType: "aiims" });
  }
  getmoreCustomized() {
    this.navCtrl.push(Exam, { examType: "cutomized" });
  }
  // backToCompletedPage() {
  //   this.navCtrl.push(HomePage);
  // }
  checkAplay() {
    this.examService.checkAppliedForExam().subscribe(data => {
      if (data.status) {
        this.registerLabel = "Register"
        this.isRegistered = false;
        this.paymentDetails = data;
      }
      else {
        this.registerLabel = "Already registered"
        this.RegisterMsg = "You are registered"
      }
    }, err => {
      this.__auth.notificationInfo("OOPS Something went wrong.")
    });
  }

  newExamfunction(plan, desc, item) {
    this.__auth.notificationInfo("only for institutional users");

    // if (item) {
    //   this.singleExamId = item._id;
    // }
    // this.plan = plan;
    // if (!this.isRegistered) {
    //   let options = {
    //     // key: 'rzp_test_V1RjYTq2Xll4NR',
    //     key: this.paymentDetails.data.keyId,
    //     amount: (this.paymentDetails.data.weeklyDetails[plan].amount) * 100,
    //     name: desc,
    //     prefill: {
    //       name: this.userData.profile.firstName + this.userData.profile.lastName,
    //       email: this.userData.username
    //     },
    //     notes: {
    //       address: ""
    //     },
    //     theme: {
    //       color: "#108fd2"
    //     }
    //   }
    //   var successCallback = (payment_id) => {
    //     this.callApi(payment_id);
    //   };
    //   var cancelCallback = (error) => {
    //   };
    //   this.platform.ready().then(() => {
    //     RazorpayCheckout.open(options, successCallback, cancelCallback);
    //   })
    // }
    // else this.__auth.notificationInfo('Somthing went wrong.')
  }

  callApi(payment_id) {
    this.paymentId = payment_id;
    if (this.singleExamId) {
      this.postData = {
        examId: this.singleExamId,
        paymentId: this.paymentId,
        paymentStatus: "done",
        paymentMethod: "online",
        PlaneName: this.plan
      }
    } else {
      this.postData = {
        paymentId: this.paymentId,
        paymentStatus: "done",
        paymentMethod: "online",
        PlaneName: this.plan
      }
    }

    this.examService.SinglePaymentExam(this.postData)
      .subscribe(
        res => {
          if (res.status) {
            // localStorage.removeItem('paymentId');

            // this.registerLabel = "Already registered"
            // this.isRegistered = true;
            this.checkAplay();
            this.getWeek();
          }
        }, err => {
          this.__auth.notificationInfo('Somthing went wrong.')
        })

  }
  viewResult(item) {
    console.log("item", item);

    let exam_id;
    if (item.examType == "grand") {
      exam_id = item._id + '/g' + '/fromCompleted'
      this.navCtrl.push(Details, { ExamIds: exam_id });
    } else if (item.examType === "week" || item.examType === "special" || item.examType === "daily") {
      this.navCtrl.push(Details, { ExamIds: item._id + '/week' });
    }else if(item.examType == "AIIMS" || item.examType == "JIPMER"){
      this.navCtrl.push(aiimsResult, { ExamIds: item.examId +  '/' + item._id  + '/' + item.examType });
    }
     else {
      exam_id = item.examId + '/' + item._id + '/fromCompleted';
      this.navCtrl.push(Details, { ExamIds: exam_id });
    }
  }


  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };
  AIIMSPAYROUTE(){
    this.__auth.notificationInfo("only for institutional users");

    // this.navCtrl.push(weekendReg)
  }
}