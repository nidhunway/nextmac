import { Component, NgZone,ViewChild  } from '@angular/core';
import { NavController, Platform,NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { StudentService } from '../../../services/student.service';
import { WeeklyService } from '../../../services/weekend.service';
import { AuthService } from '../../../services/auth.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Slides } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

// import { StatusBar, Splashscreen } from 'ionic-native';
import { termsrules } from '../../../pages/exam/weekend/TermsAndRules/terms.component';
import { Content } from 'ionic-angular';

@Component({
  selector: 'page-weekendpage',
  templateUrl: 'weekend.component.html',
  providers: [WeeklyService, AuthService, StudentService]
})
export class weekendReg {
  @ViewChild(Slides) slides: Slides;
  @ViewChild("contentRef") contentHandle: Content;
  private tabBarHeight;
  private topOrBottom: string;
  private contentBox;
  details: boolean = false;
  buttonColor;
  bgColor;
  AIIMSKEYID;
  bgColor1:boolean =false;
  bgColor2:boolean;
  color;
  details1: boolean = false;
  details2: boolean = false;
  registerLabel;
  isRegistered: boolean = true;
  bgColor3:boolean=false;
  paymentDetails;
  plan;
  userData;
  paymentId;
  examList;
  singleExamId;
  postData;
  RegisterMsg;
  details3;
  MyCode;
  paymentdata:number;
  selecteditem: boolean;
  coupenCode;
  coupenName;
  copName;
  copCode;
  myamount:number;
  refcode;
  AiimsAmount;
  AiimsButton;
  packageId;
aiimsDiv:boolean=false;
  
  paymentdata2:number;
  discountAmount;
  updateapp:boolean=true;
  dailyApply:boolean=true;
  amountRound;
  fullApply:boolean=true;
  weekendApply:boolean=true;
  basicButton:boolean=false;
  newamouts;
  examCategory;
  slideIndex:number;
  constructor(private alertCtrl: AlertController,public platform: Platform,public navParams: NavParams, private transfer: FileTransfer, private file: File, public navCtrl: NavController, public __wekklyExam: WeeklyService, public __notfiSer: AuthService, public StudentSer: StudentService) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.examCategory = navParams.get('exam');
    console.log("examCategory",this.examCategory);
    
this.paymentdata=2999;
this.paymentdata2=3599;

  }
  //nidhun coupencode
  getMycoupenCode(){
    this.__wekklyExam.getallCode().subscribe(
      code => {
        this.coupenCode=code.coupenCode;
        console.log(this.coupenCode,"my code");
        
     
      }
    );
  }
  Feedback(value){
    if(value){
      this.coupenName=value.promocode.toUpperCase();
      for(var i=0;i<this.coupenCode.length;i++){
        if(this.coupenName==this.coupenCode[i].coupenDetails.coupenName){
          this.copName=this.coupenCode[i].coupenDetails.coupenName;
          this.copCode=this.coupenCode[i].coupenDetails.discountAmount;
          console.log(this.copName,this.copCode,"coupen names")
        }
      }
      if(this.copName){
        this.Namefunction(this.copName);
        this.copName="";
  
        this.paymentdata2=(this.paymentdata2)-((this.paymentdata2)*(this.copCode))/100;
        this.discountAmount=this.paymentdata2;
        this.fullApply=true;
        this.__notfiSer.notificationInfo("coupon code applied")
      }else{
        this.paymentdata2=3599;

        this.__notfiSer.notificationInfo("Invalid coupon code")
      }

    }

  }
  basicForm(value){
    
    if(value){
      this.coupenName=value.promocode.toUpperCase();
      for(var i=0;i<this.coupenCode.length;i++){
        if(this.coupenName==this.coupenCode[i].coupenDetails.coupenName){
          this.copName=this.coupenCode[i].coupenDetails.coupenName;
          this.copCode=this.coupenCode[i].coupenDetails.discountAmount;
          console.log(this.copName,this.copCode,"coupen names")
        }
      }
      if(this.copName){
        this.Namefunction(this.copName);
        this.copName="";
  
        this.paymentdata=(this.paymentdata)-((this.paymentdata)*(this.copCode))/100;
        this.discountAmount=this.paymentdata;
        this.dailyApply=true;
        this.__notfiSer.notificationInfo("coupon code applied")
      }else{
        this.paymentdata=2999;

        this.__notfiSer.notificationInfo("Invalid coupon code")
      }

    }
   
  

  }
  Namefunction(name){
    this.refcode=name;

  }
  ionViewDidEnter() {
    this.topOrBottom = this.contentHandle._tabsPlacement;
    this.contentBox = document.querySelector(".scroll-content");

    if (this.topOrBottom == "top") {
      this.tabBarHeight = this.contentBox.marginTop;
    } else if (this.topOrBottom == "bottom") {
      this.tabBarHeight = this.contentBox.marginBottom;
    }
  }
  scrollingFun(e) {
    if (e.scrollTop > this.contentHandle.getContentDimensions().contentHeight) {
      // document.querySelector(".tabbar")['style'].display = 'none';
      if (this.topOrBottom == "top") {
        this.contentBox.marginTop = 0;
      } else if (this.topOrBottom == "bottom") {
        this.contentBox.marginBottom = 0;
      }
    } else {
      // document.querySelector(".tabbar")['style'].display = 'flex';
      if (this.topOrBottom == "top") {
        this.contentBox.marginTop = this.tabBarHeight;
      } else if (this.topOrBottom == "bottom") {
        this.contentBox.marginBottom = this.tabBarHeight;
      }

    }//if else
  }//scrollingFun
  ionViewDidLoad(){
    console.log("enter herere");
    
    if(this.slideIndex){
      setTimeout(() =>

      this.slides.slideTo(this.slideIndex, 1000)
      ,1000);
    }
  } 
  ngOnInit() {
    this.getMycoupenCode();
    this.Amount();
    this.checkAplay();
    this.singlePay();
    this.SUBSCRIBE();
    if(!this.examCategory){
   this.getData1();
    this.getfirstcolor();
    }else if(this.examCategory=="week"){
      this.getData2();

    }else if(this.examCategory=="daily"){
this.getData();
    }else if(this.examCategory=="aiims"){
      this.slideIndex=3;
      console.log("this.slideIndex",this.slideIndex);
      
      //this.goToSlide()
this.getData4();
    
  }
  }
 
  //pdf file download
  download() {
    const fileTransfer: FileTransferObject = this.transfer.create();
    const mime = 'application/pdf';
    const url = 'assets/img/netfellowTimetable.pdf';
    fileTransfer.download(url, this.file.dataDirectory + 'netfellowTimetable.pdf').then((entry) => {
      console.log(entry, "dow1")
      console.log('download complete: ' + entry.toURL());
    }, (error) => {
      console.log(error, "down2")
    });
  }
  getfirstcolor() {
    
    this.selecteditem = true;
  }
  singlePay() {
    this.__wekklyExam.getWeekExams().subscribe(
      weekRes => {
        if (weekRes.status) {
          this.examList = weekRes.data.inactiveExams;
          this.examList = this.examList.sort((a, b) => new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime());
          console.log(this.examList, "exam list bro")
        }
      }
    );
  }
  //check applyed or not
  checkAplay() {
    this.__wekklyExam.checkAppliedForExam().subscribe(data => {
      console.log(data, "data here")      

      if (data.status) {
        console.log(this.paymentdata,"data check")
        this.registerLabel = "Register"
        this.isRegistered = false;
        this.fullApply=false;
        this.paymentDetails = data;
        this.dailyApply=false;

      }
      else {
        this.__wekklyExam.basicUserTest().subscribe(data => {
          console.log(data,"basic nidhun1");

          if(data.status) {
            this.paymentDetails = data.keyId;

            this.dailyApply=false;
            this.fullApply=true;
            this.basicButton=true;
            this.registerLabel = "Register"
            this.RegisterMsg = "you have offer for full package"
          }else{

            this.dailyApply=true;
            this.fullApply=true;
            this.registerLabel = "Already registered"
            this.RegisterMsg = "You are registered"

          }
          
        })

      }
    }, err => {
      this.__notfiSer.notificationInfo("OOPS Something went wrong.")

    });
  }


  //payment function
  payment(plan, desc, item) {
    if(plan=="basic"){
      this.myamount=this.paymentdata;
       this.amountRound=Math.round(this.myamount)
    }else{
      this.myamount=this.paymentdata2;
         this.amountRound=Math.round(this.myamount)

    }
     console.log(this.myamount,"amount here")
    if (item) {
      this.singleExamId = item._id;
    }
    this.plan = plan;
    if (!this.isRegistered) {

      let options = {
        // key: 'rzp_test_V1RjYTq2Xll4NR',
        key: this.paymentDetails.data.keyId,
        amount:this.amountRound*100,
        name: desc,
        prefill: {
          name: this.userData.profile.firstName + this.userData.profile.lastName,
          email: this.userData.username
        },
        notes: {
          address: ""
        },
        theme: {
          color: "#108fd2"
        }


      }

      var successCallback = (payment_id) => { // <- Here!
        this.serviceCall(payment_id);

      };

      var cancelCallback = (error) => { // <- Here!
        console.log(error.description);
      };

      this.platform.ready().then(() => {
        RazorpayCheckout.open(options, successCallback, cancelCallback);
      })

    }
    else this.__notfiSer.notificationInfo("Already registered.")

  }
  //test
  // paymentForBasic(plan, desc,item) {
  //    this.newamouts=700;

  //     try {
  //       let options = {
  //         key: 'rzp_test_V1RjYTq2Xll4NR',
  //         // key: this.paymentDetails,
  //         amount:this.newamouts*100,
  //         name: desc,
  //         prefill: {
  //           name: this.userData.profile.firstName + this.userData.profile.lastName,
  //           email: this.userData.username
  //         },
  //         notes: {
  //           address: ""
  //         },
  //         theme: {
  //           color: "green "
  //         },
  //         handler: this.serviceCallBasic.bind(this)
  //       }
  //       let rzp1 = new this.StudentSer.nativeWindow.Razorpay(options);
  //       rzp1.open();
  //     }
  //     catch (err) {
  //       this.__notfiSer.notificationInfo("OOPS! Something went wrong");
  //     }
    
  // }
  // paymentResponseHander(){
  //   this.postData = {
  //     paymentId: this.paymentId,
  //     paymentStatus: "done",
  //     paymentMethod: "online",
  //     PlaneName: this.plan,
  //     refCode:(this.refcode) ? this.refcode : null,
  //     discount:this.discountAmount,
  //     updateapp:true



  //   }
  //   console.log(this.postData,"my data here")
  //   this.__wekklyExam.registerWeeklyExam(this.postData)
  //   .subscribe(
  //   res => {
  //     console.log(res,"nidhun check");
      

  //     if (res.status) {
  //       this.registerLabel = "Already registered"
  //       this.isRegistered = true;
  //       this.checkAplay();
  //     }else{
  //                 this.__notfiSer.notificationInfo(res.message)
  //        }
  //   }, err => {
  //     this.__notfiSer.notificationInfo("OOPS Something went wrong.")

  //   })

  // }
  //test end
  paymentForBasic(plan, desc, item){

     this.newamouts=700;
      let options = {
        // key: 'rzp_test_V1RjYTq2Xll4NR',
        key: this.paymentDetails,
        amount:this.newamouts*100,
        name: desc,
        prefill: {
          name: this.userData.profile.firstName + this.userData.profile.lastName,
          email: this.userData.username
        },
        notes: {
          address: ""
        },
        theme: {
          color: "#108fd2"
        }


      }

      var successCallback = (payment_id) => { // <- Here!
        this.serviceCallBasic(payment_id);

      };

      var cancelCallback = (error) => { // <- Here!
        console.log(error.description);
      };

      this.platform.ready().then(() => {
        RazorpayCheckout.open(options, successCallback, cancelCallback);
      })

    

  }
  serviceCallBasic(payment_id) {
    this.paymentId = payment_id;


    this.__wekklyExam.registerWeeklyExamBasic(this.paymentId)
      .subscribe(
      res => {

        if (res.status) {
          this.registerLabel = "Already registered"
          this.isRegistered = true;
          this.dailyApply=true;
          this.basicButton=false;
        this.fullApply=true;
        this.__notfiSer.notificationInfo("Register successful");

          this.checkAplay();
        }else{
          this.__notfiSer.notificationInfo(res.message)
          }
      }, err => {
        this.__notfiSer.notificationInfo("OOPS Something went wrong.")

      })

  }
  serviceCall(payment_id) {
    this.paymentId = payment_id;
    if (this.singleExamId) {
      this.postData = {
        examId: this.singleExamId,
        paymentId: this.paymentId,
        paymentStatus: "done",
        paymentMethod: "online",
        PlaneName: this.plan
      }
    } else {
      this.postData = {
        paymentId: this.paymentId,
        paymentStatus: "done",
        paymentMethod: "online",
        PlaneName: this.plan,
        refCode:(this.refcode) ? this.refcode : null,
        discount:this.amountRound,
        updateapp:true
        

      }
    }

    this.__wekklyExam.registerWeeklyExam(this.postData)
      .subscribe(
      res => {

        if (res.status) {
          this.registerLabel = "Already registered"
          this.isRegistered = true;
          this.dailyApply=true;
        this.fullApply=true;
          this.checkAplay();
        }else{
          this.__notfiSer.notificationInfo(res.message)
          }
      }, err => {
        this.__notfiSer.notificationInfo("OOPS Something went wrong.")

      })

  }

  getData() {
    this.details = true;
    this.details1 = false;
    this.details2 = false;
    this.details3=false;
    this.selecteditem = false;
    this.bgColor1 = true;
    this.bgColor2 = false;
    this.aiimsDiv=false;

  }

  getData1() {
    this.details1 = true;
    this.details = false;
    this.details2 = false;
    this.details3=false;
    this.bgColor1 = false;
    this.bgColor2 = false;
    this.aiimsDiv=false;
    this.bgColor3= false;
    this.selecteditem = true;

  }
  getData3(){
    this.details3=true;
    this.details = false;
    this.details2 = false;
    this.aiimsDiv=false;
    this.bgColor3 = false;
  }

  getData2() {


    this.details2 = true;
    this.details1 = false;
    this.details = false;
    this.selecteditem = false;
    this.bgColor1 = false;
    this.bgColor2 = true;
    this.aiimsDiv=false;
    this.bgColor3 = false;
    if (this.RegisterMsg) this.__notfiSer.notificationInfo(this.RegisterMsg)


    //this.bgColor2 = '#108fd2'; 
  }
  
  getData4() {
    this.aiimsDiv=true;
    this.details1 = false;
    this.details = false;
    this.details2 = false;
    this.details3=false;
    this.bgColor1 = false;
    this.bgColor2 = false;
    this.bgColor3 = true;
    this.selecteditem = false;


    

  }
  //AIIMs/jipmer
  SUBSCRIBE(){
    this.__wekklyExam.__SUBSCRIBEPAY().subscribe(res=>{
      
      console.log(res,"res");
      
      if(res.status) {
        this.AiimsButton=true 
      this.AIIMSKEYID=res.data.keyId;
      }
      else this.AiimsButton=false;this.AIIMSKEYID=res.data.keyId;

       },err=>{
        this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
      })

    }
  //AIIMS amount

  Amount(){
    this.__wekklyExam.__AiimsPay().subscribe(res=>{
      if(res.status) this.AiimsAmount = res.data[0].price;
      this.packageId = res.data[0]._id
      },err=>{
      this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
     })
    }
    AIIMSPAYNOW(){
      let options = {
        // key: 'rzp_test_V1RjYTq2Xll4NR',
        key: this.AIIMSKEYID,
        amount:this.AiimsAmount*100,
        name: "AIIMS/JIPMER SERIES",
        prefill: {
          name: this.userData.profile.firstName + this.userData.profile.lastName,
          email: this.userData.username
        },
        notes: {
          address: ""
        },
        theme: {
          color: "#108fd2"
        }


      }

      var successCallback = (payment_id) => { // <- Here!
        this.AIIMSPAYMENT_SUCCESS(payment_id);

      };

      var cancelCallback = (error) => { // <- Here!
        console.log(error.description);
      };

      this.platform.ready().then(() => {
        RazorpayCheckout.open(options, successCallback, cancelCallback);
      })
    }
    PROPOP(){
      this.__wekklyExam.MSGIOS().subscribe(res=>{
        console.log(res);
        
        if(res.status){
          let alert = this.alertCtrl.create({
            title: 'Notification',
            subTitle: 'Please check mail',
            buttons: ['ok']
          });
          alert.present();
        }else  this.__notfiSer.notificationInfo("Please try again");

       })
     
    }
    AIIMSPAYMENT_SUCCESS(paymnetId){
      this.AiimsButton=true;

      this.__wekklyExam.___AIIMSPAY(this.packageId,(this.MyCode)?this.MyCode:null,paymnetId,this.AiimsAmount).subscribe(res=>{
      

        if(res.status) {
          this.SUBSCRIBE();
          this.AiimsButton=true;
          this.__notfiSer.notificationInfo("Registeration successful");
        }else{
          this.SUBSCRIBE();
          this.AiimsButton=false;
          this.__notfiSer.notificationInfo(res.message);

        }
      },err=>{
        this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
  
      })
      this.SUBSCRIBE();
      this.AiimsButton=true;

    }
    //AIIMS coupon 

    CouponCode(value){
      console.log(value,"value data");
      
      this.__wekklyExam.__couponcode(this.packageId,value.couponCode).subscribe(res=>{
       
        if(res.data){
          this.MyCode = res.data[0].couponCode.discountPercentage
          this.AiimsAmount = res.data[0].couponCode.price
          this.__notfiSer.notificationInfo("Discount applied ");
        }
        else {
          this.__notfiSer.notificationInfo("Invalid discount code ");
        }
         },err=>{
              this.__notfiSer.notificationInfo("OOPS! Something went wrong ");
        
            }
             )
            
        }
        
        
      
            
       
       
}
