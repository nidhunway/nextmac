import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController, NavParams,Platform } from 'ionic-angular';
import { environment } from '../../../../environments/environment';
import { WeeklyService } from '../../../../services/weekend.service';
import { AuthService } from '../../../../services/auth.service';
import { AlertController } from 'ionic-angular';
import { Details } from '../../../../pages/exam/exam-room/details/details';
import { ToastController } from 'ionic-angular';
import { Exam } from '../../../../pages/exam/exam';
import { examComponent } from '../../../../pages/exam/examNew/examComponent';
import { naveBarShowSrvice } from '../../../../services/navBarService';
import { Events } from 'ionic-angular';

@Component({
  selector: 'page-termsrules',
  templateUrl: 'terms.component.html',
  providers: [WeeklyService, AuthService,naveBarShowSrvice]
})
export class termsrules {
  public examId;
  tabBarElement: any;
  userExamId: string;
  questionPalette: Array<any> = [];
  currentPaletteId: number = 0;
  question: string;
  options = [];
  slNumber: number;
  questionId: string;
  questionData: any;
  clickedItem: string;
  subjectArray: Array<any> = [];
  answerData;
  answered: boolean;
  pauseTime;
  playTime;
  timeFlag: boolean;
  zeroTime: boolean = false;
  buttonName: boolean = false;
  paused: boolean = false;
  resumed: boolean = false;
  buttonToggle: boolean = false;
  playTitle;
  pauseTitle;
  imageFlag: boolean;
  imageUrl: string;
  qPallete; //for sorting and marking, copy of questionPalette
  url = environment.backendUrl + '/images/';
  paletteStatus: Array<number> = [];
  time;
  previousTimer;
  x;
  timerObj;
  timer = false;
  testObject: Array<any> = [];
  timerIndex;
  timerValues;
  constructor(public _nav:naveBarShowSrvice,public navCtrl: NavController,public events: Events,public platform: Platform, private alertCtrl: AlertController, public __auth: AuthService, public weeklyService: WeeklyService, public toastCtrl: ToastController, public navParams: NavParams) {
    this.examId = navParams.get('subject');
    this.events.publish('user:created', true, Date.now());

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    console.log(this.tabBarElement,"nav show or hiden")
    this.tabBarElement.style.display = 'none';
    this._nav.navHide(true);

  }
  ionViewWillEnter(): void {
    this.tabBarElement.style.display = 'none';
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
 
  private backButtonFunc(): void {
    this.navCtrl.setRoot(examComponent);
  }
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
    window.clearInterval(this.x);
    localStorage.removeItem('timerObject');

  }
  ngOnInit() {
    this.weeklyService.loadWeekExam(this.examId)
      .subscribe(
        res => {
          if (!res.status) this.__auth.notificationInfo('OOPS! Something went wrong.');
          else {
            let examDetails = res.data;
            console.log(examDetails,"exam data here say");

            let count = 1;
            this.examId = examDetails.examId;
            this.userExamId = examDetails.userExamId;
            this.questionPalette = (examDetails.questionPalette).filter(item => {
              item.number = count;
              count++;
              return item;
            });
            
            this.qPallete = this.questionPalette;
            this.timeFlag = examDetails.examDetails.timeFlag;
            if (this.timeFlag) {
              this.timerObj = examDetails.examDetails.timeObject;
              this.testObject = JSON.parse(localStorage.getItem('testObject'));
              if (this.testObject) {
                this.testObject.map((item, i) => {
                  if (item.userExamId == this.userExamId) {
                    this.timerIndex = i;
                    this.previousTimer = this.testObject[this.timerIndex];
                  };
                })
              } else {
                this.testObject = [];
                this.previousTimer = null;
              }
              if (this.previousTimer == null || !this.previousTimer || this.zeroTime) {
                this.time = new Date(2017, 1, 1, this.timerObj.hour, this.timerObj.min, 0);
                this.timer = true;
              } else {
                this.time = new Date(2017, 1, 1, this.previousTimer.hours, this.previousTimer.minutes, this.previousTimer.seconds);
                this.timer = true;
              }
              this._timerTick();
            }
            this.getQuestion(this.currentPaletteId);

          }
        }
      )
  }
  _timerTick() {
    this.time.setSeconds(this.time.getSeconds(), -1);
    let timerObject = {
      hours: this.time.getHours(),
      minutes: this.time.getMinutes(),
      seconds: this.time.getSeconds(),
      userExamId: this.userExamId
    }
    if (this.timerIndex) this.testObject[this.timerIndex] = timerObject;
    else {
      this.testObject.push(timerObject);
      this.testObject.map((item, i) => {
        if (item.userExamId == this.userExamId) this.timerIndex = i;
      })
    }
    localStorage.setItem('testObject', JSON.stringify(this.testObject));
    if (timerObject.hours === 0 && timerObject.minutes === 5 && timerObject.seconds === 0) {
      this.__auth.notificationInfo('5 Minutes Remaining');
    }
    if (timerObject.hours === 0 && timerObject.minutes === 0 && timerObject.seconds === 0) {
      window.clearInterval(this.x);
      localStorage.removeItem('timerObject');
      this.statusCount();

      this.finishWeekExam();
    }
    this.x = setTimeout(() => this._timerTick(), 1000);
  }

  playPause() {
    if (this.buttonToggle) {
      this.buttonName = false;
      this.pauseTitle = "Pause Timer"
      if (this.pauseTime) {
        this.playTime = this.pauseTime;
      }
      else {
        this.playTime = this.time;
      }
      this.paused = false;
      this.resumed = true;
      let examurl = "/exam/write/" + this.userExamId;
      this.playTime.setSeconds(this.playTime.getSeconds(), -1);
      if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 5 && this.playTime.getSeconds() === 0) {
        this.__auth.notificationInfo('5 Minutes Remaining')
      }
      if (this.playTime.getHours() === 0 && this.playTime.getMinutes() === 0 && this.playTime.getSeconds() === 0) {
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
        this.finishWeekExam();
        this.statusCount();
      }
      var x = setTimeout(() => this._timerTick(), 1000);
    }
    else {
      this.buttonName = true;
      this.playTitle = "Resume Timer"
      this.pauseTime = this.time;
      this.resumed = false;
      this.paused = true;
      window.clearInterval(this.x);
      localStorage.removeItem('timerObject');
    }
    this.buttonToggle = !this.buttonToggle
  }


  getQuestion(index) {

    this.currentPaletteId = index;
    console.log(this.currentPaletteId,"id here1");
    console.log(this.questionPalette[this.currentPaletteId],"palet22");
    
    
    this.questionId = this.questionPalette[this.currentPaletteId]._id;
    console.log(this.questionId,"question id3");
    
    this.weeklyService.getExamQuestion(this.questionId, this.userExamId)
      .subscribe(
        res => {
          if (!res.status) this.__auth.notificationInfo("OOPS! Something went wrong.");
          else {
            this.questionData = (res.data.question[0]) ? res.data.question[0] : {};
            this.answerData = (res.data.answer[0]) ? res.data.answer[0] : {};
            this.question = this.questionData.question;
            this.options = JSON.parse(this.questionData.options);
            this.slNumber = this.questionPalette[this.currentPaletteId].number;
            this.subjectArray = this.questionData.subjectArray;
            if (this.questionData.question_image) {
              this.imageFlag = true;
              this.imageUrl = this.url + this.questionData.question_image;
            } else {
              this.imageFlag = false;
            }
            if (this.answerData.answer) {
              this.answered = true;
              this.clickedItem = this.answerData.answer;
            } else {
              this.answered = false;
              this.clickedItem = '';
            }
          }

        }
      )

  }

  nextQuestion() {
    if (this.questionPalette.length === (this.currentPaletteId + 1)) {
      this.finishExam("");
      this.statusCount();

    } else {
      this.currentPaletteId++;
      this.getQuestion(this.currentPaletteId);
    }
  }

  previousQuestion() {
    this.currentPaletteId--;
    this.getQuestion(this.currentPaletteId);
  }

  goToQuestion(index) {
    if (!this.paused) {
      this.currentPaletteId = index;
      this.getQuestion(index)
    } else {
      console.log("user paused");

    }
  }

  saveAnswer(answer, index) {
    if (!this.paused) {
      let result = null;
      let answerStatus;
      if (this.clickedItem && this.clickedItem == answer) {
        this.clickedItem = 'O';
        answerStatus = "unanswered"
        result = "wrong"
        this.answered = false;
      } else {
        this.answered = true;
        this.clickedItem = answer;
        let correctAnswer = this.questionData.answer;
        if (answer == correctAnswer) {
          result = 'correct';
        } else {
          result = 'wrong';
        }
        answerStatus = "answered"

      }

      this.weeklyService.saveExamAnswer(this.questionId, this.examId, this.userExamId, answerStatus, this.clickedItem, result, this.subjectArray)
        .subscribe(
          res => {
            if (!res.status) this.__auth.notificationInfo("OOPS! Something went wrong.");
            else {
              if (answerStatus == 'unanswered') {
                this.questionPalette[this.currentPaletteId].status = "";
                this.qPallete[this.currentPaletteId].status = "";
              } else {
                this.questionPalette[this.currentPaletteId].status = "answered";
                this.qPallete[this.currentPaletteId].status = "answered";
              }
            }
          }
        )
    } else {
      console.log("user paused");

    }
  }

  markQuestion(type) {
    console.log(type, "type da")
    this.weeklyService.markExamQuestion(this.questionId, this.userExamId, type)
      .subscribe(
        res => {
          if (!res.status) this.__auth.notificationInfo("OOPS! Something went wrong.");
          else {
            // let index = (this.qPallete).findIndex(item => item.question == this.questionId)
            if (type == 'marked') {
              // this.questionPalette[this.currentPaletteId].status = 'answered'
              this.qPallete[this.currentPaletteId].marked = "marked";
            } else if (type == 'unmarked') {
              this.questionPalette[this.currentPaletteId].marked = '';
            }
          }
        }
      )
  }

  filter(type) {
    let mapArray = (type) => {
      let tempArray = []
      tempArray = (this.qPallete).filter(item => {
        if (item.status == type) return item;
      })
      if (tempArray.length) this.questionPalette = tempArray, this.getQuestion(0), this.currentPaletteId = 0;
      else this.__auth.notificationInfo("No questions!");
    }
    let markArray = (type) => {
      let tempArray = []
      tempArray = (this.qPallete).filter(item => {
        if (item.status == type) return item;
      })
      if (tempArray.length) this.questionPalette = tempArray, this.getQuestion(0), this.currentPaletteId = 0;
      else this.__auth.notificationInfo("No questions!");
    }
    if (type == 'm') {
      markArray('marked')
    }
    else if (type == 'a') {
      mapArray('answered')
    }
    else if (type == 'na') {
      mapArray('')
    }
    else {
      this.questionPalette = this.qPallete;
      this.currentPaletteId = 0;
      this.getQuestion(0)
    }

  }
  finishExam(userId) {
    window.clearInterval(this.x);
    localStorage.removeItem('timerObject');
    let alert = this.alertCtrl.create({
      title: 'Confirm submit',
      message: 'Do you want to submit exam?',
      buttons: [
        {
          text: 'Yes',
          role: 'cancel',
          handler: () => {
            this.weeklyService.submitExam(this.userExamId).subscribe(res => {
              if (res.status) {
                this.navCtrl.push(Details, { ExamIds: this.userExamId + '/week' });
              } else {
                this.__auth.notificationInfo('OOPS! Something went wrong.')

              }
            });
          }
        },
        {
          text: 'No',
          handler: () => {
          }


        }
      ]
    });
    alert.present();

  }
  finishWeekExam() {
    this.weeklyService.submitExam(this.userExamId).subscribe(res => {
      if (res.status) {
        window.clearInterval(this.x);
        localStorage.removeItem('timerObject');
        this.navCtrl.push(Details, { ExamIds: this.userExamId + '/week' });

      } else {
        this.__auth.notificationInfo('OOPS! Something went wrong.');
      }
    });


  }

  // finishExam() {
  //   window.clearInterval(this.x);
  //   localStorage.removeItem('timerObject');
  //   this.weeklyService.submitExam(this.userExamId)
  //     .subscribe(
  //       res => {
  //         if (!res.status) this.notify("OOPS! Something went wrong.", "error");
  //         else {
  //           this.notify('Successfully submitted the mock exam', 'success')
  //           this.navCtrl.push(examresultModal, { ExamIds:  this.userExamId + '/week' });


  //         }
  //       }
  //     )
  // }

  statusCount() {
    let marked = 0, unanswered = 0, answered = 0;
    this.qPallete.filter((item) => {
      if (item.status == 'answered') answered++;
      else if (item.status == 'marked') marked++;
      else unanswered++;
    })
    this.paletteStatus = [answered, marked, unanswered];
  }




}
