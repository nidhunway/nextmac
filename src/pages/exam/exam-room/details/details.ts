import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { ExamService } from '../../../../services/exam.service';
import { AuthService } from '../../../../services/auth.service'
import { QuestionService } from '../../../../services/question.service';
import { ToastController } from 'ionic-angular';
import { environment } from '../../../../environments/environment'
import { examComponent } from '../../../../pages/exam/examNew/examComponent';
// import { Content } from 'ionic-angular';
import { Exam } from '../../../exam/exam';
import { homeNew } from '../../../homeNew/homeNew';
import { Events } from 'ionic-angular';


@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
  providers: [ExamService, AuthService, QuestionService]

})
export class Details {

  examId;
  examType;
  resultType
  wrong: number;
  correct: number;
  correctClr: boolean = false;
  wrongClr: boolean = false;
  unutendedClr: boolean = false;
  totalNumber;
  notAnswered: number;
  correctQstions;
  noQuestion: boolean = false;
  qstnName;
  currentIndexToShow: number = 0;
  reference: any;
  content;
  options;
  Qnumber: number;
  rank: Array<any> = [];
  noPrevQuestion: boolean = false;
  correctAnswer;
  description;
  url = environment.backendUrl + '/images/';
  imageFlag: boolean = false;
  imgUrl;
  weekExam: boolean = false;
  userAnswer;
  questionId;
  answerArray;
  qtnData: boolean = false;
  urlRead;
  fields;
  userExamId;
  examDetails: any = {
    name: "",
    date: "",
    noOfQuestions: "",
    examResult: {
      noOfCorrectQue: "",
      noOfQueAttended: "",
      noOfWrongQue: ""
    }
  }
  countClick:number=0;
  correctCountClick:number=0;
  wrongCountClick:number=0;
  unansweredCountClick:number=0;
  correctcount: boolean = false;
  wrongcount: boolean = false;
  unansweredcount: boolean = false;
  indexArray = [];
  userScore;
  totalQstn;
  score: number;
  scoreDiv: boolean = false;
  totalScore: number;
  rankData;
  totalUsers;
  listClr;
  userRank;
  topScorers;
  userId;
  showLeaderBoard: boolean = false;
  index: number;
  listview:boolean=false;
  listviewData;
  shownGroup;
  subjectArray;
  star;
  tabBarElement: any;
  constructor (private questionService: QuestionService,public events: Events, public sanitizer: DomSanitizer, public navCtrl: NavController, private __auth: AuthService, public toastCtrl: ToastController,public platform: Platform, public navParams: NavParams, public examService: ExamService) {
    this.urlRead = navParams.get('ExamIds');
    console.log("urlread", this.urlRead);
    this.events.publish('user:created', true, Date.now());

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    
  }
  
 
  ionViewWillEnter(): void {
    this.tabBarElement.style.display = 'none';
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }
  private backButtonFunc(): void {
    this.navCtrl.setRoot(examComponent);
  }
  ngOnInit() {
    this.tabBarElement.style.display = 'none';

    this.fields = this.urlRead.split('/');
    this.examId = this.fields[0];
    this.userExamId = this.fields[1]
    if (this.fields[1] == 'g') {
      this.examType = "grand";
    } else if (this.fields[1] == 'week') {
      this.examType = "week";
    } else {
      this.examType = "exam";
    }
    this.getQuiestions("correct");
    this.correctClr = true;
    this.wrongClr = false;
    this.unutendedClr = false;
    this.listClr = false;
    this.correctcount = true;
    this.showLeaderBoard=false;
    this.wrongcount = false;
    this.unansweredcount = false;
    if (this.examType == "grand") {
      this.examService.loadGrandExam(this.examId).subscribe(result => {
        if (result.status) {
          this.totalQstn = result.data.userExamObj.numberOfQuestions;
          this.correct = result.data.userExamObj.examResult.noOfCorrectQue;
          if(this.correct == 0)
          this.listview = false;
         
          this.wrong = result.data.userExamObj.examResult.noOfWrongQue;
          this.notAnswered = this.totalQstn - result.data.userExamObj.examResult.noOfQueAttended;
          this.score = ((result.data.userExamObj.examResult.noOfCorrectQue * 4) + (result.data.userExamObj.examResult.noOfWrongQue * -1))
          this.totalScore = this.totalQstn * 4;
          this.examDetails.name = result.data.userExamObj.name;
          this.examDetails.date = result.data.userExamObj.createdDate
        }
      })
    } else if (this.examType == 'week') {
      this.userResult();
    } else {
      this.examService.loadExam(this.userExamId).subscribe(data => {
        if (data.data) {
          console.log("data.data",data.data);
          if(data.data.userExamObj){
          this.totalQstn = data.data.userExamObj.noOfQuestions;
          }
          this.correct = data.data.userExamObj.examResult.noOfCorrectQue;
          if(this.correct == 0)
          this.listview = false;
          this.wrong = data.data.userExamObj.examResult.noOfWrongQue;
          this.notAnswered = (this.totalQstn - data.data.userExamObj.examResult.noOfQueAttended);
          this.score = ((data.data.userExamObj.examResult.noOfCorrectQue * 4) + (data.data.userExamObj.examResult.noOfWrongQue * -1))
          this.totalScore = this.totalQstn * 4;
          this.examDetails.name = data.data.userExamObj.examName
          this.examDetails.date = data.data.userExamObj.createdDate
        }
      })
    }
  }

  userResult() {
    this.examService.getUserResult(this.examId).subscribe(
      res => {
        if (res.status) {
          this.userScore = res.data[0];
          this.getRank(this.userScore.examId);
          this.totalQstn = res.data[0].examResult.totalQuestions;
          this.correct = res.data[0].examResult.noOfCorrect;
          this.wrong = res.data[0].examResult.noOfWrong;
          this.notAnswered = res.data[0].examResult.noOfUnanswered;
          this.score = res.data[0].examResult.mark
          this.totalScore = this.totalQstn * 4;
        }
        else {
          this.__auth.notificationInfo(res.message)
        }
      });
  }

  getRank(examid) {
    this.examService.getLeaderboard(examid).subscribe(
      res => {
        if (res.status) {
          this.rankData = res.data;
          this.totalUsers = res.data.length;
          for (var k = 0; k < this.rankData.length; k++) {
            this.indexArray.push({"score":this.rankData[k].examResult.mark, "name":this.rankData[k].userDetails.name });
          }
          this.indexArray.sort(function (a, b) {
            return b.score - a.score;
          });
          var rank = 1;
          for (var i = 0; i < this.indexArray.length; i++) {
            if (i > 0 && this.indexArray[i].score < this.indexArray[i - 1].score) {
              rank++;
            }
            this.indexArray[i].rank = rank;
          }
          this.userRank = this.rankData.findIndex(item => {
            return item.userId == this.userId;
          });
          this.topScorers = this.rankData.slice(0, 9);
        }
        else {
          this.__auth.notificationInfo(res.message)
        }
      });
  }
  getScore(){
this.scoreDiv=true;
this.correctClr=false;
this.wrongClr=false;
this.unutendedClr=false;
this.qtnData = false;
this.showLeaderBoard=false;
this.listview=false;
  }
  showLeaderborad() {
    this.showLeaderBoard = true;
    this.scoreDiv = false;
    this.correctClr = false;
    this.wrongClr = false;
    this.unutendedClr = false;
    this.qtnData = false;

  }
  
toggleDetails(item) {
  if (this.isGroupShown(item)) {
    this.shownGroup = null;
  } else {
    this.shownGroup = item;
  }
}
isGroupShown(item) {
  return this.shownGroup === item;
}
  getQuiestions(category){
    this.resultType=category;
    this.currentIndexToShow=0;
    this.correctQstions = [];
    this.answerArray = [];
    if (this.examType == "week") {
      this.examService.getResult(this.examId, this.resultType).subscribe(
        res => {
          if (res.status) {
            this.qtnData = true;
            this.correctClr = true;
            this.wrongClr = false;
            this.unutendedClr = false;
            this.showLeaderBoard=false;
            this.scoreDiv = false;
            if (this.resultType == "unanswered") {
              this.correctQstions = res.data;
              console.log(" this.correctQstions ", this.correctQstions );
              this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
              this.userAnswer = null;

            } else {
              this.correctQstions = res.data.question;
              console.log(" this.correctQstions33 ", this.correctQstions[42] );
              this.answerArray = res.data.answer;
              let userAnswerIndex = this.answerArray.findIndex((ans) => {
                return ans.questionId == this.correctQstions[this.currentIndexToShow]._id;
              })
              this.userAnswer = this.answerArray[userAnswerIndex].answer;
              this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
              if (this.userAnswer == this.correctAnswer) {
                this.userAnswer = null;
              }
            }
            this.content = this.correctQstions[this.currentIndexToShow].question;
            (this.content !== null) ? this.Qnumber = 1 : this.Qnumber = 0;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
  
            if (this.correctQstions[this.currentIndexToShow].question_image) {
              this.imageFlag = true;
              this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
              this.imageFlag = false;
            }
            if (this.correctQstions.length == 1) {
              this.noQuestion = true;
              this.noPrevQuestion = true;
            }
          } else {
            this.correctClr = true;
            this.wrongClr = false;
            this.unutendedClr = false;
            this.showLeaderBoard=false;
            this.qtnData = false;
            this.__auth.notificationInfo(res.message);


          }
        });

    } else {
      this.examService.getQuestions(this.examId, this.examType, category).subscribe(
        res => {
          if (res.status) {
            this.correctClr = true;
            this.wrongClr = false;
            this.unutendedClr = false;
            this.scoreDiv = false;
            this.showLeaderBoard=false;
            this.noPrevQuestion = true;
            this.showLeaderBoard=false;
            this.qtnData = true;
            this.correctQstions = res.data;
            console.log(" this.correctQstions2 ", this.correctQstions );
            this.content = this.correctQstions[this.currentIndexToShow].question;
            this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
            this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
            (this.content !== null) ? this.Qnumber = 1 : this.Qnumber = 0;
            if (this.correctQstions[this.currentIndexToShow].question_image) {
              this.imageFlag = true;
              this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
              this.imageFlag = false;
            }
            if (this.correctQstions.length == 1) {
              this.noQuestion = true;
              this.noPrevQuestion = true;
            }
          } else {
          }
        })
    }
  }

  nextButtonClick() {
    this.noPrevQuestion = false;
    this.currentIndexToShow++;
    if (this.currentIndexToShow == this.correctQstions.length - 1) {
      this.noQuestion = true;
      this.content = this.correctQstions[this.currentIndexToShow].question;
      (this.content !== null) ? this.Qnumber++ : this.Qnumber = 0;
      this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
      this.description = this.correctQstions[this.currentIndexToShow].description;
      if (this.examType == "week") {
        if (this.resultType == "unanswered") {
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          this.userAnswer = null;
        } else {
          let userAnswerIndex = this.answerArray.findIndex((ans) => {
            return ans.questionId == this.correctQstions[this.currentIndexToShow]._id;
          })
          this.userAnswer = this.answerArray[userAnswerIndex].answer;
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          if (this.userAnswer == this.correctAnswer) {
            this.userAnswer = null;
          }
        }
      } else {
        this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
        this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
      }
      if (this.correctQstions[this.currentIndexToShow].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
      }
      else {
        this.imageFlag = false;
      }
    } else {

      this.content = this.correctQstions[this.currentIndexToShow].question;
      (this.content !== null) ? this.Qnumber++ : this.Qnumber = 0;
      this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
      this.description = this.correctQstions[this.currentIndexToShow].description;
      if (this.examType == "week") {
        if (this.resultType == "unanswered") {
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          this.userAnswer = null;
        } else {
          let userAnswerIndex = this.answerArray.findIndex((ans) => {
            return ans.questionId == this.correctQstions[this.currentIndexToShow]._id;
          })
          this.userAnswer = this.answerArray[userAnswerIndex].answer;
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          if (this.userAnswer == this.correctAnswer) {
            this.userAnswer = null;
          }
        }
      } else {
        this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
        this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
      }
      if (this.correctQstions[this.currentIndexToShow].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
      }
      else {
        this.imageFlag = false;
      }
    }
  }

  previousButtonClick() {
    this.noQuestion = false;
    this.currentIndexToShow--;
    if (this.currentIndexToShow > 0) {
      this.content = this.correctQstions[this.currentIndexToShow].question;
      (this.content !== null) ? this.Qnumber-- : this.Qnumber = 0;
      this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
      this.description = this.correctQstions[this.currentIndexToShow].description;
      if (this.examType == "week") {
        if (this.resultType == "unanswered") {
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          this.userAnswer = null;
        } else {
          let userAnswerIndex = this.answerArray.findIndex((ans) => {
            return ans.questionId == this.correctQstions[this.currentIndexToShow]._id;
          })
          this.userAnswer = this.answerArray[userAnswerIndex].answer;
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          if (this.userAnswer == this.correctAnswer) {
            this.userAnswer = null;
          }
        }
      } else {
        this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
        this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
      }
      if (this.correctQstions[this.currentIndexToShow].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
      }
      else {
        this.imageFlag = false;
      }
    } else {
      this.noPrevQuestion = true;
      this.content = this.correctQstions[this.currentIndexToShow].question;
      (this.content !== null) ? this.Qnumber-- : this.Qnumber = 0;
      this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
      this.description = this.correctQstions[this.currentIndexToShow].description;
      if (this.examType == "week") {
        if (this.resultType == "unanswered") {
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          this.userAnswer = null;
        } else {
          let userAnswerIndex = this.answerArray.findIndex((ans) => {
            return ans.questionId == this.correctQstions[this.currentIndexToShow]._id;
          })
          this.userAnswer = this.answerArray[userAnswerIndex].answer;
          this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
          if (this.userAnswer == this.correctAnswer) {
            this.userAnswer = null;
          }
        }
      } else {
        this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
        this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
      }
      if (this.correctQstions[this.currentIndexToShow].question_image) {
        this.imageFlag = true;
        this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
      }
      else {
        this.imageFlag = false;
      }
    }
  }

  getCorrectQstn(type) {
   this.listview=false;
    this.noQuestion = false;
    this.currentIndexToShow = 0;
    this.noPrevQuestion = true;
    this.resultType = type;
    if (this.examType !== "week") {
      if (this.resultType == "unanswered") {
        this.resultType = "notAnswered";
      }
      this.examService.getQuestions(this.examId, this.examType, this.resultType).subscribe(
        res => {
          if (res.status) {
            this.qtnData = true;
            this.correctQstions = res.data;
            console.log(" this.correctQstions ", this.correctQstions );
            
            this.content = this.correctQstions[this.currentIndexToShow].question;
            
            (this.content !== null) ? this.Qnumber = 1 : this.Qnumber = 0;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
            this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
            this.userAnswer = this.correctQstions[this.currentIndexToShow].userAnswer;
            if (this.correctQstions[this.currentIndexToShow].question_image) {
              this.imageFlag = true;
              this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
              this.imageFlag = false;
            }
            if (this.correctQstions.length == 1) {
              this.noQuestion = true;
              this.noPrevQuestion = true;
            }
            if (this.resultType == "correct") {
              this.correct = this.correctQstions.length;
              this.correctcount = true;
              this.listClr = false;
              this.wrongcount = false;
              this.unansweredcount = false;
              this.showLeaderBoard=false;
              this.correctClr = true;
              this.wrongClr = false;
              this.showLeaderBoard=false;
              this.unutendedClr = false;
              this.scoreDiv = false;

            } else if (this.resultType == "wrong") {
              this.wrong = this.correctQstions.length;
              this.correctcount = false;
              this.listClr = false;
              this.wrongcount = true;
              this.showLeaderBoard=false;
              this.unansweredcount = false;
              this.showLeaderBoard=false;
              this.wrongClr = true;
              this.correctClr = false;
              this.unutendedClr = false;
              this.scoreDiv = false;
            } else {
              this.notAnswered = this.correctQstions.length;
              this.correctcount = false;
              this.listClr = false;
              this.wrongcount = false;
              this.unansweredcount = true;
              this.showLeaderBoard=false;
              this.unutendedClr = true;
              this.wrongClr = false;
              this.correctClr = false;
              this.scoreDiv = false;
            }
          }
          else {
            this.qtnData = false;
            if (this.resultType == "correct") {
              this.correct = 0;
              this.correctcount = true;
              this.listClr = false;
              this.wrongcount = false;
              this.showLeaderBoard=false;
              this.unansweredcount = false;
              this.unutendedClr = false;
              this.wrongClr = false;
              this.correctClr = true;
            } else if (this.resultType == "wrong") {
              this.wrong = 0;
              this.correctcount = false;
              this.listClr = false;
              this.wrongcount = true;
              this.showLeaderBoard=false;
              this.unansweredcount = false;
              this.showLeaderBoard=false;
              this.unutendedClr = false;
              this.wrongClr = true;
              this.correctClr = false;
            } else {
              this.notAnswered = 0;
              this.correctcount = false;
              this.listClr = false;
              this.wrongcount = false;
              this.unansweredcount = true;
              this.showLeaderBoard=false;
              this.unutendedClr = true;
              this.showLeaderBoard=false;
              this.wrongClr = false;
              this.correctClr = false;
            }
            this.listview = false;
            this.__auth.notificationInfo("There is no answer");
          }
        })
    } else {
      this.examService.getResult(this.examId, this.resultType).subscribe(
        res => {
          if (!res.status) {
            this.qtnData = false;
            if (this.resultType == "correct") {
              this.correct = 0;
              this.correctcount = true;
              this.showLeaderBoard=false;
              this.listClr = false;
              this.wrongcount = false;
              this.unansweredcount = false;
              this.unutendedClr = false;
              this.showLeaderBoard=false;
              this.wrongClr = false;
              this.correctClr = true;
            } else if (this.resultType == "wrong") {
              this.wrong = 0;
              this.correctcount = false;
              this.showLeaderBoard=false;
              this.listClr = false;
              this.wrongcount = true;
              this.showLeaderBoard=false;
              this.unansweredcount = false;
              this.unutendedClr = false;
              this.wrongClr = true;
              this.correctClr = false;
            } else {
              this.notAnswered = 0;
              this.correctcount = false;
              this.listClr = false;
              this.showLeaderBoard=false;
              this.wrongcount = false;
              this.showLeaderBoard=false;
              this.unansweredcount = true;
              this.unutendedClr = true;
              this.wrongClr = false;
              this.correctClr = false;
            }
            this.listview = false;
            this.__auth.notificationInfo(res.message)
          }
          else {
            this.noPrevQuestion = true;
            this.qtnData = true;
            if (this.resultType == "unanswered") {
              this.correctQstions = res.data;
              this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
              this.subjectArray = this.correctQstions[this.currentIndexToShow].subjectArray;
              this.userAnswer = null;
            } else {
              this.correctQstions = res.data.question;
              this.answerArray = res.data.answer;
              let userAnswerIndex = this.answerArray.findIndex((ans) => {
                return ans.questionId == this.correctQstions[this.currentIndexToShow]._id;
              })
              this.userAnswer = this.answerArray[userAnswerIndex].answer;
              this.correctAnswer = this.correctQstions[this.currentIndexToShow].answer;
              if (this.userAnswer == this.correctAnswer) {
                this.userAnswer = null;
              }
            }
            this.content = this.correctQstions[this.currentIndexToShow].question;
            (this.content !== null) ? this.Qnumber = 1 : this.Qnumber = 0;
            this.options = JSON.parse(this.correctQstions[this.currentIndexToShow].options);
            this.description = this.correctQstions[this.currentIndexToShow].description;
            if (this.correctQstions[this.currentIndexToShow].question_image) {
              this.imageFlag = true;
              this.imgUrl = this.correctQstions[this.currentIndexToShow].question_image;
            }
            else {
              this.imageFlag = false;
            }
            if (this.correctQstions.length == 1) {
              this.noQuestion = true;
              this.noPrevQuestion = true;
            }
            if (this.resultType == "correct") {
             // this.correct = this.correctQstions.length;
              this.correctClr = true;
              this.wrongClr = false;
              this.listClr = false;
              this.unutendedClr = false;
              this.showLeaderBoard=false;
              this.scoreDiv = false;
              this.showLeaderBoard=false;
            } else if (this.resultType == "wrong") {
             // this.wrong = this.correctQstions.length;
              this.wrongClr = true;
              this.correctClr = false;
              this.listClr = false;
              this.unutendedClr = false;
              this.showLeaderBoard=false;
              this.scoreDiv = false;
            } else {
              //this.notAnswered = this.correctQstions.length;
              this.unutendedClr = true;
              this.wrongClr = false;
              this.listClr = false;
              this.correctClr = false;
              this.showLeaderBoard=false;
              this.scoreDiv = false;
            }
          }
        })
    }
  }
  starQuestions(){
    this.star = !this.star;
    this.questionService.starQuestion(this.questionId, this.subjectArray, this.star)
      .subscribe(
        res => {
          if (!res.status) this.__auth.notificationInfo('OOPS! Something went wrong')
        }
      )
  }
  listView(){
    
    if(this.correctcount){
      this.listClr = false;
      this.correctCountClick++
      this.countClick = this.correctCountClick
    }
    if(this.wrongcount){
      this.listClr = false;
      this.wrongCountClick++
      this.countClick = this.wrongCountClick
    }
    if(this.unansweredcount){
      this.listClr = false;
      this.unansweredCountClick++
      this.countClick = this.unansweredCountClick
    }
    if (this.countClick % 2 == 0) {
      this.listview=false;
      this.listClr = false;
      this.qtnData = true;
    }else{
    this.listview=true;
    this.listClr = true;
    this.qtnData = false;
    this.listviewData=[];
    if (this.examType == 'week') {
      this.examService.getResult(this.examId, this.resultType).subscribe(
        res => {
          if (!res.status) {
          }else{
            if (this.resultType == "unanswered") {
              this.listviewData = res.data;
            } else {
              this.listviewData = res.data.question;
            }
            let data = (this.listviewData).map(item => {
              item.options = JSON.parse(item.options);
              item.icon = "ios-add-circle-outline";
              return item;
            });
            if (data) {
              this.listviewData = data;
              console.log("listdata", this.listviewData);
              
            }
          }
        });
    }else{
      this.examService.getQuestions(this.examId, this.examType,this.resultType).subscribe(
        res => {
          if (res.status) {
            this.listviewData=res.data;
            let data = (res.data).map(item => {
              item.options = JSON.parse(item.options);
              item.icon = "ios-add-circle-outline";
              return item;
            });
            if (data) {
              this.listviewData = data;
            }
          }else{
            this.listview=false;
            this.qtnData = false;
            this.__auth.notificationInfo(res.message)
          }
  });
  }
}
}
  backToCompletedPage() {
    this.navCtrl.push(examComponent);
  }
}