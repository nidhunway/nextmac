import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { ExamService } from '../../../../services/exam.service';
import { AuthService } from '../../../../services/auth.service'
import { QuestionService } from '../../../../services/question.service';
import { ToastController } from 'ionic-angular';
import { environment } from '../../../../environments/environment'
import { examComponent } from '../../../../pages/exam/examNew/examComponent';
// import { Content } from 'ionic-angular';
import { Exam } from '../../../exam/exam';
import { homeNew } from '../../../homeNew/homeNew';
import { Events } from 'ionic-angular';

@Component({
    selector: 'page-aiimsResult',
    templateUrl: 'aiimsResult.html',
    providers: [ExamService, AuthService, QuestionService]

})
export class aiimsResult {
    urlRead;
    tabBarElement: any;
    fields;
    examId;
    userExamId;
    examType;
    userResult;
    totalQuestion: number;
    correct;
    wrong;
    unanswered;
    examName;
    totalUsers: number;
    score;
    QstnDiv: boolean = false;
    qstnName;
    currentIndexToShow: number = 0;
    Qnumber: number;
    questionArray = [];
    options;
    imgUrl;
    url = environment.backendUrl + '/images/';
    scoreDiv: boolean = false;
    imageFlag: boolean = false;
    noQuestion: boolean = false;
    noPrevQuestion: boolean = false;
    description;
    answerData;
    userAnswer;
    correctAnswer;
    totalMark: number;
    showLeaderBoard: boolean = false;
    rankData;
    indexArray = [];
    userRank;
    userId;
    topScorers;
    correctClr: boolean = false;
    wrongClr: boolean = false;
    unutendedClr: boolean = false;
    resultType;
    countClick: number = 0;
    listviewData;
    listview: boolean = false;
    listClr: boolean = false;

    shownGroup;
    constructor(private questionService: QuestionService, public events: Events, public sanitizer: DomSanitizer, public navCtrl: NavController, private __auth: AuthService, public toastCtrl: ToastController, public platform: Platform, public navParams: NavParams, public examService: ExamService) {
        this.urlRead = navParams.get('ExamIds');
        console.log("urlread", this.urlRead);
        this.events.publish('user:created', true, Date.now());

        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

    }
    ionViewWillEnter(): void {
        this.tabBarElement.style.display = 'none';
        this.platform.registerBackButtonAction(() => this.backButtonFunc());
    }
    ionViewWillLeave() {
        this.tabBarElement.style.display = 'flex';
    }
    private backButtonFunc(): void {
        this.navCtrl.setRoot(examComponent);
    }
    ngOnInit() {
        this.tabBarElement.style.display = 'none';

        this.fields = this.urlRead.split('/');
        this.examId = this.fields[0];
        this.userExamId = this.fields[1];
        this.examType = this.fields[2];
        this.getResultScore();
        this.correctClr = true;
        this.wrongClr = false;
        this.unutendedClr = false;
        this.listview = false;
        this.getQuiestions("CORRECT");
    }
    getQuiestions(category) {
        this.listview = false;
        this.listClr = false;
        this.resultType = category;
        if (category == "CORRECT") {
            this.correctClr = true;
            this.wrongClr = false;
            this.unutendedClr = false;
        } else if (category == "WRONG") {
            this.correctClr = false;
            this.wrongClr = true;
            this.unutendedClr = false;
        } else {
            this.correctClr = false;
            this.wrongClr = false;
            this.unutendedClr = true;
        }
        this.scoreDiv = false;
        this.showLeaderBoard = false;
        this.currentIndexToShow = 0;
        this.questionArray = [];
        //this.answerArray = [];
        this.examService.getResultAiims(this.userExamId, this.examId, this.examType, category).subscribe(
            res => {
                console.log("res subcribed exams", res);
                if (res.data.length == 0) {
                    console.log("noo", res.data.length);
                    this.QstnDiv = false;
                    this.__auth.notificationInfo(res.message);
                } else {
                    console.log("here", res.data.length);
                    this.noPrevQuestion = true;
                    this.noQuestion = false;
                    this.QstnDiv = true;
                    this.questionArray = res.data.question;
                    this.answerData = res.data.answer;
                    if (category == "WRONG") {
                        // this.answerData = res.data.answer[this.currentIndexToShow];

                        this.userAnswer = this.answerData[this.currentIndexToShow].answer;
                    } else {
                        this.userAnswer = null;
                    }

                    this.qstnName = this.questionArray[this.currentIndexToShow].question;
                    (this.qstnName !== null) ? this.Qnumber = 1 : this.Qnumber = 0;
                    this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
                    this.description = this.questionArray[this.currentIndexToShow].description;
                    this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;
                    if (this.questionArray[this.currentIndexToShow].question_image) {
                        this.imageFlag = true;
                        this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;

                    } else {
                        this.imageFlag = false;
                    }

                    if (this.questionArray.length == 1) {
                        this.noQuestion = true;
                        this.noPrevQuestion = true;
                    }
                }
            });

    }
    previousButtonClick() {
        this.noQuestion = false;
        this.currentIndexToShow--;
        if (this.currentIndexToShow > 0) {
            this.noPrevQuestion = false;
            if (this.resultType == "WRONG") {
                this.userAnswer = this.answerData[this.currentIndexToShow].answer;

            } else {
                this.userAnswer = null;
            }

            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;
            } else {
                this.imageFlag = false;
            }
        } else {
            this.noPrevQuestion = true;
            if (this.answerData) {

                this.userAnswer = this.answerData.answer;
            } else {
                this.userAnswer = null;
            }
            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber-- : this.Qnumber = 0;
            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;

            } else {
                this.imageFlag = false;
            }

        }
    }
    nextButtonClick() {
        this.noPrevQuestion = false;
        this.currentIndexToShow++;
        if (this.currentIndexToShow == this.questionArray.length - 1) {
            this.noQuestion = true;
            if (this.resultType == "WRONG") {
                this.userAnswer = this.answerData[this.currentIndexToShow].answer;

            } else {
                this.userAnswer = null;
            }

            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;

            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;

            } else {
                this.imageFlag = false;
            }

        } else {
            this.noQuestion = false;
            if (this.answerData) {

                this.userAnswer = this.answerData.answer;
            } else {
                this.userAnswer = null;
            }
            this.qstnName = this.questionArray[this.currentIndexToShow].question;
            (this.qstnName !== null) ? this.Qnumber++ : this.Qnumber = 0;
            this.description = this.questionArray[this.currentIndexToShow].description;
            this.correctAnswer = this.questionArray[this.currentIndexToShow].answer;

            this.options = JSON.parse(this.questionArray[this.currentIndexToShow].options);
            if (this.questionArray[this.currentIndexToShow].question_image) {
                this.imageFlag = true;
                this.imgUrl = this.questionArray[this.currentIndexToShow].question_image;

            } else {
                this.imageFlag = false;
            }


        }
    }
    getScore() {
        this.listview = false;
        this.scoreDiv = true;
        this.showLeaderBoard = false;
        this.QstnDiv = false;
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;

    }
    toggleDetails(item) {
        if (this.isGroupShown(item)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = item;
        }
    }
    isGroupShown(item) {
        return this.shownGroup === item;
    }
    getResultScore() {

        this.examService.getResultScoreAiims(this.examId).subscribe(res => {
            if (res.data) {
                this.rankData = res.data.totalResult;
                this.totalUsers = res.data.totalResult.length;
                this.userResult = res.data.userResult[0];
                this.examName = this.userResult.examDetails.name;
                this.totalQuestion = this.userResult.examResult.total;
                this.correct = this.userResult.examResult.correct;
                this.wrong = this.userResult.examResult.wrong;
                this.unanswered = this.userResult.examResult.unanswered;
                let Nscore = this.userResult.examResult.score;
                this.score = String(Nscore).substr(0, 4);
                if (this.examType == "JIPMER")
                    this.totalMark = this.userResult.examResult.total * 4;
                else this.totalMark = this.userResult.examResult.total * 1;
                for (var k = 0; k < this.rankData.length; k++) {
                    this.indexArray.push({ "score": String(this.rankData[k].examResult.score).substr(0, 4), "name": this.rankData[k].userDetails.name });
                }
                this.indexArray.sort(function (a, b) {
                    return b.score - a.score;
                });
                var rank = 1;
                for (var i = 0; i < this.indexArray.length; i++) {
                    if (i > 0 && this.indexArray[i].score < this.indexArray[i - 1].score) {
                        rank++;
                    }
                    this.indexArray[i].rank = rank;
                }
                this.userRank = this.rankData.findIndex(item => {
                    return item.userId == this.userId;
                });
                this.topScorers = this.rankData.slice(0, 9);
            }
        });
    }
    showLeaderborad() {
        this.listClr = false;
        this.listview = false;
        this.showLeaderBoard = true;
        this.scoreDiv = false;
        this.QstnDiv = false;
        this.correctClr = false;
        this.wrongClr = false;
        this.unutendedClr = false;

    }
    backToCompletedPage() {
        this.navCtrl.push(examComponent);
    }
    listView() {

        this.countClick++;
        if (this.countClick % 2 == 0) {
            this.listview = false;
            this.listClr = false;

            this.QstnDiv = true;
        } else {
            this.listview = true;
            this.listClr = true;
            this.QstnDiv = false;
            this.listviewData = [];
            this.examService.getResultAiims(this.userExamId, this.examId, this.examType, this.resultType).subscribe(
                res => {
                    console.log("res subcribed exams", res);

                    if (!res.status) {
                    } else {
                        console.log("this.resultType", this.resultType);


                        this.listviewData = res.data.question;

                        let data = (this.listviewData).map(item => {
                            item.options = JSON.parse(item.options);

                            return item;
                        });
                        if (data) {
                            this.listviewData = data;
                            console.log("listdata", this.listviewData);

                        }
                    }

                });

        }
    }

}