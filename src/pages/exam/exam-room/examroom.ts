import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ModalController } from 'ionic-angular';
import { writeExam } from '../../../pages/exam/createexam/write-exam/write-exam';
import { grantExam } from '../../../.../../pages/exam/GrandExam/grantexam.component';

import { ExamService } from '../../../services/exam.service';
import { ToastController } from 'ionic-angular';
// import { examresultModal } from '../../../pages/exam/exam-room/resultModal/exam-result';
import { Details } from '../../../pages/exam/exam-room/details/details';





@Component({
  selector: 'page-examroom',
  templateUrl: 'examroom.html',
  providers:[ExamService]
})
export class ExamRoom {
 
  completed:boolean=false;
 
  mode;
  examList;
  pendingsDiv;
  currentListId;
  constructor(public navCtrl: NavController,private modalCtrl:ModalController,private examService:ExamService, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    this.completed=true;
   
    this.complete();
  //  document.getElementById("myCheck").click();
    
  }
  complete(){
    this.completed=true;
    this.pendingsDiv=false;

    this.mode = 'completed';
    this.examService.fetchAllExam(this.mode).subscribe(
      res => {
        this.examList = res.data.examList;
        this.examService.fetchGrandExam().subscribe(
          res => {
            let completedGexam=[];
            try{
            if(res.data.completedExam.length>0){
              completedGexam=(res.data.completedExam).map((item)=>{
              item.examType="grand";
              return item;
              })
            }
            } 
            catch (e){
                return this.examList;
            };
            this.examList=(completedGexam.length>0)?this.examList.concat(completedGexam):this.examList;
            if(!this.examList.length){
              const toast = this.toastCtrl.create({
                message: 'You dont have any complete exam',
                duration: 3000,
                position: 'bottom'
              });
              toast.present();
            }
          })
     
      }
    )
  }
  pendingExams() {
    this.completed=false;
    this.pendingsDiv=true;
   
    this.mode = 'pending';

    this.examService.fetchAllExam(this.mode).subscribe(
      res => {

        this.examList = res.data.examList;
       
        console.log(this.examList,"exam List data1")
       
        this.examService.fetchGrandExam().subscribe(
          grandRes => {
            console.log("Pendin Grand", grandRes)
            let pendingGexam = [];
            try {
              if (grandRes.data.pendingExams.length > 0) {
                pendingGexam = (grandRes.data.pendingExams).map((item) => {
                  item.examType = "grand";
                  return item;
                })
              }
            }
            catch (e) {
              return this.examList;
            };
            let examList = (pendingGexam.length > 0) ? this.examList.concat(pendingGexam) : this.examList;
            this.examList = examList.reverse();
            console.log(this.examList,"exam List data2")
          })
          if(!this.examList.length){
            const toast = this.toastCtrl.create({
              message: 'You dont have any pending exam',
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          }
       
      }
    )
  
  }
  DeletePop(i, item) {
    this.currentListId = i;

    const alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure want to delete this exam?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {

            // if (confirm('Are you sure you want to delete this exam?')){
            if (this.examList[this.currentListId].examType == "grand") {
              this.examService.deleteGrandExam(this.examList[this.currentListId]._id).subscribe(
                res => {
                  const toast = this.toastCtrl.create({
                    message: 'Exam deleted.',
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();



                  this.pendingExams();
                },
                error => {
                  const toast = this.toastCtrl.create({
                    message: 'OOPS! Something went wrong.',
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                });
            }
            else {
              let id = (this.examList[this.currentListId].examId) ? this.examList[this.currentListId].examId : this.examList[this.currentListId]._id;
              this.examService.deleteExam(id).subscribe(
                res => {
                  const toast = this.toastCtrl.create({
                    message: 'Exam deleted.',
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                  this.pendingExams();
                },
                error => {
                  const toast = this.toastCtrl.create({
                    message: 'OOPS! Something went wrong.',
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                });
            }
            // }
            // else {
            //   //Do Nothing
            // }
            console.log('yes');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('noo');
          }
        }
      ]
    });

    alert.present();
  }
  resumeExam(exam){
    
      if(exam.examType === "grand"){
  
        this.navCtrl.push(grantExam,{subject:exam._id});
      }
      else{
        if(exam.examId){
          this.navCtrl.push(Details,{subject:exam._id});
     
         
                  }
        else{
          this.examService.startExam(exam._id).subscribe(
          res => {
            let userExamId = res.data.userExamId;
            this.navCtrl.push(Details,{subject:userExamId});
            console.log(userExamId,'start')
           
        
            }
          );
  
        }
  
      }
    }
    viewResult(userExam){
      if(userExam){
      let profileModal = this.modalCtrl.create(Details, { data:userExam },{ cssClass: 'inset-modal' });
      profileModal.onDidDismiss(value => {
      })
      profileModal.present();  
    //   console.log("userexamm",userExam);
    //   let exam_id;
    //   if(userExam.examType == "grand"){
    //     exam_id = userExam._id + '/g'
    //     this.navCtrl.push(examresultModal,{subject:exam_id});
     
    //   }
    //   else{
    //     exam_id = userExam.examId + '/'+ userExam._id;
    //     this.navCtrl.push(examresultModal,{subject:exam_id});
      
    // }
    }
  }

}
