import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { naveBarShowSrvice } from '../../services/navBarService';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [naveBarShowSrvice]
})
export class Profile {
  userData;
  testArry = [];
  UserName: string;
  profileDiv: boolean = true;
  Editdiv: boolean = false;
  spinnerLoad: boolean = false;
  activeUID: string;
  firstName: string;
  Lastname: string;
  birthDay: string;
  phoneNo: string;
  city: string;
  zip: string;
  country: string
  profile = { fname: "", lname: "", bday: "", phone: "", city: "", zip: "", country: "" };


  constructor(public navCtrl: NavController, private _navBar: naveBarShowSrvice, public toastCtrl: ToastController) {

  }
  ngOnInit() {
    this.profileDiv = true;
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.testArry.push(this.userData);
    this.UserName = this.testArry[0].profile.firstName;

  }

  edit() {
    this.Editdiv = true;
    this.profileDiv = false;
  }

  profileSubmit(form: NgForm) {


    if (form.valid) {

      this.spinnerLoad = true;
      this.activeUID = this.userData._id;
      this.firstName = this.testArry[0].profile.firstName;
      this.Lastname = this.testArry[0].profile.lastName;
      this.birthDay = this.profile.bday;
      this.phoneNo = this.testArry[0].profile.phone;
      this.city = this.testArry[0].profile.city;
      this.zip = this.profile.zip;
      this.country = this.testArry[0].profile.country;
      console.log(this.activeUID, this.firstName, this.Lastname, this.phoneNo, this.city, this.country, "credetioal details")
      this._navBar.profileEdit(this.activeUID, this.firstName, this.Lastname, this.phoneNo, this.city, this.country).subscribe(
        res => {
          if (res.status) {

            localStorage.setItem('userData', JSON.stringify(this.testArry[0]));
            this.UserName = this.testArry[0].profile.firstName;
            this.profile.phone = this.userData.profile.phone;
            this.profile.city = this.userData.profile.city
            this.profile.country = this.userData.profile.country
            this.Editdiv = false;
            this.profileDiv = true;
            this.spinnerLoad = false;
            const toast = this.toastCtrl.create({
              message: 'You profile has been updated',
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          } else {
            const toast = this.toastCtrl.create({
              message: 'OOPS! Something went wrong',
              duration: 3000,
              position: 'bottom'
            });
            toast.present();

          }

          // this.tostr.notify("You profile has been updated", "success");

        },
        error => {
        this.spinnerLoad = false;
          const toast = this.toastCtrl.create({
            message: 'OOPS! Something went wrong',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        });
    }
  }
}
