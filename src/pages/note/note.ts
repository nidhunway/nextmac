import { Component } from '@angular/core';
import { NavController, AlertController, Platform,NavParams } from 'ionic-angular';
import { NotesService } from '../../services/notes.service';
import { ToastController } from 'ionic-angular';
import { Questions } from '../../pages/learn/questions/questions';
import { DailyNote } from '../../pages/note/dailyNotes/dailynotes';

import { homeNew } from '../../pages/homeNew/homeNew';
import { HomePage } from '../../pages/dashboard/home';


@Component({
  selector: 'page-note',
  templateUrl: 'note.html',
  providers: [NotesService]

})
export class Note {

  allNotesData;
  subjectId;
  pageLoadNote;
  chapterId;
  totalIds;
  sectionId;
  created: boolean = false;
  create;
  daily: boolean = false;
  bgColor;
  newData;
  images: Array<string>;
  grid;
  backgroundcolors;
  questionId;
  newDataA;
  miniNotes;
  registrationId;
  tabBarElement;
  shownGroup=null;
  getParam;
  recentNotes;
  recentDiv:boolean=false;
  miniNoteDiv:boolean=false;
  noteArray=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public platform: Platform, private alertCtrl: AlertController, private noteService: NotesService, public toastCtrl: ToastController) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

    this.getParam = navParams.get('parms');
    console.log(" this.getParam ", this.getParam );
    
  
    
  }
  ionViewWillEnter(): void {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }

  private backButtonFunc(): void {
    this.navCtrl.setRoot(HomePage);
  }
  ngOnInit() {
    
    this.grid = ["a", "v"]
   // console.log("this.getParam///",this.getParam);
    
    //if(this.getParam){
      this.create = "daily";
      //console.log("this.create",this.create);
      this.created = false;
      this.create = "daily";
      this.daily = true;
      this.loadMiniNotes();
      this.loadRecentNote();
      this.loadAddedNote();
    // }else{
    //   this.created = true;
    //   this.daily = false;
    //   this.create = "created";
    //   this.loadNotes();
    // }

   
    //this.loadMiniNotes();

  }
  loadAddedNote(){
  this.noteService.fetchAllMiniNotes().subscribe(
    res => {
      if(res){
      if (res.status) {
        this.miniNoteDiv=true;
        console.log("res mininotes",res);
        
        this.noteArray = res.data.notes.sort((a, b) => new Date(b.activeDate).getTime() - new Date(a.activeDate).getTime());
console.log("noteArray",this.noteArray);
      }
    }
  });
}
  saveRecentNote(id,mode){
    console.log("idkkkkkkkkkk",id,mode);
    this.noteService.saveRecentNote(id,mode).subscribe(
      res => {
        if(res){
          console.log("recent note save ",res);
          
        }
      });

  }
  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };
  loadRecentNote(){
    this.noteService.loadRecentNote().subscribe(
      res => {
        console.log("recent note////// ",res);
        if(res.data){
          this.recentDiv=true;
        
          this.recentNotes=res.data;
          
        }else{
         
          
          this.recentDiv=false;
          console.log("this.recentDiv",this.recentDiv);
        }
      });
  }
  loadMiniNotes(){
    this.noteService.fetchAllMiniNotes().subscribe(
      res => {
        if(res){
        if (res.status) {
          this.miniNoteDiv=true;
          console.log("res mininotes",res);
          
         // this.noteArray = res.data.notes.sort((a, b) => new Date(b.activeDate).getTime() - new Date(a.activeDate).getTime());
//console.log("noteArray",this.noteArray);

          if (res.data.notes.length > 0) {
            let subjectNotes=[];
            this.miniNotes=res.data.notes;
            this.registrationId=res.data.registrationId
            
           var groups = {};
            for(let i=0;i<this.miniNotes.length;i++){
              let groupName = this.miniNotes[i].subjectArray[0].name;
              if (!groups[groupName]) {
                groups[groupName] = [];
              }
              groups[groupName].push(this.miniNotes[i]);
 
            }
            this.miniNotes=[];
            for (let groupName in groups) {
              console.log("gropppppppppppinsidepppppp", groups);
              
              this.miniNotes.push({subjectName: groupName, noteTitle: groups[groupName]});
            }
            console.log("this.miniNotesthis.miniNotes...//",this.miniNotes);
            console.log("this.miniNotes",this.miniNotes[0].noteTitle);
            
       
           

                 }
        
      }
      else {
        this.miniNoteDiv=false;

        const toast = this.toastCtrl.create({
          message: 'Oh! You have no Notes!',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
    }
      else {
        const toast = this.toastCtrl.create({
          message: 'OOPS! Something went wrong',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
        });
  }
  loadNotes() {
    let loadMore = false;
    this.noteService.fetchAllNotes(loadMore).subscribe(
      res => {
        if (res) {
          if (res.data.notes.length > 0) {
            this.allNotesData = res.data.notes
console.log("  this.pageLoadNote",  this.allNotesData);

          } else {
            const toast = this.toastCtrl.create({
              message: 'Oh! You have no Notes!',
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          }
        } else {
          const toast = this.toastCtrl.create({
            message: 'OOPS! Something went wrong',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      });
  }
  ionViewLoaded() {

    let rowNum = 0; //counter to iterate over the rows in the grid

    for (let i = 0; i < this.images.length; i += 2) { //iterate images

      this.grid[rowNum] = Array(2); //declare two elements per row

      if (this.images[i]) { //check file URI exists
        this.grid[rowNum][0] = this.images[i] //insert image
      }

      if (this.images[i + 1]) { //repeat for the second image
        this.grid[rowNum][1] = this.images[i + 1]
      }

      rowNum++; //go on to the next row
    }

  }
  segmentChanged(event) {
    if (event._value == "created") {
      this.created = true;
      this.loadNotes();
      this.create = "created";
      this.daily = false;
      this.loadNotes();
    } else if (event._value == "daily") {
      this.loadMiniNotes();
      this.created = false;
      this.create = "daily";
      this.daily = true;
      console.log("event", event);
    }


  }
  deleteNote(index) {
    const alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure want to delete this note?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            let noteId = this.allNotesData[index]._id;
            this.noteService.deleteNote(noteId).subscribe(
              res => {
                const toast = this.toastCtrl.create({
                  message: 'Note deleted.',
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
              },
              error => {
                const toast = this.toastCtrl.create({
                  message: 'OOPS! Something went wrong',
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
              });
            this.allNotesData.splice(index, 1);
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('No');
          }
        }
      ]
    });
    alert.present();
  }
 
  goLastStudy(index) {
    let loadMore = false;
    let noteId = this.allNotesData[index]._id;
    var len = this.allNotesData.length
    for (var i = 0; i < len; i += 1) {
      if (this.allNotesData[i]["_id"] === noteId) {
        this.chapterId = this.allNotesData[i].subjectArray[2].id;
        this.sectionId = this.allNotesData[i].subjectArray[1].id
        this.subjectId = this.allNotesData[i].subjectArray[0].id;
        this.questionId = this.allNotesData[i].questionId;
      }
    }
    this.totalIds = this.subjectId + '/' + this.sectionId + '/' + this.chapterId + '/' + this.questionId;
    this.navCtrl.push(Questions, { subject: this.totalIds });

  }

  goDailyNotes(index,demoflag,mode){
    console.log("demoflag,,,,,,,,,",demoflag);
    this.tabBarElement.style.display = 'none';

    this.navCtrl.push(DailyNote,{noteId:index,RegId:this.registrationId,demoflag:demoflag,mode:mode});
  }

}
