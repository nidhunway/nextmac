import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, Platform, NavParams, Slides } from 'ionic-angular';
import { NotesService } from '../../../services/notes.service';
import { Note } from '../../../pages/note/note';
import { environment } from '../../../environments/environment'
import { AuthService } from '../../../services/auth.service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Events } from 'ionic-angular';


@Component({
  selector: 'page-dailynotes',
  templateUrl: 'dailynotes.html',
  providers: [NotesService, AuthService,SocialSharing]
})
export class DailyNote {
  @ViewChild('sliderOne') sliderOne: Slides;
  tabBarElement: any;
  noteContent;
  noteId;
  regId;
  miniNoteTitle;
  miniNotes;
  currentIndex: number;
  indexShow: number = 0;
  miniNoteData = [];
  noNext: boolean = false;
  noPrev: boolean = false;
  demoFlag: boolean = false;
  imgFlag: boolean = false;
  note;
  noId;
  bookmark: boolean = false;
  bookmarkNote: boolean = false;
  StarCount: number = 0;
  bookmarkedNote: boolean = false;
  bookmarkicon: boolean = false;
  url = environment.backendUrl + '/images/';
  imgUrl = [];
  StarimgUrl = [];
  newNote = [];
  noteTags;
  starimgFlag: boolean = false;
  noQuestion: boolean = false;
  noPrevQuestion: boolean = false;
  currentIndexToShow: number = 0;
  questionStarData;
  notestarTags;
  nostarId;
  notestarContent;
  ministarNoteTitle;
  noPrevBtn: boolean = false;
  noNextBtn: boolean = false;
  //bookmarkedOnlyData=[];
  bookmarkedStarNote;
  showBar:boolean=false;
  noteDiv: boolean = false;
  demoFlagstar:boolean=false;
  ModeOfNote;
  notIndex:number=0;
  recentNoteContent;
  recentMiniNoteTitle;
  recentNoteTags;
  recentBookmarkedNote:boolean=false;
 
  recentBookmarkicon:boolean = false;
  recentNoteDiv:boolean=false;
  recentNotes=[];
  recentNoteId;
  constructor(private socialSharing: SocialSharing,public navCtrl: NavController,public events: Events, public navParams: NavParams, private __auth: AuthService, public platform: Platform, private noteService: NotesService, private alertCtrl: AlertController) {
    this.noteId = navParams.get('noteId');
    this.regId = navParams.get('RegId');
    this.demoFlag = navParams.get('demoflag');
    this.ModeOfNote = navParams.get('mode');
    console.log("this.ModeOfNote",this.ModeOfNote);
    this.events.publish('user:created', true, Date.now());
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }
  private backButtonFunc(): void {
    this.navCtrl.setRoot(Note, { parms: "daily" });
  }
  ngOnInit() {
    this.tabBarElement.style.display = 'none';
if(this.ModeOfNote == "daily"){
  this.loadMiniNotes();

}else if(this.ModeOfNote == "recent"){
  console.log("haaaaaaaaaaaaaai");
  
  this.loadRecent();
}
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }
  onCardInteract(event) {
  }

  bookmarked(id) {

    if (this.bookmarkedNote) {
      this.bookmark = false;
    } else {
      this.bookmark = true;
    }
    this.noteService.bookmarkMiniNote(id, this.bookmark).subscribe(
      res => {
        if (res.status) {
          this.noteService.loadMininote(id, this.regId, this.demoFlag).subscribe(
            res => {
              if (res.data) {
                this.bookmarkedNote = res.data[0].bookmarked;
                if (this.bookmarkedNote) {
                  this.bookmarkicon = true;
                } else {
                  this.bookmarkicon = false;
                }
              }
            });
        }
      })
  }
  bookmarkedStar(nostarId) {
    this.noteService.bookmarkMiniNote(nostarId, false).subscribe(
      res => {

        if (res.status) {
          this.logEvent(true);
        }
      });

  }
  logEvent(event) {
    this.questionStarData=[];
    if (event._value == true || event == true) {
      this.showBar=true
      this.noteDiv = false;
      //this.noPrevBtn = true;
      this.bookmarkNote = true;
      this.bookmarkicon = true;
      this.currentIndexToShow = 0;
      this.noteService.bookmarkedOnly().subscribe(
        res => {

console.log("bookmrked res",res);

          if (res.data) {
            this.questionStarData = res.data;
            if (res.data.length == 1) {

              this.noPrevBtn = true;
              this.noNextBtn = true;


              this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
              this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
              this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
              this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
              this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;
              //this.demoFlagstar = this.miniNotes[this.currentIndex].demoFlag;

            }
            if (res.data.length > 1) {
              this.noPrevBtn = true;
              this.noNextBtn = false;

              this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
              this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
              this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
              this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
              this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;
              ////this.demoFlagstar = this.miniNotes[this.currentIndex].demoFlag;


            }
          } else {
            //this.questionStarData=[];
           this.showBar=true
            this.noteDiv = false;
            this.bookmarkNote = false;
            this.noPrevBtn = true;
            this.noNextBtn = true;
            this.__auth.notificationInfo("No Notes found !")
          }
        })
    
    } else {
     console.log("(this.ModeOfNote",this.ModeOfNote);
     this.loadMiniNotes();
      // if(this.ModeOfNote == "daily"){
      //  
      
      // }else if(this.ModeOfNote == "recent"){
      //   console.log("hooooooooooooooooooo");
        
      //   this.loadRecent();
      // }
//       this.showBar=false;
//       this.noteDiv = true;
// console.log("mmmmmmmmmmmmmmmmmmm",this.bookmarkedStarNote);

//       if (this.bookmarkedStarNote == true) {
//         this.bookmarkicon = true;
//       } else {
//         this.bookmarkicon = false;

//       }
//       this.bookmarkNote = false
   }
  }

  next() {



    if (!this.bookmarkNote) {
      this.noPrev = false;
      this.currentIndex++;

      if (this.currentIndex == this.miniNotes.length - 1) {
        this.noNext = true;

        this.noId = this.miniNotes[this.currentIndex]._id;
        this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
        this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(
          res => {
            if (res.data) {
              this.bookmarkedNote = res.data[this.indexShow].bookmarked;
              if (this.bookmarkedNote) {
                this.bookmarkicon = true;
              } else {
                this.bookmarkicon = false;
              }
            }
          })
        this.noteContent = this.miniNotes[this.currentIndex].noteContent;
        this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
        this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];


      } else {
        this.noNext = false;
        this.noId = this.miniNotes[this.currentIndex]._id;
        this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
        this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(
          res => {
            if (res.data) {
              this.bookmarkedNote = res.data[this.indexShow].bookmarked;
              if (this.bookmarkedNote) {

                this.bookmarkicon = true;
              } else {
                this.bookmarkicon = false;
              }
            }
          })
        this.noteContent = this.miniNotes[this.currentIndex].noteContent;
        this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
        this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];

      }
    } else {
      this.noPrevBtn = false;
      this.currentIndexToShow++;
      if (this.currentIndexToShow == this.questionStarData.length - 1) {
        this.noNextBtn = true;
        this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
        this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
        this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
        this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
        this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;

      } else {
        this.noNextBtn = false;
        this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
        this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
        this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
        this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
        this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;

      }
    }
  }

  prev() {


    if (!this.bookmarkNote) {
      this.noNext = false;
      this.currentIndex--;
      if (this.currentIndex > 0) {
        this.noPrev = false;
        this.noId = this.miniNotes[this.currentIndex]._id;
        this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
        this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(
          res => {
            if (res.data) {
              this.bookmarkedNote = res.data[this.indexShow].bookmarked;
              if (this.bookmarkedNote) {
                this.bookmarkicon = true;
              } else {
                this.bookmarkicon = false;
              }
            }
          })
        this.noteContent = this.miniNotes[this.currentIndex].noteContent;
        this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
        this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];

      } else {
        this.noPrev = true;
        this.noId = this.miniNotes[this.currentIndex]._id;
        this.demoFlag = this.miniNotes[this.currentIndex].demoFlag;
        this.noteService.loadMininote(this.noId, this.regId, this.demoFlag).subscribe(
          res => {
            if (res.data) {
              this.bookmarkedNote = res.data[this.indexShow].bookmarked;
              if (this.bookmarkedNote) {

                this.bookmarkicon = true;
              } else {
                this.bookmarkicon = false;
              }
            }
          })
        this.noteContent = this.miniNotes[this.currentIndex].noteContent;
        this.miniNoteTitle = this.miniNotes[this.currentIndex].noteTitle;
        this.noteTags = this.miniNotes[this.currentIndex].noteTags[0];

      }
    } else {
      this.noNextBtn = false;
      this.currentIndexToShow--;
      if (this.currentIndexToShow > 0) {
        this.noPrevBtn = false;
        this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
        this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
        this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
        this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
        this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;

      } else {
        this.noPrevBtn = true;
        this.nostarId = this.questionStarData[this.currentIndexToShow].noteId;
        this.notestarContent = this.questionStarData[this.currentIndexToShow].noteContent;
        this.ministarNoteTitle = this.questionStarData[this.currentIndexToShow].noteTitle;
        this.notestarTags = this.questionStarData[this.currentIndexToShow].noteTags[0];
        this.bookmarkedStarNote = this.questionStarData[this.currentIndexToShow].bookmarked;

      }
    }
  }
  saveRecentNote(id,mode){
    console.log("idkkkkkkkkkk",id,mode);
    this.noteService.saveRecentNote(id,mode).subscribe(
      res => {
        if(res){
          console.log("recent note save ",res);
          
        }
      });

  }
  shareData(){
    this.socialSharing.share(document.getElementById("demo").innerHTML, this.miniNoteTitle, "URL to file or image", "www.nextfellow.com").then(() => {
      console.log("shareSheetShare: Success");
    }).catch(() => {
      console.error("shareSheetShare: failed");
    });
    

  }
  loadRecent(){
    console.log("recent inssssssssssssssssssssssssssssssssssssssss");
    this.recentNoteDiv = true;
    this.noteDiv=false;
    this.bookmarkNote=false;
    
    this.noteService.loadRecentNote().subscribe(
      res => {
        if(res.data){
         
          console.log("recent note////// ",res.data);
       this.recentNotes=res.data;
       if (res.data.length == 1) {
        this.noPrev = true;
        this.noNext = true;
      }
        (res.data).map((item, index) => {

        if (item.noteId ==  this.noteId) {
          console.log("indexx",index);
          this.recentNoteId=item.noteId;
          this.notIndex=index;
         this.recentNoteContent= item.noteContent;
         this.recentMiniNoteTitle=item.noteTitle;
         this.recentNoteTags = item.noteTags[0];
         this.recentBookmarkedNote=item.bookmarked;
         if (this.recentBookmarkedNote) {
          this.recentBookmarkicon = true;
        } else {
          this.recentBookmarkicon = false;
        }
        }
        return item;
      });

      if (this.notIndex == 0) {

        this.noPrev = true;
      }  
        }
      });
    
  }
  nextRecent(){
    
      this.noPrev = false;
      this.notIndex++;
      console.log("this.notIndex",this.notIndex);
      

      if (this.notIndex == this.recentNotes.length - 1) {
        this.noNext = true;
        this.recentNoteContent= this.recentNotes[this.notIndex].noteContent;
        this.recentMiniNoteTitle=this.recentNotes[this.notIndex].noteTitle;
        this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
        this.recentNoteId=this.recentNotes[this.notIndex].noteId;

        this.recentBookmarkedNote=this.recentNotes[this.notIndex].bookmarked;
        if (this.recentBookmarkedNote) {
         this.recentBookmarkicon = true;
       } else {
         this.recentBookmarkicon = false;
       }
      }else{
        this.noNext = false;
        this.recentNoteContent= this.recentNotes[this.notIndex].noteContent;
        this.recentMiniNoteTitle=this.recentNotes[this.notIndex].noteTitle;
        this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
        this.recentNoteId=this.recentNotes[this.notIndex].noteId;
        this.recentBookmarkedNote=this.recentNotes[this.notIndex].bookmarked;

        if (this.recentBookmarkedNote) {
         this.recentBookmarkicon = true;
       } else {
         this.recentBookmarkicon = false;
       }
      }
    
  }
  prevRecent(){
    this.noNext = false;
    this.notIndex--;
    if (this.notIndex > 0) {
      this.noPrev = false;
      this.recentNoteContent= this.recentNotes[this.notIndex].noteContent;
      this.recentMiniNoteTitle=this.recentNotes[this.notIndex].noteTitle;
      this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
      this.recentNoteId=this.recentNotes[this.notIndex].noteId;

      this.recentBookmarkedNote=this.recentNotes[this.notIndex].bookmarked;
      if (this.recentBookmarkedNote) {
       this.recentBookmarkicon = true;
     } else {
       this.recentBookmarkicon = false;
     }
    }else{
      this.noPrev = true;
      this.recentNoteContent= this.recentNotes[this.notIndex].noteContent;
      this.recentMiniNoteTitle=this.recentNotes[this.notIndex].noteTitle;
      this.recentNoteTags = this.recentNotes[this.notIndex].noteTags[0];
      this.recentNoteId=this.recentNotes[this.notIndex].noteId;

      this.recentBookmarkedNote=this.recentNotes[this.notIndex].bookmarked;
      if (this.recentBookmarkedNote) {
       this.recentBookmarkicon = true;
     } else {
       this.recentBookmarkicon = false;
     }
    }
  }
  loadMiniNotes() {
    this.noteDiv = true;
    this.recentNoteDiv = false;
    this.bookmarkNote=false;
    this.noteService.loadMininote(this.noteId, this.regId, this.demoFlag).subscribe(
      res => {
        if (res.data) {
        
          this.noteService.fetchAllMiniNotes().subscribe(
            res => {
              if (res) {
                this.miniNotes = res.data.notes;
                if (this.miniNotes.length == 1) {
                  this.noPrev = true;
                  this.noNext = true;
                }
                let newData = (this.miniNotes).map((item, index) => {

                  if (item._id == this.noId) {
                    this.currentIndex = index;

                  }
                  return item;
                });
              }
              if (this.currentIndex == 0) {

                this.noPrev = true;
              }
            });
          this.miniNoteData = res.data;
          this.bookmarkedNote = res.data[this.indexShow].bookmarked;
          if (this.bookmarkedNote) {
            this.bookmarkicon = true;
          } else {
            this.bookmarkicon = false;
          }
          this.noId = res.data[this.indexShow].noteId;
          this.noteContent = res.data[this.indexShow].noteContent;
          this.miniNoteTitle = res.data[this.indexShow].noteTitle;
          this.noteTags = res.data[this.indexShow].noteTags[0];

        } else {
          this.noteDiv = false;

        }
      });
  }
}
