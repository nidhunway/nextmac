import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { QuestionService } from '../../../services/question.service';
import { ToastController } from 'ionic-angular';



@IonicPage({
    segment: "learnOver",
})
@Component({
    selector: 'page-learnOver',
    templateUrl: 'learnOver.html',
    providers: [QuestionService]
})
export class LearnOver {
    @ViewChild('barCanvas') barCanvas;
    @ViewChild('doughnutCanvas') doughnutCanvas;
    @ViewChild('doughnutCanvas') doughnutCanvas0;

    @ViewChild('doughnutCanvas') doughnutCanvas1;
    @ViewChild('doughnutCanvas') doughnutCanvas2;
    @ViewChild('doughnutCanvas') doughnutCanvas3;
    @ViewChild('doughnutCanvas') doughnutCanvas4;
    @ViewChild('doughnutCanvas') doughnutCanvas5;
    @ViewChild('doughnutCanvas') doughnutCanvas6;
    @ViewChild('doughnutCanvas') doughnutCanvas7;
    @ViewChild('doughnutCanvas') doughnutCanvas8;
    @ViewChild('doughnutCanvas') doughnutCanvas9;
    @ViewChild('doughnutCanvas') doughnutCanvas10;
    @ViewChild('doughnutCanvas') doughnutCanvas11;
    @ViewChild('doughnutCanvas') doughnutCanvas12;
    @ViewChild('doughnutCanvas') doughnutCanvas13;
    @ViewChild('doughnutCanvas') doughnutCanvas14;
    @ViewChild('doughnutCanvas') doughnutCanvas15;
    @ViewChild('doughnutCanvas') doughnutCanvas16;
    @ViewChild('doughnutCanvas') doughnutCanvas17;
    @ViewChild('doughnutCanvas') doughnutCanvas18;


    @ViewChild('lineCanvas') lineCanvas;

    barChart: any;
    doughnutChart: any;
    lineChart: any;
    subjectList;
    userData;
    activeUserId;
    correct;
    wrong;
    TotalCount;
    TotalScore;
    DataInfo;
    chapterList;
    sub_percent:number=0;
    firstLoad: boolean = false;
    subArray=[];
    constructor(public navCtrl: NavController,public toastCtrl:ToastController, public navParams: NavParams, public questionService: QuestionService) {
        this.subjectList = JSON.parse(localStorage.getItem('subjectData'));
console.log("chapterListchapterListchapterListchapterList",this.chapterList);

        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;

    }

    ionViewDidLoad() {
        // this.DataInfo={test:"data",test2:"data2"}
        // this.firstLoad = false;
        // const toast = this.toastCtrl.create({
        //     message: "Please select subject",
        //     duration: 3000,
        //     position: 'bottom'
        //   });
        //   toast.present();
        
        console.log("subject list",this.subjectList);
    for( let i=0;i<this.subjectList.length;i++){
        
        this.questionService.totalQuestions(this.subjectList[i].id).subscribe(
            res => {
              //  console.log("res",res);
                
              if (res.status) {
                // this.TotalCount = res.data.totalNumber;
                // console.log("total number",this.TotalCount);
                
              }
              else {
      
              }
      
            }, err => {
      
            });
        this.questionService.learnPerformance(this.activeUserId, this.subjectList[i].id).subscribe(
            res => {
             console.log("rem///////mmms",res);
               this.DataInfo=res.data;
                this.correct = res.data.correct;
                this.wrong = res.data.wrong;
                this.TotalScore = this.correct+this.wrong;
                console.log("total score",this.TotalScore);
                this.TotalCount=500;
                this.sub_percent=  ( this.TotalScore/   this.TotalCount  ) * 100; 
                this.subArray.push()        
                if (this.TotalScore) {
                    this.firstLoad=true;
                    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

                        type: 'doughnut',
                        data: {
                            labels: ["Total", "Attend"],
                            datasets: [{
                                label: this. sub_percent,
                                data: [this.TotalCount, this.TotalScore],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)'
                                ],
                                hoverBackgroundColor: [
                                    "#FF6384",
                                    "#36A2EB"

                                ]
                            }]
                        }

                    });
                    let data = (this.subjectList).map((item,i) => {
       
                        item.graph = "doughnutCanvas"+i;
                
                        return item;
                      });
                      if(data){
                          this.subjectList=data;
                          console.log("subject",data);
                          
                      }
                
                }else{
                    console.log("no data");
                    
                }
            }) 
    }
    
    }
   
    evaluateLearn(item, count) {
        this.TotalCount = count;
        
     
        var subID = item
        this.questionService.learnPerformance(this.activeUserId, subID).subscribe(
            res => {
               this.DataInfo=res.data;
                this.correct = res.data.correct;
                this.wrong = res.data.wrong;
                this.TotalScore = this.correct+this.wrong;
                
             
                
                if (this.TotalScore) {
                    this.firstLoad=true;
                    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

                        type: 'doughnut',
                        data: {
                            labels: ["Total", "Attend"],
                            datasets: [{
                                label: '# of Votes',
                                data: [this.TotalCount, this.TotalScore],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)'
                                ],
                                hoverBackgroundColor: [
                                    "#FF6384",
                                    "#36A2EB"

                                ]
                            }]
                        }

                    });

                }else{
                    const toast = this.toastCtrl.create({
                        message: "No data available",
                        duration: 3000,
                        position: 'bottom'
                      });
                      toast.present();
                }
            }
        );


    }


}


