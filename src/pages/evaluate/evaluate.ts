import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import { EvaluateOver } from '../../pages/evaluate/evaluateOver/evaluateOver';
import { ExamOver } from '../../pages/evaluate/examOver/examOver';
import { LearnOver } from '../../pages/evaluate/learnOver/learnOver';
import { homeNew } from '../../pages/homeNew/homeNew';
import { HomePage } from '../../pages/dashboard/home';

@Component({
  selector: 'page-eval',
  templateUrl: 'evaluate.html'
})
export class Evaluate {
  tab1Root=LearnOver;
  tab2Root=EvaluateOver;
  tab3Root=ExamOver;

  constructor(public navCtrl: NavController,public platform: Platform) {

  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
 
  private backButtonFunc(): void {
    this.navCtrl.setRoot(HomePage);
  }

}
