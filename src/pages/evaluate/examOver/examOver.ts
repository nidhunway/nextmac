import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ExamService } from '../../../services/exam.service';
import { ToastController } from 'ionic-angular';



@IonicPage({
    segment: "examOver",
})
@Component({
    selector: 'page-examOver',
    templateUrl: 'examOver.html',
    providers: [ExamService]
})
export class ExamOver {
    @ViewChild('barCanvas') barCanvas;
    @ViewChild('ExamGraph') ExamGraph;
    @ViewChild('lineCanvas') lineCanvas;
    @ViewChild('doughnutCanvas') doughnutCanvas;

    barChart: any;
    examChart: any;
    lineChart: any;
    doughnutChart:any;
    mode;
    userData;
    activeUserId;
    userId;
    examList;
    examOverTot;
    examCorr;
    examWrong;
    examQstnCount;
    ExamLoad: boolean = false;
    constructor(public navCtrl: NavController,public toastCtrl:ToastController, public navParams: NavParams, private examService: ExamService) {
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
    }

    ionViewDidLoad() {

    
        this.examTotCount(this.userId, this.activeUserId);
        this.ExamLoad = false;
        const toast = this.toastCtrl.create({
            message: "Please select Exam",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
    }
    examTotCount(userId, activeUserId) {


        this.mode = 'completed';

        this.examService.fetchAllExam(this.mode).subscribe(
            res => {
                console.log(res, "1")
                this.examList = res.data.examList;
                if(this.examList){
                    const toast = this.toastCtrl.create({
                        message: "Please try Exams",
                        duration: 3000,
                        position: 'bottom'
                      });
                      toast.present();

                }
            });
        // Grand test question count

    }
    evaluateExam(tot, corr, wrng, attend, item) {

        this.ExamLoad = true;
        this.examOverTot = attend;//Total questions attended in exam
        this.examCorr = corr;//Number of correct in exam
        this.examWrong = wrng;//Number of wrong in exam
        this.examQstnCount = tot;//Total questions in exam
 
        
            if(this.examQstnCount&&this.examOverTot){
        this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
            
                                 type: 'doughnut',
                                 data: {
                                     labels: ["Total", "Attend"],
                                     datasets: [{
                                         label: '# of Votes',
                                         data: [this.examQstnCount,this.examOverTot],
                                         backgroundColor: [
                                             'rgba(255, 99, 132, 0.2)',
                                             'rgba(54, 162, 235, 0.2)'
                                         ],
                                         hoverBackgroundColor: [
                                             "#FF6384",
                                             "#36A2EB"
            
                                         ]
                                     }]
                                 }
            
                             });
                            }else{
                                const toast = this.toastCtrl.create({
                                    message: "No data available",
                                    duration: 3000,
                                    position: 'bottom'
                                  });
                                  toast.present();
                            }



    }

}
