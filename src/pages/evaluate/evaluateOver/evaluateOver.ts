import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ExamService } from '../../../services/exam.service';
import { QuestionService } from '../../../services/question.service';
import {Observable} from 'rxjs/Rx';



@IonicPage({
    segment: "evaluateOver",
})
@Component({
    selector: 'page-evaluateOver',
    templateUrl: 'evaluateOver.html',
    providers: [ExamService,QuestionService]
})
export class EvaluateOver {
    @ViewChild('barCanvas') barCanvas;
    @ViewChild('doughnutCanvas') doughnutCanvas;
    @ViewChild('supExam') supExam;
    @ViewChild('lineCanvas') lineCanvas;

    barChart: any;
    doughnutChart: any;
    suprExamGraph:any;
    lineChart: any;
    subjectList;
    mode;
    userData;
    activeUserId;
    userId;
    examList;
    examOverTot;
    examCorr;
    examWrong;
    examQstnCount;
    examTotQuestions=0;
    examCount;
    grandCount;
    grandTotQuestions;
    constructor(public navCtrl: NavController,private questionService:QuestionService, public navParams: NavParams, private examService: ExamService) {
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
               this.subjectList = JSON.parse(localStorage.getItem('subjectData'));


    }

    ionViewDidLoad() {
        this.learnSum(); 
        this.examTotCount(this.userId, this.activeUserId);
        
    }
    learnSum(){
   
        console.log("subject list",this.subjectList)
        var subjectId;
        var count=0;
        let promise=[];
        for(var i=0;i<this.subjectList.length;i++){
            subjectId=this.subjectList[i].id
            promise.push(this.questionService.learnPerformance(this.activeUserId,subjectId));
        }
        Observable.forkJoin(promise)
        .subscribe((response) => {
          var total=0;
          for(var i=0;i<this.subjectList.length;i++){
            total += response[i]['data'].correct+response[i]['data'].wrong;
            if(i==18){
                this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
                    
                                         type: 'doughnut',
                                         data: {
                                             labels: ["Total", "Attend"],
                                             datasets: [{
                                                 label: '# of Votes',
                                                 data: [28000,total],
                                                 backgroundColor: [
                                                     'rgba(255, 99, 132, 0.2)',
                                                     'rgba(54, 162, 235, 0.2)'
                                                 ],
                                                 hoverBackgroundColor: [
                                                     "#FF6384",
                                                     "#36A2EB"
                    
                                                 ]
                                             }]
                                         }
                    
                                     });
              
            }
          }
        });

    }
    examTotCount(userId, activeUserId){
        // Exam question Count
    
        this.mode = 'completed';
        let count;
        this.examService.fetchAllExam(this.mode).subscribe(
          res => {
            this.examList = res.data.examList;  
            for(var i=0;i<this.examList.length;i++){
              this.examTotQuestions=this.examTotQuestions+this.examList[i]['examResult'].noOfQueAttended;
             
            }
            var count=res.data.examList.length
            if(count>0){
              this.examCount=res.data.examList.length;            
            }
            else{
              this.examCount=0                       
            }
          });
        // Grand test question count
          let complete,pending,compCount,pendCount;     
          this.examService.fetchGrandExam().subscribe(
            res =>{
              var pendingCount=0;
              var completeCount=0;
              var compCount=0;
              compCount=res.data.completedExam.length;
              if(compCount>0){
                this.grandCount=res.data.completedExam.length;            
              }
              else{
                this.grandCount=0                       
              }
              complete=res.data.completedExam;
              pending=res.data.pendingExams;
              for(var i=0;i<pending.length;i++){
                for(var j=0;j<pending[i].questionPalette.length;j++){
                  if(pending[i].questionPalette[j].status=='answered'){
                    pendingCount=pendingCount+1
                  }
                }
              }          
            //  For Completed Grand Exams
              for(var i=0;i<complete.length;i++){
                for(var j=0;j<complete[i].questionPalette.length;j++){
                  if(complete[i].questionPalette[j].status=='answered'){
                    completeCount=completeCount+1
                  }
                }
              }
              this.grandTotQuestions=completeCount+pendingCount
              console.log(this.grandTotQuestions,"grantg")
              this.examTotQuestions=this.grandTotQuestions+this.examTotQuestions
              console.log(this.examTotQuestions,"value of last")
    
        //gdsgfdsg
        this.suprExamGraph = new Chart(this.supExam.nativeElement, {
            
                                 type: 'doughnut',
                                 data: {
                                     labels: ["Total", "Attend"],
                                     datasets: [{
                                         label: '# of Votes',
                                         data: [28000,this.examTotQuestions],
                                         backgroundColor: [
                                             'rgba(255, 99, 132, 0.2)',
                                             'rgba(54, 162, 235, 0.2)'
                                         ],
                                         hoverBackgroundColor: [
                                             "#FF6384",
                                             "#36A2EB"
            
                                         ]
                                     }]
                                 }
            
                             });
               
            }
          );
      }


}
