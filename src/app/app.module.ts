import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import {HideheaderDirective} from '../directives/hideheader/hideheader'
import {TabHiddenDirective} from '../directives/hideheader/tabsHiden'

import { MyApp } from './app.component';
import { HomePage } from '../pages/dashboard/home';
import { ListPage } from '../pages/learn/list';
import { Questions } from '../pages/learn/questions/questions';
import { Details } from '../pages/exam/exam-room/details/details';
import { HintModalPage } from '../pages/learn/questions/note/note.modal';
import { editModal } from  '../pages/learn/questions/editModal/editModal';
import { ReportProb } from '../pages/learn/questions/report-questions/report';
// import { DailyNote } from '../pages/learn/questions/dailyNotes/dailynotes';
import { DailyNote } from '../pages/note/dailyNotes/dailynotes';

import { grantModal } from '../pages/exam/GrandExam/grantModal/grantModal';
import { ExamModal } from '../pages/exam/examModal/examModal';
import { CreateExam } from '../pages/exam/createexam/createExam';
import { examComponent } from '../pages/exam/examNew/examComponent';

import { writeExam } from '../pages/exam/createexam/write-exam/write-exam';
import { termsrules } from '../pages/exam/weekend/TermsAndRules/terms.component';
import { grantExam } from '../pages/exam/GrandExam/grantexam.component';
import { Profile } from '../pages/profile/profile';
import { Exam } from '../pages/exam/exam';
import { ExamRoom } from '../pages/exam/exam-room/examroom';
import { weekendReg } from '../pages/exam/weekend/weekend.component';
import { Evaluate } from '../pages/evaluate/evaluate';
import { EvaluateOver } from '../pages/evaluate/evaluateOver/evaluateOver';
import { ExamOver } from '../pages/evaluate/examOver/examOver';
import { LearnOver } from '../pages/evaluate/learnOver/learnOver';
import { Note } from '../pages/note/note';
import { Login } from '../commen/login/login';
import { Forgot } from '../commen/forgot/forgot';
import { Setting } from '../commen/setting/setting';
import { moreSetting } from '../commen/setting/moreSettings/moreSettings';

import { About } from '../commen/setting/about/about';
import { contactUs } from '../commen/setting/contactUs/contactUs';
import { privacyPolicy } from '../commen/setting/privacyPolicy/privacyPolicy';
import { returnPolicy } from '../commen/setting/returnPolicy/returnPolicy';
import { faq } from '../commen/setting/FAQ/faq';
import { teachers } from '../commen/setting/teachersPage/teachers';
import { timetable } from '../commen/setting/timetable/timetable';

import { PopoverPage } from '../pages/exam/popoverPage/popoverPage';

import {HttpModule} from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { naveBarShowSrvice } from '../services/navBarService'
import { homeNew } from '../pages/homeNew/homeNew';
import { aiimsResult } from '../pages/exam/exam-room/aiimsResult/aiimsResult';
import { aiimsExam } from '../pages/exam/createexam/aiimsExam/aiimsExam';



@NgModule({
  declarations: [
    MyApp,
    homeNew,
    HomePage,
    examComponent,
    ListPage,
    Login,
    DailyNote,
    Exam,
    Evaluate,
    Note,
    Questions,
    Details,
    Forgot,
    HintModalPage,
    editModal,
    ReportProb,
    Setting,
    Profile,
    grantModal,
    grantExam,
    ExamRoom,
    ExamModal,
    CreateExam,
    writeExam,
    timetable,
    EvaluateOver,
    ExamOver,
    LearnOver,
    PopoverPage,
    termsrules,
    weekendReg,
    HideheaderDirective,
    TabHiddenDirective,
    About,
    contactUs,
    privacyPolicy,
    returnPolicy,
    faq,
    teachers,
    moreSetting,
    aiimsResult,
    aiimsExam
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    IonicModule.forRoot(MyApp,{},{
      links: [
       
        
       
      ]
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    homeNew,
    HomePage,
    examComponent,
    ListPage,
    Login,
    editModal,
    DailyNote,
    Exam,
    Evaluate,
    Note,
    Questions,
    Details,
    Forgot,
    HintModalPage,
    PopoverPage,
    ReportProb,
    Setting,
    Profile,
    timetable,
    grantModal,
    grantExam,
    ExamRoom,
    ExamModal,
    CreateExam,
    writeExam,
    EvaluateOver,
    ExamOver,
    LearnOver,
    termsrules,
    weekendReg,
    About,
    contactUs,
    privacyPolicy,
    returnPolicy,
    faq,
    moreSetting,
    teachers,
    aiimsResult,
    aiimsExam
    ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    FileTransfer,
    naveBarShowSrvice,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {

  constructor(public _navBar:naveBarShowSrvice) {
  }
}
