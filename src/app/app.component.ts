import { Component, ViewChild } from '@angular/core';

import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Login } from '../commen/login/login';
import { Setting } from '../commen/setting/setting';
import { HomePage } from '../pages/dashboard/home';
import { ListPage } from '../pages/learn/list';
import { Exam } from '../pages/exam/exam';
import { Evaluate } from '../pages/evaluate/evaluate';
import { Note } from '../pages/note/note';

import { weekendReg } from '../pages/exam/weekend/weekend.component';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ToastController } from 'ionic-angular';
import { examComponent } from '../pages/exam/examNew/examComponent';
import { homeNew } from '../pages/homeNew/homeNew';


@Component({
  templateUrl: 'app.html',
  providers:[]
})

export class MyApp {
  
  @ViewChild(Nav) nav: Nav;
  username;
  firstname;
  lastName;
  str3;
  rootPage: any = Login;

  pages: Array<{title: string, component: any,icon:string}>;

  constructor(public toastCtrl: ToastController,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
   if(localStorage.getItem("userData"))this.rootPage= homeNew;
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: HomePage ,icon:'ios-home'},
      { title: 'Learn', component: ListPage,icon:'ios-book'},
      { title: 'Exam room', component: examComponent,icon:'ios-create' },
      { title: 'Registration', component: weekendReg,icon:'ios-card' },
      { title: 'Evaluate', component: Evaluate,icon:'paper' },
      { title: 'Notes', component: Note,icon:'bookmarks' },
      { title: 'Settings', component: Setting,icon:'ios-cog' }

    ];
    platform.ready().then(() => {
    
            statusBar.styleDefault();
            splashScreen.hide();
       
      
          //  var notificationOpenedCallback = (jsonData) => { // <- Here!
          //    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      
          //  };
          //  window["plugins"].OneSignal
          //    .startInit("fb35a8d2-1388-41a7-9a00-192f077e6911", "885727504851")
          //    .handleNotificationOpened(notificationOpenedCallback)
          //    .endInit();
         });

  }
  ngOnInit() {
    this.nav.setRoot(Login);
    this.username=JSON.parse(localStorage.getItem('userData'));
    
    if(this.username){
      this.firstname=this.username.profile.firstName;
      this.lastName=this.username.profile.lastName;
      this.str3 = this.firstname + " " + this.lastName 
    }
  
    
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
