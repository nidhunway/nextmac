import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Login } from '../../commen/login/login';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service'
import { ToastController } from 'ionic-angular';

@Component({
    selector: 'page-forgot',
    templateUrl: 'forgot.html',
    providers: [AuthService]
})
export class Forgot {
    
    login = Login;
    spinnerLoad: boolean = false;

    constructor(public navCtrl: NavController, private authservice: AuthService, public toastCtrl: ToastController) {

    }

    Reset(value) {
   
        this.spinnerLoad = true;
        let emails={
            email:value.email.toLowerCase()
        }
       console.log(emails,"emails")
       
        this.authservice.forgotPassword(emails).subscribe(
            res => {
                this.spinnerLoad = false;
                console.log(res);

                if (res.status == 1) {
                    this.navCtrl.push(Login);
                    const toast = this.toastCtrl.create({
                        message: 'Reset Email Sent to user',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();

                }
                else if (res.status == 0) {
                    const toast = this.toastCtrl.create({
                        message: res.msg,
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();

                }
                else {
                    const toast = this.toastCtrl.create({
                        message: "OOPS! Somethng went wrong.",
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
            }, err => {
                const toast = this.toastCtrl.create({
                    message: "OOPS! Somethng went wrong.",
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();

            }
        )
        /* Write function to send email to users inbox */
    }

}
