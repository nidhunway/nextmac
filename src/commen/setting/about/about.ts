import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';
import { privacyPolicy } from '../../../commen/setting/privacyPolicy/privacyPolicy';
import { returnPolicy } from '../../../commen/setting/returnPolicy/returnPolicy';

import { contactUs } from '../../../commen/setting/contactUs/contactUs';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  providers:[AppVersion]
 
})
export class About {
 appversions:any;
    constructor(public navCtrl: NavController,private appVersion: AppVersion) {

    }
    ngOnInit() {
 this.getAppversion();
// this.appversions=this.appVersion.getVersionNumber();


    }
   async  getAppversion(){
     this.appversions= await this.appVersion.getVersionNumber(); 
 console.log("appversion",this.appversions);

    }
    privacy(){
        console.log("privacy");
        this.navCtrl.push(privacyPolicy);
    }
    return(){
        this.navCtrl.push(returnPolicy);
    }
    contact(){
        this.navCtrl.push(contactUs);
    }
}