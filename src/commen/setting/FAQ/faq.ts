import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ExamService } from '../../../services/exam.service';
import { Questions } from '../../../pages/learn/questions/questions';


@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
  providers: [ExamService]

 
})
export class faq {
  userData;
  userId;
  questionData;
  shownGroup=null;
  noteQstn=[];
  noteHead;
  dashbrdHead;
  dashQstn=[];
  learnQstn=[];
  learnHead;
  examQstn=[];
  examHead;
  regQstn=[];
  regHead;
    constructor(public navCtrl: NavController,private examservice:ExamService) {
    }
    ngOnInit() {

      this.userData = JSON.parse(localStorage.getItem('userData'));
      console.log("userdata",this.userData.userId);
      this.userId=this.userData.userId;
      console.log("userid,",this.userId);
      
      this.examservice.faq(this.userId).subscribe(
        res => {
          console.log("res",res);
          if(res.status){
            console.log("resdata",res.data);
            this.questionData=res.data;
            for(let i=0;i<this.questionData.length;i++){
              if(this.questionData[i].category=="Note"){
                this.noteHead=this.questionData[i].category;
                this.noteQstn.push({"quiestion":this.questionData[i].Questions,"answer":this.questionData[i].answer});
                console.log(this.noteQstn,"this.noteQstnthis.noteQstn");
                
              }
              else if(this.questionData[i].category=="Dashboard"){
                this.dashbrdHead=this.questionData[i].category;
                this.dashQstn.push({"quiestion":this.questionData[i].Questions,"answer":this.questionData[i].answer});

            }
           else if(this.questionData[i].category=="Learn"){
              this.learnHead=this.questionData[i].category;
              this.learnQstn.push({"quiestion":this.questionData[i].Questions,"answer":this.questionData[i].answer});

          }else if(this.questionData[i].category=="Exam Room"){
            this.examHead=this.questionData[i].category;
            this.examQstn.push({"quiestion":this.questionData[i].Questions,"answer":this.questionData[i].answer});

        }else{
          this.regHead=this.questionData[i].category;
            this.regQstn.push({"quiestion":this.questionData[i].Questions,"answer":this.questionData[i].answer});
        console.log("this.regQstn",this.regQstn);
        
          }
          
            
          }
        }
          
        });
    }
    toggleGroup(group) {
      if (this.isGroupShown(group)) {
          this.shownGroup = null;
      } else {
          this.shownGroup = group;
      }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };
    }
    

