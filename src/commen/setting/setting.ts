import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';

import { Login } from '../../commen/login/login';
import { Profile } from '../../pages/profile/profile';
import { AuthService } from '../../services/auth.service'
import { AngularFireAuth } from 'angularfire2/auth';
import { About } from '../../commen/setting/about/about';
import { faq } from '../../commen/setting/FAQ/faq';
import { teachers } from '../../commen/setting/teachersPage/teachers';
import { timetable } from '../../commen/setting/timetable/timetable';

import { homeNew } from '../../pages/homeNew/homeNew';
import { HomePage } from '../../pages/dashboard/home';
import { weekendReg } from '../../pages/exam/weekend/weekend.component';
import { naveBarShowSrvice } from '../../services/navBarService';
import { moreSetting } from '../../commen/setting/moreSettings/moreSettings';

import { Evaluate } from '../../pages/evaluate/evaluate';
import { Events } from 'ionic-angular';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
  providers: [AuthService,naveBarShowSrvice]
})
export class Setting {
  userData;
  UserName;
  tabBarElement;
  constructor(public _navBar:naveBarShowSrvice,public events: Events, public navCtrl: NavController,public platform: Platform, private authservice: AuthService, private fireBase: AngularFireAuth) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    this.events.publish('user:created', false, Date.now());

        
  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
 
  private backButtonFunc(): void {
    this.navCtrl.setRoot(HomePage);
  }
  ngOnInit() {

    this.userData = JSON.parse(localStorage.getItem('userData'));
    console.log("userdata",this.userData);
    
    this.UserName = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
      return letter.toUpperCase();
    })


  }
  evaluate(){
    this.navCtrl.push(Evaluate);
  }
  subscribe(){
    this.navCtrl.push(weekendReg);
    
  }
  timetable(){
    this.navCtrl.push(timetable);
  }
  aboutPage(){
    console.log("kkkkkkkkkkkkkkkkkk");
    
       this.navCtrl.push(About);
    
        
      }
      faq(){
        this.navCtrl.push(faq);

      }
      teachers(){
        this.navCtrl.push(teachers);
      }
  EditProfile() {
    this.navCtrl.push(Profile);

  }
  logOut() {
    localStorage.removeItem('userData');      
    this.navCtrl.push(Login);
    this._navBar.navHide(true);
    this.tabBarElement.style.display = 'none';
    this.logoutFromSocial();

    this.authservice.logout(this.userData._id).subscribe((result) => {
      if(this.tabBarElement){
        this.tabBarElement.style.display = 'none';
      }
      this._navBar.navHide(true);

    }, error => {
      console.log(error)
    });
    

  }
  settings(){
    this.navCtrl.push(moreSetting);
  }
  logoutFromSocial() {
    this.fireBase.auth.signOut();
  }
 

}
