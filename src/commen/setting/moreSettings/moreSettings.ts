
import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';

import { Login } from '../../commen/login/login';
import { Profile } from '../../../pages/profile/profile';
import { AuthService } from '../../../services/auth.service'
import { AngularFireAuth } from 'angularfire2/auth';
import { About } from '../../../commen/setting/about/about';
import { faq } from '../../../commen/setting/FAQ/faq';
import { teachers } from '../../../commen/setting/teachersPage/teachers';
import { homeNew } from '../../../pages/homeNew/homeNew';
import { HomePage } from '../../../pages/dashboard/home';
import { naveBarShowSrvice } from '../../../services/navBarService';


@Component({
  selector: 'page-moreSettings',
  templateUrl: 'moreSettings.html',
  providers: [AuthService,naveBarShowSrvice]
})
export class moreSetting {
  userData;
  UserName;

  constructor(public _navBar:naveBarShowSrvice, public navCtrl: NavController,public platform: Platform, private authservice: AuthService, private fireBase: AngularFireAuth) {

  }
  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }
 
  private backButtonFunc(): void {
    this.navCtrl.setRoot(HomePage);
  }
  ngOnInit() {

    this.userData = JSON.parse(localStorage.getItem('userData'));
    console.log("userdata",this.userData);
    
    this.UserName = this.userData.profile.firstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
      return letter.toUpperCase();
    })


  }
 
  aboutPage(){
    console.log("kkkkkkkkkkkkkkkkkk");
    
       this.navCtrl.push(About);
    
        
      }
      faq(){
        this.navCtrl.push(faq);

      }
      teachers(){
        this.navCtrl.push(teachers);
      }
  EditProfile() {
    this.navCtrl.push(Profile);

  }
 
 
  logoutFromSocial() {
    this.fireBase.auth.signOut();
  }
 

}