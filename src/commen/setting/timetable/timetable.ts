import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ExamService } from '../../../services/exam.service';
import { environment } from '../../../environments/environment'


@Component({
  selector: 'page-timetable',
  templateUrl: 'timetable.html',
  providers: [ExamService]
 
})
export class timetable {
 
    constructor(public navCtrl: NavController,private examservice:ExamService) {
    }
}