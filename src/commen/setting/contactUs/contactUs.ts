import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';


@Component({
  selector: 'page-contactUs',
  templateUrl: 'contactUs.html',

 
})
export class contactUs {
    constructor(public navCtrl: NavController,public toastCtrl: ToastController,) {
    }
    Feedback(value){
        console.log("value",value);
        if(value.email){
        let email = value.email.toLowerCase();
      
            
            const toast = this.toastCtrl.create({
                message:"Thank you for your honest feedback,success",
                duration: 3000,
                position: 'bottom'
              });
              toast.present();
    
          }
    }
}