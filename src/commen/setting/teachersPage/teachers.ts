import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ExamService } from '../../../services/exam.service';
import { environment } from '../../../environments/environment'


@Component({
  selector: 'page-teachers',
  templateUrl: 'teachers.html',
  providers: [ExamService]
 
})
export class teachers {
  userData;
  userId;
  teachersData;
  url = environment.backendUrl + '/images/';
    constructor(public navCtrl: NavController,private examservice:ExamService) {
    }
    ngOnInit() {

      this.userData = JSON.parse(localStorage.getItem('userData'));
      console.log("userdata",this.userData.userId);
      this.userId=this.userData.userId;
      console.log("userid,",this.userId);
      
      this.examservice.viewTeachers(this.userId).subscribe(
        res => {
          console.log("res",res);
          if(res.status){
            this.teachersData=res.data;
            console.log("texhers data",this.teachersData);
            
          }else{
            console.log("no data");
            
          }
          
        });
    }
}