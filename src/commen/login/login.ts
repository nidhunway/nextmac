import { Component } from '@angular/core';
import { NavController ,Platform,App} from 'ionic-angular';
import { HomePage } from '../../pages/dashboard/home';
import { Forgot } from '../../commen/forgot/forgot';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { ToastController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { homeNew } from '../../pages/homeNew/homeNew';
import { naveBarShowSrvice } from '../../services/navBarService';


// import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';

@Component({
  selector: 'page-Login',
  templateUrl: 'login.html',
  providers: [AuthService,Facebook,naveBarShowSrvice]
})
export class Login {
   tabBarElement: any;
  public unregisterBackButtonAction: any;
  userDatas;
  forgot: any;
  temp: boolean = false;
  colorLog;
  colorSign;
  fontLog;
  fontSign;
  login: boolean = true;
  spinnerLoad: boolean = false;
  msg: any;
  mydat
  constructor(
    public navCtrl: NavController,
    private authservice: AuthService,
    public toastCtrl: ToastController,
    private fireBase: AngularFireAuth,
    public platform:Platform,
    public fb:Facebook,
    public _navBar:naveBarShowSrvice,
    public appCtrl: App


  ) {
  
   this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    console.log(this.tabBarElement,"login d")
    if(this.tabBarElement){
      this.tabBarElement.style.display = 'none';

    }
    this.forgot = Forgot;


  }
  ionViewWillEnter(): void {
    // this.mydat = JSON.parse(localStorage.getItem('navbardata'));
    // if(this.mydat){
    //   this.mydat.style.display = 'none';

    // }
    if(this.tabBarElement){
      this.tabBarElement.style.display = 'none';

    }

}
  ngOnInit() {

    this.colorLog = "#108fd2";
    this.colorSign = "#fff";
    this.fontLog = "#fff";
    this.fontSign = "#108fd2";

    // firebase.auth().getRedirectResult().then(function (result) {
    //   if (result.credential) {
    //     console.log(result);
    //   }
    // }).catch(function (error) {
    //   var errorCode = error.code;
    //   var errorMessage = error.message;
    // });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(user,"user");
        
        this.firebaseLogin(user);
      }
    })
  }
  ionViewDidLoad() {
    this.initializeBackButtonCustomHandler();
    
}
//  ionViewWillEnter() {
   
//     this.tabBarElement.style.display = 'none';
//   }
ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
//this.tabBarElement.style.display = 'flex';
}
  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
    }, 101);
}  


  Submitlogin(value: any) {


    if (value.email && value.password) {
      this.spinnerLoad = true;
      let email = value.email.toLowerCase();
      let pass = value.password;


      this.authservice.login(email, pass).subscribe((result) => {
        this.spinnerLoad = false;
        if (result)
          console.log(result, "ites Result")
        if (result.status) {
           console.log(result.data,"login");
           
          localStorage.setItem('userData', JSON.stringify(result.data));
          localStorage.setItem('googledata', JSON.stringify(true));
           //this.navCtrl.push(homeNew);

         this.navCtrl.push(HomePage);

        } else {
          const toast = this.toastCtrl.create({
            message: result.message,
            duration: 3000,
            position: 'bottom'
          });
          toast.present();

          this.spinnerLoad = false;



        }
      }, error => {
        this.spinnerLoad = true;

      });
    }

  }

  SubmitSignup(value: any) {

    if (value.email) {
      let name = value.name;
      let email = value.email.toLowerCase();;
      let pass = value.password;
      this.spinnerLoad = true;
      /* avoid multiple click on login button */
      // this.clicked = true;

      this.authservice.signup(name, email, pass).subscribe((res) => {

        this.spinnerLoad = false;
        if (res) {
          console.log(res)
          if (res.status) {
            const toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
            //  document.getElementById("routeLogin").click();
            this.navCtrl.push(Login);


          }
          else {
            const toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          }
        }

      }, error => {
        const toast = this.toastCtrl.create({
          message: "Somthing went wrong",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      });
    }
  }
  onLogin() {

    this.login = true;
    this.temp = false;
    this.colorSign = "#fff"
    this.colorLog = "#108fd2"

    this.fontLog = "#fff";
    this.fontSign = "#108fd2";

  }
  onSignup() {
    this.temp = true;
    this.login = false;

    this.colorSign = "#108fd2"
    this.colorLog = "#fff"

    this.fontLog = "#108fd2 ";
    this.fontSign = "#fff";

  }


  loginWithFacebookNew() {
    this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
      this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        this.userDatas = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'],providerId:"facebook.com"}
        console.log(this.userDatas,"user data");
        
        if(this.userDatas) this.facebookService(this.userDatas);
         
      });
    })
      
  }

  loginWithFacebook() {
    try {
      firebase.auth().signInWithRedirect(new firebase.auth.FacebookAuthProvider);
    } catch (error) {
      console.log(error,'error');
    }
  }
//   loginWithFacebookNew(){
//   this.fb.login(['public_profile', 'user_friends', 'email'])
//   .then((res: FacebookLoginResponse) => 
//   console.log('Logged into Facebook!', res)
// )
//   .catch(e => console.log('Error logging into Facebook', e));
//   }
  loginWithGoogle() {
    try {
      firebase.auth().signInWithRedirect(new firebase.auth.GoogleAuthProvider);
    } catch (error) {
      console.log(error,'error');
    }
    // this.googlePlus.login({})
    // .then(res => console.log('done',res))
    // .catch(err => console.error('error',err));
  }

  logoutFromSocial() {
    this.fireBase.auth.signOut();
  }
  facebookService(user){
   var  data = {
      email: user.email,
      firstName: user.username,
      lastName:user.first_name ,
      loginType: "facebook"
    }
    this.authservice.loginWithFirebase(data).subscribe((res) => {
      
      if (res.status) {

        localStorage.setItem('userData', JSON.stringify(res.data));
        // this.navCtrl.push(homeNew);
        this.navCtrl.setRoot(homeNew);

      } else {
        this.navCtrl.push(Login);
      }
    })

  }
  firebaseLogin(user) {
    let data;
    if (user.providerData[0].providerId == "google.com") {
      data = {
        email: user.providerData[0].email,
        firstName: user.providerData[0].displayName.split(' ', 2)[0],
        lastName: user.providerData[0].displayName.split(' ', 2)[1],
        loginType: "google"
      }
    }   
    this.authservice.loginWithFirebase(data).subscribe((res) => {
      
      if (res.status) {
        localStorage.setItem('userData', JSON.stringify(res.data));
        // this.appCtrl.getRootNav().push(homeNew );

        // localStorage.setItem('googledata', JSON.stringify(true));
        // this.navCtrl.push(homeNew);
        this.navCtrl.setRoot(homeNew);



      } else {
        this.navCtrl.push(Login);
      }
    })
  }

}
