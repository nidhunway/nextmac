import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../environments/environment';


@Injectable()
export class ExamService {

  url = environment.backendUrl;
  activeUserId: string;
  userDetailsId: string;
  userId: string;

  constructor(private http: Http) {
    let userData = JSON.parse(localStorage.getItem('userData'));
    console.log("userData", userData);

    this.activeUserId = userData._id;
    this.userDetailsId = userData.userDetailsId;
    this.userId = userData.userId;

  }
  registerWeeklyExam(paramss) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    paramss.activeUserId = this.activeUserId,
      paramss.userId = this.userId;
      console.log(paramss,"date to send");
      
    return this.http.post(`${this.url}/week_exam_reg`, paramss, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  SinglePaymentExam(paramss) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    paramss.activeUserId = this.activeUserId,
      paramss.userId = this.userId;
      console.log(paramss,"date to send");
      
    return this.http.post(`${this.url}/week_exam_single_reg`, paramss, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  loadPackageExams(userExamId, packageId) {

    let data = {
      "activeUserId": this.activeUserId,
      "examId": userExamId,
      "packageId": packageId,
      "userId": this.userId
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/load-package-exam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  createExam(examName, examconfig, numOfQuestion, dueflag, dueObj, timeflag, minutes, hours) {
    let data = {
      'userId': this.userId,
      'activeUserId': this.activeUserId,
      'examName': examName,
      'examConfig': examconfig,
      'noOfQuestions': numOfQuestion,
      'dueFlag': dueflag,
      'dueObj': dueObj,
      'timeFlag': timeflag,
      'timeObj': {
        'hour': hours,
        'minute': minutes
      }
    };

    console.log("Data to send nidhfdsfjksafasfnkjasfnkj", data);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/createExam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  startExam(examId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'examId': examId
    };


    console.log("Data sending from startExam");

    return this.http.post(`${this.url}/startExam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
  getSubscribeExam() {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId
     
    };


    console.log("Data sending from startExam");

    return this.http.post(`${this.url}/get-subscribed-exams`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
  viewTeam() {
    return this.http.get('http://localhost:3000/api/view/team')
      .map(res => res.json());
  }
  
  // getDailyExams(){
  //   let data = {
  //     'activeUserId': this.activeUserId
     
  //   };

  //   return this.http.post(`${this.url}/fetch-daily-exams`, data)
  //   .map(res => res.json())
  //   .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  // }
  loadExam(userExamId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userExamId': userExamId,
      'userId': this.userId
    };


    return this.http.post(`${this.url}/loadExam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  faq(userId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': userId
    };
    console.log("examservicedata", data);
    
    return this.http.post(`${this.url}/dynamicFaqGet`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  viewTeachers(userId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': userId
    };
    console.log("examservicedata", data);
    
    return this.http.post(`${this.url}/viewallteachers`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getExamQuestions(userExamId, questionId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'userExamId': userExamId,
      'questionId': questionId
    };


    return this.http.post(`${this.url}/getExamQuestion`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getAiimsQuestions(userExamId, questionId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'questionId': questionId,
      'userExamId': userExamId
    };
   

    return this.http.post(`${this.url}/fetch-package-question`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
 
  finishAiimsExam(userExamId,examId,examType) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'userExamId': userExamId,
      'examId':examId,
      'examType':examType
    };
  

    return this.http.post(`${this.url}/finish-package-exam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  markAiimsQuestions(questionId,userExamId,markType) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'questionId': questionId,
      'userExamId': userExamId,
      'markType':markType
    };
   

    return this.http.post(`${this.url}/mark-package-exam-questions`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  getResultScoreAiims(examId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'examId':examId,
     
    };
  

    return this.http.post(`${this.url}/get-top-rank-package-exam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getResultAiims(userExamId,examId,examType,resultType) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'userExamId': userExamId,
      'examId':examId,
      'examType':examType,
      'resultType':resultType
    };
  

    return this.http.post(`${this.url}/get-package-result-details`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  saveExamAnswer(index, userExamId, examId, questionId, answer, result, updateFlag, subjectArray) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let FirstCallData = {
      'activeUserId': this.activeUserId,
      'answer': answer,
      'examId': examId,
      'questionId': questionId,
      'result': result,
      'subjectArray': subjectArray,
      'updateFlag': updateFlag,
      'userExamId': userExamId,
      'userId': this.userId,
      'questionStatus': 'answered',
      'qPaletteIndex': index
    };


    console.log("Save answer sending to db :", FirstCallData);

    return this.http.post(`${this.url}/saveExamAnswer`, FirstCallData, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
  saveAiimsExamAnswer(questionId,examId,userExamId,result,answer,examType, marked){
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'questionId': questionId,
      'userExamId':userExamId,
      'examId':examId,
      'examType':examType,
      'answer':answer,
      'marked':marked,
      'result':result
    };


    return this.http.post(`${this.url}/save-package-exam-answers`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }


  fetchAllExam(mode) {
    console.log("mode", mode);

    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'mode': mode
    };

    return this.http.post(`${this.url}/fetchExams`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  checkAppliedForExam() {
    let data = {
      'activeUserId': this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/get-payment-weeklyexam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  fetchGrandExam() {
    let data = {
      'activeUserId': this.activeUserId
    };

    return this.http.post(`${this.url}/pending-grand-exam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  finishExam(userExamId) {

    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'userExamId': userExamId
    };

    return this.http.post(`${this.url}/finishExam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteExam(examId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'examId': examId
    };

    console.log("Data sending for delete", data);

    return this.http.post(`${this.url}/deleteExam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  createGrandExam(examName, examconfig, numOfQuestion, dueflag, timeflag, minutes, hours, dueObj, examType) {
    let data = {
      'userId': this.userId,
      'activeUserId': this.activeUserId,
      'name': examName,
      'examConfig': examconfig,
      'numberOfQuestion': numOfQuestion,
      'dueFlag': dueflag,
      'timeFlag': timeflag,
      'dueObj': dueObj,
      'examType': examType,
      'timeDetails': {
        'hour': hours,
        'minut': minutes

      }
    };

    console.log("Data to send to sever", data);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/create-grand-exam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  loadGrandExam(grandExamId) {

    let data = {
      'activeUserId': this.activeUserId,
      'grandExamId': grandExamId,
    };

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/load-grant-exam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getGrandExamQuestions(userExamId, questionId) {
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'grandExamId': userExamId,
      'questionId': questionId
    };

    return this.http.post(`${this.url}/get-grand-exam-question`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  saveGrandExamAnswer(userExamId, currentPaletteId, questionStatus, questionId, answer, result, updateFlag, subjectArray) {

    let data = {
      "activeUserId": this.activeUserId,
      "grandExamId": userExamId,
      "questionStatus": questionStatus,
      "qPaletteIndex": currentPaletteId,
      "questionId": questionId,
      "answer": answer,
      "subjectArray": subjectArray,
      "result": result,
      "updateFlag": updateFlag,
    };

    return this.http.post(`${this.url}/save-grand-exam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  finishGrandExam(userExamId) {
    let data = {
      "activeUserId": this.activeUserId,
      "grandExamId": userExamId,
    }

    return this.http.post(`${this.url}/finish-grant-exam`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }



  deleteGrandExam(userExamId) {
    let data = {
      "activeUserId": this.activeUserId,
      "grandExamId": userExamId
    }
    console.log("Data to delete grand", data);
    return this.http.post(`${this.url}/delete-grand-exam-question`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getQuestions(userExamId, examType, resultType) {
    let data = {
      "activeUserId": this.activeUserId,
      "examId": userExamId,
      "examType": examType,
      "resultType": resultType
    }
    return this.http.post(`${this.url}/result-details`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getLeaderboard(userexamid) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "activeUserId": this.activeUserId,
      "examId": userexamid
    }
    console.log(data, "sending")
    return this.http.post(`${this.url}/get-top-rank`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
  getUserResult(userexamid) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "activeUserId": this.activeUserId,
      "userExamId": userexamid
    }

    return this.http.post(`${this.url}/get-week-exam-details`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
  getResult(userexamid, category) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "activeUserId": this.activeUserId,
      "userExamId": userexamid,
      "type": category
    }

    return this.http.post(`${this.url}/get-answered-questions`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  updateExamAnswer(index, answer, result, userExamAnswerId, userExamId, examId, questionId) {

    let secondCall = {
      'userExamAnswerId': userExamAnswerId,
      'activeUserId': this.activeUserId,
      'answer': answer,
      'result': result,
      'updateFlag': true,
      'userExamId': userExamId,
      'questionStatus': 'answered',
      'userId': this.userId,
      'qPaletteIndex': index,
      'examId': examId,
      'questionId': questionId,
    };


    return this.http.post(`${this.url}/saveExamAnswer`, secondCall)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
  markExam(userExamId, questionId, status) {
    let data = {
      "userExamId": userExamId,
      "questionId": questionId,
      "type": status
    };


    return this.http.post(`${this.url}/mark-answer`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  markGrandExamQuestion(userExamId, questionId, status) {
    let data = {
      "activeUserId": this.activeUserId,
      "grandExamId": userExamId,
      "questionId": questionId,
      "type": status
    }
    return this.http.post(`${this.url}/mark-grand-exam-question`, data)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getWeekExams() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');


    let data = {
      activeUserId: this.activeUserId
    }

    return this.http.post(`${this.url}/get-weekend-exam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}

