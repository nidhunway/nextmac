import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {environment} from '../environments/environment';

@Injectable()
export class NotesService {

  url = environment.backendUrl;
  activeUserId: string;
  userDetailsId: string;
  userId: string;

  constructor(private http:Http) {
    let userData = JSON.parse(localStorage.getItem('userData'));
    this.activeUserId =  userData._id;
    this.userDetailsId =  userData.userDetailsId;
    this.userId = userData.userId;
  }

  createNote(noteContent, questionId, subjectArray){
    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      "questionId": questionId,
      "noteTitle": "No Title",
      "color": "#ffc",
      "noteContent": noteContent,
      "subjectArray": subjectArray
    };

    console.log("Note Data sending: ",data);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/createStudyNote`, data, {headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  editNote(noteId,noteContent, subjectArray){
    let data = {
      'activeUserId': this.activeUserId,
      "noteId": noteId,
      "noteTitle": "No Title",
      "color": '#ffc',
      "noteContent": noteContent,
      "subjectArray": subjectArray
    };

    console.log(data);

    return this.http.post(`${this.url}/editStudyNote`, data )
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteNote(noteId){

    let data = {
      'activeUserId': this.activeUserId,
      "userId": this.userId,
      "noteId": noteId
    };

    return this.http.post(`${this.url}/deleteStudyNote`, data )
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }


  fetchAllNotes(loadMore){

    let data= {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      'loadMore': loadMore
    };

    console.log("Data sending to server", data);

    return this.http.post(`${this.url}/fetchAllNotes`, data)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  fetchAllMiniNotes(){

    let data= {
      'activeUserId': this.activeUserId,
      'userId': this.userId
     
    };

    console.log("Data sending to server", data);

    return this.http.post(`${this.url}/get-all-mini-note`, data)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  saveRecentNote(notId,mode){
    let data= {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      "noteId":notId,
      "mode": mode
    };

    console.log("Data sending to server//", data);

    return this.http.post(`${this.url}/recent-daily-notes`, data)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  loadRecentNote(){

    let data= {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      "mode":"get"
    };

    console.log("Data sending to server//", data);

    return this.http.post(`${this.url}/recent-daily-notes`, data)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  loadMininote(noteid,regId,demoFlag){

    let data= {
      'activeUserId': this.activeUserId,
      'userId': this.userId,
      "noteId": noteid,
      "registrationId": regId,
      "demoFlag":demoFlag
    };

    console.log("Data sending to server//", data);

    return this.http.post(`${this.url}/load-mini-note`, data)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  bookmarkedOnly(){

    let data=  {
      "activeUserId": this.activeUserId,
      "userId": this.userId
  }
    return this.http.post(`${this.url}/bookmarked-notes-all`, data)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    
      
  }
  
  bookmarkMiniNote(noteId,bookmark){

    let data=  {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "noteId": noteId,
      "bookmarked": bookmark 
  }

    console.log("Data sending to server", data);

    return this.http.post(`${this.url}/finish-bookmark-mini-note`, data)
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
