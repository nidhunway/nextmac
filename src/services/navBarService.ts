import { Injectable,OnInit } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, Headers} from '@angular/http';
import {environment} from '../environments/environment';


@Injectable()
export class naveBarShowSrvice  {

showNaveBar;
navebarHide:boolean=true;
localNavbar;
indexValue;
myvalue;
// localNavbar:boolean=true;
url = environment.backendUrl;
constructor(private http:Http){}
 

OnInit(){
    // this.localNavbar=true;
    if(!localStorage.getItem('navBar')) this.showNaveBar=false;
    else this.showNaveBar=localStorage.getItem('navBar');
}
show(index){
    localStorage.setItem('navBar',index);
    this.showNaveBar=index;

}
//index pass navbar

navHide(index){
    this.localNavbar=index;
    console.log(this.localNavbar,"hey its nave hiden")
}


profileEdit(userId,firstname,lastname,phone,city,country) {
    let data = {
        "activeUserId":userId,
        "firstName":firstname,
        "lastName":lastname,
        "phone":phone,
        "city":city,
        "country":country
      };
  
     
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
  
      return this.http.post(`${this.url}/update-user-profile`, data, {headers})
        .map(res => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}
InviteFrds(userId,inviteFrds) {
    let data = {
        "activeUserId":userId,
        "email":inviteFrds
       
      };
      console.log(data);
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
  
      return this.http.post(`${this.url}/invite-friend`, data, {headers})
        .map(res => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}
UserFeedback(userId,feedback) {
    let data = {
        "userId":userId,
        "message":feedback
       
      };
  
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
  
      return this.http.post(`${this.url}/invite-friend`, data, {headers})
        .map(res => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}

}
