import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';
import { ToastController } from 'ionic-angular';




@Injectable()
export class AuthService {

  private loggedIn = false;
  url = environment.backendUrl;


  constructor(private http: Http, public Toaser: ToastController) {



  }

  login(email, password) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
      .post(
        `${this.url}/login`,
        JSON.stringify({ email, password }),
        { headers }
      )
      .map(res => res.json())
      .map((res) => {
        console.log("Result: ", res);
        if (res.status) this.loggedIn = true;
        else this.loggedIn = false;


        return res;
      });
  }


  logout(activeUserId) {//
    let data = {
      activeUserId: activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
      .post(
        `${this.url}/logout`,
        JSON.stringify(data),
        { headers }
      )
      .map(res => res.json())
      .map((res) => {
        localStorage.removeItem('userData');
        localStorage.removeItem('subjectData');
        this.loggedIn = false;
        return res;
      });

  }

  isLoggedIn() {
    return this.loggedIn;
  }

  signup(firstName, email, password) {

    let data = {
      email: email,
      password: password,
      firstName: firstName,
      lastName: ""
    };

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
      .post(
        `${this.url}/signup`,
        JSON.stringify(data),
        { headers }
      )
      .map(res => res.json())
      .map((res) => {
        if (res.status === true) {
          console.log("Response from AuthService: ", res);
          this.loggedIn = true;
        } else {
          console.log(res.status);
        }
        return res;
      });
  }


  forgotPassword(email) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/login/reset`, email).map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  resetPassword(resetCode, pass) {
    let data = {
      "newPassword": pass,
      "resetCode": resetCode
    };
    return this.http.post(`${this.url}/login/reset/newpassword`, data).map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  verifyEmail(code) {
    return this.http.get(`${this.url}/login/verify/${code}`)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  facebookLogin() {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    headers.append('Access-Control-Allow-Origin', 'http://devservice.eslurn.com');

    headers.append('Access-Control-Allow-Credentials', 'true');


    return this.http.get(`${this.url}/login/facebook/`, { withCredentials: true })
      .map(res => console.log("response from fb", res))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  notificationInfo(message) {
    const toast = this.Toaser.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  loginWithFirebase(userinfo) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');


    return this.http.post(`${this.url}/firebase-login`, userinfo).map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
}
