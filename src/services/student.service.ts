import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/publishReplay';
import {environment} from '../environments/environment';
function _window(): any {
  // return the native window obj
  return window;
}
@Injectable()
export class StudentService {

  url = environment.backendUrl;
  activeUserId:string;
  userDetailsId:string;
  userId:string;
  subjects:Observable<any> = null;

  constructor(private http:Http) {
    let userData = JSON.parse(localStorage.getItem('userData'));
    this.activeUserId = userData._id;
    this.userDetailsId = userData.userDetailsId;
    this.userId = userData.userId;


  }

  /* List all subjects */
  //mock test
  checkMockApplied(){
    let data = {
      'activeUserId': this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/check-mock-applied`, data,{headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

}
getPaymetDetails(code){
  let data = {
    'activeUserId': this.activeUserId,
    'refCode':code
  };
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  console.log(data,'here')
  return this.http.post(`${this.url}/get-payment-details`, data,{headers})
    .map(res => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

}
applayMock(data){
  data.activeUserId=this.activeUserId;  
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');    
  return this.http.post(`${this.url}/user-mock`, data,{headers})
    .map(res => res.json())
    .do(data => {
      console.log('daa kitty',data)
      if(data.status){
        alert(data.message)
        //  this.__tostr.notify(data.message,'success');
      }
       else alert(data.message)
    })      
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}
get nativeWindow(): any {
  return _window();
}

  getUserData() {
    return localStorage.getItem("userData");
  }


  getSubjects(userId, activeUserId) {

    let data = {
      'activeUserId': activeUserId,
      'userId': userId
    };

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if(!this.subjects){
      this.subjects = this.http.post(`${this.url}/getSubjects`, data, {headers})
        .map(res => res.json())
        .do(data => console.log(" "))
        .publishReplay(1)
        .refCount()
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    return this.subjects;
  }


  /* Get first question */
  getQuestion(subjectId) {

    let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "dir": "",
      "subjectId": subjectId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/getStudyQuestion`, data,{headers})
      .map(res => res.json())
      .publishReplay(1)
      .refCount()
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  loadLearnIDs(subjectId, sectionId, chapterId) {

    let data = { 
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "subjectId": subjectId,
      "sectionId": sectionId,
      "chapterId": chapterId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/load-learn-total-question`, data,{headers})
      .map(res => res.json())
      .publishReplay(1)
      .refCount()
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  getLearnQuestion(questionId) {

    let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "questionId": questionId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/get-learn-question`, data,{headers})
      .map(res => res.json())
      .publishReplay(1)
      .refCount()
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getListQuestion(subjectId) {

    let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "subjectId": subjectId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/total-questions`, data,{headers})
      .map(res => res.json())
      .publishReplay(1)
      .refCount()
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  

  //get stared api 
  getStredQuestion(subjectId,subarray) {
    let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "dir": "",
      "subjectId": subjectId,
      "subjectArray":subarray
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/get-staredQuestionsNidhun`, data,{headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  //end stared api
  getNextQuestion(subjectId, questionId) {
    let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "dir": "next",
      "questionId": questionId,
      "subjectId": subjectId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/getStudyQuestion`, data,{headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getPreviousQuestion(subjectId, questionId) {
    let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "dir": "prev",
      "questionId": questionId,
      "subjectId": subjectId
    };

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/getStudyQuestion`, data,{headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }


  /* Save the result and option selected by user */
  saveAnswer(questionId, answer, result, subjectArray) {

    let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "questionId": questionId,
      "answer": answer,
      "result": result,
      "subjectArray": subjectArray
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/saveStudyAnswer`, data,{headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }


  dashboardInfo(){

    let data = {
      'activeUserId': this.activeUserId,
      'userId': this.userId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/frontPageInfos`, data,{headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }



}
