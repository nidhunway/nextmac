

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../environments/environment';

@Injectable()

export class WeeklyService {
  url = environment.backendUrl;
  activeUserId: string;
  userId: string;
  constructor( private http: Http) {
    let userData = JSON.parse(localStorage.getItem('userData'));
    this.activeUserId = userData._id;
    this.userId = userData.userId;
  }

//get coupencode
getallCode() {
  let data = {
    'userId': this.userId,
    "activeUserId":this.activeUserId
  };
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');

  return this.http.post(`${this.url}/get-all-coupencode`, data, { headers })
    .map(res => res.json())
    .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
}
  checkAppliedForExam() {
    let data = {
      'activeUserId': this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/get-payment-weeklyexam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  basicUserTest() {
    let data = {
      'userId': this.userId,

      'activeUserId': this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/check-user-weekend-subscription`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  registerWeeklyExamBasic(paymentid){
    let data = {
      'paymentId':paymentid,
      'userId': this.userId,

      'activeUserId': this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/upgrade-weekend-subscription`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }
  registerWeeklyExam(paramss) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    paramss.activeUserId = this.activeUserId,
      paramss.userId = this.userId;
      console.log(paramss,"date to send");
      
    return this.http.post(`${this.url}/week_exam_reg`, paramss, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  SinglePaymentExam(paramss) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    paramss.activeUserId = this.activeUserId,
      paramss.userId = this.userId;
      console.log(paramss,"date to send");
      
    return this.http.post(`${this.url}/week_exam_single_reg`, paramss, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getWeekExams() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');


    let data = {
      activeUserId: this.activeUserId
    }

    return this.http.post(`${this.url}/get-weekend-exam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  loadWeekExam(weekexamid) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      activeUserId: this.activeUserId,
      examId: weekexamid
    }

    return this.http.post(`${this.url}/load-exam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getExamQuestion(questionid, userexamid) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "activeUserId": this.activeUserId,
      "questionId": questionid,
      "userExamId": userexamid
    }

    return this.http.post(`${this.url}/get-weekend-question`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  saveExamAnswer(questionid, examid, userexamid, answerStatus, answer, result, subjectarray) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "activeUserId": this.activeUserId,
      "questionId": questionid,
      "examId": examid,
      "userExamId": userexamid,
      "answerType": answerStatus,
      "answer": answer,
      "result": result,
      "subjectArray": subjectarray
    }

    return this.http.post(`${this.url}/save-weekend-question`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  markExamQuestion(questionid, userexamid, qstatus) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "activeUserId": this.activeUserId,
      "questionId": questionid,
      "userExamId": userexamid,
      "type": qstatus
    }

    return this.http.post(`${this.url}/mark-week-exam-question`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  submitExam(userexamid) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "activeUserId": this.activeUserId,
      "userExamId": userexamid
    }

    return this.http.post(`${this.url}/finish-week-exam`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
    // aiims pay

  __AiimsPay(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = {
      "userId": this.userId,
      "activeUserId": this.activeUserId,
      
    }

    return this.http.post(`${this.url}/get-packages`, data, { headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  } 
   __couponcode(packageId,code){
        let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let data = {
        "userId": this.userId,
        "activeUserId": this.activeUserId,
        "packageId":packageId,
        "couponCode":code
      }
     
      
      return this.http.post(`${this.url}/check-package-coupon-code`, data, { headers })
        .map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   
      }
      __SUBSCRIBEPAY(){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let data = {
          "activeUserId": this.activeUserId,
          "userId": this.userId
        }
        
        return this.http.post(`${this.url}/get-subscribed-packages`, data, { headers })
          .map(res => res.json())
          .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
     
      }
      
      ___AIIMSPAY(packageId,couponCode,paymentId,amount){
        let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let data = {
        "userId": this.userId,
        "activeUserId": this.activeUserId,
        "paymentId":paymentId,
        "packageId":packageId,
        "couponCode":couponCode,
        'amount':amount
      }
      
      
      return this.http.post(`${this.url}/package-registration`, data, { headers })
        .map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   
      }
      //send enquery
      MSGIOS(){
        let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let data = {
        "userId": this.userId,
        "activeUserId": this.activeUserId,
    
      }
      
      
      return this.http.post(`${this.url}/mail-registration`, data, { headers })
        .map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   
      }
}


