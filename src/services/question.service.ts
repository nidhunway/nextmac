import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/publishReplay';
import {environment} from '../environments/environment';

@Injectable()
export class QuestionService {

  url = environment.backendUrl;
  activeUserId:string;
  userDetailsId:string;
  userId:string;
  subjects:Observable<any> = null;

  constructor(private http:Http) {
    let userData = JSON.parse(localStorage.getItem('userData'));
    this.activeUserId = userData._id;
    this.userDetailsId = userData.userDetailsId;
    this.userId = userData.userId;


  }

  /* List all subjects */
createProblem(comment,question){
    let data = {
     comment:comment,
     question:question,
     activeUserId:this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/report-question`, data, {headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }

  createLastQuestion(subjectId,numberOfQus,questionId){
  
    let data = {
     subjectId:subjectId,
     questionNumber:numberOfQus,
     activeUserId:this.activeUserId,
     questionId:questionId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/last-question-attended`, data, {headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }
  getLastQuestion(subjectId){
  
    let data = {
     subjectId:subjectId,
     activeUserId:this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.url}/get-last-attended-question`, data, {headers})
      .map(res => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  learnPerformance(activeUserId,subjectId){
    let data={
      subjectId:subjectId,
      activeUserId:this.activeUserId
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/learn-question-answer-count`, data, {headers})
    .map(res => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }

  totalQuestions(subjectId){
   
    let data = {
      subjectId:subjectId,
      activeUserId:this.activeUserId
     };
     let headers = new Headers();
     headers.append('Content-Type', 'application/json');
 
     return this.http.post(`${this.url}/total-number-of-question`, data, {headers})
       .map(res => res.json())
       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  starQuestion(questionid, subjectarray, star) {
    let data = {
        "activeUserId": this.activeUserId,
        "userId": this.userId,
        "questionId": questionid,
        "stared": star,
        "subjectArray": subjectarray
    }
  console.log(data,"to start")
    return this.http.post(`${this.url}/save-staredQuestions`, data)
    .map(res => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
}
getAllStaredQuestion(subjectarray) {
  let data = {
      "activeUserId": this.activeUserId,
      "userId": this.userId,
      "subjectArray": subjectarray
  }
console.log(data,"to start")
  return this.http.post(`${this.url}/get-staredQuestions`, data)
  .map(res => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
}

}
