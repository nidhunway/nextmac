// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

//  backendUrl: 'http://localhost:8081',
  backendUrl: 'https://d4v.nextfellow.com',
 //backendUrl: 'https://d4v.nextfellow.in',

  firebaseConfig: {
    apiKey: "AIzaSyAF9h3usyyt8LvOU7IAxO_ZEPCPfI_-kq0",
    authDomain: "nextfellow-1521033569049.firebaseapp.com",
    databaseURL: "https://nextfellow-1521033569049.firebaseio.com",
    projectId: "nextfellow-1521033569049",
    storageBucket: "nextfellow-1521033569049.appspot.com",
    messagingSenderId: "885727504851"
  }

};
