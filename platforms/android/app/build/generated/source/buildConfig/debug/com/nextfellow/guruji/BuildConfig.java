/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.nextfellow.guruji;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.nextfellow.guruji";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 20102;
  public static final String VERSION_NAME = "2.1.2";
  // Fields from default config.
  public static final long _BUILDINFO_TIMESTAMP = 1535797480858L;
}
